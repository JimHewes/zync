﻿namespace Zync
{
	partial class TagForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagForm));
			this.tagButton = new System.Windows.Forms.Button();
			this.tagCancelButton = new System.Windows.Forms.Button();
			this.revisionLabel = new System.Windows.Forms.Label();
			this.revisionTextBox = new System.Windows.Forms.TextBox();
			this.tagsTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tagCommitMessageTextBox = new System.Windows.Forms.TextBox();
			this.forceCheckBox = new System.Windows.Forms.CheckBox();
			this.makeLocalCheckBox = new System.Windows.Forms.CheckBox();
			this.removeCheckBox = new System.Windows.Forms.CheckBox();
			this.changesetLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// tagButton
			// 
			this.tagButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.tagButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.tagButton.Location = new System.Drawing.Point(281, 276);
			this.tagButton.Name = "tagButton";
			this.tagButton.Size = new System.Drawing.Size(75, 23);
			this.tagButton.TabIndex = 6;
			this.tagButton.Text = "Tag";
			this.tagButton.UseVisualStyleBackColor = true;
			this.tagButton.Click += new System.EventHandler(this.OnClickedTagButton);
			// 
			// tagCancelButton
			// 
			this.tagCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.tagCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.tagCancelButton.Location = new System.Drawing.Point(362, 276);
			this.tagCancelButton.Name = "tagCancelButton";
			this.tagCancelButton.Size = new System.Drawing.Size(75, 23);
			this.tagCancelButton.TabIndex = 7;
			this.tagCancelButton.Text = "Cancel";
			this.tagCancelButton.UseVisualStyleBackColor = true;
			// 
			// revisionLabel
			// 
			this.revisionLabel.AutoSize = true;
			this.revisionLabel.Location = new System.Drawing.Point(15, 15);
			this.revisionLabel.Name = "revisionLabel";
			this.revisionLabel.Size = new System.Drawing.Size(51, 13);
			this.revisionLabel.TabIndex = 0;
			this.revisionLabel.Text = "Revision:";
			// 
			// revisionTextBox
			// 
			this.revisionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.revisionTextBox.Location = new System.Drawing.Point(72, 12);
			this.revisionTextBox.Name = "revisionTextBox";
			this.revisionTextBox.Size = new System.Drawing.Size(177, 20);
			this.revisionTextBox.TabIndex = 0;
			// 
			// tagsTextBox
			// 
			this.tagsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tagsTextBox.Location = new System.Drawing.Point(18, 81);
			this.tagsTextBox.Multiline = true;
			this.tagsTextBox.Name = "tagsTextBox";
			this.tagsTextBox.Size = new System.Drawing.Size(231, 62);
			this.tagsTextBox.TabIndex = 1;
			this.tagsTextBox.WordWrap = false;
			this.tagsTextBox.TextChanged += new System.EventHandler(this.OnTagsTextBoxTextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(15, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(110, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Tags:     (one per line)";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 154);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(89, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Commit message:";
			// 
			// tagCommitMessageTextBox
			// 
			this.tagCommitMessageTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tagCommitMessageTextBox.Location = new System.Drawing.Point(15, 176);
			this.tagCommitMessageTextBox.Multiline = true;
			this.tagCommitMessageTextBox.Name = "tagCommitMessageTextBox";
			this.tagCommitMessageTextBox.Size = new System.Drawing.Size(421, 84);
			this.tagCommitMessageTextBox.TabIndex = 5;
			// 
			// forceCheckBox
			// 
			this.forceCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.forceCheckBox.AutoSize = true;
			this.forceCheckBox.Location = new System.Drawing.Point(283, 80);
			this.forceCheckBox.Name = "forceCheckBox";
			this.forceCheckBox.Size = new System.Drawing.Size(122, 17);
			this.forceCheckBox.TabIndex = 2;
			this.forceCheckBox.Text = "Replace existing tag";
			this.forceCheckBox.UseVisualStyleBackColor = true;
			// 
			// makeLocalCheckBox
			// 
			this.makeLocalCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.makeLocalCheckBox.AutoSize = true;
			this.makeLocalCheckBox.Location = new System.Drawing.Point(283, 103);
			this.makeLocalCheckBox.Name = "makeLocalCheckBox";
			this.makeLocalCheckBox.Size = new System.Drawing.Size(78, 17);
			this.makeLocalCheckBox.TabIndex = 3;
			this.makeLocalCheckBox.Text = "Make local";
			this.makeLocalCheckBox.UseVisualStyleBackColor = true;
			this.makeLocalCheckBox.CheckedChanged += new System.EventHandler(this.OnMakeLocalCheckBoxChanged);
			// 
			// removeCheckBox
			// 
			this.removeCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.removeCheckBox.AutoSize = true;
			this.removeCheckBox.Location = new System.Drawing.Point(283, 126);
			this.removeCheckBox.Name = "removeCheckBox";
			this.removeCheckBox.Size = new System.Drawing.Size(84, 17);
			this.removeCheckBox.TabIndex = 4;
			this.removeCheckBox.Text = "Remove tag";
			this.removeCheckBox.UseVisualStyleBackColor = true;
			this.removeCheckBox.CheckedChanged += new System.EventHandler(this.OnRemoveCheckBoxChanged);
			// 
			// changesetLabel
			// 
			this.changesetLabel.AutoSize = true;
			this.changesetLabel.Location = new System.Drawing.Point(15, 38);
			this.changesetLabel.Name = "changesetLabel";
			this.changesetLabel.Size = new System.Drawing.Size(61, 13);
			this.changesetLabel.TabIndex = 0;
			this.changesetLabel.Text = "Changeset:";
			// 
			// TagForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.tagCancelButton;
			this.ClientSize = new System.Drawing.Size(449, 311);
			this.Controls.Add(this.removeCheckBox);
			this.Controls.Add(this.makeLocalCheckBox);
			this.Controls.Add(this.forceCheckBox);
			this.Controls.Add(this.tagCommitMessageTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tagsTextBox);
			this.Controls.Add(this.revisionTextBox);
			this.Controls.Add(this.changesetLabel);
			this.Controls.Add(this.revisionLabel);
			this.Controls.Add(this.tagCancelButton);
			this.Controls.Add(this.tagButton);
			this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "TagFormLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = global::Zync.Properties.Settings.Default.TagFormLocation;
			this.MinimumSize = new System.Drawing.Size(457, 334);
			this.Name = "TagForm";
			this.Text = "Tag";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button tagButton;
		private System.Windows.Forms.Button tagCancelButton;
		private System.Windows.Forms.Label revisionLabel;
		private System.Windows.Forms.TextBox revisionTextBox;
		private System.Windows.Forms.TextBox tagsTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tagCommitMessageTextBox;
		private System.Windows.Forms.CheckBox forceCheckBox;
		private System.Windows.Forms.CheckBox makeLocalCheckBox;
		private System.Windows.Forms.CheckBox removeCheckBox;
		private System.Windows.Forms.Label changesetLabel;
	}
}