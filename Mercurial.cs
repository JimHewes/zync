using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;


namespace Zync
{

	//public static class ExtensionMethods
	//{
	//    public static string GetMercurialDateTime(this DateTime dateTime)
	//    {
	//        return string.Format("{0:####}-{1:00}-{2:00} {3:00}:{4:00}:00", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute);
	//    }
	//}

	/** Even though the items of the FileStatus enumeration are mutually exclusive, 
	 *	they are made to be flags so that you can use them to specify which groups
	 *	of files you want. 
	 *	
	 *  So note the difference between FileStatus and FileStatusFlag. A variable of type 
	 *  FileStatusFlag can contain multiple items ORed together. But variable of type 
	 *  FileStatus can only contain one of the items at a time. For example, a variable 
	 *  of type FileStatusFlag can contain a value of (FileStatusFlag.Clean | FileStatusFlag.Modified).
	 *  But a variable of type FileStatus cannot contain a value of 
	 *  (FileStatus.Clean | FileStatus.Modified).
	 * **/
	public enum FileStatus
	{ 
		None =		0,
		Clean =			1, 
		Modified =		2,
		Added =			4,
		Removed =		8,
		NotTracked =	16,
		Deleted =		32,
		Ignored =		64,
		Folder =		128,
	};

	// The Flags attribute allows all possible combinations of the flags
	[Flags]
	public enum FileStatusFlag
	{
		None =			FileStatus.None,
		Clean =			FileStatus.Clean,
		Modified =		FileStatus.Modified,
		Added =			FileStatus.Added,
		Removed =		FileStatus.Removed,
		NotTracked =	FileStatus.NotTracked,
		Deleted =		FileStatus.Deleted,
		Ignored =		FileStatus.Ignored,
		Folder =		FileStatus.Folder,
		All = FileStatus.Clean | FileStatus.Clean | FileStatus.Modified | FileStatus.Added | FileStatus.Removed | 
				FileStatus.NotTracked | FileStatus.Deleted | FileStatus.Ignored | FileStatus.Folder,
	};

	public enum ResolveStatus
	{
		None = 0,
		Resolved = 1,
		Unresolved = 2
	};

	public struct FileItem
	{
		public string		filename;
		public FileStatus status;
		public ResolveStatus resolveStatus;
		public override string ToString()
		{
			return filename;
		}
	}

	public struct RevIdPair
	{
		public string revision;
		public string changesetId;
	}
	public struct FileLogItem
	{
		public int revision;
		public string changesetId;
		public string message;		///< The log message. This may be a summary if verbose was not requested.
		public string user;
		public DateTime date;
		public string branch;
		public List<string> tags;
		public List<string> files;
        public List<RevIdPair> parents;
        public List<RevIdPair> children;
	}

	public struct RepoLogItem
	{
		public int revision;
		public string hash;
		public string message;		///< The log message. This may be a summary if verbose was not requested.
		public string user;
		public DateTime date;
		public List<string> files;
	}

	public struct AnnotateItem
	{
		public string revision;
		public string changesetId;
		public string user;
		public DateTime date;
		public string filename;
		public string lineNumber;
		public string source;
	}



	public delegate void CommandOutput(string text);


	public abstract class SCC

	{
		
		/** @brief Reports whether the given directory is tracked by source code control.
			 * @param directory		The full path to the directory to check;
			 * @returns				true if the directory is tracked by SCCS. Otherwise, false.
			 */
		abstract public bool IsSCCDirectory(string directory);

		abstract public FileStatus GetFileStatus(string Filepath);
		abstract public List<FileItem> GetFileList(string filepath, FileStatusFlag flags);

		public static SCC Instance { get { return m_instance; } }
		abstract public string GetName();
		abstract public bool IsInstalled();
		abstract public string GetVersion();
		abstract public bool HasViewer();
		abstract public void ViewRepository(string folderPath);
		//abstract public void DiffExternal(DiffParams diffParams);
		abstract public string GetDiff(DiffParams diffParams);
		abstract public string LogRepository(string folderPath);
		abstract public List<AnnotateItem> Annotate(AnnotateParams annParams);
		abstract public string LogFile(string folderPath, string filename);
		abstract public List<FileLogItem> GetLog(LogParams logParams);
		abstract public List<FileLogItem> GetOutgoing(string folderPath);
		abstract public List<FileLogItem> GetParents(string folderPath, ParentsParams parentsParams);
		abstract public string GetDefaultPushRepository(string folderPath);
		abstract public void AddFiles(string folderPath, List<string> filenames);
		abstract public bool RenameFile(string folderPath, RenameParams renameParams);
		abstract public void RemoveFiles(string folderPath, RemoveParams removeParams);
		abstract public void RevertFiles(string folderPath, List<string> filenames);
		abstract public void IgnoreFiles(string folderPath, List<string> filenames);
		abstract public void StopIgnoringFiles(string folderPath, List<string> filenames);
		abstract public bool CommitFiles(CommitParams commitParams);
		public virtual void Rollback(string folderPath) { }
		abstract public void Tag(string folderPath, TagParams tagParams);
		abstract public void CreateRepository(string folderPath);
		abstract public void CloneRepository(string sourcePath, string destinationPath, bool update, bool compress);
		abstract public List<FileItem> GetResolvedList(string filepath);
		abstract public int GetNumberOfHeads(string folderPath);
		abstract public string GetCurrentBranch(string folderPath);
		abstract public bool HasIncoming(string folderPath);
		abstract public bool Update(string folderPath, UpdateParams updateParams);
		abstract public bool Push(string folderPath, PushParams pushParams);
		abstract public bool Pull(string folderPath, PullParams pullParams);
		abstract public bool Merge(string folderPath, MergeParams mergeParams);
		abstract public bool Branch(BranchParams branchParams);
		abstract public void UnmarkResolvedFile(string folderPath, string filename);

		private static SCC m_instance;
		public static void SetInstance(SCC instance)
		{
			m_instance = instance;
		}

		CommandOutput commandOutputDelegate = null;
		public void SetCommandOutput(CommandOutput outputDelegate)
		{
			commandOutputDelegate += outputDelegate;
		}

		protected void ShowCommandOutput(string text)
		{
			if (commandOutputDelegate != null)
			{
				commandOutputDelegate(text);
			}
		}
	}

	sealed class Mercurial : SCC
	{
		// The variable m_instance is never used.
		// This static construction causes an instance of Mercurial to be created 
		// so that the Mercurial constructor gets executed. The Mercurial constructor 
		// creates another the real instance and assigns it to the base class.
		// The base class can then be called to get the instance.
		//private static readonly Mercurial m_instance = new Mercurial();

		// define as private to disallow construction by outsiders.
		//private Mercurial()
		//{
			//m_instance = new Mercurial();
		//}
		//protected override static SCC GetInstance()
		//{
		//	new Mercurial();
		//}

		/** @copydoc SCC::IsSCCDirectory() */
		public override bool IsSCCDirectory(string directory)
		{
			bool retval = false;
			try
			{
				string[] subSubDirs = Directory.GetDirectories(directory);

				foreach (string ss in subSubDirs)
				{
					if (Path.GetFileName(ss) == ".hg")
					{
						retval = true;
						break;
					}
				}
			}
			catch { }

			return retval;

		}

		public override int GetNumberOfHeads(string folderPath)
		{
			if (!IsSCCDirectory(folderPath))
			{
				return 0;
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "heads";
			zc.WaitForExit = true;
			zc.Execute();

			int numHeads = 0;

			if (zc.ExitCode == 0)
			{

				string[] lines = zc.Result.Split('\n');

				foreach (string line in lines)
				{
					if(line.StartsWith("changeset:"))
					{
						numHeads++;
					}
				}
			}

			return numHeads;
		}
		
		public override string GetCurrentBranch(string folderPath)
		{
			string branch = String.Empty;

			if (IsSCCDirectory(folderPath))
			{
				ZyncCommand zc = new ZyncCommand();
				zc.CurrentFolder = folderPath;
				zc.ExePath = "hg";

				zc.Arguments = "branch";
				zc.WaitForExit = true;
				zc.Execute();

				if (zc.ExitCode == 0)
				{
					branch = zc.Result;

					// remove trailing line feed
					branch = branch.Replace("\n", "");
				}

			}
			return branch;
		}


		public override bool HasIncoming(string folderPath)
		{

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			//zc.Arguments = "incoming -l 1";	// only get 1 changeset
			zc.Arguments = "incoming";	// only get 1 changeset
			zc.WaitForExit = true;
			zc.Execute();

			bool hasIncoming = false;

			if (zc.ExitCode == 0)
			{

				string[] lines = zc.Result.Split('\n');

				foreach (string line in lines)
				{
					if (line.StartsWith("changeset:"))
					{
						hasIncoming = true;
						break;
					}
				}
			}

			return hasIncoming;
		}

		public override string GetName()
		{
			return "Mercurial";
		}

		public override string GetVersion()
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = Directory.GetCurrentDirectory();
			zc.ExePath = "hg";
			zc.Arguments = "version";

			try
			{
				zc.Execute();
			}
			catch
			{
				return "";
			}

			if (zc.ExitCode == 0)
			{
				Regex reVersion = new Regex(@"\w*[(]version\s*(?<version>[0-9.]*).*", RegexOptions.Compiled | RegexOptions.IgnoreCase);
				MatchCollection matches;

				string[] lines = zc.Result.Split('\n');
				foreach (string line in lines)
				{
					if ((matches = reVersion.Matches(line)).Count > 0)
					{
						return matches[0].Groups["version"].Value;
					}
				}
	
			}
			
			return "";

		}

		public override bool IsInstalled()
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = Directory.GetCurrentDirectory();
			zc.ExePath = "hg";
			zc.Arguments = "version";

			try
			{
				zc.Execute();
			}
			catch
			{
				return false;
			}
			

			return (zc.ExitCode == 0);
		}

		public override bool HasViewer()
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = Directory.GetCurrentDirectory();
			zc.ExePath = "hg";
			zc.Arguments = "help";

			zc.Execute();

			string[] lines = zc.Result.Split('\n');
			foreach (string s in lines)
			{
				if (s.StartsWith(" hgk    ") || s.StartsWith(" view   "))
				{
					return true;
				}
			}
			return false;
		}



		/** @brief	Combines a given list of filenames into a single string.
		
					The filenames in the string are seperated by spaces. There is a leading space before
					the first filename but no trailing space at the end. If a filename as a space within it, 
					that filename is enclosed in double quotes.
		 
			@param  filenames	the list of filename to combine.

			@returns			a string that contains the combined filenames.
		 */
		string GenerateFilenameString(List<string> filenames)
		{
			//string result = string.Empty;
			StringBuilder result = new StringBuilder();
			
			foreach (string filename in filenames)
			{
				if (filename.Contains(" "))
				{
					result.Append(" \"" + filename + "\"");	// surround filename in quotes incase it has any spaces
				}
				else
				{
					result.Append(" " + filename);
				}
			}
			return result.ToString();
		}

		private string QuoteIfNeeded(string pathname)
		{
			StringBuilder result = new StringBuilder();
			if (pathname.Contains(" "))
			{
				result.Append(" \"" + pathname + "\"");	// surround pathname in quotes in case it has any spaces
			}
			else
			{
				result.Append(" " + pathname);
			}
			return result.ToString();
		}

		public override void ViewRepository(string folderPath)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";
			zc.Arguments = "view ";
			zc.WaitForExit = false;
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();

		}
		
		public override void CreateRepository(string folderPath)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";
			zc.Arguments = "init";
			zc.WaitForExit = true;
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
			if (!zc.Succeeded)
			{
				throw new Exception(zc.Result);
			}
		}

		public override string GetDiff(DiffParams diffParams)
		{
			if(diffParams.filename == string.Empty)
			{
				throw new ArgumentException("Filename parameter missing in GetDiff commmand.");
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = diffParams.repoPath;
			zc.ExePath = "hg";

			bool useExternalDiff = diffParams.extCommand != String.Empty;

			StringBuilder args = new StringBuilder(useExternalDiff ? diffParams.extCommand : "diff");
			zc.WaitForExit = !useExternalDiff;	// Only wait for internal diff. Not external diff.

			if (diffParams.revision1 != string.Empty)
			{
				args.Append(" --rev " + diffParams.revision1);
			}
			if (diffParams.revision2 != string.Empty)
			{
				args.Append(" --rev " + diffParams.revision2);
			}

			args.Append(" " + QuoteIfNeeded(diffParams.filename));

			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			string result = zc.Result;

			if (!useExternalDiff)
			{
				if (zc.ErrorMsg.Length > 0)
				{
					ShowCommandOutput(zc.ErrorMsg);
				}
			}

			return result;
		}
#if false
		public override void DiffExternal(DiffParams diffParams)
		{
			if (diffParams.filename == string.Empty)
			{
				throw new ArgumentException("Filename parameter missing in DiffExternal commmand.");
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = diffParams.repoPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder(diffParams.extCommand);


			if (diffParams.revision1 != string.Empty)
			{
				args.Append(" --rev " + diffParams.revision1);
			}
			if (diffParams.revision2 != string.Empty)
			{
				args.Append(" --rev " + diffParams.revision2);
			}
			args.Append(" " + diffParams.filename);

			zc.Arguments = args.ToString();
			zc.WaitForExit = false;
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();

		}
#endif
		public override string LogRepository(string folderPath)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "log";
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			return zc.Result;
	
		}

		public override string LogFile(string folderPath, string filename)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "log -f " + filename;
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();

			return zc.Result;
		}

		public override List<FileLogItem> GetParents(string folderPath, ParentsParams parentsParams)
		{

			StringBuilder args = new StringBuilder("parents");
			if (parentsParams.revision.Length > 0)
			{
				args.Append(" --rev " + parentsParams.revision );
			}

			return DoLogCommand(folderPath, args.ToString(), parentsParams.filename, false, false);

		}


		/** @brief Retrives a log of the repository or of a single file.
 
			If no revision number is specfied, the order of the list is such that the latest entry will be 
			first in the list. Earliest entry is last in the list.
			If only the starting revision is specified, then all entries after the starting one will be returned.
			If only the ending revision is specified, then all entries up to the ending one will be returned.
			To get a single entry returned, specify the same revision for both start and end.
		*/
		public override List<FileLogItem> GetLog(LogParams logParams)
		{
			if (logParams.logByDateTime && logParams.logByRevision)
			{
				throw new ArgumentException("You can't specify both a revision and a date to the GetLog command.");
			}
			if ((logParams.repoPath == String.Empty) || !Directory.Exists(logParams.repoPath))
			{
				throw new ArgumentException("Repository path is invalid to the GetLog command.");
			}

			StringBuilder args = new StringBuilder("log");

			if (logParams.follow)
			{
				args.Append(" -f");
			}


			if (logParams.logByRevision)
			{
				if ((logParams.startRevision == logParams.endRevision) && (logParams.startRevision.Length > 0))
				{
					args.Append(" --rev " + logParams.startRevision);
				}
				else
				{
					args.Append(" --rev " + logParams.startRevision + ":" + logParams.endRevision);
				}
			}

			if (logParams.logByDateTime)
			{
				DateTime sdt = logParams.startDateTime;
				DateTime edt = logParams.endDateTime;
				args.Append(string.Format(" --date \"{0:####}-{1:00}-{2:00} {3:00}:{4:00}:00 to {5:####}-{6:00}-{7:00} {8:00}:{9:00}:00\"",
					sdt.Year, sdt.Month, sdt.Day, sdt.Hour, sdt.Minute,
					edt.Year, edt.Month, edt.Day, edt.Hour, edt.Minute));
			}

			// Avoid the list being returned with latest revision first, which happens if you don't specify a revision.
			if (!logParams.logByRevision && !logParams.logByDateTime)
			{
				args.Append(" --rev 0:tip");
			}

			return DoLogCommand(logParams.repoPath, args.ToString() , logParams.filename, logParams.getFiles, logParams.verbose);
		}

		public override List<FileLogItem> GetOutgoing(string folderPath)
		{
			return DoLogCommand(folderPath, "outgoing", "", false, false);
		}

		/** @brief Common function used for commands that produce a log-type output.
		 * 
		 *	So far this includes the commands log, outgoing, parents. All produce a list of log items 
		 *	which are parsed the same way.
		*/
		private List<FileLogItem> DoLogCommand(string folderPath, string arguments, string filename, bool getFiles, bool verbose)
		{

			// Write out the style file
			string styleFilename = Path.Combine(folderPath, "ZyncStyle-710F6CE2EBDC4C8493DEAE4C49800496.txt");
			TextWriter tw = new StreamWriter(styleFilename);
			string filesPart = getFiles ? "{files}" : "";
			string firstline = verbose ? "" : "|firstline";
            tw.WriteLine("changeset = \"" + @"rev:{rev}\nnode:{node|short}\nuser:{author}\ndate:{date|date}\nbranch:{branch}\nparents:{parents}\nchildren:{children}\n" + filesPart + "{tags}desc:{desc" + firstline + @"}\ndesc-end:\n" + "\"");
			tw.WriteLine("tag = \"tag:{tag}" + @"\n" + "\"");
			if (getFiles)
			{
				tw.WriteLine("file = \"file:{file}" + @"\n" + "\"");
			}
			tw.Close();


			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder(arguments);

			args.Append(" --style \"" + styleFilename + "\"");

			// The debug option is used to get the non-trivial parents to be output. 
			// It otherwise doesn't affect the output given the style we're using.
			// A trivial parent is a parent that has the previous revision number.
			// A non-trivial parent is one that does not.
			// So a merge commit will always have a non-trivial parent. 
			args.Append(" --debug");

			if (filename != string.Empty)
			{
				// The filename may have spaces in it, so surround it with double quotes.
				args.Append(" \"" + filename + "\"");
			}

			zc.Arguments = args.ToString();
			//ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			string log = zc.Result;

			File.Delete(styleFilename);

			return ParseLogOutput(ref log);
		}

#if false
		private List<FileLogItem> DoLogCommand(string folderPath, LogParams logParams, string command)
		{

			if (logParams.logByDateTime && logParams.logByRevision)
			{
				throw new ArgumentException("You can't specify both a revision and a date to the LogFileParsed command.");
			}

			// Write out the style file
			string styleFilename = Path.Combine(folderPath,"ZyncStyle-710F6CE2EBDC4C8493DEAE4C49800496.txt");
			TextWriter tw = new StreamWriter(styleFilename);
			string filesPart = logParams.getFiles ? "{files}" : "";
			string firstline = logParams.verbose ? "" : "|firstline";
			tw.WriteLine("changeset = \"" + @"rev:{rev}\nnode:{node|short}\nuser:{author}\ndate:{date|date}\nparents:{parents}\n" + filesPart + "{tags}desc:{desc" + firstline + @"}\ndesc-end:\n" + "\"");
			tw.WriteLine("tag = \"tag:{tag}" + @"\n" + "\"");
			if (logParams.getFiles)
			{
				tw.WriteLine("file = \"file:{file}" + @"\n" + "\"");
			}
			//tw.WriteLine("parent = \"parent:{parent}" + @"\n" + "\"");

			tw.Close();


			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder(command);

			if (logParams.follow)
			{
				args.Append(" -f");
			}

			// The debug option is used to get the non-trivial parents to be output. 
			// A trivial parent is a parent that has the previous revision number. A non-trivial parent is oen that does not.
			// So a merge commit will always have a non-trivial parent. 
			args.Append(" --debug");


			if (logParams.logByRevision)
			{
				if ((logParams.startRevision == logParams.endRevision) && (logParams.startRevision.Length > 0))
				{
					args.Append(" --rev " + logParams.startRevision);
				}
				else
				{
					args.Append(" --rev " + logParams.startRevision + ":" + logParams.endRevision);
				}
			}

			if (logParams.logByDateTime)
			{
				DateTime sdt = logParams.startDateTime;
				DateTime edt = logParams.endDateTime;
				args.Append(string.Format(" --date \"{0:####}-{1:00}-{2:00} {3:00}:{4:00}:00 to {5:####}-{6:00}-{7:00} {8:00}:{9:00}:00\"", 
					sdt.Year, sdt.Month, sdt.Day, sdt.Hour, sdt.Minute,
					edt.Year, edt.Month, edt.Day, edt.Hour, edt.Minute));
			}

			// Avoid the list being returned with latest revision first, which happens if you don't specify a revision.
			if (!logParams.logByRevision && !logParams.logByDateTime && (command == "log"))
			{
				args.Append(" --rev 0:tip");
			}

			args.Append(" --style \"" + styleFilename + "\"");


			if (logParams.filename != string.Empty)
			{
				// The filename may have spaces in it, so surround it with double quotes.
				args.Append(" \"" + logParams.filename + "\"");
			}

			zc.Arguments = args.ToString();
			//ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			string log = zc.Result;

			File.Delete(styleFilename);

			return ParseLogOutput(ref log);
		}
#endif

		private List<FileLogItem> ParseLogOutput(ref string log)
		{
			List<FileLogItem> list = new List<FileLogItem>();
			FileLogItem fli = new FileLogItem();
			fli.tags = new List<string>();
			fli.files = new List<string>();
			fli.children = new List<RevIdPair>();
			fli.parents = new List<RevIdPair>();

			Regex reChangeset = new Regex(@"^node:(?<changeset>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reRevision = new Regex(@"^rev:(?<revision>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reTag = new Regex(@"^tag:(?<tag>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reFile = new Regex(@"^file:(?<file>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reUser = new Regex(@"^user:(?<user>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			Regex reDate = new Regex(@"^date:(?<date>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reBranch = new Regex(@"^branch:(?<branch>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reSummary = new Regex(@"^summary:\s+(?<summary>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reDescription = new Regex(@"^desc:(?<desc>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reDescriptionEnd = new Regex(@"^desc-end:", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
			Regex reParents = new Regex(@"^parents:(?<par1rev>-?[a-zA-Z0-9]+):(?<par1id>-?[a-zA-Z0-9]+)\s+(?<par2rev>-?[a-zA-Z0-9]+):(?<par2id>-?[a-zA-Z0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Regex reChildren = new Regex(@"^children:(?<children>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

			IFormatProvider culture = new CultureInfo("en-US", true);
			string dateFormat = "ddd MMM dd HH:mm:ss yyyy zzz";


			MatchCollection matches;
			bool descriptionInProgress = false;

			// Parse the log file
			string[] lines = log.Split('\n');
			foreach (string line in lines)
			{
				if ((matches = reChangeset.Matches(line)).Count > 0)
				{
					fli.changesetId = matches[0].Groups["changeset"].Value;
				}
				else if ((matches = reRevision.Matches(line)).Count > 0)
				{
					fli.revision = Int32.Parse(matches[0].Groups["revision"].Value);
				}
				else if ((matches = reTag.Matches(line)).Count > 0)
				{
					fli.tags.Add(matches[0].Groups["tag"].Value);
				}
				else if ((matches = reFile.Matches(line)).Count > 0)
				{
					fli.files.Add(matches[0].Groups["file"].Value);
				}
				else if ((matches = reUser.Matches(line)).Count > 0)
				{
					fli.user = matches[0].Groups["user"].Value;
				}
				else if ((matches = reDate.Matches(line)).Count > 0)
				{
					fli.date = DateTime.ParseExact(matches[0].Groups["date"].Value, dateFormat, culture);
				}
				else if ((matches = reBranch.Matches(line)).Count > 0)
				{
					fli.branch = matches[0].Groups["branch"].Value;
				}
				else if ((matches = reSummary.Matches(line)).Count > 0)
				{
					fli.message = matches[0].Groups["summary"].Value;

					// The Add function will make a copy of the File Log item. It doesn't just copy the reference.
					list.Add(fli);
					fli.tags = new List<string>();
				}
				else if ((matches = reParents.Matches(line)).Count > 0)
				{
					string par1rev = matches[0].Groups["par1rev"].Value;
					string par1id = matches[0].Groups["par1id"].Value;
					string par2rev = matches[0].Groups["par2rev"].Value;
					string par2id = matches[0].Groups["par2id"].Value;
					//fli.parents = new List<RevIdPair>();
					if ((par1rev.Length > 0) && (Int32.Parse(par1rev) >= 0))
					{
						fli.parents.Add(new RevIdPair { revision = par1rev, changesetId = par1id });
					}
					if ((par2rev.Length > 0) && (Int32.Parse(par2rev) >= 0))
					{
						fli.parents.Add(new RevIdPair { revision = par2rev, changesetId = par2id });
					}
				}
				else if ((matches = reChildren.Matches(line)).Count > 0)
				{
					string childrenLine = matches[0].Groups["children"].Value;

					if (childrenLine.Length > 0)
					{
						//fli.children = new List<RevIdPair>();
						string[] childList = childrenLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
						foreach (string child in childList)
						{
							string[] childRev = child.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
							if (childRev.Length == 2)
							{
								fli.children.Add(new RevIdPair { revision = childRev[0], changesetId = childRev[1] });
							}
							else
							{
								Debug.Assert(false);
							}
						}
					}
				}
                else if ((matches = reDescription.Matches(line)).Count > 0)
                {
                    fli.message = matches[0].Groups["desc"].Value + "\n";
                    descriptionInProgress = true;
                }
                else if ((matches = reDescriptionEnd.Matches(line)).Count > 0)
                {
                    // A line starting with "desc-end:" marks the end of the description.
                    descriptionInProgress = false;

                    // The Add function will make a copy of the File Log item. It doesn't just copy the reference.
                    list.Add(fli);

                    // Description is last, so slear things for nex revision 
                    fli.tags = new List<string>();
                    fli.files = new List<string>();
					fli.children = new List<RevIdPair>();
					fli.parents = new List<RevIdPair>();
                }
                else
                {
                    if (descriptionInProgress)
                    {
                        fli.message += line + "\n";
                    }
                }

			}

			return list;
		}


		public override List<AnnotateItem> Annotate(AnnotateParams annParams)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = annParams.repoPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("annotate");

			if (annParams.annotateRevision)
			{
				args.Append(" --rev " + annParams.revisionToAnnotate);
			}
			if (annParams.showRevision)
			{
				args.Append(" -n");
			}
			if (annParams.showChangeset)
			{
				args.Append(" -c");
			}
			if (annParams.showAuthor )
			{
				args.Append(" -u");
			}
			if (annParams.showDate)
			{
				args.Append(" -d");
			}
			if (annParams.showLineNumber)
			{
				args.Append(" -l");
			}
			if (annParams.showFilename)
			{
				args.Append(" -f");
			}
			if (annParams.dontFollow)
			{
				args.Append(" --no-follow");
			}

			args.Append(" " + QuoteIfNeeded(annParams.filename));

			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			List<AnnotateItem> list = new List<AnnotateItem>();
			if (zc.Result.Length > 0)
			{
				// If this is a binary file, Mercurial will output a message saying so, but it 
				// won't appear as an error. The message line will end with the text below.
				if (zc.Result.EndsWith(": binary file\n"))
				{
					ShowCommandOutput(zc.Result);
				}
				else
				{
					list = AnnotateParse(zc.Result, annParams);
				}
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}
			return list;
		}

		/* @brief Parses the annotated text coming from the Mercurial command line.

			@param text			The full output from a Mercurial annotate command
			@param annParams	The annotate command line options that were selected which 
								helps to parse the text.

			It the -l option is specified together with any of -f, -u, -d, then either -n or -c 
			must also be specified. (If -l is specified alone, then -n is assumed even though it's 
			not specified.

			Elements on the line are listed in the following order:
			user, revision, changeset, date, filename, line number, text of source code.
			
			The source code text is preceeded by a colon. If line numbers are specified they are also 
			preceeded by a colon. All other elements are separated by a space.

			

		*/
		private List<AnnotateItem> AnnotateParse(string text, AnnotateParams annParams)
		{
			List<AnnotateItem> rows = new List<AnnotateItem>();

			string reUserStr = @"(?<user>\w+)";
			string reDateStr = @"(?<date>(Mon|Tue|Wed|Thu|Fri|Sat|Sun)\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s+\d\d\s+\d\d:\d\d:\d\d\s+\d{4}\s+(\+|\-)\d{4})";
			string reRevisionStr = @"(?<revision>\d+)";
			string reChangesetStr = @"(?<changeset>[A-Fa-f0-9]{12})";
			string reFilenameStr = @"(?<filename>[^:]+)";
			string reLineNumberStr = @"\s*(?<linenumber>\d+):";
			string reSourceTextStr = @" ?(?<source>.*)";

			// We use the annParams to construct an appropriate regex matching string 
			// so that we only have to do a search once per line, and we only search for what 
			// should be there.
			StringBuilder regExStr = new StringBuilder(@"^\s*");

			bool notFirst = false;
			if(annParams.showAuthor)
			{
				regExStr.Append(reUserStr);
				notFirst = true;
			}
			if (annParams.showRevision)
			{
				regExStr.Append((notFirst ? @"\s+" : "") + reRevisionStr);
				notFirst = true;
			}
			if (annParams.showChangeset)
			{
				regExStr.Append((notFirst ? @"\s+" : "") + reChangesetStr);
				notFirst = true;
			}
			if(annParams.showDate)
			{
				regExStr.Append((notFirst ? @"\s+" : "") + reDateStr);
				notFirst = true;
			}
			if (annParams.showFilename)
			{
				regExStr.Append((notFirst ? @"\s+" : "") + reFilenameStr);
				notFirst = true;
			}
			// There is always a colon before the source code text, even if there is no line number.
			// There must always be something before the colon. Even if nothing is selected, by default 
			// the revision is given.
			regExStr.Append(":");
			if (annParams.showLineNumber)
			{
				regExStr.Append(reLineNumberStr);
			}
			// Source text is always there of course.
			regExStr.Append(reSourceTextStr);


			Regex reLine = new Regex(regExStr.ToString(), RegexOptions.Compiled | RegexOptions.IgnoreCase);

			IFormatProvider culture = new CultureInfo("en-US", true);
			string dateFormat = "ddd MMM dd HH:mm:ss yyyy zzz";
			MatchCollection matches;

			string[] lines = text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

			// The first line may contain a byte order mark (BOM) which declares the file to be UTF-8 or 
			// some other encoding. Mercurial's annotate doesn't filter this out so we'll do it here.
			// We only check for the UTF-8 BOM for now.
			if ((lines.Length > 0) && lines[0].Contains("\x00EF\x00BB\x00BF"))
			{
				lines[0] = lines[0].Replace("\x00EF\x00BB\x00BF", "");
			}

			AnnotateItem ai = new AnnotateItem();
			foreach (string line in lines)
			{
				if ((matches = reLine.Matches(line)).Count > 0)
				{
					if (annParams.showAuthor)
					{
						ai.user = matches[0].Groups["user"].Value;
					}
					if (annParams.showRevision)
					{
						ai.revision = matches[0].Groups["revision"].Value;
					}
					if (annParams.showChangeset)
					{
						ai.changesetId = matches[0].Groups["changeset"].Value;
					}
					if (annParams.showDate)
					{
						ai.date = DateTime.ParseExact(matches[0].Groups["date"].Value, dateFormat, culture);
					}
					if (annParams.showFilename)
					{
						ai.filename = matches[0].Groups["filename"].Value;
					}
					if (annParams.showLineNumber)
					{
						ai.lineNumber = matches[0].Groups["linenumber"].Value;
					}
					ai.source = matches[0].Groups["source"].Value;
				}


				rows.Add(ai);
			}
			return rows;

		}

		public override void AddFiles(string folderPath, List<string> filenames)
		{
			if (filenames.Count == 0)
			{
				return;
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";
			zc.Arguments = "add";

			zc.Arguments += GenerateFilenameString(filenames);

			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
		}

		public override bool RenameFile(string folderPath, RenameParams renameParams)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "rename ";
			if (renameParams.after)
			{
				zc.Arguments += "--after ";
			}
			zc.Arguments += QuoteIfNeeded(renameParams.oldFilename) + " " + QuoteIfNeeded(renameParams.newFilename);
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);

				// If the -v switch is not used and the rename is successful, there will be no result output.
				// If the rename was not successful, there will be a couple lines output regardless of the -v switch.
				// The second line will begin with the word "abort".
				string[] lines = zc.Result.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

				foreach (string line in lines)
				{
					// If a line that starts with "abort" appears, then Rename did not actually do the rename and there are no changes.
					if (line.ToLower().StartsWith("abort"))
					{
						return false;	// no change
					}
				}

				return true; 
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
				return false;	// no change
			}
			return true;
		}


		public override void RemoveFiles(string folderPath, RemoveParams removeParams)
		{
			if (removeParams.files.Count == 0)
			{
				return;
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "remove";

			if (removeParams.after)
			{
				zc.Arguments += " -A";
				if (removeParams.force)
				{
					zc.Arguments += "f";
				}
			}
			else if (removeParams.force)
			{
				zc.Arguments += " -f";
			}

			zc.Arguments += GenerateFilenameString(removeParams.files);

			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}

		}
		
		public override void RevertFiles(string folderPath, List<string> filenames)
		{
			if (filenames.Count == 0)
			{
				return;
			}
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "revert";

			zc.Arguments += GenerateFilenameString(filenames);

			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}

		}


		public override void Rollback(string folderPath)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "rollback";
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
		}


		public override void Tag(string folderPath, TagParams tagParams)
		{
			if ((tagParams.revision.Length == 0) || (tagParams.tagList.Count == 0))
			{	// nothing to tag or no tags specified
				return;
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("tag");
			if (tagParams.replaceExisting)
			{
				args.Append(" -f");
			}
			if (tagParams.makeLocal)
			{
				args.Append(" -l");
			}
			if (tagParams.remove)
			{
				args.Append(" --remove");
			}
			else
			{
				args.Append(" --rev " + tagParams.revision);
			}

			if (tagParams.commitMessage.Length > 0)
			{
				args.Append(" -m " + "\"" + tagParams.commitMessage + "\"");
			}
			foreach (string tag in tagParams.tagList)
			{
				args.Append(" \"" + tag + "\"");
			}


			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
			else if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}

		}


		
		public override bool Update(string folderPath, UpdateParams updateParams)
		{

			if (updateParams.byDateTime && updateParams.byRevision)
			{
				throw new ArgumentException("You can't specify both a revision and a date to the Update command.");
			}
			if (updateParams.byRevision && (updateParams.revision == String.Empty))
			{
				throw new ArgumentException("Revision parameter missing in Update commmand.");
			}


			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("update");
			if (updateParams.clean)
			{
				args.Append(" -C");
			}
			if (updateParams.byRevision)
			{
				args.Append(" --rev " + updateParams.revision);
			}
			else if (updateParams.byDateTime)
			{
				DateTime dt = updateParams.dateTime;
				args.Append(string.Format(" --date \"<{0:####}-{1:00}-{2:00} {3:00}:{4:00}:00\"", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute));
			}

			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);

				// Count how many changes there were.
				// If there were more than 0 changes, return true;
				int changes = 0;

				string[] lines = zc.Result.Split('\n');
				foreach (string line in lines)
				{
					string[] sections = line.Split(',');
					if (sections.Length == 4)
					{
						foreach (string section in sections)
						{
							if (section.Length > 0)
							{
								string[] words = section.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
								changes += Convert.ToInt32(words[0]);
							}
						}
						if (changes > 0)
						{
							return true;
						}
					}
				}
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}
			return false;
		}


		public override void UnmarkResolvedFile(string folderPath, string filename)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "resolve --unmark " + filename;
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();

			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}
		}

		/** @brief Push to a remote repository

			@param folderPath	The path to the local, working folder (not the repository to push to)
			@param pushParams	A PushParams structure containing the push parameters.

			@retval		true if the remote repository has been updated. You can use this to trigger a UI update.
			@retval		false if the remote repository was not changed. A successful result could return false if nothing was Pushed.
		*/
		public override bool Push(string folderPath, PushParams pushParams)
		{

			if (pushParams.upToRevision && (pushParams.revision == String.Empty))
			{
				throw new ArgumentException("Revision parameter missing in Push commmand.");
			}
			if (!pushParams.toDefaultRepository && pushParams.otherRepository == String.Empty)
			{
				throw new ArgumentException("Repository path parameter missing in Push commmand.");
			}


			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("push");

			if (pushParams.force)
			{
				args.Append(" -f");
			}
			if (pushParams.allowNewBranch)
			{
				args.Append(" --new-branch" + pushParams.revision);
			}
			if (pushParams.upToRevision)
			{
				args.Append(" --rev " + pushParams.revision);
			}
			if (!pushParams.toDefaultRepository)
			{
				args.Append(" " + pushParams.otherRepository);
			}

			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}

			return false;
		}

		public override bool Merge(string folderPath, MergeParams mergeParams)
		{

			if (mergeParams.withRevision && (mergeParams.revision == String.Empty))
			{
				throw new ArgumentException("Revision parameter missing in Merge commmand.");
			}


			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("merge");

			if (mergeParams.force)
			{
				args.Append(" -f");
			}
			if (mergeParams.withRevision)
			{
				args.Append(" --rev " + mergeParams.revision);
			}

			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);

				Regex r = new Regex(@"(?<updated>\d+)\s+files updated,\s+(?<merged>\d+)\s+files merged,\s+(?<removed>\d+)\s+files removed,\s+(?<unresolved>\d+)\s+files unresolved", RegexOptions.Compiled);
				string[] lines = zc.Result.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

				foreach (string line in lines)
				{
					MatchCollection matches = r.Matches(line);
					if (matches.Count > 0)
					{
						GroupCollection groups = matches[0].Groups;
						int numMergedFiles = Int32.Parse(groups["merged"].Value);
						int numUpdatedFiles = Int32.Parse(groups["updated"].Value);
						int numRemovedFiles = Int32.Parse(groups["removed"].Value);
						int numUnresolvedFiles = Int32.Parse(groups["unresolved"].Value);

						if ((numMergedFiles > 0) || (numUpdatedFiles > 0) || (numRemovedFiles > 0) || (numUnresolvedFiles > 0))
						{
							return true;
						}
					}
				}
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}

			return false;
		}


		public override bool Branch(BranchParams branchParams)
		{
			if (branchParams.set == branchParams.clean)
			{
				throw new ArgumentException("Branch set and clean params cannot be equal.");
			}
			if (branchParams.set && branchParams.branchName == String.Empty)
			{
				throw new ArgumentException("A branch name must be provided when setting the branch name.");
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = branchParams.repoPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("branch");

			if (branchParams.set)
			{
				if (branchParams.force)
				{
					args.Append(" -f");
				}
				args.Append(" " + branchParams.branchName);
			}
			else if (branchParams.clean)
			{
				args.Append(" -C");
			}
			
			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);

				if (zc.Result.StartsWith("marked") || zc.Result.StartsWith("reset"))
				{
					return true;
				}
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}
			return false;
		}

		/** @brief Pull from remote repository

			@param folderPath	The path to the local, working folder (not the repository to pull from)
			@param pullParams	A PullParams structure containing the pull parameters.

			@retval		true if the local working repository has been changed. You can use this to trigger a UI update.
			@retval		false if the local repository was not changed. A successful result could return false if there was nothing to pull.
		*/
		public override bool Pull(string folderPath, PullParams pullParams)
		{

			if (pullParams.byRevision && (pullParams.revision == String.Empty))
			{
				throw new ArgumentException("Revision parameter missing in Pull commmand.");
			}
			if(!pullParams.fromDefaultRepository && pullParams.otherRepository == String.Empty)
			{
				throw new ArgumentException("Repository path parameter missing in Pull commmand.");
			}

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			StringBuilder args = new StringBuilder("pull");

			if (pullParams.update)
			{
				args.Append(" -u");
			}
			if (pullParams.force)
			{
				args.Append(" -f");
			}
			if (pullParams.byRevision)
			{
				args.Append(" --rev " + pullParams.revision);
			}
			if (!pullParams.fromDefaultRepository)
			{
				args.Append(" " + pullParams.otherRepository);
			}

			zc.Arguments = args.ToString();
			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);

				string[] lines = zc.Result.Split(new char[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

				foreach (string line in lines)
				{
					// If a line that starts with "add" appears, then Pull actually did something.
					if (line.ToLower().StartsWith("add"))
					{
						return true;
					}
				}
			}
			if (zc.ErrorMsg.Length > 0)
			{
				ShowCommandOutput(zc.ErrorMsg);
			}
			return false;
		}

		/** @brief Commits a list of files to the repository.
		 * 
		 * This function assumes that the files' status is either modified, added, or removed 
		 * and does not check to validate that. It just blindly tries to commit them.
		 * 
		 * @returns	true if the commit succeeded. False otherwise;
		 * 
		 */ 
		public override bool CommitFiles(CommitParams commitParams)
		{
			//if (files.Count == 0)
			//{
			//	return false;
			//}

			// Create a temporary file to hold the commit message because it might be 
			// too long for the command line. It will be deleted when it is closed.
			using (var logMsgFile = new TemporaryFileStream())
			{
				StreamWriter sw = new StreamWriter(logMsgFile);
				sw.Write(commitParams.commitMessage);
				sw.Flush();

				string args = "commit";
				if (commitParams.closeBranch)
				{
					args += " --close-branch";
				}
					
				args +=" -l " + " \"" + logMsgFile.Filename + "\"";

				args += GenerateFilenameString(commitParams.fileList);

				ZyncCommand zc = new ZyncCommand();
				zc.CurrentFolder = commitParams.repoPath;
				zc.ExePath = "hg";

				zc.Arguments = args;
				ShowCommandOutput("hg " + zc.Arguments);
				zc.Execute();
				if (zc.Result.Length > 0)
				{
					ShowCommandOutput(zc.Result);
				}
				if (zc.ErrorMsg.Length > 0)
				{
					ShowCommandOutput(zc.ErrorMsg);
				}

				return zc.Succeeded;
			}
		}


		public override void IgnoreFiles(string folderPath, List<string> filenames)
		{
			IgnoredFiles ignoredFiles = new IgnoredFiles(folderPath);
			foreach (string filename in filenames)
			{
				ignoredFiles.AddIgnoredFile(filename);
			}
		}

		public override void StopIgnoringFiles(string folderPath, List<string> filenames)
		{
			IgnoredFiles ignoredFiles = new IgnoredFiles(folderPath);
			foreach (string filename in filenames)
			{
				ignoredFiles.RemoveIgnoredFile(filename);
			}
		}

		public override void CloneRepository(string sourcePath, string destinationPath, bool update, bool compressedTransfer)
		{

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = sourcePath;
			zc.ExePath = "hg";

			zc.Arguments = "clone";
			if (!update)
			{
				zc.Arguments += " --noupdate";
			}
			if (!compressedTransfer)
			{
				zc.Arguments += " --uncompressed";
			}

			zc.Arguments += " " + QuoteIfNeeded(sourcePath) + " " + QuoteIfNeeded(destinationPath);

			ShowCommandOutput("hg " + zc.Arguments);
			zc.Execute();
			if (zc.Result.Length > 0)
			{
				ShowCommandOutput(zc.Result);
			}
		}

		public override string GetDefaultPushRepository(string folderPath)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = folderPath;
			zc.ExePath = "hg";

			zc.Arguments = "showconfig";
			zc.Execute();
			string result = zc.Result;

			string[] lines = result.Split('\n');
			string pushRepo = string.Empty;
			foreach (string line in lines)
			{
				if (line.StartsWith("paths.default="))
				{
					pushRepo = line.Substring(14);
					break;
				}
			}
			return pushRepo;
		}
		
		/** @brief Return the parent directory which is the source code control directory,
		 * or a null reference if there isn't one.
		 * 
		 * Once it finds a source code control directory, it returns. If there is an SCC 
		 * directory within another SCC directory, the outer ones won't be found.\n
		 * \n
		 * This does not check if there are any files under SCC that reside in the 
		 * directory passed in. Only that there is some parent which is directory under SCC.
		 * 
		 * @param directory		This is the subdirectory from which to start the search.
		 *						If any parent of this is a source code control directory,
		 *						then that parent path will be returned.
		 * @returns		The path of the parent directory that is a source code control directory,
		 *				or a null reference if there is none. If the directory passed in is itself 
		 *				an SCC directory, then the string returned will be equal to the directory parameter.
		 */
		string GetParentSCCDirectory(string directory)
		{
			if (IsSCCDirectory(directory))
			{
				return directory;
			}

			// note that Directory.GetParent() returns a null reference when it gets to the root.
			DirectoryInfo dirInfo = Directory.GetParent(directory);
			if (dirInfo == null)
			{
				return null;
			}

			return GetParentSCCDirectory(dirInfo.FullName);
		}

		public override FileStatus GetFileStatus(string filepath)
		{
			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = Path.GetDirectoryName(filepath);
			zc.ExePath = "hg";
			zc.Arguments = "status -A " + filepath;
			if (Properties.Settings.Default.ModifiedCheckboxState)
			{
				zc.Arguments += " -m";
			}

			zc.Execute();

			string result = zc.Result;

			return FileStatus.Clean;

		}

		public override List<FileItem> GetResolvedList(string filepath)
		{

			List<FileItem> resolvedList = new List<FileItem>();

			ZyncCommand zc = new ZyncCommand();
			zc.CurrentFolder = filepath;
			zc.ExePath = "hg";
			zc.Arguments = " resolve -l";
			zc.Execute();

			string result = zc.Result;

			string[] lines = result.Split('\n');

			foreach (string s in lines)
			{
				if (s.Length > 0)
				{
					FileItem fli = new FileItem();

					fli.filename = s.Substring(2);
					switch (s.Substring(0, 1))
					{
						case "C": fli.status = FileStatus.Clean; break;
						case "M": fli.status = FileStatus.Modified; break;
						case "A": fli.status = FileStatus.Added; break;
						case "R": fli.status = FileStatus.Removed; break;
						case "?": fli.status = FileStatus.NotTracked; break;
						case "!": fli.status = FileStatus.Deleted; break;
						case "I": fli.status = FileStatus.Ignored; break;
					}

					resolvedList.Add(fli);
				}
			}
			return resolvedList;
		}


		public override List<FileItem> GetFileList(string filepath, FileStatusFlag flags)
		{
			List<FileItem> fileList = new List<FileItem>();
			if (flags == FileStatusFlag.None)
			{
				return fileList;
			}

			ZyncCommand zfc = new ZyncCommand();
			zfc.CurrentFolder = filepath;
			zfc.ExePath = "hg";
			zfc.Arguments = " status";

			if ((flags & FileStatusFlag.Clean) == FileStatusFlag.Clean)
			{
				zfc.Arguments += " -c";
			}
			if ((flags & FileStatusFlag.Modified) == FileStatusFlag.Modified)
			{
				zfc.Arguments += " -m";
			}
			if ((flags & FileStatusFlag.NotTracked) == FileStatusFlag.NotTracked)
			{
				zfc.Arguments += " -u";
			}
            if ((flags & FileStatusFlag.Added) == FileStatusFlag.Added)
            {
                zfc.Arguments += " -a";
            }
			if ((flags & FileStatusFlag.Removed) == FileStatusFlag.Removed)
			{
				zfc.Arguments += " -r";
			}
			if ((flags & FileStatusFlag.Deleted) == FileStatusFlag.Deleted)
			{
				zfc.Arguments += " -d";
			}
			if ((flags & FileStatusFlag.Ignored) == FileStatusFlag.Ignored)
			{
				zfc.Arguments += " -i";
			}

			zfc.Execute();

			if (zfc.ExitCode != 0)
			{
				ShowCommandOutput(zfc.ErrorMsg);
				return fileList;
			}

			string result = zfc.Result;

			string[] lines = result.Split('\n');


			///////////////////////////// Now get the resolved status for files, if there is any
			//List<FileItem> resolvedList = new List<FileItem>();

			ZyncCommand zrc = new ZyncCommand();
			zrc.CurrentFolder = filepath;
			zrc.ExePath = "hg";
			zrc.Arguments = " resolve -l";
			zrc.Execute();
			bool hasResolveCommand = true;
			List<string> resolvedList = null;
			if (zrc.ExitCode != 0)
			{
				//ShowCommandOutput(zrc.ErrorMsg);
				hasResolveCommand = false;
			}
			else
			{
				string[] resolvedLines = zrc.Result.Split('\n');
				resolvedList = new List<string>(resolvedLines);
			}

			//////////////////////////////////////////////////////////////////////////////////////

			foreach (string s in lines)
			{
				if (s.Length > 0)
				{
					FileItem fli = new FileItem();

					fli.filename = s.Substring(2);
					switch (s.Substring(0, 1))
					{
						case "C": fli.status = FileStatus.Clean; break;
						case "M": fli.status = FileStatus.Modified; break;
						case "A": fli.status = FileStatus.Added; break;
						case "R": fli.status = FileStatus.Removed; break;
						case "?": fli.status = FileStatus.NotTracked; break;
						case "!": fli.status = FileStatus.Deleted; break;
						case "I": fli.status = FileStatus.Ignored; break;
					}

					if (hasResolveCommand)
					{
						string resolvedLine = resolvedList.Find(str => (str.Length > 2) ? (str.Substring(2) == fli.filename) : false);
						if (resolvedLine != null)
						{
							switch (resolvedLine.Substring(0, 1))
							{
								case "U": fli.resolveStatus = ResolveStatus.Unresolved; break;
								case "R": fli.resolveStatus = ResolveStatus.Resolved; break;
							};
						}
					}

					fileList.Add(fli);
				}
			}

#if false
			foreach (string s in lines)
			{
				if (s.Length > 0)
				{
					FileItem fileItem = fileList.Find(fi => fi.filename == s.Substring(2));
					if (fileItem.filename != String.Empty)
					{
						switch (s.Substring(0, 1))
						{
							case "U": fileItem.resolveStatus = ResolveStatus.Unresolved; break;
							case "R": fileItem.resolveStatus = ResolveStatus.Resolved; break;
						};
					}
				}
			}
#endif
			return fileList;
		}


	}

	/** @brief	IgnoredFiles is a class to manage the list of ignored files which resides 
	 *			in the file .hgignore for each repository.
	 *			
	 * This class does not have a function to get a list of the ignored files because you 
	 * can get that from the SCC.GetFileList() function.
	 */	
	public class IgnoredFiles
	{
		private List<string> m_ignoredFileList = new List<string>();
		private string m_folderPath;
		private string m_hgIgnorePath;

		/** @brief	Constructor for the IgnoredFiles class. Opens the file to prepare for reading and writing.
		 * 
		 *	@param folderPath	The full path to the repository of the ignored files. This path must point to 
		 *						a valid repository.
		 *						
		 * @throws ArgumentException	If the folderPath argument does not refer to a valid SCC repository.
		 * @throws System.*				May throws any of the .NET exceptions related to failed file access.
		 */
		public IgnoredFiles(string folderPath)
		{
			if (!SCC.Instance.IsSCCDirectory(folderPath))
			{
				throw new ArgumentException("The path specified is not a source code controlled directory.");
			}
			m_folderPath = folderPath;
			m_hgIgnorePath = Path.Combine(folderPath, ".hgignore");

			// If the file doesn't exist, don't do anything until the user adds an ignored file.
			if (File.Exists(m_hgIgnorePath))
			{
				using (StreamReader sr = File.OpenText(m_hgIgnorePath))
				{
					string str = sr.ReadLine();
					while (str != null)
					{
						if (str.Length > 0)
						{
							m_ignoredFileList.Add(str);
						}
						str = sr.ReadLine();
					}
					sr.Close();
				}
			}
		}

		public void AddIgnoredFile(string filename)
		{
			// If the filename doesn't exist in the list already...
			if (!m_ignoredFileList.Exists(s => s == filename))
			{
				//... add it
				m_ignoredFileList.Add(filename);
			}
			SaveToFile();
		}

		public void RemoveIgnoredFile(string filename)
		{
			// Remove all occurrances of the filename if there are any.
			m_ignoredFileList.RemoveAll(s => s == filename);
			SaveToFile();
		}

		private void SaveToFile()
		{
			// If the line  "syntax: glob" doesn't exist in the list, insert it at the begining.
			// At this point we only handle global syntax in hgignore files.
			if(!m_ignoredFileList.Exists(s => s.Contains("syntax") && s.Contains("glob")))
			{
				m_ignoredFileList.Insert(0, "syntax: glob");
			}

			using (StreamWriter sw = File.CreateText(m_hgIgnorePath))
			{
				foreach (string s in m_ignoredFileList)
				{
					sw.WriteLine(s);
				}
				sw.Close();
			}
		}

	
	}

	public class TemporaryFileStream : FileStream
	{
		private string m_fileName = string.Empty;

		public TemporaryFileStream()
			: this(System.IO.Path.GetTempFileName())
		{
		}

		public TemporaryFileStream(string fileName)
			: base(fileName, FileMode.Open)
		{
			m_fileName = fileName;
		}

		public string Filename
		{
			get
			{
				if (m_fileName == null)
				{
					throw new ObjectDisposedException(GetType().Name);
				}
				return m_fileName;
			}
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing) 
			{
				GC.SuppressFinalize(this);
			}
			if (m_fileName != null)
			{
				try
				{
					File.Delete(m_fileName);
				}
				catch
				{ } // best effort 
				m_fileName = null;
			}
		}
	}




}
