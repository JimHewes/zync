
Zync is a GUI front end for the Mercurial source code control program. It is written in C# and can be built using Microsoft Visual Studio 2015.

In the company I worked for back in 2008 we were transitioning from CVS to Mercurual. We had been using WinCVS and I wanted to create a 
GUI for Mercurial that was similar to WinCVS to make the transition easier. Zync doesn't have many features, but we didn't use any 
of the advanced features of Mercurial anyway. I would have liked to have added a graphical tree to it, but never did. These days there 
are better tools available so there's not much use for Zync anymore.
