﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class AnnotateParamForm : Form
	{

		private AnnotateParams m_annotateParams;

		public AnnotateParamForm(string revision = "")
		{
			InitializeComponent();

			if (revision != String.Empty)
			{
				revisionCheckBox.Checked = true;
				revisionTextBox.Enabled = true;
				revisionTextBox.Text = revision;
			}
			else
			{
				revisionCheckBox.Checked = false;
				revisionTextBox.Enabled = false;
				revisionTextBox.Text = String.Empty;
			}
			DontFollowCheckbox.Checked = Properties.Settings.Default.AnnotateDontFollow;

			annotateShiftKeyCheckBox.Checked = Properties.Settings.Default.AnnotateFormDisplayOnlyOnShift;
		}

		public AnnotateParams GetAnnotateParams()
		{
			return m_annotateParams;
		}

		private void OnClickedOkButton(object sender, MouseEventArgs e)
		{
			m_annotateParams.annotateRevision = revisionCheckBox.Checked;
			m_annotateParams.revisionToAnnotate = (m_annotateParams.annotateRevision) ? revisionTextBox.Text : String.Empty;
			m_annotateParams.dontFollow = DontFollowCheckbox.Checked;
		}

		// Save the state of the checkboxes no matter how the form is closed.
		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.AnnotateFormDisplayOnlyOnShift = annotateShiftKeyCheckBox.Checked;
			Properties.Settings.Default.AnnotateDontFollow = DontFollowCheckbox.Checked; 
			
			Properties.Settings.Default.Save();
		}

		private void OnAnnotateRevisionCheckboxChanged(object sender, EventArgs e)
		{
			revisionTextBox.Enabled = revisionCheckBox.Checked;
		}

		//private void ValidateLineNumberCheckbox(object sender, EventArgs e)
		//{
		//    if (showLineNumberCheckBox.Checked && !showRevisionCheckBox.Checked && !showChangesetCheckBox.Checked)
		//    {
		//        MessageBox.Show(this, "To display line numbers you must also select at least one of Revision or Changeset");
		//        if (((CheckBox)sender) == showLineNumberCheckBox)
		//        {
		//            showLineNumberCheckBox.Checked = false;
		//        }
		//        if (((CheckBox)sender) == showRevisionCheckBox)
		//        {
		//            showRevisionCheckBox.Checked = true;
		//        }
		//        if (((CheckBox)sender) == showChangesetCheckBox)
		//        {
		//            showChangesetCheckBox.Checked = true;
		//        }

		//    }
		//}

	}
}
