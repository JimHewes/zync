﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class FolderView : TreeView, Observer
	{
		private Model m_model = null;
		private Controller m_controller = null;
        private MainForm m_mainForm = null;
		ContextMenuStrip m_contextMenu = null;

		private struct ZyncDirInfo
		{
			public string fullPath;
			public bool isSCCDir;
		}

		private string m_rootPath = "";

		Subject m_subject = new Subject();
		/** @brief Sent if the currently selected folder changes.
		*/
		public class CurrentFolderAspect : Aspect { }
		private static CurrentFolderAspect currentFolderAspect = new CurrentFolderAspect();

		public FolderView()
		{
			InitializeComponent();

			// Load the TreeView images from the resource.
			// For instructions on how to add an image as a resource and use it, 
			// see http://support.microsoft.com/kb/324567 . 
			Stream s = this.GetType().Assembly.GetManifestResourceStream("Zync.Resources.FolderViewIcons.bmp");
			ImageList imageList = new ImageList();
			imageList.Images.AddStrip(Image.FromStream(s));
			s.Close();

			// Assign the ImageList to the TreeView.
			ImageList = imageList;

			// Set the TreeView control's default image index.
			ImageIndex = 0;
			
			//KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressHandler);

		}

		// We set the model and controller at the same time because we might use both
		public void Initialize(Model model, Controller controller, MainForm mainForm)
		{
			m_model = model;
			m_controller = controller;
            m_mainForm = mainForm;

			m_model.Attach(this);
            m_mainForm.Attach(this);

			UpdateRootPath();

			string currentFolder = Properties.Settings.Default.SelectedFolder;
			if ((currentFolder == String.Empty) || !Directory.Exists(currentFolder))
			{
				currentFolder = m_mainForm.GetRootFolder();
			}
			SelectDirectory(currentFolder);

		}

		public void Update(Subject subject, Aspect interest)
		{
			if (interest is MainForm.RootFolderAspect)
			{
				UpdateRootPath();
			}
			else if (interest is Model.FolderStatusChangeAspect)
			{
				string folderpath = ((Model.FolderStatusChangeAspect)interest).FolderPath;

				// The Nodes.Find function doesn't work the way we need it to, so do our own search.
				TreeNode foundNode = FindNode(Nodes[0], folderpath);
				if (foundNode != null)
				{
					ZyncDirInfo zdi = (ZyncDirInfo)foundNode.Tag;

					if (m_model.IsFolderScc(folderpath))
					{
						bool hasIncoming = m_model.HasIncoming(folderpath);
						foundNode.ImageIndex = hasIncoming ? 5 : 1; 
						foundNode.SelectedImageIndex = hasIncoming ? 5 : 1;
						zdi.isSCCDir = true;	// Can't modify Tag directly using a cast. Use local variable and reassign Tag.
						foundNode.Tag = zdi;

						// If this node has no child nodes, then add a dummy node so that the plus sign appears to the left of it.
						// We can do this because we know there is at least one subdurectory due to the SCC.
						if (foundNode.Nodes.Count == 0)
						{
							foundNode.Nodes.Add("ZyncDummy");
						}

					}
					else
					{
						foundNode.ImageIndex = 0;
						foundNode.SelectedImageIndex = 0;
						zdi.isSCCDir = false;		// Can't modify Tag directly using a cast. Use local variable and reassign Tag.
						foundNode.Tag = zdi;

					}
					
					// There is no need to notify anyone here that the folder status has changed 
					// to become a source code controlled folder. That is done by the Model (In fact, 
					// it's why this function got called). Thus the FileView object will observe the model.
				}
			}
		}

		public void Notify(Aspect aspect)
		{
			m_subject.Notify(aspect);
		}
		public virtual void Attach(Observer observer)
		{
			m_subject.Attach(observer);
		}
		public virtual void Detach(Observer observer)
		{
			m_subject.Detach(observer);
		}

		private TreeNode FindNode(TreeNode node, string folderPath)
		{
			if (node.Text == "ZyncDummy")
			{
				return null;
			}
			if (((ZyncDirInfo)node.Tag).fullPath == folderPath)
			{
				return node;
			}
			else
			{
				foreach (TreeNode childNode in node.Nodes)	// if there are any children
				{
					TreeNode foundNode = FindNode(childNode, folderPath);
					if (foundNode != null)
					{
						return foundNode;
					}
				}
				return null;
			}
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			base.OnPaint(pe);
		}

		// We cannot make SetRootPath and GetRootPath into a property if we also want to set the value 
		// in the constructor. This is because the VS designer will detect the property and make it available 
		// as one of the GUI designer properties, where it will insist on setting some initial value for it. 
		// Since the initial value is set in the designer-maintained code and happens AFTER the contructor, whatever 
		// setting the constructor loads from the saved settings will get wiped out by the generated designer code.
		private void UpdateRootPath()
		{
			string rootPath = m_mainForm.GetRootFolder();

			// The path passed in must be rooted. That means it must start with 
			// either a drive specifier or a "//".
			//if (!Path.IsPathRooted(rootPath))
			//{
			//	throw new ArgumentException("The path has no root");
			//}

			// Make sure directory exists
			if (!Directory.Exists(rootPath))
			{
				throw new ArgumentException("Root directory does not exist.");
			}

			Nodes.Clear();

			string dirName = GetDirectoryName(rootPath);


			ZyncDirInfo zdi = new ZyncDirInfo();
			zdi.fullPath = rootPath;

			TreeNode treeNode = new TreeNode(dirName);

			// If this is a source code controlled directory, then give it 
			// a different image. 
			if (m_model.IsFolderScc(m_mainForm.GetRootFolder()))
			{
				bool hasIncoming = m_model.HasIncoming(rootPath);
				treeNode.ImageIndex = hasIncoming ? 5 : 1;
				treeNode.SelectedImageIndex = hasIncoming ? 5 : 1;
				zdi.isSCCDir = true;
			}
			else
			{
				treeNode.ImageIndex = 0;
				treeNode.SelectedImageIndex = 0;
				zdi.isSCCDir = false;
			}

			Nodes.Add(treeNode);
			Nodes[0].Tag = zdi;

			// Does the root have any sub directories? If so, add a dummy.
			// Note that on Windows at least, this path may seem to exist as reported 
			// by the Directory.Exists() call above, but if it's a link, it will cause an 
			// exception in the GetDirectories call.
			string[] subDirs;
			try
			{
				subDirs = Directory.GetDirectories(rootPath);
			}
			catch
			{
				throw new ArgumentException("Root directory does not exist.");
			}
			if (subDirs.Length > 0)
			{
				Nodes[0].Nodes.Add("ZyncDummy");
			}

			// Call DoExpand to expand the root path. That is, to populate the child nodes of the root node.
			// We need to do that
			Nodes[0].Expand();
			DoExpand(Nodes[0]);
			// Note that OnBeforeExpand will not get called until this function we're in returns.

			m_rootPath = rootPath;

			// If the root path is not a substring of the selected directory path,
			// then we have to reset the selected path to be the root.
			if (!GetCurrentFolder().StartsWith(rootPath))
			{
				SelectDirectory(rootPath);
				Properties.Settings.Default.SelectedFolder = rootPath;
				//EventManager.Instance.PostEvent(new SelectedDirectoryChanged(this, m_rootPath));
			}

			// When the root changes, the view is refreshed and selected directory no longer looks selected.
			// So we need to reselect it.
			//if (m_model.CurrentFolder.Length > 0)
			//{
				// The SelectDirectory() function return the directory actually selected. The directory last saved 
				// to the properties might no longer exist on disk if the user deleted it. So the actual selected 
				// directory will be different that what we ask for here.
			//	SelectDirectory(m_model.CurrentFolder);
				// EventManager.Instance.PostEvent(new SelectedDirectoryChanged(this, Properties.Settings.Default.SelectedFolder));
			//}
		}

		private void DoExpand(TreeNode node)
		{
			// if this node being expanded has one child and that's a dummy node,
			// then we need to populate it.
			if (node.Nodes.Count == 1)
			{
				if (node.FirstNode.Text == "ZyncDummy")
				{
					node.Nodes.Clear();
					// Get the full path of the directory in question. Our custom-saved info is in the Tag.
					string subDir = ((ZyncDirInfo)node.Tag).fullPath;
					string[] subDirs = Directory.GetDirectories(subDir);

					foreach (string s in subDirs)
					{

						DirectoryInfo dInfo = new DirectoryInfo(s);

						// We want the directory attribute to be set, but not hidden or system.
						if ((dInfo.Attributes &
							(FileAttributes.Hidden | FileAttributes.System | FileAttributes.Directory))
							== FileAttributes.Directory)
						{
							TreeNode treeNode = new TreeNode(GetDirectoryName(s));

							// Does this directory have any sub directories? If so, add a dummy.
							// Note that if the directory s is a Windows link, the call to 
							// GetDirectories() will throw an exception.

							string[] subSubDirs;
							try
							{
								subSubDirs = Directory.GetDirectories(s);

								if (subSubDirs.Length > 0)
								{
									treeNode.Nodes.Add("ZyncDummy");
								}

								if (!Properties.Settings.Default.ShowOnlyRepositoryFolders || m_model.IsFolderScc(s))
								{
									node.Nodes.Add(treeNode);
								}

								ZyncDirInfo zdi = new ZyncDirInfo();
								zdi.fullPath = s;

								// If this is a source code controlled directory, then give it 
								// a different image. We have to check for null instance to avoid 
								// an exception in the dialog designer.
								if (m_model.IsFolderScc(s))
								{
									bool hasIncoming = false;
									if (Properties.Settings.Default.ShowIncoming)
									{
										hasIncoming = m_model.HasIncoming(s);
									}
									treeNode.ImageIndex = hasIncoming ? 5 : 1;
									treeNode.SelectedImageIndex = hasIncoming ? 5 : 1;
									treeNode.ForeColor = hasIncoming ? Color.DarkGreen : Color.Black;
									zdi.isSCCDir = true;
								}
								else
								{
									treeNode.ImageIndex = 0;
									treeNode.SelectedImageIndex = 0;
									zdi.isSCCDir = false;
								}
#if false
							// Are any of the subSubDirs named ".hg"? If so, this is a Mercurial
							// directory and we should signify it.
							foreach (string ss in subSubDirs)
							{
								if (GetDirectoryName(ss) == ".hg")
								{
									t.ImageIndex = 1;
									t.SelectedImageIndex = 1;
									//t.ForeColor = Color.Green;
									zdi.isHgDir = true;
									break;
								}
								else
								{
									t.ImageIndex = 0;
									t.SelectedImageIndex = 0;
									zdi.isHgDir = false;
								}
							}
#endif
								// Save our custom directory information to the node
								treeNode.Tag = zdi;

							}
							catch
							{
							}
						}

					}
				}
			}
		}

		protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
		{
			base.OnBeforeExpand(e);

			DoExpand(e.Node);

		}

		delegate void DrillDownDelegate(TreeNodeCollection nodes, List<string> dirList);

		// Drills down to the specified directory and makes it the selected directory.
		// So far this is only used during construction to get back to the previously saved node selection.
		// The return value is the directory actually selected. This may be different than the 
		// directory passed in the path parameter if that directory is not found. In that case the 
		// root directory is selected.
		public string SelectDirectory(string path)
		{

			// If the selected path does not start with the root path, then it cannot be a child of the root 
			// path and we can't drill down to it. So just select the root.
			if (!path.StartsWith(m_mainForm.GetRootFolder()))
			{
				SelectedNode = Nodes[0];
				return ((ZyncDirInfo)SelectedNode.Tag).fullPath;
			}

			// Get all of the path after the root path.
			// If the selected path and root path are equal, then subpath is an empty string.
			string subpath = path.Length > m_mainForm.GetRootFolder().Length ? path.Substring(m_mainForm.GetRootFolder().Length) : String.Empty;

			// Just because the selected path DOES start with the root path doesn't mean that it is a child 
			// (or the same path). For example, C:\\ZyncCopy is not a child of C:\\Zync.
			// But if we subtract the root path from the selected path, then there are three possibilities:
			//   1. The remaining part begins with a DirectorySeparatorChar or AltDirectorySeparatorChar 
			//		in which case the selected path is a child of the root path.
			//   2. The remaining part does not begin with a DirectorySeparatorChar or AltDirectorySeparatorChar 
			//		in which case the selected path is not a child of the root path.
			//	 3. The part left is an empty string, in which case the selected path is the same as the root path.
			//

			// Check for root path same as selected path.
			if (	(subpath == string.Empty) || 
					((subpath.Length == 1) && (subpath[0] != Path.DirectorySeparatorChar)) || 
					((subpath.Length == 1) && (subpath[0] != Path.AltDirectorySeparatorChar)))
			{
				SelectedNode = Nodes[0];
				return ((ZyncDirInfo)SelectedNode.Tag).fullPath;
			}

			// In this program, we normally don't append separators on the end or paths. But here we do temporarily 
			// to make this job easier.
			if ((path[path.Length - 1] != Path.DirectorySeparatorChar) && (path[path.Length - 1] != Path.AltDirectorySeparatorChar))
			{
				subpath += Path.DirectorySeparatorChar;
			}
			string tmpRoot = m_rootPath;
			if ((tmpRoot[tmpRoot.Length - 1] != Path.DirectorySeparatorChar) && (tmpRoot[tmpRoot.Length - 1] != Path.AltDirectorySeparatorChar))
			{
				tmpRoot += Path.DirectorySeparatorChar;
			}
			if (!path.StartsWith(tmpRoot))
			{
				SelectedNode = Nodes[0];
				return ((ZyncDirInfo)SelectedNode.Tag).fullPath;
			}

			// now separate this into individual directories
			//string[] dirs = subpath.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
			List<string> dirs = new List<string>(subpath.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries));

			// Here we create a "recursive" function that will drill down the tree node heirarchy according to the directory list array.
			// (This is technically not recusion because when it calls DrillDown it is calling a named delegate "DrillDown" which is 
			// a reference to a delegate. This reference could be be reassigned later. So this function would not be calling itself. 
			// But in this context we don't reassign it so it will still have the resursive effect we want.
			// See http://blogs.msdn.com/wesdyer/archive/2007/02/02/anonymous-recursion-in-c.aspx  )
			// Also, don't use Func<> here because that requires .NET Framework 3.5 and we're sticking to 2.0 for now.
			DrillDownDelegate DrillDown = null;	// <--- We have to assign null to this first to avoid a "use of an unassiged variable" compiler error.
			bool found = false;
			DrillDown = (nodes, dirList) => 
			{
				if (dirList.Count > 0)
				{
					foreach (TreeNode node in nodes)
					{
						// To just pick off the directory name we actually need to ask for the filename.
						string dirname = Path.GetFileName(((ZyncDirInfo)node.Tag).fullPath);

						if (dirname == dirList[0])
						{
							if (dirList.Count == 1)
							{
								// When the node gets selected, the TreeView control will automatically expand the tree to that node.
								SelectedNode = node;
								//node.Expand();	// <-- and let's expand the selected node because that's what the user probably wants anyway.
								found = true;
							}
							else
							{
								DoExpand(node);
								DrillDown(node.Nodes, dirList.GetRange(1, dirList.Count - 1));
							}
							break;
						}
					}
				}
			};

			// Now call it to do the work
			// The initial state of the treeview is that the root is expanded to one level and that is all.
			// So the first subdirectory in the array should be one of the nodes. We need to search for that node.
			// Nodes[0] is the root node. Nodes[0].Nodes is the collection of subdirectories at the first level below that.
			DrillDown(Nodes[0].Nodes, dirs);

			// If the directory looked for was not found, then we just use the root as the selected directory.
			if (!found)
			{
				SelectedNode = Nodes[0];
			}
			return ((ZyncDirInfo)SelectedNode.Tag).fullPath;

		}


		// Given a full directory path, this returns just the name, assuming the last 
		// part is a directory and not a filename.
		private string GetDirectoryName(string directoryPath)
		{
			// Remove any ending seperators.
			// Assume that any path of 3 chars length is the root, although it may be 
			// different on non-Windows platforms.
			while (directoryPath[directoryPath.Length - 1] == Path.DirectorySeparatorChar)
			{
				directoryPath = directoryPath.Substring(0, directoryPath.Length - 1);
			};

			// Now get the directory name.
			// If sepIndex is -1, the seperator wasn't found, probably because it was the root 
			// directory and we have something like "C:". So leave it as it is.
			int sepIndex = directoryPath.LastIndexOf(Path.DirectorySeparatorChar);
			string dirName = directoryPath;
			if (sepIndex > 0)
			{
				dirName = dirName.Substring(sepIndex + 1);
			}

			return dirName;
		}
#if false
		public void RefreshSelected()
		{
			if (SelectedNode == null)
			{
				return;
			}

			m_fileView.Items.Clear();

			// Get our custom directory info for the selected node.
			ZyncDirInfo zdi = (ZyncDirInfo)SelectedNode.Tag;

			if (zdi.isSCCDir && (m_fileView != null))
			{

				ZyncCommand zc = new ZyncCommand();
				zc.CurrentFolder = zdi.fullPath;
				zc.ExePath = "hg";
				zc.Arguments = "status -c";
				if (Properties.Settings.Default.ModifiedCheckboxState)
				{
					zc.Arguments += " -m";
				}
				if (Properties.Settings.Default.CleanCheckboxState)
				{
					zc.Arguments += " -c";
				}
				zc.Execute();

				string result = zc.Result;

				//List<string> lines = SeparateToLines(result);
				string[] lines = result.Split('\n');
				
				string[] record = new string[2];

				foreach (string s in lines)
				{
					//m_fileView
					if (s.Length > 0)
					{
						record[0] = s.Substring(2);
						record[1] = s.Substring(0, 1);

						m_fileView.Items.Add(new ListViewItem(record));

					}
				}

			}
		}

		List<string> SeparateToLines(string text)
		{

			return new List<string>();

		}
#endif
		protected override void OnAfterSelect(TreeViewEventArgs e)
		{
			//base.OnAfterSelect(e);
			//RefreshSelected();

			// Get our custom directory info for the selected node.
			ZyncDirInfo zdi = (ZyncDirInfo)SelectedNode.Tag;

			//m_controller.SetCurrentFolder(zdi.fullPath);

			// EventManager.Instance.PostEvent(new SelectedDirectoryChanged(this, zdi.fullPath));

			Properties.Settings.Default.SelectedFolder = zdi.fullPath;

			Notify(currentFolderAspect);

		}

		public string GetCurrentFolder()
		{
            // There may be no selected node, so check for that. 
            // This may happen when the application first starts.
			if (SelectedNode != null)
			{
				return ((ZyncDirInfo)SelectedNode.Tag).fullPath;
			}
			else
			{
				// Check if the directory exists and if the saved setting is valid. If not, 
				// then use the root folder which is governed by the main form.
				string currentFolder = Properties.Settings.Default.SelectedFolder;
				if ((currentFolder == String.Empty) || !Directory.Exists(currentFolder))
				{
					currentFolder = m_mainForm.GetRootFolder();
				}
				return currentFolder;
			}
		}

		protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
		{
			base.OnMouseUp(e);

			if (e.Button == MouseButtons.Right)
			{
				Point clickPoint = new Point(e.X, e.Y);
				TreeNode clickedNode = GetNodeAt(clickPoint);
				if (clickedNode == null)
				{
					return;
				}

				SelectedNode = clickedNode;

				// Get our custom directory info for the selected node.
				ZyncDirInfo zdi = (ZyncDirInfo)clickedNode.Tag;

				m_contextMenu = new ContextMenuStrip();
				m_contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(OnContextMenuItemClicked);
				ToolStripMenuItem item;

				if (m_model.IsFolderScc(GetCurrentFolder()))
				{

					// Show context menu 
					m_contextMenu.Items.Clear();


					item = new ToolStripMenuItem("Pull...");
					item.ToolTipText = "Pull changes from another repository";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					item = new ToolStripMenuItem("Push...");
					item.ToolTipText = "Push changes to another repository";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					item = new ToolStripMenuItem("Update...");
					item.ToolTipText = "Commit changes to the repository";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					item = new ToolStripMenuItem("Commit...");
					item.ToolTipText = "Commit changes to the repository";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					item = new ToolStripMenuItem("Merge...");
					item.ToolTipText = "Merge heads of this repository";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					item = new ToolStripMenuItem("Branch...");
					item.ToolTipText = "Create or reset a named branch for this working folder";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					m_contextMenu.Items.Add(new ToolStripSeparator());

				}

				item = new ToolStripMenuItem("Clone...");
				item.ToolTipText = "Make a clone of an existing reposotory";
				item.Tag = clickedNode;
				m_contextMenu.Items.Add(item);

				if (!zdi.isSCCDir)
				{
					item = new ToolStripMenuItem("Create repository here");
					item.ToolTipText = "Create a reposotory in " + zdi.fullPath;
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);
				}


				if (zdi.isSCCDir)
				{
					m_contextMenu.Items.Add(new ToolStripSeparator());

					if (m_model.HasViewer)
					{
						item = new ToolStripMenuItem("View...");
						item.ToolTipText = "Start interractive history viewer";
						item.Tag = clickedNode;
						m_contextMenu.Items.Add(item);

					} item = new ToolStripMenuItem("Log...");
					item.ToolTipText = "Display the project log";
					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);

					item = new ToolStripMenuItem("Rollback...");
					item.ToolTipText = " Use with care! Rolls back (undoes) the most recent commit, push, pull, tag, import, or unbundle";

					item.Tag = clickedNode;
					m_contextMenu.Items.Add(item);
				}


				m_contextMenu.Show(this, clickPoint);

			}

		}

		void OnContextMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			m_contextMenu.Close();

			bool shiftDown = ((Control.ModifierKeys & Keys.Shift) == Keys.Shift);

			TreeNode clickedNode = (TreeNode)e.ClickedItem.Tag;
			ZyncDirInfo zdi = (ZyncDirInfo)clickedNode.Tag;

			string folderPath = (string)zdi.fullPath;

			if (e.ClickedItem.Text == "View...")
			{
				m_controller.ViewSelectedRepository();
			}
			if (e.ClickedItem.Text == "Log...")
			{
				m_controller.LogRepository(this, folderPath);
			}

			if (e.ClickedItem.Text == "Push...")
			{
				m_controller.Push(this, folderPath);
			}

			if (e.ClickedItem.Text == "Pull...")
			{
				m_controller.Pull(this, shiftDown, folderPath);
			}

			if (e.ClickedItem.Text == "Update...")
			{
				m_controller.UpdateRepository(this, shiftDown, folderPath, "");
			}

			if (e.ClickedItem.Text == "Commit...")
			{
				m_controller.Commit(this);
			}

			if (e.ClickedItem.Text == "Merge...")
			{
				m_controller.MergeRepository(this, shiftDown, folderPath);
			}
			if (e.ClickedItem.Text == "Branch...")
			{
				m_controller.BranchRepository(this, folderPath);
			}
			if (e.ClickedItem.Text == "Rollback...")
			{
				m_controller.RollbackRepository(this, folderPath);
			}

			if (e.ClickedItem.Text == "Clone...")
			{
				m_controller.Clone();
			}


			if (e.ClickedItem.Text == "Create repository here")
			{
				try
				{
					m_controller.CreateRepository(folderPath);
				}
				catch
				{
					return;
				}
#if false
				if (m_model.IsSccFolder(folderPath))	// This should be true since CreateRepositoryInFolder() must have succeeded.
				{
					clickedNode.ImageIndex = 1;
					clickedNode.SelectedImageIndex = 1;
					zdi.isSCCDir = true;
					zdi.fullPath = folderPath;
					clickedNode.Tag = zdi;

					// If this node has no child nodes, then add a dummy node so that the plus sign appears to the left of it.
					// We can do this because we know there is at least one subdurectory due to the SCC.
					if (clickedNode.Nodes.Count == 0)
					{
						clickedNode.Nodes.Add("ZyncDummy");
					}
				}
#endif
			}

		}


		public void RefreshView()
		{
			// Has the effect of clearing and repainting the view.
			UpdateRootPath();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			if(e.KeyCode == Keys.F5)
			{
				RefreshView();
			}
			if (e.KeyCode == Keys.F4)
			{
				bool oldSI = Properties.Settings.Default.ShowIncoming;
				Properties.Settings.Default.ShowIncoming = true;
				RefreshView();
				Properties.Settings.Default.ShowIncoming = oldSI;
			}
		}

	}
}
