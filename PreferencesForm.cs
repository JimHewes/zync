﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class PreferencesForm : Form
	{
		public PreferencesForm()
		{
			InitializeComponent();

			defaultCloneSourceTextBox.Text = Properties.Settings.Default.PrefDefaultCloneSource;
			showOnlyRepositoryFoldersCheckBox.Checked = Properties.Settings.Default.ShowOnlyRepositoryFolders;
			showIncomingCheckBox.Checked = Properties.Settings.Default.ShowIncoming;

			// Command Dialogs
			skipPullDialogCheckBox.Checked = Properties.Settings.Default.PullFormDisplayOnlyOnShift;
			skipUpdateDialogCheckBox.Checked = Properties.Settings.Default.UpdateFormDisplayOnlyOnShift;
			skipMergeDialogCheckBox.Checked = Properties.Settings.Default.MergeFormDisplayOnlyOnShift;
			skipFileLogDialogCheckBox.Checked = Properties.Settings.Default.LogFileFormDisplayOnlyOnShift;
			skipAnnotateDialogCheckBox.Checked = Properties.Settings.Default.AnnotateFormDisplayOnlyOnShift;

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000;
		}

		private void OnClickedPreferencesOk(object sender, EventArgs e)
		{
			Properties.Settings.Default.PrefDefaultCloneSource = defaultCloneSourceTextBox.Text;
			Properties.Settings.Default.ShowOnlyRepositoryFolders = showOnlyRepositoryFoldersCheckBox.Checked;
			Properties.Settings.Default.ShowIncoming = showIncomingCheckBox.Checked;

			// Command Dialogs
			Properties.Settings.Default.PullFormDisplayOnlyOnShift = skipPullDialogCheckBox.Checked;
			Properties.Settings.Default.UpdateFormDisplayOnlyOnShift = skipUpdateDialogCheckBox.Checked;
			Properties.Settings.Default.MergeFormDisplayOnlyOnShift = skipMergeDialogCheckBox.Checked;
			Properties.Settings.Default.LogFileFormDisplayOnlyOnShift = skipFileLogDialogCheckBox.Checked;
			Properties.Settings.Default.AnnotateFormDisplayOnlyOnShift = skipAnnotateDialogCheckBox.Checked;
		}

		private void OnFormLoad(object sender, EventArgs e)
		{
			this.Size = Properties.Settings.Default.PreferencesFormSize;
		}

		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.PreferencesFormSize = this.Size;
		}
	}
}
