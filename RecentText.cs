﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;

namespace Zync
{
	/** @brief	The RecentText class represents a list of recent text used for various purposes.
	 * 
	 * Recent text messages are saved to an XML file. The RecentText class is an 
	 * in-memory representation of that file allowing easy access to recent text messages.
	 * The file is stored in the user's local application data folder.
	 */
	public class RecentText
	{
		private readonly string m_localAppData;
		private readonly string m_appName;
		private readonly string m_filename;
		private readonly string m_filePath;

		private readonly string m_rootNodeName;
		private readonly string m_textNodeName;
		private const string m_maxEntriesNodeName = "MaxEntries";
		private const string m_indexAttrName = "Index";

		private const int m_maxCapacity = 1000;	///< never let the max number of messages exceed this value. 
		public int MaxNumberOfEntries { get; private set; }

		List<string> m_messageList;


		/**	@brief RecentText Constructor
		 * 
		 * @param appName			Used as the folder name to store the XML file in within the user's local application data folder.
		 * @param filename			The filename of the XML file to store the text in.
		 * @param rootNodeName		The name of the root node within the XML file
		 * @param textNodeName		The name of the nodes that hold the text.
		 * @param defaultMaxNumberOfEntries		If the file doesn't exist yet, this is the maximum number text entries you want the file to hold.
												If the file already exists, this value is ignored and the value in the file is used and is reflected 
												by the MaxNumberOfEntries property.
		 * 
		 */
		public RecentText(
			string appName,
			string filename,
			string rootNodeName,
			string textNodeName,
			int defaultMaxNumberOfEntries
			)
		{
			m_appName = appName;
			m_filename = filename;
			m_rootNodeName = rootNodeName;
			m_textNodeName = textNodeName;
			MaxNumberOfEntries = defaultMaxNumberOfEntries;

			// Get the file path of the XML file
			m_localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

			m_filePath = Path.Combine(Path.Combine(m_localAppData, m_appName), m_filename);

			//MakeSureFileExists();

			Load();


		}


		/** @brief Removes all messages.
		 * 
		 * Calling this function will cause all message to be deleted. But they will still exist 
		 * in the file until Save() is called.
		 * 
		 **/
		public void Clear()
		{
			m_messageList.Clear();
		}


		/** @brief Adds the given message as the most recent message in the list.
		 * 
		 * Note that calling this function will cause the oldest message to be deleted if the 
		 * list is already full. But the old messages still exist in the file until Save() is called.
		 * 
		 *	@param message	The message to add.
		 **/
		public void Add(string message)
		{
			// If there are any messages in the list that exactly match the one we're about 
			// to insert, then remove them first.
			m_messageList.RemoveAll(s => s == message);

			// Insert the most recent message at the beginning.
			m_messageList.Insert(0, message);

			// Trim old entries, which are at the end.
			int messagesToTrim = m_messageList.Count - MaxNumberOfEntries;
			if (messagesToTrim > 0)
			{
				m_messageList.RemoveRange(m_messageList.Count - messagesToTrim, messagesToTrim);
			}
		}

		public List<string> GetList()
		{
			return m_messageList;
		}

		public string Filepath
		{
			get { return m_filePath; }
		}


		private void Load()
		{

			XmlDocument doc = new XmlDocument();	///< The XML document loaded from the disk file.
			
			// Although not directly documented, the Load function will throw an 
			// UnauthorizedAccessException if the file cannot be opened for reading.
			// It will throw a FileNotFoundException if the file doesn't exist.
			// But we catch exceptions because we don't require the file to exist.
			// If it doesn't, then we just go with defaults.
			try
			{
				doc.Load(m_filePath);
			}
			catch
			{
				m_messageList = new List<string>(MaxNumberOfEntries);		// MaxNumberOfEntries holds the default value
				return;
			}

			// We should only get back one node here.
			var maxNumberOfMessagesNodeList = doc.DocumentElement.SelectNodes(m_maxEntriesNodeName);
			Debug.Assert(maxNumberOfMessagesNodeList.Count == 1 || maxNumberOfMessagesNodeList.Count == 0,
				"There are more than one [" + m_maxEntriesNodeName + "] elements in the file.");

			//int maxNumberOfMessages = m_defaultMaxNumberOfMessages;
			if(maxNumberOfMessagesNodeList.Count == 1)
			{
				MaxNumberOfEntries = Int32.Parse(maxNumberOfMessagesNodeList[0].InnerText);
			}
			else if (maxNumberOfMessagesNodeList.Count == 0)
			{
				// If there is no MaxEntries node, create one with the default value
				XmlElement mmNode = doc.CreateElement(m_maxEntriesNodeName);
				mmNode.InnerText = MaxNumberOfEntries.ToString();
				doc.DocumentElement.AppendChild(mmNode);

			}
			else if (maxNumberOfMessagesNodeList.Count > 1)
			{
				// If there are more than one, find the one with the highest value.
				int maxValueFound = 0;
				foreach (XmlNode node in maxNumberOfMessagesNodeList)
				{
					int valueFound = Int32.Parse(node.InnerText);
					maxValueFound = Math.Min(m_maxCapacity, Math.Max(valueFound, maxValueFound));
				}
				// We don't need to clean up by removing extraneous nodes.
				// We're going to rewrite the entire file anyway if anything gets changed.
				MaxNumberOfEntries = maxValueFound;
			}

			// Create a list with the capacity the size of the max messages we want to allow.
			m_messageList = new List<string>(MaxNumberOfEntries);

			var messageNodeList = doc.DocumentElement.SelectNodes(m_textNodeName);
			foreach (XmlNode node in messageNodeList)
			{
				// Get this node's index
				int index = Int32.Parse(node.Attributes.GetNamedItem(m_indexAttrName).Value);

				// if the index is outside the max number of messages, ignore it.
				if (index < MaxNumberOfEntries)
				{
					// Lists cannot automatically grow when you use the index operator. They can only 
					// grow using the Add function (or AddRange). So before we try to assign the message to 
					// an slots in the list, check if the list has enough slots.
					// (There are more clever ways to do this with later versions of .NET Framework, 
					// but we're trying to stick with 2.0.)
					if (index > m_messageList.Count - 1)
					{
						int itemsToAdd = index - m_messageList.Count;
						while (itemsToAdd-- > 0)
						{	// Most of the time this loop won't even execute, i.e. if the 
							// file has all messages in order with no gaps.
							m_messageList.Add(default(string));
						}
						m_messageList.Add(node.InnerText);
					}
					else
					{
						// The reason we use the index operator and not Insert is because we don't want to move 
						// all following items up. We just want to overwrite the item in this slot.
						m_messageList[index] = node.InnerText;
					}
				}

				// Now let's remove any empty entries because we assume the user doesn't care about those.
				m_messageList.RemoveAll(s => s == string.Empty);
			}
		}

		/** @brief Saves the commit message to the file on disk.
		 * 
		 * The user of this class must call this Save() function to have the changes saved.
		 * The data is NOT saved automatically when the class is destroyed.
		 * 
		 * 
		 **/
		public void Save()
		{

			// Generate an XML document.
			XmlDocument newDoc = new XmlDocument();

			XmlDeclaration xmlDeclaration = newDoc.CreateXmlDeclaration("1.0", "utf-8", null);

			// Create the root element
			XmlElement rootNode = newDoc.CreateElement(m_rootNodeName);
			newDoc.InsertBefore(xmlDeclaration, newDoc.DocumentElement);
			newDoc.AppendChild(rootNode);

			XmlElement mmNode = newDoc.CreateElement(m_maxEntriesNodeName);
			mmNode.InnerText = MaxNumberOfEntries.ToString();
			newDoc.DocumentElement.AppendChild(mmNode);

			for (int i = 0; i < m_messageList.Count; ++i)
			{
				XmlElement newNode = newDoc.CreateElement(m_textNodeName);
				newNode.SetAttribute(m_indexAttrName, i.ToString());
				newNode.InnerText = m_messageList[i];
				newDoc.DocumentElement.AppendChild(newNode);
			}

			// Make sure the folder exists.
			string appFolder = Path.Combine(m_localAppData, m_appName);
			if (!Directory.Exists(appFolder))
			{
				Directory.CreateDirectory(appFolder);
			}	

			newDoc.Save(m_filePath);
		}

	}

}
