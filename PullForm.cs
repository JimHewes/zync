﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class PullForm : Form
	{

		public bool FromDefault { get; private set; }
		public bool ByRevision { get; private set; }
		public string Revision { get; private set; }
		public string OtherRepositoryPath { get; private set; }
		public bool UpdateToNewTip { get; private set; }
		public bool ForceEvenIfUnrelated { get; private set; }
		Model m_model;

		public PullForm(Model model, string defaultRepositoryPath)
		{
			InitializeComponent();

			m_model = model;
			OtherRepositoryPath = String.Empty;
			ByRevision = false;
			Revision = String.Empty;
			UpdateToNewTip = false;
			ForceEvenIfUnrelated = false;

			if (defaultRepositoryPath != String.Empty)
			{
				pullFromDefaultRadioButton.Checked = true;
				pullFromOtherRadioButton.Checked = false;

				pullFromDefaultLabel.Text = defaultRepositoryPath;
				FromDefault = true;
			}
			else
			{
				pullFromDefaultLabel.Text = String.Empty;
				pullFromOtherRadioButton.Checked = true;
				pullFromDefaultRadioButton.Checked = false;
				pullFromDefaultRadioButton.Enabled = false;
				
				FromDefault = false;
			}

			// Changing the background color of the pullFromOtherTextBox control would mess up it's color 
			// when it is disabled. So we create a dummy text box that is displayed when the text control 
			// is disabled and that has the color we want. When the pullFromOtherRadioButton is selected and 
			// the pushToOtherTextBox is enabled and made visible, the dummy text box is made not visible.
			pullFromOtherTextBoxDummy.Enabled = false;	// This is always disabled, but only visible when pullFromDefault radio button is selected.
			pullFromOtherTextBoxDummy.Location = pullFromOtherTextBox.Location;
			pullFromOtherTextBoxDummy.Text = String.Empty;
			pullFromOtherTextBoxDummy.Visible = !pullFromOtherRadioButton.Checked;

			pullFromOtherTextBox.Text = String.Empty;
			pullFromOtherTextBox.Enabled = pullFromOtherRadioButton.Checked;

			pullByRevTextBox.Enabled = false;
			pullForceCheckBox.Checked = false;
			pullUpdateCheckBox.Checked = false;

			pullOkButton.Enabled = !pullFromOtherRadioButton.Checked || (pullFromOtherTextBox.Text != String.Empty);

			pullShiftKeyCheckBox.Checked = Properties.Settings.Default.PullFormDisplayOnlyOnShift;

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 
		}

		private void OnByRevisionCheckBoxChanged(object sender, EventArgs e)
		{
			pullByRevTextBox.Enabled = pullByRevCheckBox.Checked;
		}

		private void OnPullFromOtherRadioButtonChanged(object sender, EventArgs e)
		{
			if (pullFromOtherRadioButton.Checked)
			{
				pullFromOtherTextBoxDummy.Visible = false;
				pullFromOtherTextBox.Visible = true;
				pullFromOtherTextBox.BackColor = ValidateOtherRepository() ? Color.White : Color.LightYellow;
			}
			else
			{
				pullFromOtherTextBoxDummy.Visible = true;
				pullFromOtherTextBox.Visible = false;
				pullFromOtherTextBox.BackColor = Color.White;
			}


			pullFromOtherTextBox.Enabled = pullFromOtherRadioButton.Checked;
			pullFromOtherBrowseButton.Enabled = pullFromOtherRadioButton.Checked;

			pullOkButton.Enabled = !pullFromOtherRadioButton.Checked || (pullFromOtherTextBox.Text != String.Empty);
		}

		private void OnPullFromOtherTextBoxTextChanged(object sender, EventArgs e)
		{
			pullFromOtherTextBox.BackColor = ValidateOtherRepository() ? Color.White : Color.LightYellow;
			pullOkButton.Enabled = (pullFromOtherTextBox.BackColor == Color.White);
		}

		private void OnClickedOkButton(object sender, EventArgs e)
		{
			FromDefault = pullFromDefaultRadioButton.Checked;
			if (FromDefault == false)
			{
				OtherRepositoryPath = pullFromOtherTextBox.Text;
			}

			ByRevision = pullByRevCheckBox.Checked;
			if (ByRevision)
			{
				Revision = pullByRevTextBox.Text;
			}

			UpdateToNewTip = pullUpdateCheckBox.Checked;
			ForceEvenIfUnrelated = pullForceCheckBox.Checked;

			Properties.Settings.Default.PullFormDisplayOnlyOnShift = pullShiftKeyCheckBox.Checked;
			Properties.Settings.Default.Save();
		}

		private void OnBrowseOtherRepositoryButtonClicked(object sender, EventArgs e)
		{
			FolderBrowserDialog browseDialog = new FolderBrowserDialog();
			if (pullFromOtherTextBox.Text != String.Empty)
			{
				browseDialog.SelectedPath = pullFromOtherTextBox.Text;
			}
			browseDialog.ShowNewFolderButton = false;

			if (DialogResult.OK == browseDialog.ShowDialog())
			{
				pullFromOtherTextBox.Text = browseDialog.SelectedPath;
			}
		}


		private bool ValidateOtherRepository()
		{
			try
			{
				if (Directory.Exists(pullFromOtherTextBox.Text))
				{
					if (m_model.IsFolderScc(pullFromOtherTextBox.Text))
					{
						return true;
					}
				}
			}
			catch
			{
			}
			return false;
		}
	}
}
