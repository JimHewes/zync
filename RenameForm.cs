﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	/** @brief Renames a file in the source code control system.
	**/
	public partial class RenameForm : Form
	{
		private string m_repositoryFolder;

		public string ToFilename { get; private set; }
		public string FromFilename { get; private set; }
		public bool After { get; private set; }

		/** @brief Constructor for the Rename form.

			@repository	The repository the file to be rename is in.

			@filename	The name of the file as it currently exists.
						This may be the "From" name is the file is currently tracked, 
						or it may be the "To" filename if he file has aleady been rename 
						on the disk and is not currently considered tracked.

			@fileStatus The status of the file to be renamed.\n
						If the status is NotTracked, then it is assumed the file has 
						already been renamed on disk and the filename parameter represents 
						the "To" filename. The After checkbox is fixed as checked.\n
						If the status is other than NotTracked, then it is assumed the file has 
						not yet been renamed on disk and the filename parameter represents 
						the "From" filename.\n
						(In either case, the filename is the name as it currently exists on disk.)

		**/
		public RenameForm(string repositoryFolder, string filename, FileStatus fileStatus)
		{
			InitializeComponent();
			m_repositoryFolder = repositoryFolder;
			ToFilename = "";
			FromFilename = "";
			After = false;

			RenameButton.Enabled = false;

			RenameFromFilenameTextbox.Text =
			RenameFromFilenameLabel.Text =
			RenameToFilenameTextbox.Text =
			RenameToFilenameLabel.Text = string.Empty;


			if (fileStatus == FileStatus.NotTracked)
			{
				RenameFromFilenameTextbox.Enabled = 
				RenameFromFilenameTextbox.Visible = true;
				RenameFromFilenameLabel.Enabled = 
				RenameFromFilenameLabel.Visible = false;

				RenameToFilenameTextbox.Enabled = 
				RenameToFilenameTextbox.Visible = false;
				RenameToFilenameLabel.Enabled = 
				RenameToFilenameLabel.Visible = true;

				RenameToFilenameLabel.Text = filename;

				RenameAfterCheckBox.Checked = true;
				RenameAfterCheckBox.Enabled = false;
			}
			else
			{
				RenameFromFilenameTextbox.Enabled =
				RenameFromFilenameTextbox.Visible = false;
				RenameFromFilenameLabel.Enabled =
				RenameFromFilenameLabel.Visible = true;

				RenameToFilenameTextbox.Enabled =
				RenameToFilenameTextbox.Visible = true;
				RenameToFilenameLabel.Enabled =
				RenameToFilenameLabel.Visible = false;

				RenameFromFilenameLabel.Text = filename;

				RenameAfterCheckBox.Checked = false;
				RenameAfterCheckBox.Enabled = true;

			}

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 
		}


		private void OnToTextChanged(object sender, EventArgs e)
		{
			bool toFilenameValid = !File.Exists(Path.Combine(m_repositoryFolder, RenameToFilenameTextbox.Text))
									|| RenameAfterCheckBox.Checked;

			RenameToFilenameTextbox.BackColor = toFilenameValid ? Color.White : Color.LightYellow;

			RenameButton.Enabled = toFilenameValid && (RenameToFilenameTextbox.Text.Length > 0) ;
		}


		private void OnFromTextChanged(object sender, EventArgs e)
		{
			bool fromFilenameValid = !File.Exists(Path.Combine(m_repositoryFolder, RenameFromFilenameTextbox.Text));

			RenameFromFilenameTextbox.BackColor = fromFilenameValid ? Color.White : Color.LightYellow;

			RenameButton.Enabled = fromFilenameValid && (RenameFromFilenameTextbox.Text.Length > 0);
		}

		private void OnClickedRenameButton(object sender, EventArgs e)
		{
			if (RenameFromFilenameTextbox.Enabled)
			{
				FromFilename = RenameFromFilenameTextbox.Text;
				ToFilename = RenameToFilenameLabel.Text;
			}
			else
			{
				FromFilename = RenameFromFilenameLabel.Text;
				ToFilename = RenameToFilenameTextbox.Text;
			}
			After = RenameAfterCheckBox.Checked;
		}

		private void OnAfterCheckboxChanged(object sender, EventArgs e)
		{
			// The After checkbox won't be changed if the file status is not tracked.
			// This function is only for whe the file status is tracked

			bool toFilenameValid = !File.Exists(Path.Combine(m_repositoryFolder, RenameToFilenameTextbox.Text))
						|| RenameAfterCheckBox.Checked;

			RenameToFilenameTextbox.BackColor = toFilenameValid ? Color.White : Color.LightYellow;

			RenameButton.Enabled = toFilenameValid && (RenameToFilenameTextbox.Text.Length > 0);

		}

	}
}
