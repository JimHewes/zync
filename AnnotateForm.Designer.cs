﻿namespace Zync
{
	partial class AnnotateForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnnotateForm));
			this.m_annotateListView = new System.Windows.Forms.ListView();
			this.userColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.revisionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.changesetColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.dateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.filenameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lineNumberColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.sourceColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.panel1 = new System.Windows.Forms.Panel();
			this.m_lineNumerCheckBox = new System.Windows.Forms.CheckBox();
			this.m_filenameCheckBox = new System.Windows.Forms.CheckBox();
			this.m_dateCheckBox = new System.Windows.Forms.CheckBox();
			this.m_changesetCheckBox = new System.Windows.Forms.CheckBox();
			this.m_revisionCheckBox = new System.Windows.Forms.CheckBox();
			this.m_userCheckBox = new System.Windows.Forms.CheckBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_annotateListView
			// 
			this.m_annotateListView.AllowColumnReorder = true;
			this.m_annotateListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.userColumnHeader,
            this.revisionColumnHeader,
            this.changesetColumnHeader,
            this.dateColumnHeader,
            this.filenameColumnHeader,
            this.lineNumberColumnHeader,
            this.sourceColumnHeader});
			this.m_annotateListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_annotateListView.FullRowSelect = true;
			this.m_annotateListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.m_annotateListView.Location = new System.Drawing.Point(0, 0);
			this.m_annotateListView.MultiSelect = false;
			this.m_annotateListView.Name = "m_annotateListView";
			this.m_annotateListView.Size = new System.Drawing.Size(871, 414);
			this.m_annotateListView.TabIndex = 0;
			this.m_annotateListView.UseCompatibleStateImageBehavior = false;
			this.m_annotateListView.View = System.Windows.Forms.View.Details;
			this.m_annotateListView.ColumnReordered += new System.Windows.Forms.ColumnReorderedEventHandler(this.OnColumnReordered);
			this.m_annotateListView.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.OnColumnWidthChanged);
			// 
			// userColumnHeader
			// 
			this.userColumnHeader.Text = "User";
			this.userColumnHeader.Width = 80;
			// 
			// revisionColumnHeader
			// 
			this.revisionColumnHeader.Text = "Rev";
			this.revisionColumnHeader.Width = 40;
			// 
			// changesetColumnHeader
			// 
			this.changesetColumnHeader.Text = "ID";
			// 
			// dateColumnHeader
			// 
			this.dateColumnHeader.Text = "Date";
			// 
			// filenameColumnHeader
			// 
			this.filenameColumnHeader.Text = "Filename";
			// 
			// lineNumberColumnHeader
			// 
			this.lineNumberColumnHeader.Text = "Line";
			// 
			// sourceColumnHeader
			// 
			this.sourceColumnHeader.Text = "Source";
			this.sourceColumnHeader.Width = 500;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.m_lineNumerCheckBox);
			this.panel1.Controls.Add(this.m_filenameCheckBox);
			this.panel1.Controls.Add(this.m_dateCheckBox);
			this.panel1.Controls.Add(this.m_changesetCheckBox);
			this.panel1.Controls.Add(this.m_revisionCheckBox);
			this.panel1.Controls.Add(this.m_userCheckBox);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(871, 35);
			this.panel1.TabIndex = 2;
			// 
			// m_lineNumerCheckBox
			// 
			this.m_lineNumerCheckBox.AutoSize = true;
			this.m_lineNumerCheckBox.Location = new System.Drawing.Point(441, 9);
			this.m_lineNumerCheckBox.Name = "m_lineNumerCheckBox";
			this.m_lineNumerCheckBox.Size = new System.Drawing.Size(56, 17);
			this.m_lineNumerCheckBox.TabIndex = 5;
			this.m_lineNumerCheckBox.Text = "Line #";
			this.m_lineNumerCheckBox.UseVisualStyleBackColor = true;
			this.m_lineNumerCheckBox.CheckedChanged += new System.EventHandler(this.OnCheckboxChanged);
			// 
			// m_filenameCheckBox
			// 
			this.m_filenameCheckBox.AutoSize = true;
			this.m_filenameCheckBox.Location = new System.Drawing.Point(352, 9);
			this.m_filenameCheckBox.Name = "m_filenameCheckBox";
			this.m_filenameCheckBox.Size = new System.Drawing.Size(68, 17);
			this.m_filenameCheckBox.TabIndex = 4;
			this.m_filenameCheckBox.Text = "Filename";
			this.m_filenameCheckBox.UseVisualStyleBackColor = true;
			this.m_filenameCheckBox.CheckedChanged += new System.EventHandler(this.OnCheckboxChanged);
			// 
			// m_dateCheckBox
			// 
			this.m_dateCheckBox.AutoSize = true;
			this.m_dateCheckBox.Location = new System.Drawing.Point(282, 9);
			this.m_dateCheckBox.Name = "m_dateCheckBox";
			this.m_dateCheckBox.Size = new System.Drawing.Size(49, 17);
			this.m_dateCheckBox.TabIndex = 3;
			this.m_dateCheckBox.Text = "Date";
			this.m_dateCheckBox.UseVisualStyleBackColor = true;
			this.m_dateCheckBox.CheckedChanged += new System.EventHandler(this.OnCheckboxChanged);
			// 
			// m_changesetCheckBox
			// 
			this.m_changesetCheckBox.AutoSize = true;
			this.m_changesetCheckBox.Location = new System.Drawing.Point(170, 9);
			this.m_changesetCheckBox.Name = "m_changesetCheckBox";
			this.m_changesetCheckBox.Size = new System.Drawing.Size(91, 17);
			this.m_changesetCheckBox.TabIndex = 2;
			this.m_changesetCheckBox.Text = "Changeset ID";
			this.m_changesetCheckBox.UseVisualStyleBackColor = true;
			this.m_changesetCheckBox.CheckedChanged += new System.EventHandler(this.OnCheckboxChanged);
			// 
			// m_revisionCheckBox
			// 
			this.m_revisionCheckBox.AutoSize = true;
			this.m_revisionCheckBox.Location = new System.Drawing.Point(82, 9);
			this.m_revisionCheckBox.Name = "m_revisionCheckBox";
			this.m_revisionCheckBox.Size = new System.Drawing.Size(67, 17);
			this.m_revisionCheckBox.TabIndex = 1;
			this.m_revisionCheckBox.Text = "Revision";
			this.m_revisionCheckBox.UseVisualStyleBackColor = true;
			this.m_revisionCheckBox.CheckedChanged += new System.EventHandler(this.OnCheckboxChanged);
			// 
			// m_userCheckBox
			// 
			this.m_userCheckBox.AutoSize = true;
			this.m_userCheckBox.Location = new System.Drawing.Point(13, 9);
			this.m_userCheckBox.Name = "m_userCheckBox";
			this.m_userCheckBox.Size = new System.Drawing.Size(48, 17);
			this.m_userCheckBox.TabIndex = 0;
			this.m_userCheckBox.Text = "User";
			this.m_userCheckBox.UseVisualStyleBackColor = true;
			this.m_userCheckBox.CheckedChanged += new System.EventHandler(this.OnCheckboxChanged);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.m_annotateListView);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 35);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(871, 414);
			this.panel2.TabIndex = 1;
			// 
			// AnnotateForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(871, 449);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AnnotateForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Annotate";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView m_annotateListView;
		private System.Windows.Forms.ColumnHeader revisionColumnHeader;
		private System.Windows.Forms.ColumnHeader changesetColumnHeader;
		private System.Windows.Forms.ColumnHeader userColumnHeader;
		private System.Windows.Forms.ColumnHeader dateColumnHeader;
		private System.Windows.Forms.ColumnHeader lineNumberColumnHeader;
		private System.Windows.Forms.ColumnHeader sourceColumnHeader;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox m_lineNumerCheckBox;
		private System.Windows.Forms.CheckBox m_filenameCheckBox;
		private System.Windows.Forms.CheckBox m_dateCheckBox;
		private System.Windows.Forms.CheckBox m_changesetCheckBox;
		private System.Windows.Forms.CheckBox m_revisionCheckBox;
		private System.Windows.Forms.CheckBox m_userCheckBox;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ColumnHeader filenameColumnHeader;
	}
}