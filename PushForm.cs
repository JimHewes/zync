﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class PushForm : Form
	{
		public bool ToDefault { get; private set; }
		public bool UpToRevision { get; private set; }
		public string Revision { get; private set; }
		public string OtherRepositoryPath { get; private set; }
		public bool ForceEvenIfUnrelated { get; private set; }
		public bool AllowNewBranch { get; private set; }
		Model m_model;

		public PushForm(Model model, string defaultRepositoryPath)
		{
			InitializeComponent();

			m_model = model;
			OtherRepositoryPath = String.Empty;
			UpToRevision = false;
			Revision = String.Empty;
			ForceEvenIfUnrelated = false;

			if (defaultRepositoryPath != String.Empty)
			{
				pushToDefaultRadioButton.Checked = true;
				pushToOtherRadioButton.Checked = false;

				pushToDefaultLabel.Text = defaultRepositoryPath;
				ToDefault = true;
			}
			else
			{
				pushToDefaultLabel.Text = String.Empty;
				pushToOtherRadioButton.Checked = true;
				pushToDefaultRadioButton.Checked = false;
				pushToDefaultRadioButton.Enabled = false;

				ToDefault = false;
			}

			// Changing the background color of the pushToOtherTextBox control would mess up it's color 
			// when it is disabled. So we create a dummy text box that is displayed when the text control 
			// is disabled and that has the color we want. When the pushToOtherRadioButton is selected and 
			// the pushToOtherTextBox is enabled and made visible, the dummy text box is made not visible.
			pushToOtherTextBoxDummy.Enabled = false;	// This is always disabled, but only visible when pushToDefault radio button is selected.
			pushToOtherTextBoxDummy.Location = pushToOtherTextBox.Location;
			pushToOtherTextBoxDummy.Text = String.Empty;
			pushToOtherTextBoxDummy.Visible = !pushToOtherRadioButton.Checked;


			pushToOtherTextBox.Text = String.Empty;
			pushToOtherTextBox.Enabled = pushToOtherRadioButton.Checked;
			pushToOtherTextBox.BackColor = Color.LightYellow;	// since it is empty is it not a valid directory
			pushToOtherTextBox.Visible = pushToOtherRadioButton.Checked;

			pushUpToRevTextBox.Enabled = false;
			pushForceCheckBox.Checked = false;
			pushAllowNewBranchCheckBox.Checked = false;

			pushOkButton.Enabled = !pushToOtherRadioButton.Checked || (pushToOtherTextBox.Text != String.Empty);

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 
		}

		private void OnUpToRevisionCheckBoxChanged(object sender, EventArgs e)
		{
			pushUpToRevTextBox.Enabled = pushUpToRevCheckBox.Checked;
		}

		private void OnPushToOtherRadioButtonChanged(object sender, EventArgs e)
		{

			if (pushToOtherRadioButton.Checked)
			{
				pushToOtherTextBoxDummy.Visible = false;
				pushToOtherTextBox.Visible = true;
				pushToOtherTextBox.BackColor = ValidateOtherRepository() ? Color.White : Color.LightYellow;
			}
			else
			{
				pushToOtherTextBoxDummy.Visible = true;
				pushToOtherTextBox.Visible = false;
				pushToOtherTextBox.BackColor = Color.White;
			}
			pushToOtherTextBox.Enabled = pushToOtherRadioButton.Checked;
			pushToOtherBrowseButton.Enabled = pushToOtherRadioButton.Checked;

			pushOkButton.Enabled = !pushToOtherRadioButton.Checked || (pushToOtherTextBox.Text != String.Empty);
		}

		private void OnPushToOtherTextBoxTextChanged(object sender, EventArgs e)
		{
			pushToOtherTextBox.BackColor = ValidateOtherRepository() ? Color.White : Color.LightYellow;
			pushOkButton.Enabled = (pushToOtherTextBox.BackColor == Color.White);
		}

		private void OnClickedOkButton(object sender, EventArgs e)
		{
			ToDefault = pushToDefaultRadioButton.Checked;
			if (ToDefault == false)
			{
				OtherRepositoryPath = pushToOtherTextBox.Text;
			}

			UpToRevision = pushUpToRevCheckBox.Checked;
			if (UpToRevision)
			{
				Revision = pushUpToRevTextBox.Text;
			}

			ForceEvenIfUnrelated = pushForceCheckBox.Checked;
			AllowNewBranch = pushAllowNewBranchCheckBox.Checked;

			Properties.Settings.Default.Save();
		}

		private void OnBrowseOtherRepositoryButtonClicked(object sender, EventArgs e)
		{
			FolderBrowserDialog browseDialog = new FolderBrowserDialog();
			if (pushToOtherTextBox.Text != String.Empty)
			{
				browseDialog.SelectedPath = pushToOtherTextBox.Text;
			}
			browseDialog.ShowNewFolderButton = false;

			if (DialogResult.OK == browseDialog.ShowDialog())
			{
				pushToOtherTextBox.Text = browseDialog.SelectedPath;
			}
		}

		private bool ValidateOtherRepository()
		{
			try
			{
				if (Directory.Exists(pushToOtherTextBox.Text))
				{
					if (m_model.IsFolderScc(pushToOtherTextBox.Text))
					{
						return true;
					}
				}
			}
			catch
			{
			}
			return false;
		}
	}
}
