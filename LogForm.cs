﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class LogForm : Form
	{
		public LogForm()
		{
			InitializeComponent();

			DiffHighlighting = false;

			// In the designer we set the ReadOnly property to true. But that also makes the 
			// background color automatically set to gray. We want it to stay as white, so reset 
			// it back to white here.
			m_logTextBox.BackColor = Color.White;

		}
		public bool DiffHighlighting { get; set; }

		private void OnFormLoad(object sender, EventArgs e)
		{
			this.Size = Properties.Settings.Default.LogWindowSize;
			this.Location = Properties.Settings.Default.LogWindowLocation;
			m_logTextBox.DetectUrls = false;

		}


		public string LogText
		{
			set
			{
				if (DiffHighlighting)
				{
					DoDiffHighlight(value);
				}
				else
				{
					m_logTextBox.Font = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

					m_logTextBox.Text = value;
				}
			}
			get
			{
				return m_logTextBox.Text;
			}
		}

		private void DoDiffHighlight(string textToAdd)
		{

			//Font oldFont = m_logTextBox.SelectionFont;

			string[] lines = textToAdd.Split('\n');
			m_logTextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

			foreach (string line in lines)
			{
				m_logTextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

				if (line.Length > 0)
				{
					if (line.Substring(0, 1) == "+")
					{
						m_logTextBox.SelectionColor = Color.Green;
					}
					else if (line.Substring(0, 1) == "-")
					{
						m_logTextBox.SelectionColor = Color.Red;
					}
					else if (line.Substring(0, 1) == "@")
					{
						m_logTextBox.SelectionColor = Color.Purple;
					}
					else
					{
						m_logTextBox.SelectionColor = Color.Black;
					}
				}

				// SelectedText acts like an append text function. The line will be added in the font and color we previously selected.
				m_logTextBox.SelectedText = line + "\n";

			}
			m_logTextBox.SelectionStart = 0;
			m_logTextBox.SelectionLength = 0;

		}


		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.LogWindowSize = this.Size;
			Properties.Settings.Default.LogWindowLocation = this.Location;

		}


	}
}
