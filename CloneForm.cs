﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Zync
{
	public partial class CloneForm : Form
	{
		//private string m_currentFolder = String.Empty;
		Timer autoCompleteTimer = new Timer();
		CloneSourceHistory m_cloneSourceHistory;
		Model m_model = null;
		Controller m_controller = null;
		Timer destValidateTimer = new Timer();

		public CloneForm(Model model, Controller controller, string sourceFolder)
		{
			InitializeComponent();
			cloneButton.Enabled = false;

			m_model = model;
			m_controller = controller;

			// ---- Clone Source comboBox ------------
			cloneSourceComboBox.Items.Add(sourceFolder);
			m_cloneSourceHistory = new CloneSourceHistory();
			foreach (string cloneSource in m_cloneSourceHistory.GetList())
			{
				cloneSourceComboBox.Items.Add(cloneSource);
			}
			cloneSourceComboBox.Text = sourceFolder;
			cloneSourceComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
			cloneSourceComboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
			cloneSourceComboBox.BackColor = ValidateSource() ? Color.White : Color.LightYellow;

			// ---- Clone Destination comboBox ------------
			cloneDestinationTextBox.BackColor = Color.LightYellow;
			cloneDestinationTextBox.Text = ""; // m_model.CurrentFolder;

			PopulateAutocompleteList();

			//autoCompleteTimer.Interval = 700;
			//autoCompleteTimer.Tick += new EventHandler(autoCompleteTimerHandler);

			destValidateTimer.Tick += new EventHandler(DestinationValidateTimerHandler);

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 
		}

		private void autoCompleteTimerHandler(object sender, System.EventArgs e)
		{
			autoCompleteTimer.Stop();
			PopulateAutocompleteList();
		}

		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.CloneFormSize = this.Size;
		}

		private void OnFormLoad(object sender, EventArgs e)
		{
			this.Size = Properties.Settings.Default.CloneFormSize;
		}


		// It's not good to try to remove items from the cloneSourceComboBox.AutoCompleteCustomSource list,
		// or to try to call Clear() to clear it. This causes trouble.
		// However, you don't really need to remove items because only those items which match the beginning of  
		// the text the user typed in will be displayed. So you can keep adding directories to AutoCompleteCustomSource list,
		// making sure they are all SCC directories and you don't add duplicates. All you'll ever have in that list is 
		// SCC directories, and only the relevant ones will be displayed, not all of them.
		private void PopulateAutocompleteList()
		{

			// The functions below such as Directory.Exists() will happily accept a path that has any number of 
			// consecutive directory separators and will ignore the extras. For example "G:\\\\\\Zync" is treated just like 
			// "G:\Zync". But we don't want to accept that because it puts a lot of junk strings in the AutoComplete source list.
			// So filter these out. (We'll need to fix this later to accept paths such as "\\MyServer\projects".
			char[] badStr1 = { Path.DirectorySeparatorChar, Path.DirectorySeparatorChar };
			char[] badStr2 = { Path.AltDirectorySeparatorChar, Path.AltDirectorySeparatorChar };
			if (cloneSourceComboBox.Text.Contains(new string(badStr1)) || cloneSourceComboBox.Text.Contains(new string(badStr2)))
			{
				return;
			}

			if (Directory.Exists(cloneSourceComboBox.Text))
			{
				string[] subdirs = Directory.GetDirectories(cloneSourceComboBox.Text);

				foreach (string subdir in subdirs)
				{
					if (m_model.IsFolderScc(subdir))
					{
						if (!cloneSourceComboBox.AutoCompleteCustomSource.Contains(subdir))
						{
							cloneSourceComboBox.AutoCompleteCustomSource.Add(subdir);
						}
					}
				}
			}
		}

#if false
		private void PopulateSourceList()
		{
#if true
			int cursorIndex = cloneSourceComboBox.SelectionStart;
			string oldText = cloneSourceComboBox.Text;

			cloneSourceComboBox.Items.Clear();
			//cloneSourceComboBox.Text = oldText;
			cloneSourceComboBox.Items.Add(oldText);
			if (Directory.Exists(cloneSourceComboBox.Text))
			{
				string[] subdirs = Directory.GetDirectories(cloneSourceComboBox.Text);

				foreach (string subdir in subdirs)
				{
					if (m_model.IsSCCFolder(subdir))
					{
						cloneSourceComboBox.Items.Add(subdir);
					}
				}
			}
			cursorIndex = cloneSourceComboBox.SelectionStart = cursorIndex;
			//if (cloneSourceComboBox.SelectedIndex != 0)
			//{
			//	cloneSourceComboBox.SelectedIndex = 0;
			//}

			//cloneSourceComboBox.DroppedDown = (cloneSourceComboBox.Items.Count > 0);
#endif
		}
#endif

		private bool ValidateSource()
		{
			try
			{
				if (Directory.Exists(cloneSourceComboBox.Text))
				{
					if (m_model.IsFolderScc(cloneSourceComboBox.Text))
					{
						return true;
					}
				}
			}
			catch
			{
			}
			return false;
		}

		private bool ValidateDestination()
		{
			try
			{
				// Destination directory must not exist already and must not be the same as the source directory.
				// But if the directory doesn't exist and the source and desination are the same, we don't flag it 
				// as a destination path error. It's a source path error because the diretory doesn't exist.
				// If the destination path is a file that exists, then Directory.Exists() returns false.
				// So we have to check that the path isn't really a file that exists.
				if (Directory.Exists(cloneDestinationTextBox.Text) || File.Exists(cloneDestinationTextBox.Text))
				{
					return false;
				}

				// The following line has the effect of removing any trailing directory separator
				string temp = Path.GetDirectoryName(cloneDestinationTextBox.Text);

				// The path up to and not including the specified directory must exist.
				// So get this directory.
				int index = temp.LastIndexOfAny(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
				if (index < 0)
				{
					return false;
				}

				string parent = temp.Substring(0, index);
				if (!Directory.Exists(parent))
				{
					return false;
				}
			}
			catch
			{
				return false;
			}
			return true;
		}

		private void OnSourceTextChanged(object sender, EventArgs e)
		{

			//Debug.WriteLine("Text changed. Now it's:  " + cloneSourceComboBox.Text);
			//autoCompleteTimer.Start();
			PopulateAutocompleteList();

			//PopulateSourceList();

			bool sourceValid = ValidateSource();

			// warning: the source directory does not exist or is not a SCC directory.
			cloneSourceComboBox.BackColor = sourceValid ? Color.White : Color.LightYellow;

			// An empty source textbox is not considered a warning
			//if (cloneSourceTextBox.Text == String.Empty)
			//{
			//	backColor = Color.White;
			//}

			cloneButton.Enabled = sourceValid && ValidateDestination();
		}
		
		private void RefreshCloneSourceComboboxHistory()
		{
			string savetext = cloneSourceComboBox.Text;	// save the text because the call to Clear() will clear it.
			cloneSourceComboBox.Items.Clear();
			foreach (string cloneSource in m_cloneSourceHistory.GetList())
			{
				cloneSourceComboBox.Items.Add(cloneSource);
			}
			cloneSourceComboBox.Text = savetext;
		}

		private void OnSourceSelectedIndexChanged(object sender, EventArgs e)
		{
			//string curText = cloneSourceComboBox.Text;
			//int curIndex = cloneSourceComboBox.SelectedIndex;
			//string selectedItem = (string)cloneSourceComboBox.SelectedItem;

			if (cloneSourceComboBox.SelectedIndex > 0)	// This works as a guard because updating the combox box 
			{												// history will trigger another call to this function. We avoid an infinite loop.

				string oldText = cloneSourceComboBox.Text;

				cloneSourceComboBox.Items.Clear();
				cloneSourceComboBox.Items.Add(oldText);
				if (Directory.Exists(cloneSourceComboBox.Text))
				{
					string[] subdirs = Directory.GetDirectories(cloneSourceComboBox.Text);

					foreach (string subdir in subdirs)
					{
						if (m_model.IsFolderScc(subdir))
						{
							cloneSourceComboBox.Items.Add(subdir);
						}
					}
				}
				cloneSourceComboBox.Text = oldText;
			}

			bool sourceValid = ValidateSource();

			// warning: the source directory does not exist or is not a SCC directory.
			cloneSourceComboBox.BackColor = sourceValid ? Color.White : Color.LightYellow;

			// An empty source textbox is not considered a warning
			//if (cloneSourceTextBox.Text == String.Empty)
			//{
			//	backColor = Color.White;
			//}

			cloneButton.Enabled = sourceValid && ValidateDestination();
			return;
		}

		private void OnDestinationTextChanged(object sender, EventArgs e)
		{
			destValidateTimer.Stop();
			destValidateTimer.Interval = 300;
			destValidateTimer.Start();
		}

		private void DestinationValidateTimerHandler(object sender, System.EventArgs e)
		{
			destValidateTimer.Stop();

			bool destValid = ValidateDestination();

			// warning: the destination directory is not valid
			cloneDestinationTextBox.BackColor = destValid ? Color.White : Color.LightYellow;

			cloneButton.Enabled = destValid && ValidateSource();
		}

		private void OnClickedSourceBrowseButton(object sender, EventArgs e)
		{
			if (Directory.Exists(cloneSourceComboBox.Text))
			{
				cloneSourceFolderBrowserDialog.SelectedPath = cloneSourceComboBox.Text;
			}

			if (DialogResult.OK == cloneSourceFolderBrowserDialog.ShowDialog())
			{
				cloneSourceComboBox.Text = cloneSourceFolderBrowserDialog.SelectedPath;
			}

		}

		private void OnClickedDestinationBrowseButton(object sender, EventArgs e)
		{
			if (Directory.Exists(cloneDestinationTextBox.Text))
			{
				cloneDestinationFolderBrowserDialog.SelectedPath = cloneDestinationTextBox.Text;
			}

			if (DialogResult.OK == cloneDestinationFolderBrowserDialog.ShowDialog())
			{
				cloneDestinationTextBox.Text = cloneDestinationFolderBrowserDialog.SelectedPath;
			}
		}

		private void OnClickedCloneButton(object sender, EventArgs e)
		{
			m_controller.CloneRepository(
				cloneSourceComboBox.Text,
				cloneDestinationTextBox.Text,
				!cloneNoupdateCheckBox.Checked,
				cloneCompressedCheckBox.Checked);

			// Update controls now that the current destination is no longer a valid destination.
			// Parameters don't matter really since they are not used.
			OnDestinationTextChanged(cloneDestinationTextBox, new EventArgs());

			// Save the current clone source text in the history list
			m_cloneSourceHistory.Add(cloneSourceComboBox.Text);
			m_cloneSourceHistory.Save();
			RefreshCloneSourceComboboxHistory();
		}
	}


	public class CloneSourceHistory : RecentText
	{
		public CloneSourceHistory()
			: base("Zync", "CloneSourceHistory.xml", "CloneSourceHistory", "Path", 8)
		{ }
	}
}
