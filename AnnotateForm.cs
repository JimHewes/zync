﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Timers;

namespace Zync
{
	public partial class AnnotateForm : Form
	{
		Model m_model;
		List<AnnotateItem> m_annotateList;

		const int CiUser = 0;
		const int CiRevision = 1;
		const int CiChangesetId = 2;
		const int CiDate = 3;
		const int CiFilename = 4;
		const int CiLineNumber = 5;
		const int CiSource = 6;

		int m_userColumnWidth;
		int m_revisionColumnWidth;
		int m_changesetColumnWidth;
		int m_dateColumnWidth;
		int m_filenameColumnWidth;
		int m_linenumberColumnWidth;

		public AnnotateForm(Model model, List<AnnotateItem> annotateList, string filename)
		{
			InitializeComponent();

			m_model = model;
			m_annotateList = annotateList;

			m_userColumnWidth = Properties.Settings.Default.AnnotateUserColumnWidth;
			m_revisionColumnWidth = Properties.Settings.Default.AnnotateRevisionColumnWidth;
			m_changesetColumnWidth = Properties.Settings.Default.AnnotateChangesetColumnWidth;
			m_dateColumnWidth = Properties.Settings.Default.AnnotateDateColumnWidth;
			m_filenameColumnWidth = Properties.Settings.Default.AnnotateFilenameColumnWidth;
			m_linenumberColumnWidth = Properties.Settings.Default.AnnotateLinenumberColumnWidth;

			

			m_userCheckBox.Checked = Properties.Settings.Default.AnnotateShowUser;
			m_revisionCheckBox.Checked = Properties.Settings.Default.AnnotateShowRevision;
			m_changesetCheckBox.Checked = Properties.Settings.Default.AnnotateShowChangeset;
			m_dateCheckBox.Checked = Properties.Settings.Default.AnnotateShowDate;
			m_filenameCheckBox.Checked = Properties.Settings.Default.AnnotateShowFilename;
			m_lineNumerCheckBox.Checked = Properties.Settings.Default.AnnotateShowLineNumber;

			userColumnHeader.Width = m_userCheckBox.Checked ? m_userColumnWidth : 0;
			userColumnHeader.DisplayIndex = Properties.Settings.Default.AnnotateUserDisplayIndex;
			revisionColumnHeader.Width = m_revisionCheckBox.Checked ? m_revisionColumnWidth : 0;
			revisionColumnHeader.DisplayIndex = Properties.Settings.Default.AnnotateRevisionDisplayIndex;
			changesetColumnHeader.Width = m_changesetCheckBox.Checked ? m_changesetColumnWidth : 0;
			changesetColumnHeader.DisplayIndex = Properties.Settings.Default.AnnotateChangesetDisplayIndex;
			dateColumnHeader.Width = m_dateCheckBox.Checked ? m_dateColumnWidth : 0;
			dateColumnHeader.DisplayIndex = Properties.Settings.Default.AnnotateDateDisplayIndex;
			filenameColumnHeader.Width = m_filenameCheckBox.Checked ? m_filenameColumnWidth : 0;
			filenameColumnHeader.DisplayIndex = Properties.Settings.Default.AnnotateFilenameDisplayIndex;
			lineNumberColumnHeader.Width = m_lineNumerCheckBox.Checked ? m_linenumberColumnWidth : 0;
			lineNumberColumnHeader.DisplayIndex = Properties.Settings.Default.AnnotateLinenumberDisplayIndex;

			this.Font = new Font(FontFamily.GenericMonospace, 9.0f);
			int numColumns = m_annotateListView.Columns.Count;

			string[] record = new string[numColumns];
			foreach (AnnotateItem item in m_annotateList)
			{
				record[CiUser] = item.user;
				record[CiRevision] = item.revision;
				record[CiChangesetId] = item.changesetId;
				record[CiDate] = item.date.ToShortDateString() + " " + item.date.ToShortTimeString();
				record[CiFilename] = item.filename;
				record[CiLineNumber] = item.lineNumber;

				record[CiSource] = item.source.Replace("\t", "    ");
				record[CiSource] = record[CiSource].Replace("\r", "");

				// Create a new ListViewItem from the array of strings. Each string in the array is 
				// for a column, the text is shown in that column. This causes a number of SubItems 
				// to be created in the ListViewItem equal to the number of length of the array.
				m_annotateListView.Items.Add(new ListViewItem(record));
			}
			// By setting the column header width to -1, we're telling it to automatically resize to 
			// the widest text in the column. Note we need to set this every time after we add new items.
			// (Also, it we wanted the column to autosize to the text in the column header, we could set 
			//  the width to -2.)
			//m_annotateListView.Columns[CiUser].Width = -1;
			//m_annotateListView.Columns[CiRevision].Width = -1;
			//m_annotateListView.Columns[CiChangesetId].Width = -1;
			//m_annotateListView.Columns[CiDate].Width = -1;
			//m_annotateListView.Columns[CiLineNumber].Width = -1;

			Text = "Annotate " + filename;

			// Always set source column to fit all text.
			m_annotateListView.Columns[CiSource].Width = -1;

			// Make sure you set the size as the last thing because all of the 
			// ListView changes above can alter the size of the window.
			this.Size = Properties.Settings.Default.AnnotateFormSize;
			this.Location = Properties.Settings.Default.AnnotateFormLocation;
		}


		/** @brief This function gets called when the columns are reordered by the user. Column 
		*			order is saved to application settings.
		*
		* When this function is entered, the column header display indexes have not get been 
		* reordered. We only know the new index of the column that was changed. But we'd normally 
		* have to figure out the new display indices of all the other columns.
		* So rather than bother to calculate the new order ourselves, we just set a timer
		* and wait for the new order to get set. Then by the time the timer handler 
		* gets called, the new order has been established and we can easily save it 
		* to the application settings.
		* Alternatively, we could wait until the repository log form is closed to save settings, but I'd 
		* rather save settings as soon as changes are made in case something catatrophic happens 
		* in the program and the repository log form doesn't close properly. Then the changes are 
		* still preserved. 
		*/
		private void OnColumnReordered(object sender, ColumnReorderedEventArgs e)
		{
			// Don't allow the source column---which is index 6---to be reordered.
			// Keep it as the the rightmost column.
			if ((e.OldDisplayIndex == CiSource) || (e.NewDisplayIndex == CiSource))
			{
				e.Cancel = true;
			}
			else
			{
				System.Timers.Timer timer = new System.Timers.Timer();
				timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
				timer.Interval = 10;	// Set the Interval to 10 milliseconds. We want it to happen soon.
				timer.AutoReset = false;	// make it a one-shot timer
				timer.Enabled = true;
			}
		}

		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			// Save the column order
			Properties.Settings.Default.AnnotateRevisionDisplayIndex = revisionColumnHeader.DisplayIndex;
			Properties.Settings.Default.AnnotateChangesetDisplayIndex = changesetColumnHeader.DisplayIndex;
			Properties.Settings.Default.AnnotateUserDisplayIndex = userColumnHeader.DisplayIndex;
			Properties.Settings.Default.AnnotateDateDisplayIndex = dateColumnHeader.DisplayIndex;
			Properties.Settings.Default.AnnotateFilenameDisplayIndex = filenameColumnHeader.DisplayIndex;
			Properties.Settings.Default.AnnotateLinenumberDisplayIndex = lineNumberColumnHeader.DisplayIndex;

			Properties.Settings.Default.Save();
		}

		private void OnColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
		{
			switch (e.ColumnIndex)
			{
				case CiUser:
					// If the column width is > 0 and the corresponding checkbox is not checked, then 
					// the user adjusted the width. So we need to check the checkbox.
					if (userColumnHeader.Width > 0)
					{
						if (userColumnHeader.Width != m_userColumnWidth)
						{
							// User adjusted the column wodth and it is not zero width.
							// So save the new width.
							m_userColumnWidth = userColumnHeader.Width;
							if (!m_userCheckBox.Checked)
							{
								// This will trigger a call to OnCheckboxChanged, but since (userColumnHeader.Width == m_userColumnWidth)
								// nothing will happen.
								m_userCheckBox.Checked = true;
							}
							Properties.Settings.Default.AnnotateUserColumnWidth = userColumnHeader.Width;
							Properties.Settings.Default.Save();
						}
						else
						{
							// since the width is not really changed, we got here as a result of 
							// the checkbox being changed to ON. No need to do anything.
						}					
					}
					else
					{
						// Since the width is zero, either the used adjusted it to zero manually, or the checkbox 
						// was unchecked and the width set to zero by the OnCheckboxChanged function.
						if (m_userCheckBox.Checked)
						{
							// The width was manually adjusted to zero. We don't save a width of zero, we just 
							// uncheck the checkbox.
							// This will trigger a call to the OnCheckboxChanged function. But since the Width is 
							// already zero, nothing will happen
							m_userCheckBox.Checked = false;
						}
						else
						{
							// Since the check box is not checked, the width was set by the OnCheckboxChanged function.
							// So no need to do anything.
						}

					}
					break;
				case CiRevision:
					if (revisionColumnHeader.Width > 0)
					{
						if (revisionColumnHeader.Width != m_revisionColumnWidth)
						{
							m_revisionColumnWidth = revisionColumnHeader.Width;
							if (!m_revisionCheckBox.Checked)
							{
								m_revisionCheckBox.Checked = true;
							}
							Properties.Settings.Default.AnnotateRevisionColumnWidth = revisionColumnHeader.Width;
							Properties.Settings.Default.Save();
						}
					}
					else
					{
						if (m_revisionCheckBox.Checked)
						{
							m_revisionCheckBox.Checked = false;
						}
					}
					break;
				case CiChangesetId:
					if (changesetColumnHeader.Width > 0)
					{
						if (changesetColumnHeader.Width != m_changesetColumnWidth)
						{
							m_changesetColumnWidth = changesetColumnHeader.Width;
							if (!m_changesetCheckBox.Checked)
							{
								m_changesetCheckBox.Checked = true;
							}
							Properties.Settings.Default.AnnotateChangesetColumnWidth = changesetColumnHeader.Width;
							Properties.Settings.Default.Save();
						}
					}
					else
					{
						if (m_changesetCheckBox.Checked)
						{
							m_changesetCheckBox.Checked = false;
						}
					}
					break;

				case CiDate:
					if (dateColumnHeader.Width > 0)
					{
						if (dateColumnHeader.Width != m_dateColumnWidth)
						{
							m_dateColumnWidth = dateColumnHeader.Width;
							if (!m_dateCheckBox.Checked)
							{
								m_dateCheckBox.Checked = true;
							}
							Properties.Settings.Default.AnnotateDateColumnWidth = dateColumnHeader.Width;
							Properties.Settings.Default.Save();
						}
					}
					else
					{
						if (m_dateCheckBox.Checked)
						{
							m_dateCheckBox.Checked = false;
						}
					}
					break;
				case CiFilename:
					if (filenameColumnHeader.Width > 0)
					{
						if (filenameColumnHeader.Width != m_filenameColumnWidth)
						{
							m_filenameColumnWidth = filenameColumnHeader.Width;
							if (!m_filenameCheckBox.Checked)
							{
								m_filenameCheckBox.Checked = true;
							}
							Properties.Settings.Default.AnnotateFilenameColumnWidth = filenameColumnHeader.Width;
							Properties.Settings.Default.Save();
						}
					}
					else
					{
						if (m_filenameCheckBox.Checked)
						{
							m_filenameCheckBox.Checked = false;
						}
					}
					break;
				case CiLineNumber:
					if (lineNumberColumnHeader.Width > 0)
					{
						if (lineNumberColumnHeader.Width != m_linenumberColumnWidth)
						{
							m_linenumberColumnWidth = lineNumberColumnHeader.Width;
							if (!m_lineNumerCheckBox.Checked)
							{
								m_lineNumerCheckBox.Checked = true;
							}
							Properties.Settings.Default.AnnotateLinenumberColumnWidth = lineNumberColumnHeader.Width;
							Properties.Settings.Default.Save();
						}
					}
					else
					{
						if (m_lineNumerCheckBox.Checked)
						{
							m_lineNumerCheckBox.Checked = false;
						}
					}
					break;
			}
			Properties.Settings.Default.Save();
		}
	

		// Note that it's necessary to take control of loading and saving the window location to the settings 
		// and not let it be done automatically by the Designer. If you let the Designer do it, then it causes 
		// trouble in the case or multiple log forms open. When you move one form, it immediately saves the 
		// location to the settings, which causes an event to all other Log forms and they all reload the new location 
		// from the settings. So what you end up with is that all open log forms have their upper-left corner stuck 
		// together so that you can't separate the windows.
		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.WindowState == FormWindowState.Normal)
			{
				Properties.Settings.Default.AnnotateFormSize = this.Size;
				Properties.Settings.Default.AnnotateFormLocation = this.Location;
			}
			else
			{
				Properties.Settings.Default.AnnotateFormSize = this.RestoreBounds.Size;
				Properties.Settings.Default.AnnotateFormLocation = this.RestoreBounds.Location;
			}


			Properties.Settings.Default.Save();
		}

		/* @brief Called when one of the checkboxes changes state.

		If the width of a column is changed as a result of this function, then the OnColumnWidthChanged 
		function will also get called.

		*/
		private void OnCheckboxChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb == m_userCheckBox)
			{
				if (!cb.Checked)
				{
					if (userColumnHeader.Width != 0)
					{
						// Checkbox has been unchecked so set with to zero. This will trigger a call to 
						// OnColumnWidthChanged, but since the checkbox is not checked nothing will happen.
						userColumnHeader.Width = 0;
					}
					else
					{	// If width is already zero, it means that the width was set to zero manually.
						// No need to do anything.
					}
				}
				else // Checkbox is checked
				{
					if (userColumnHeader.Width == 0)
					{
						// The checkbox changed to be checked. So set the header to the saved width.
						userColumnHeader.Width = m_userColumnWidth;
					}
				}
				// Save new state of the checkbox.
				Properties.Settings.Default.AnnotateShowUser = cb.Checked;
			}
			if (cb == m_revisionCheckBox)
			{
				if (!cb.Checked)
				{
					if (revisionColumnHeader.Width != 0)
					{
						revisionColumnHeader.Width = 0;
					}
				}
				else // Checkbox is checked
				{
					if (revisionColumnHeader.Width == 0)
					{
						revisionColumnHeader.Width = m_revisionColumnWidth;
					}
				}
				// Save new state of the checkbox.
				Properties.Settings.Default.AnnotateShowRevision = cb.Checked;
			}
			if (cb == m_changesetCheckBox)
			{
				if (!cb.Checked)
				{
					if (changesetColumnHeader.Width != 0)
					{
						changesetColumnHeader.Width = 0;
					}
				}
				else // Checkbox is checked
				{
					if (changesetColumnHeader.Width == 0)
					{
						changesetColumnHeader.Width = m_changesetColumnWidth;
					}
				}
				// Save new state of the checkbox.
				Properties.Settings.Default.AnnotateShowChangeset= cb.Checked;
			}
			if (cb == m_dateCheckBox)
			{
				if (!cb.Checked)
				{
					if (dateColumnHeader.Width != 0)
					{
						dateColumnHeader.Width = 0;
					}
				}
				else // Checkbox is checked
				{
					if (dateColumnHeader.Width == 0)
					{
						dateColumnHeader.Width = m_dateColumnWidth;
					}
				}
				// Save new state of the checkbox.
				Properties.Settings.Default.AnnotateShowDate = cb.Checked;
			}
			if (cb == m_filenameCheckBox)
			{
				if (!cb.Checked)
				{
					if (filenameColumnHeader.Width != 0)
					{
						filenameColumnHeader.Width = 0;
					}
				}
				else // Checkbox is checked
				{
					if (filenameColumnHeader.Width == 0)
					{
						filenameColumnHeader.Width = m_filenameColumnWidth;
					}
				}
				// Save new state of the checkbox.
				Properties.Settings.Default.AnnotateShowFilename = cb.Checked;
			}
			if (cb == m_lineNumerCheckBox)
			{
				if (!cb.Checked)
				{
					if (lineNumberColumnHeader.Width != 0)
					{
						lineNumberColumnHeader.Width = 0;
					}
				}
				else // Checkbox is checked
				{
					if (lineNumberColumnHeader.Width == 0)
					{
						lineNumberColumnHeader.Width = m_linenumberColumnWidth;
					}
				}
				// Save new state of the checkbox.
				Properties.Settings.Default.AnnotateShowLineNumber = cb.Checked;
			}


			Properties.Settings.Default.Save();
		}


	}
}
