﻿namespace Zync
{
	partial class FileLogForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileLogForm));
			this.FileLogSplitContainer = new System.Windows.Forms.SplitContainer();
			this.m_fileLogListView = new System.Windows.Forms.ListView();
			this.revisionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.changesetIdColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.messageColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.userColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.dateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tagsColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.m_TextBox = new System.Windows.Forms.RichTextBox();
			this.FileLogSplitContainer.Panel1.SuspendLayout();
			this.FileLogSplitContainer.Panel2.SuspendLayout();
			this.FileLogSplitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// FileLogSplitContainer
			// 
			this.FileLogSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FileLogSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.FileLogSplitContainer.Name = "FileLogSplitContainer";
			this.FileLogSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// FileLogSplitContainer.Panel1
			// 
			this.FileLogSplitContainer.Panel1.Controls.Add(this.m_fileLogListView);
			// 
			// FileLogSplitContainer.Panel2
			// 
			this.FileLogSplitContainer.Panel2.Controls.Add(this.m_TextBox);
			this.FileLogSplitContainer.Size = new System.Drawing.Size(667, 488);
			this.FileLogSplitContainer.SplitterDistance = 282;
			this.FileLogSplitContainer.TabIndex = 0;
			// 
			// m_fileLogListView
			// 
			this.m_fileLogListView.AllowColumnReorder = true;
			this.m_fileLogListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.revisionColumnHeader,
            this.changesetIdColumnHeader,
            this.messageColumnHeader,
            this.userColumnHeader,
            this.dateColumnHeader,
            this.tagsColumnHeader});
			this.m_fileLogListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_fileLogListView.FullRowSelect = true;
			this.m_fileLogListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.m_fileLogListView.HideSelection = false;
			this.m_fileLogListView.Location = new System.Drawing.Point(0, 0);
			this.m_fileLogListView.Name = "m_fileLogListView";
			this.m_fileLogListView.Size = new System.Drawing.Size(667, 282);
			this.m_fileLogListView.TabIndex = 0;
			this.m_fileLogListView.UseCompatibleStateImageBehavior = false;
			this.m_fileLogListView.View = System.Windows.Forms.View.Details;
			this.m_fileLogListView.ColumnReordered += new System.Windows.Forms.ColumnReorderedEventHandler(this.OnColumnReordered);
			this.m_fileLogListView.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.OnColumnWidthChanged);
			this.m_fileLogListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.OnListViewSelectionChanged);
			this.m_fileLogListView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnMouseUp);
			// 
			// revisionColumnHeader
			// 
			this.revisionColumnHeader.Text = "Rev";
			this.revisionColumnHeader.Width = 46;
			// 
			// changesetIdColumnHeader
			// 
			this.changesetIdColumnHeader.Text = "ID";
			this.changesetIdColumnHeader.Width = 75;
			// 
			// messageColumnHeader
			// 
			this.messageColumnHeader.Text = "Summary";
			this.messageColumnHeader.Width = 268;
			// 
			// userColumnHeader
			// 
			this.userColumnHeader.Text = "User";
			// 
			// dateColumnHeader
			// 
			this.dateColumnHeader.Text = "Date";
			// 
			// tagsColumnHeader
			// 
			this.tagsColumnHeader.Text = "Tags";
			this.tagsColumnHeader.Width = 124;
			// 
			// m_TextBox
			// 
			this.m_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_TextBox.Location = new System.Drawing.Point(0, 0);
			this.m_TextBox.Name = "m_TextBox";
			this.m_TextBox.ReadOnly = true;
			this.m_TextBox.Size = new System.Drawing.Size(667, 202);
			this.m_TextBox.TabIndex = 0;
			this.m_TextBox.Text = "";
			// 
			// FileLogForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(667, 488);
			this.Controls.Add(this.FileLogSplitContainer);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FileLogForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "File Log";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnFormClosed);
			this.SizeChanged += new System.EventHandler(this.OnSizeChanged);
			this.FileLogSplitContainer.Panel1.ResumeLayout(false);
			this.FileLogSplitContainer.Panel2.ResumeLayout(false);
			this.FileLogSplitContainer.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer FileLogSplitContainer;
		private System.Windows.Forms.ListView m_fileLogListView;
		private System.Windows.Forms.ColumnHeader revisionColumnHeader;
		private System.Windows.Forms.ColumnHeader messageColumnHeader;
		private System.Windows.Forms.ColumnHeader changesetIdColumnHeader;
		private System.Windows.Forms.ColumnHeader userColumnHeader;
		private System.Windows.Forms.ColumnHeader dateColumnHeader;
		private System.Windows.Forms.ColumnHeader tagsColumnHeader;
		private System.Windows.Forms.RichTextBox m_TextBox;
	}
}