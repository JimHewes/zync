﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zync
{
	public abstract class Aspect
	{
	}

	public interface Observer
	{
		void Update(Subject theChangedSubject, Aspect interest);
	}
}
