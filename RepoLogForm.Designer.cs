﻿namespace Zync
{
	partial class RepoLogForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepoLogForm));
			this.RepoLogSplitContainer = new System.Windows.Forms.SplitContainer();
			this.m_repoLogListView = new System.Windows.Forms.ListView();
			this.revisionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.changesetIdColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.messageColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.userColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.dateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tagsColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.branchColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.RepoLogBottomSplitContainer = new System.Windows.Forms.SplitContainer();
			this.m_TextBox = new System.Windows.Forms.RichTextBox();
			this.RepoLogBottomRightSplitContainer = new System.Windows.Forms.SplitContainer();
			this.m_repoLogFileListView = new System.Windows.Forms.ListView();
			this.filenameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.m_repoLogTagListView = new System.Windows.Forms.ListView();
			this.tagnameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.RepoLogSplitContainer.Panel1.SuspendLayout();
			this.RepoLogSplitContainer.Panel2.SuspendLayout();
			this.RepoLogSplitContainer.SuspendLayout();
			this.RepoLogBottomSplitContainer.Panel1.SuspendLayout();
			this.RepoLogBottomSplitContainer.Panel2.SuspendLayout();
			this.RepoLogBottomSplitContainer.SuspendLayout();
			this.RepoLogBottomRightSplitContainer.Panel1.SuspendLayout();
			this.RepoLogBottomRightSplitContainer.Panel2.SuspendLayout();
			this.RepoLogBottomRightSplitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// RepoLogSplitContainer
			// 
			this.RepoLogSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RepoLogSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.RepoLogSplitContainer.Name = "RepoLogSplitContainer";
			this.RepoLogSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// RepoLogSplitContainer.Panel1
			// 
			this.RepoLogSplitContainer.Panel1.Controls.Add(this.m_repoLogListView);
			// 
			// RepoLogSplitContainer.Panel2
			// 
			this.RepoLogSplitContainer.Panel2.Controls.Add(this.RepoLogBottomSplitContainer);
			this.RepoLogSplitContainer.Size = new System.Drawing.Size(811, 460);
			this.RepoLogSplitContainer.SplitterDistance = 273;
			this.RepoLogSplitContainer.TabIndex = 0;
			// 
			// m_repoLogListView
			// 
			this.m_repoLogListView.AllowColumnReorder = true;
			this.m_repoLogListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.revisionColumnHeader,
            this.changesetIdColumnHeader,
            this.messageColumnHeader,
            this.userColumnHeader,
            this.dateColumnHeader,
            this.tagsColumnHeader,
            this.branchColumnHeader});
			this.m_repoLogListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_repoLogListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_repoLogListView.FullRowSelect = true;
			this.m_repoLogListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.m_repoLogListView.HideSelection = false;
			this.m_repoLogListView.Location = new System.Drawing.Point(0, 0);
			this.m_repoLogListView.Name = "m_repoLogListView";
			this.m_repoLogListView.Size = new System.Drawing.Size(811, 273);
			this.m_repoLogListView.TabIndex = 0;
			this.m_repoLogListView.UseCompatibleStateImageBehavior = false;
			this.m_repoLogListView.View = System.Windows.Forms.View.Details;
			this.m_repoLogListView.ColumnReordered += new System.Windows.Forms.ColumnReorderedEventHandler(this.OnColumnReordered);
			this.m_repoLogListView.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.OnColumnWidthChanged);
			this.m_repoLogListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.OnLogItemSelectionChanged);
			this.m_repoLogListView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnLogItemMouseUp);
			// 
			// revisionColumnHeader
			// 
			this.revisionColumnHeader.Text = "Rev";
			this.revisionColumnHeader.Width = 46;
			// 
			// changesetIdColumnHeader
			// 
			this.changesetIdColumnHeader.Text = "Changeset";
			this.changesetIdColumnHeader.Width = 133;
			// 
			// messageColumnHeader
			// 
			this.messageColumnHeader.Text = "Summary";
			this.messageColumnHeader.Width = 360;
			// 
			// userColumnHeader
			// 
			this.userColumnHeader.Text = "User";
			this.userColumnHeader.Width = 108;
			// 
			// dateColumnHeader
			// 
			this.dateColumnHeader.Text = "Date";
			this.dateColumnHeader.Width = 117;
			// 
			// tagsColumnHeader
			// 
			this.tagsColumnHeader.Text = "Tags";
			this.tagsColumnHeader.Width = 150;
			// 
			// branchColumnHeader
			// 
			this.branchColumnHeader.Text = "Branch";
			this.branchColumnHeader.Width = 100;
			// 
			// RepoLogBottomSplitContainer
			// 
			this.RepoLogBottomSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RepoLogBottomSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.RepoLogBottomSplitContainer.Name = "RepoLogBottomSplitContainer";
			// 
			// RepoLogBottomSplitContainer.Panel1
			// 
			this.RepoLogBottomSplitContainer.Panel1.Controls.Add(this.m_TextBox);
			// 
			// RepoLogBottomSplitContainer.Panel2
			// 
			this.RepoLogBottomSplitContainer.Panel2.Controls.Add(this.RepoLogBottomRightSplitContainer);
			this.RepoLogBottomSplitContainer.Size = new System.Drawing.Size(811, 183);
			this.RepoLogBottomSplitContainer.SplitterDistance = 425;
			this.RepoLogBottomSplitContainer.TabIndex = 0;
			// 
			// m_TextBox
			// 
			this.m_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_TextBox.Location = new System.Drawing.Point(0, 0);
			this.m_TextBox.Name = "m_TextBox";
			this.m_TextBox.ReadOnly = true;
			this.m_TextBox.Size = new System.Drawing.Size(425, 183);
			this.m_TextBox.TabIndex = 0;
			this.m_TextBox.Text = "";
			// 
			// RepoLogBottomRightSplitContainer
			// 
			this.RepoLogBottomRightSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RepoLogBottomRightSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.RepoLogBottomRightSplitContainer.Name = "RepoLogBottomRightSplitContainer";
			// 
			// RepoLogBottomRightSplitContainer.Panel1
			// 
			this.RepoLogBottomRightSplitContainer.Panel1.Controls.Add(this.m_repoLogFileListView);
			// 
			// RepoLogBottomRightSplitContainer.Panel2
			// 
			this.RepoLogBottomRightSplitContainer.Panel2.Controls.Add(this.m_repoLogTagListView);
			this.RepoLogBottomRightSplitContainer.Size = new System.Drawing.Size(382, 183);
			this.RepoLogBottomRightSplitContainer.SplitterDistance = 199;
			this.RepoLogBottomRightSplitContainer.TabIndex = 1;
			// 
			// m_repoLogFileListView
			// 
			this.m_repoLogFileListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.filenameColumnHeader});
			this.m_repoLogFileListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_repoLogFileListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.m_repoLogFileListView.HideSelection = false;
			this.m_repoLogFileListView.LabelWrap = false;
			this.m_repoLogFileListView.Location = new System.Drawing.Point(0, 0);
			this.m_repoLogFileListView.MultiSelect = false;
			this.m_repoLogFileListView.Name = "m_repoLogFileListView";
			this.m_repoLogFileListView.Size = new System.Drawing.Size(199, 183);
			this.m_repoLogFileListView.TabIndex = 0;
			this.m_repoLogFileListView.UseCompatibleStateImageBehavior = false;
			this.m_repoLogFileListView.View = System.Windows.Forms.View.Details;
			this.m_repoLogFileListView.SelectedIndexChanged += new System.EventHandler(this.OnRepoLogFileListSelectionChanged);
			this.m_repoLogFileListView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnRepoLogFileListMouseUp);
			// 
			// filenameColumnHeader
			// 
			this.filenameColumnHeader.Width = 379;
			// 
			// m_repoLogTagListView
			// 
			this.m_repoLogTagListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tagnameColumnHeader});
			this.m_repoLogTagListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_repoLogTagListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.m_repoLogTagListView.HideSelection = false;
			this.m_repoLogTagListView.LabelWrap = false;
			this.m_repoLogTagListView.Location = new System.Drawing.Point(0, 0);
			this.m_repoLogTagListView.MultiSelect = false;
			this.m_repoLogTagListView.Name = "m_repoLogTagListView";
			this.m_repoLogTagListView.Size = new System.Drawing.Size(179, 183);
			this.m_repoLogTagListView.TabIndex = 0;
			this.m_repoLogTagListView.UseCompatibleStateImageBehavior = false;
			this.m_repoLogTagListView.View = System.Windows.Forms.View.Details;
			// 
			// RepoLogForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(811, 460);
			this.Controls.Add(this.RepoLogSplitContainer);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "RepoLogForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Repository Log";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnFormClosed);
			this.SizeChanged += new System.EventHandler(this.OnSizeChanged);
			this.RepoLogSplitContainer.Panel1.ResumeLayout(false);
			this.RepoLogSplitContainer.Panel2.ResumeLayout(false);
			this.RepoLogSplitContainer.ResumeLayout(false);
			this.RepoLogBottomSplitContainer.Panel1.ResumeLayout(false);
			this.RepoLogBottomSplitContainer.Panel2.ResumeLayout(false);
			this.RepoLogBottomSplitContainer.ResumeLayout(false);
			this.RepoLogBottomRightSplitContainer.Panel1.ResumeLayout(false);
			this.RepoLogBottomRightSplitContainer.Panel2.ResumeLayout(false);
			this.RepoLogBottomRightSplitContainer.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer RepoLogSplitContainer;
		private System.Windows.Forms.SplitContainer RepoLogBottomSplitContainer;
		private System.Windows.Forms.ListView m_repoLogListView;
		private System.Windows.Forms.ColumnHeader revisionColumnHeader;
		private System.Windows.Forms.ColumnHeader changesetIdColumnHeader;
		private System.Windows.Forms.ColumnHeader messageColumnHeader;
		private System.Windows.Forms.ColumnHeader userColumnHeader;
		private System.Windows.Forms.ColumnHeader dateColumnHeader;
		private System.Windows.Forms.ColumnHeader tagsColumnHeader;
		private System.Windows.Forms.RichTextBox m_TextBox;
		private System.Windows.Forms.ListView m_repoLogFileListView;
		private System.Windows.Forms.ColumnHeader filenameColumnHeader;
		private System.Windows.Forms.ColumnHeader branchColumnHeader;
		private System.Windows.Forms.SplitContainer RepoLogBottomRightSplitContainer;
		private System.Windows.Forms.ListView m_repoLogTagListView;
		private System.Windows.Forms.ColumnHeader tagnameColumnHeader;
	}
}