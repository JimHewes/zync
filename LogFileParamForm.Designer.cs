﻿namespace Zync
{
	partial class LogFileParamForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.Label ToDateLabel;
			System.Windows.Forms.Label FromDateLabel;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogFileParamForm));
			this.FollowCheckbox = new System.Windows.Forms.CheckBox();
			this.FromDatePicker = new System.Windows.Forms.DateTimePicker();
			this.ByDateCheckBox = new System.Windows.Forms.CheckBox();
			this.ToDatePicker = new System.Windows.Forms.DateTimePicker();
			this.ByRevisionCheckBox = new System.Windows.Forms.CheckBox();
			this.FromRevisionTextBox = new System.Windows.Forms.TextBox();
			this.ToRevisionTextBox = new System.Windows.Forms.TextBox();
			this.logFileParamShiftKeyCheckBox = new System.Windows.Forms.CheckBox();
			this.LogFileParamCancelButton = new System.Windows.Forms.Button();
			this.LogFileParamOkButton = new System.Windows.Forms.Button();
			ToDateLabel = new System.Windows.Forms.Label();
			FromDateLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// ToDateLabel
			// 
			ToDateLabel.AutoSize = true;
			ToDateLabel.Location = new System.Drawing.Point(12, 79);
			ToDateLabel.Name = "ToDateLabel";
			ToDateLabel.Size = new System.Drawing.Size(23, 13);
			ToDateLabel.TabIndex = 3;
			ToDateLabel.Text = "To:";
			// 
			// FromDateLabel
			// 
			FromDateLabel.AutoSize = true;
			FromDateLabel.Location = new System.Drawing.Point(12, 45);
			FromDateLabel.Name = "FromDateLabel";
			FromDateLabel.Size = new System.Drawing.Size(33, 13);
			FromDateLabel.TabIndex = 3;
			FromDateLabel.Text = "From:";
			// 
			// FollowCheckbox
			// 
			this.FollowCheckbox.AutoSize = true;
			this.FollowCheckbox.Location = new System.Drawing.Point(14, 120);
			this.FollowCheckbox.Name = "FollowCheckbox";
			this.FollowCheckbox.Size = new System.Drawing.Size(188, 17);
			this.FollowCheckbox.TabIndex = 0;
			this.FollowCheckbox.Text = "Follow across copies and renames";
			this.FollowCheckbox.UseVisualStyleBackColor = true;
			// 
			// FromDatePicker
			// 
			this.FromDatePicker.Checked = false;
			this.FromDatePicker.CustomFormat = "dd-MMM-yyyy h:mm tt";
			this.FromDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FromDatePicker.Location = new System.Drawing.Point(51, 41);
			this.FromDatePicker.Name = "FromDatePicker";
			this.FromDatePicker.Size = new System.Drawing.Size(143, 20);
			this.FromDatePicker.TabIndex = 1;
			this.FromDatePicker.ValueChanged += new System.EventHandler(this.OnDateChanged);
			// 
			// ByDateCheckBox
			// 
			this.ByDateCheckBox.AutoSize = true;
			this.ByDateCheckBox.Location = new System.Drawing.Point(14, 18);
			this.ByDateCheckBox.Name = "ByDateCheckBox";
			this.ByDateCheckBox.Size = new System.Drawing.Size(67, 17);
			this.ByDateCheckBox.TabIndex = 2;
			this.ByDateCheckBox.Text = "By Date:";
			this.ByDateCheckBox.UseVisualStyleBackColor = true;
			this.ByDateCheckBox.Click += new System.EventHandler(this.OnClickedByDateCheckbox);
			// 
			// ToDatePicker
			// 
			this.ToDatePicker.CustomFormat = "dd-MMM-yyyy h:mm tt";
			this.ToDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.ToDatePicker.Location = new System.Drawing.Point(51, 76);
			this.ToDatePicker.Name = "ToDatePicker";
			this.ToDatePicker.Size = new System.Drawing.Size(143, 20);
			this.ToDatePicker.TabIndex = 1;
			this.ToDatePicker.ValueChanged += new System.EventHandler(this.OnDateChanged);
			// 
			// ByRevisionCheckBox
			// 
			this.ByRevisionCheckBox.AutoSize = true;
			this.ByRevisionCheckBox.Location = new System.Drawing.Point(232, 18);
			this.ByRevisionCheckBox.Name = "ByRevisionCheckBox";
			this.ByRevisionCheckBox.Size = new System.Drawing.Size(85, 17);
			this.ByRevisionCheckBox.TabIndex = 2;
			this.ByRevisionCheckBox.Text = "By Revision:";
			this.ByRevisionCheckBox.UseVisualStyleBackColor = true;
			this.ByRevisionCheckBox.Click += new System.EventHandler(this.OnClickedByRevisionCheckbox);
			// 
			// FromRevisionTextBox
			// 
			this.FromRevisionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.FromRevisionTextBox.Location = new System.Drawing.Point(232, 41);
			this.FromRevisionTextBox.Name = "FromRevisionTextBox";
			this.FromRevisionTextBox.Size = new System.Drawing.Size(135, 20);
			this.FromRevisionTextBox.TabIndex = 4;
			// 
			// ToRevisionTextBox
			// 
			this.ToRevisionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.ToRevisionTextBox.Location = new System.Drawing.Point(232, 76);
			this.ToRevisionTextBox.Name = "ToRevisionTextBox";
			this.ToRevisionTextBox.Size = new System.Drawing.Size(135, 20);
			this.ToRevisionTextBox.TabIndex = 4;
			// 
			// logFileParamShiftKeyCheckBox
			// 
			this.logFileParamShiftKeyCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.logFileParamShiftKeyCheckBox.AutoSize = true;
			this.logFileParamShiftKeyCheckBox.Location = new System.Drawing.Point(15, 173);
			this.logFileParamShiftKeyCheckBox.Name = "logFileParamShiftKeyCheckBox";
			this.logFileParamShiftKeyCheckBox.Size = new System.Drawing.Size(173, 17);
			this.logFileParamShiftKeyCheckBox.TabIndex = 5;
			this.logFileParamShiftKeyCheckBox.Text = "Display only if Shift key is down";
			this.logFileParamShiftKeyCheckBox.UseVisualStyleBackColor = true;
			this.logFileParamShiftKeyCheckBox.CheckedChanged += new System.EventHandler(this.OnShiftCheckboxChanged);
			// 
			// LogFileParamCancelButton
			// 
			this.LogFileParamCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.LogFileParamCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.LogFileParamCancelButton.Location = new System.Drawing.Point(312, 169);
			this.LogFileParamCancelButton.Name = "LogFileParamCancelButton";
			this.LogFileParamCancelButton.Size = new System.Drawing.Size(75, 23);
			this.LogFileParamCancelButton.TabIndex = 7;
			this.LogFileParamCancelButton.Text = "Cancel";
			this.LogFileParamCancelButton.UseVisualStyleBackColor = true;
			// 
			// LogFileParamOkButton
			// 
			this.LogFileParamOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.LogFileParamOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.LogFileParamOkButton.Location = new System.Drawing.Point(223, 169);
			this.LogFileParamOkButton.Name = "LogFileParamOkButton";
			this.LogFileParamOkButton.Size = new System.Drawing.Size(75, 23);
			this.LogFileParamOkButton.TabIndex = 6;
			this.LogFileParamOkButton.Text = "Show Log";
			this.LogFileParamOkButton.UseVisualStyleBackColor = true;
			this.LogFileParamOkButton.Click += new System.EventHandler(this.OnClickedOkButton);
			// 
			// LogFileParamForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(399, 204);
			this.Controls.Add(this.LogFileParamCancelButton);
			this.Controls.Add(this.LogFileParamOkButton);
			this.Controls.Add(this.logFileParamShiftKeyCheckBox);
			this.Controls.Add(this.ToRevisionTextBox);
			this.Controls.Add(this.FromRevisionTextBox);
			this.Controls.Add(FromDateLabel);
			this.Controls.Add(ToDateLabel);
			this.Controls.Add(this.ToDatePicker);
			this.Controls.Add(this.ByRevisionCheckBox);
			this.Controls.Add(this.ByDateCheckBox);
			this.Controls.Add(this.FromDatePicker);
			this.Controls.Add(this.FollowCheckbox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(407, 238);
			this.Name = "LogFileParamForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Log File";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox FollowCheckbox;
		private System.Windows.Forms.DateTimePicker FromDatePicker;
		private System.Windows.Forms.CheckBox ByDateCheckBox;
		private System.Windows.Forms.DateTimePicker ToDatePicker;
		private System.Windows.Forms.CheckBox ByRevisionCheckBox;
		private System.Windows.Forms.TextBox FromRevisionTextBox;
		private System.Windows.Forms.TextBox ToRevisionTextBox;
		private System.Windows.Forms.CheckBox logFileParamShiftKeyCheckBox;
		private System.Windows.Forms.Button LogFileParamCancelButton;
		private System.Windows.Forms.Button LogFileParamOkButton;
	}
}