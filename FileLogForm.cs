﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Timers;

namespace Zync
{
	public partial class FileLogForm : Form
	{
		Model m_model;
		Controller m_controller;

		LogParams m_logParams;
		
		public const int CiRevision = 0;
		public const int CiChangesetId = 1;
		public const int CiMessage = 2;
		public const int CiUser = 3;
		public const int CiDate = 4;
		public const int CiTags = 5;

		Font m_originalFont = null;

		public FileLogForm(Model model, Controller controller, LogParams logFileParams)
		{
			InitializeComponent();

			m_model = model;
			m_controller = controller;
			//m_filename = filename;
			m_logParams = logFileParams;
			Text = "Log " + m_logParams.filename;

			revisionColumnHeader.Width = Properties.Settings.Default.FileLogRevisionColumnWidth;
			revisionColumnHeader.DisplayIndex = Properties.Settings.Default.FileLogRevisionColumnDisplayIndex;
			changesetIdColumnHeader.Width = Properties.Settings.Default.FileLogChangesetIdColumnWidth;
			changesetIdColumnHeader.DisplayIndex = Properties.Settings.Default.FileLogChangesetIdColumnDisplayIndex;
			messageColumnHeader.Width = Properties.Settings.Default.FileLogMessageColumnWidth;
			messageColumnHeader.DisplayIndex = Properties.Settings.Default.FileLogMessageColumnDisplayIndex;
			userColumnHeader.Width = Properties.Settings.Default.FileLogUserColumnWidth;
			userColumnHeader.DisplayIndex = Properties.Settings.Default.FileLogUserColumnDisplayIndex;
			dateColumnHeader.Width = Properties.Settings.Default.FileLogDateColumnWidth;
			dateColumnHeader.DisplayIndex = Properties.Settings.Default.FileLogDateColumnDisplayIndex;
			tagsColumnHeader.Width = Properties.Settings.Default.FileLogTagsColumnWidth;
			tagsColumnHeader.DisplayIndex = Properties.Settings.Default.FileLogTagsColumnDisplayIndex;

			FileLogSplitContainer.SplitterDistance = Properties.Settings.Default.FileLogSplitterDistance;
			this.Location = Properties.Settings.Default.FileLogFormLocation;
			this.Size = Properties.Settings.Default.FileLogFormSize;


			m_logParams.verbose = true;

			List<FileLogItem> list = model.GetLog(m_logParams);
			int numColumns = m_fileLogListView.Columns.Count;

			string[] record = new string[numColumns];

			foreach (FileLogItem item in list)
			{
				record[CiRevision] = item.revision.ToString();
				record[CiChangesetId] = item.changesetId;
				record[CiMessage] = item.message.Contains("\n") ? item.message.Split('\n')[0] : item.message;
				record[CiUser] = item.user;
				record[CiDate] = item.date.ToShortDateString() + " " + item.date.ToShortTimeString();
				record[CiTags] = "";
				foreach (string tag in item.tags)
				{
					record[CiTags] += tag + " ";
				}

				// Create a new ListViewItem from the array of strings. Each string in the array is 
				// for a column, the text is shown in that column. This causes a number of SubItems 
				// to be created in the ListViewItem equal to the number of length of the array.
				m_fileLogListView.Items.Insert(0,new ListViewItem(record));
			}

			m_originalFont = m_TextBox.SelectionFont;

			// In the designer we set the ReadOnly property to true. But that also makes the 
			// background color automatically set to gray. We want it to stay as white, so reset 
			// it back to white here.
			m_TextBox.BackColor = Color.White;

			//m_fileLogListView.AutoResizeColumn(CiRevision, ColumnHeaderAutoResizeStyle.HeaderSize);
			//m_fileLogListView.AutoResizeColumn(CiChangesetId, ColumnHeaderAutoResizeStyle.ColumnContent);
			//m_fileLogListView.AutoResizeColumn(CiMessage, ColumnHeaderAutoResizeStyle.None);
			//m_fileLogListView.AutoResizeColumn(CiUser, ColumnHeaderAutoResizeStyle.ColumnContent);
			//m_fileLogListView.AutoResizeColumn(CiDate, ColumnHeaderAutoResizeStyle.ColumnContent);


		}
		
		// Note that it's necessary to take control of loading and saving the window location to the settings 
		// and not let it be done automatically by the Designer. If you let the Designer do it, then it causes 
		// trouble in the case or multiple log forms open. When you move one form, it immediately saves the 
		// location to the settings, which causes an event to all other Log forms and they all reload the new location 
		// from the settings. So what you end up with is that all open log forms have thei upper-left corner stuck 
		// together so that you can't separate the windows.
		private void OnFormClosed(object sender, FormClosedEventArgs e)
		{
			if (this.WindowState == FormWindowState.Normal)
			{
				Properties.Settings.Default.FileLogFormSize = this.Size;
				Properties.Settings.Default.FileLogFormLocation = this.Location;
			}
			else
			{
				Properties.Settings.Default.FileLogFormSize = this.RestoreBounds.Size;
				Properties.Settings.Default.FileLogFormLocation = this.RestoreBounds.Location;
			}


			Properties.Settings.Default.Save();
		}

		private void OnListViewSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (m_fileLogListView.SelectedItems.Count == 1)
			{
				LogParams logParams = new LogParams();	// new initializes unused fields to default values
				
				// If were to specify the filename then we'd hit a Mercurial quirk.
				// If the file was renamed and we want to log a revision before the rename, then we'd have to 
				// use the old filename at the time of that revision otherwise Mercurial won't give us anything. 
				// But all we really need here  is the log message which we can get by logging the repository 
				// (without the filename). And that doesn't care about renames. So it works better for us.
				logParams.filename = "";   //m_logFileParams.filename;
				logParams.logByRevision = true;
				logParams.startRevision = m_fileLogListView.SelectedItems[0].SubItems[CiRevision].Text;
				logParams.endRevision = logParams.startRevision;
				logParams.verbose = true;
				logParams.follow = true;
				logParams.repoPath = m_logParams.repoPath;

				m_TextBox.Clear();
				m_TextBox.WordWrap = true;
				m_TextBox.SelectionFont = m_originalFont;

				List<FileLogItem> list = m_model.GetLog(logParams);
				if (list.Count == 1)
				{
					m_TextBox.SelectedText = list[0].message;
				}
			}
			else if (m_fileLogListView.SelectedItems.Count == 2)
			{
				DiffParams diffParams;
				diffParams.filename = m_logParams.filename;
				diffParams.revision1 = m_fileLogListView.SelectedItems[1].SubItems[CiRevision].Text;
				diffParams.revision2 = m_fileLogListView.SelectedItems[0].SubItems[CiRevision].Text;
				diffParams.repoPath = m_logParams.repoPath;
				diffParams.extCommand = "";	// don't use external diff here

				m_TextBox.Clear();
				m_TextBox.WordWrap = false;

				string[] lines = m_model.GetDiff(diffParams).Split('\n');
				m_TextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

				foreach (string line in lines)
				{
					m_TextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

					if (line.Length > 0)
					{
						if (line.Substring(0, 1) == "+")
						{
							m_TextBox.SelectionColor = Color.Green;
						}
						else if (line.Substring(0, 1) == "-")
						{
							m_TextBox.SelectionColor = Color.Red;
						}
						else if (line.Substring(0, 1) == "@")
						{
							m_TextBox.SelectionColor = Color.Purple;
						}
						else
						{
							m_TextBox.SelectionColor = Color.Black;
						}
					}

					// SelectedText acts like an append text function. The line will be added in the font and color we previously selected.
					m_TextBox.SelectedText = line + "\n";

				}

			}
		}

		private void OnMouseUp(object sender, MouseEventArgs e)
		{

			base.OnMouseUp(e);

			if (e.Button == MouseButtons.Right)
			{
				if (m_fileLogListView.SelectedItems.Count == 2)
				{
					// ListViewItem clickedItem = m_fileLogListView.GetItemAt(e.X, e.Y);
					ContextMenuStrip contextMenu = new ContextMenuStrip();
					contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContextMenuItemClicked);

					ToolStripMenuItem item;
					item = new ToolStripMenuItem("Diff selected files");
					contextMenu.Items.Add(item);

					if (contextMenu.Items.Count > 0)
					{
						contextMenu.Show( m_fileLogListView, e.Location);
					}
				}
			}

		}
		void ContextMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			if ((e.ClickedItem.Text.Substring(0, 4) == "Diff") && (m_fileLogListView.SelectedItems.Count >= 2))
			{
				DiffParams diffParams;
				diffParams.revision1 = m_fileLogListView.SelectedItems[1].SubItems[CiRevision].Text;
				diffParams.revision2 = m_fileLogListView.SelectedItems[0].SubItems[CiRevision].Text;
				diffParams.filename = m_logParams.filename;
				diffParams.repoPath = m_logParams.repoPath;
				diffParams.extCommand = (Properties.Settings.Default.PrefUseExternalDiff) ?
						Properties.Settings.Default.PrefExternalDiffCommand : "";

				m_controller.DiffFile(diffParams);
			}
		}

		private void OnColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
		{
			switch (e.ColumnIndex)
			{
				case CiRevision:
					Properties.Settings.Default.FileLogRevisionColumnWidth = revisionColumnHeader.Width;
					break;
				case CiChangesetId:
					Properties.Settings.Default.FileLogChangesetIdColumnWidth = changesetIdColumnHeader.Width;
					break;
				case CiMessage:
					Properties.Settings.Default.FileLogMessageColumnWidth = messageColumnHeader.Width;
					break;
				case CiUser:
					Properties.Settings.Default.FileLogUserColumnWidth = userColumnHeader.Width;
					break;
				case CiDate:
					Properties.Settings.Default.FileLogDateColumnWidth = dateColumnHeader.Width;
					break;
				case CiTags:
					Properties.Settings.Default.FileLogTagsColumnWidth = tagsColumnHeader.Width;
					break;
			}
			Properties.Settings.Default.Save();

		}

		/** @brief This function gets called when the columns are reordered by the user. Column 
		 *			order is saved to application settings.
		 *
		 * When this function is entered, the column header display indexes have not get been 
		 * reordered. We only know the new index of the column that was changed. But we'd normally 
		 * have to figure out the new display indices of all the other columns.
		 * So rather than bother to calculate the new order ourselves, we just set a timer
		 * and wait for the new order to get set. Then by the time the timer handler 
		 * gets called, the new order has been established and we can easily save it 
		 * to the application settings.
		 * Alternatively, we could wait until the fil log form is closed to save settings, but I'd 
		 * rather save settings as soon as changes are made in case something catatrophic happens 
		 * in the program and the file log form doesn't close properly. Then the changes are 
		 * still preserved. 
		 */

		private void OnColumnReordered(object sender, ColumnReorderedEventArgs e)
		{
			System.Timers.Timer timer = new System.Timers.Timer();
			timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			timer.Interval = 10;	// Set the Interval to 10 milliseconds. We want it to happen soon.
			timer.AutoReset = false;	// make it a one-shot timer
			timer.Enabled = true;
		}

		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			// Save the column order
			Properties.Settings.Default.FileLogRevisionColumnDisplayIndex= revisionColumnHeader.DisplayIndex;
			Properties.Settings.Default.FileLogChangesetIdColumnDisplayIndex = changesetIdColumnHeader.DisplayIndex;
			Properties.Settings.Default.FileLogMessageColumnDisplayIndex = messageColumnHeader.DisplayIndex;
			Properties.Settings.Default.FileLogUserColumnDisplayIndex = userColumnHeader.DisplayIndex;
			Properties.Settings.Default.FileLogDateColumnDisplayIndex = dateColumnHeader.DisplayIndex;
			Properties.Settings.Default.FileLogTagsColumnDisplayIndex = tagsColumnHeader.DisplayIndex;

			Properties.Settings.Default.Save();
		}


		private void OnSizeChanged(object sender, EventArgs e)
		{
			Properties.Settings.Default.FileLogFormSize = this.Size;
		}


	}
}
