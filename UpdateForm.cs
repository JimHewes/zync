﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Zync
{
	public partial class UpdateForm : Form
	{

		public bool GetCleanCopy { get; private set; }
		public bool ByDateTime { get; private set; }
		public bool ByRevision { get; private set; }
		public string Revision { get; private set; }
		public DateTime DateAndTime
		{
			get { return m_dateAndTime; }
		}
		private DateTime m_dateAndTime = new DateTime();

		public UpdateForm()
		{
			Initialize("");
		}

		public UpdateForm(string toRevision)
		{
			Initialize(toRevision);
		}

		private void Initialize(string toRevision)
		{
			InitializeComponent();
			if (toRevision != String.Empty)
			{
				updateByRevCheckBox.Checked = true;
				updateByRevTextBox.Enabled = true;
				updateByRevTextBox.Text = toRevision;
			}
			else
			{
				updateByRevCheckBox.Checked = false;
				updateByRevTextBox.Enabled = false;
			}

			updateDateDateTimePicker.Value = System.DateTime.Now;
			updateByDateCheckBox.Checked = false;
			updateDateDateTimePicker.Enabled = false;
			updateTimeDateTimePicker.Enabled = false;

			updateCleanCopyCheckBox.Checked = false;

			updateShiftKeyCheckBox.Checked = Properties.Settings.Default.UpdateFormDisplayOnlyOnShift;

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 
		}


		private void OnByRevisionCheckedChanged(object sender, EventArgs e)
		{
			updateByRevTextBox.Enabled = updateByRevCheckBox.Checked;

			if (updateByRevCheckBox.Checked)
			{
				updateByDateCheckBox.Checked = false;
			}
		}

		private void OnByDateCheckedChanged(object sender, EventArgs e)
		{
			updateDateDateTimePicker.Enabled = updateByDateCheckBox.Checked;
			updateTimeDateTimePicker.Enabled = updateByDateCheckBox.Checked;

			if (updateByDateCheckBox.Checked)
			{
				updateByRevCheckBox.Checked = false;
			}
		}

		private void OnClickedOkButton(object sender, EventArgs e)
		{
			ByRevision = updateByRevCheckBox.Checked;
			ByDateTime = updateByDateCheckBox.Checked;
			GetCleanCopy = updateCleanCopyCheckBox.Checked;

			Revision = ByRevision ? updateByRevTextBox.Text : String.Empty;

			if (ByDateTime)
			{
				System.DateTime date = updateDateDateTimePicker.Value;
				System.DateTime time = updateTimeDateTimePicker.Value;

				// DateTime = string.Format("{0:####}-{1:00}-{2:00} {3:00}:{4:00}:00", date.Year, date.Month, date.Day, time.Hour, time.Minute);

				m_dateAndTime = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Second,0);
			}

			Properties.Settings.Default.UpdateFormDisplayOnlyOnShift = updateShiftKeyCheckBox.Checked;
			Properties.Settings.Default.Save();
		}

	}
}
