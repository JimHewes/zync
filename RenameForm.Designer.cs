﻿namespace Zync
{
	partial class RenameForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenameForm));
			this.RenameFromLabel = new System.Windows.Forms.Label();
			this.RenameToLabel = new System.Windows.Forms.Label();
			this.RenameToFilenameTextbox = new System.Windows.Forms.TextBox();
			this.RenameFromFilenameLabel = new System.Windows.Forms.Label();
			this.RenameAfterCheckBox = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.RenameButton = new System.Windows.Forms.Button();
			this.RenameCancelButton = new System.Windows.Forms.Button();
			this.RenameFromFilenameTextbox = new System.Windows.Forms.TextBox();
			this.RenameToFilenameLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// RenameFromLabel
			// 
			this.RenameFromLabel.AutoSize = true;
			this.RenameFromLabel.Location = new System.Drawing.Point(8, 49);
			this.RenameFromLabel.Name = "RenameFromLabel";
			this.RenameFromLabel.Size = new System.Drawing.Size(33, 13);
			this.RenameFromLabel.TabIndex = 0;
			this.RenameFromLabel.Text = "From:";
			// 
			// RenameToLabel
			// 
			this.RenameToLabel.AutoSize = true;
			this.RenameToLabel.Location = new System.Drawing.Point(8, 75);
			this.RenameToLabel.Name = "RenameToLabel";
			this.RenameToLabel.Size = new System.Drawing.Size(23, 13);
			this.RenameToLabel.TabIndex = 1;
			this.RenameToLabel.Text = "To:";
			// 
			// RenameToFilenameTextbox
			// 
			this.RenameToFilenameTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.RenameToFilenameTextbox.Location = new System.Drawing.Point(50, 72);
			this.RenameToFilenameTextbox.Name = "RenameToFilenameTextbox";
			this.RenameToFilenameTextbox.Size = new System.Drawing.Size(240, 20);
			this.RenameToFilenameTextbox.TabIndex = 2;
			this.RenameToFilenameTextbox.Text = "Filename";
			this.toolTip1.SetToolTip(this.RenameToFilenameTextbox, "The new name of the file.");
			this.RenameToFilenameTextbox.TextChanged += new System.EventHandler(this.OnToTextChanged);
			// 
			// RenameFromFilenameLabel
			// 
			this.RenameFromFilenameLabel.AutoSize = true;
			this.RenameFromFilenameLabel.Location = new System.Drawing.Point(47, 49);
			this.RenameFromFilenameLabel.Name = "RenameFromFilenameLabel";
			this.RenameFromFilenameLabel.Size = new System.Drawing.Size(49, 13);
			this.RenameFromFilenameLabel.TabIndex = 3;
			this.RenameFromFilenameLabel.Text = "Filename";
			this.RenameFromFilenameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.toolTip1.SetToolTip(this.RenameFromFilenameLabel, "The current name of the file.");
			this.RenameFromFilenameLabel.Visible = false;
			// 
			// RenameAfterCheckBox
			// 
			this.RenameAfterCheckBox.AutoSize = true;
			this.RenameAfterCheckBox.Location = new System.Drawing.Point(11, 117);
			this.RenameAfterCheckBox.Name = "RenameAfterCheckBox";
			this.RenameAfterCheckBox.Size = new System.Drawing.Size(48, 17);
			this.RenameAfterCheckBox.TabIndex = 4;
			this.RenameAfterCheckBox.Text = "After";
			this.toolTip1.SetToolTip(this.RenameAfterCheckBox, "Record a rename in version control after you\'ve already renamed the file on disk." +
					"");
			this.RenameAfterCheckBox.UseVisualStyleBackColor = true;
			this.RenameAfterCheckBox.CheckStateChanged += new System.EventHandler(this.OnAfterCheckboxChanged);
			// 
			// RenameButton
			// 
			this.RenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.RenameButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.RenameButton.Location = new System.Drawing.Point(125, 137);
			this.RenameButton.Name = "RenameButton";
			this.RenameButton.Size = new System.Drawing.Size(75, 23);
			this.RenameButton.TabIndex = 5;
			this.RenameButton.Text = "Rename";
			this.toolTip1.SetToolTip(this.RenameButton, "Rename the file to the new name in the version control system.");
			this.RenameButton.UseVisualStyleBackColor = true;
			this.RenameButton.Click += new System.EventHandler(this.OnClickedRenameButton);
			// 
			// RenameCancelButton
			// 
			this.RenameCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.RenameCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.RenameCancelButton.Location = new System.Drawing.Point(216, 137);
			this.RenameCancelButton.Name = "RenameCancelButton";
			this.RenameCancelButton.Size = new System.Drawing.Size(75, 23);
			this.RenameCancelButton.TabIndex = 6;
			this.RenameCancelButton.Text = "Cancel";
			this.toolTip1.SetToolTip(this.RenameCancelButton, "Cancel the rename of the file.");
			this.RenameCancelButton.UseVisualStyleBackColor = true;
			// 
			// RenameFromFilenameTextbox
			// 
			this.RenameFromFilenameTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.RenameFromFilenameTextbox.Location = new System.Drawing.Point(50, 46);
			this.RenameFromFilenameTextbox.Name = "RenameFromFilenameTextbox";
			this.RenameFromFilenameTextbox.Size = new System.Drawing.Size(240, 20);
			this.RenameFromFilenameTextbox.TabIndex = 2;
			this.RenameFromFilenameTextbox.Text = "Filename";
			this.toolTip1.SetToolTip(this.RenameFromFilenameTextbox, "The old name of the file.");
			this.RenameFromFilenameTextbox.TextChanged += new System.EventHandler(this.OnFromTextChanged);
			// 
			// RenameToFilenameLabel
			// 
			this.RenameToFilenameLabel.AutoSize = true;
			this.RenameToFilenameLabel.Location = new System.Drawing.Point(47, 75);
			this.RenameToFilenameLabel.Name = "RenameToFilenameLabel";
			this.RenameToFilenameLabel.Size = new System.Drawing.Size(49, 13);
			this.RenameToFilenameLabel.TabIndex = 3;
			this.RenameToFilenameLabel.Text = "Filename";
			this.RenameToFilenameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.RenameToFilenameLabel.Visible = false;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(278, 34);
			this.label1.TabIndex = 7;
			this.label1.Text = "If the file is in a subfolder, include the path as shown unless you intend to mov" +
				"e the file.";
			// 
			// RenameForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(306, 170);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.RenameCancelButton);
			this.Controls.Add(this.RenameButton);
			this.Controls.Add(this.RenameAfterCheckBox);
			this.Controls.Add(this.RenameToFilenameLabel);
			this.Controls.Add(this.RenameFromFilenameLabel);
			this.Controls.Add(this.RenameFromFilenameTextbox);
			this.Controls.Add(this.RenameToFilenameTextbox);
			this.Controls.Add(this.RenameToLabel);
			this.Controls.Add(this.RenameFromLabel);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(314, 204);
			this.Name = "RenameForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Rename File";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label RenameFromLabel;
		private System.Windows.Forms.Label RenameToLabel;
		private System.Windows.Forms.TextBox RenameToFilenameTextbox;
		private System.Windows.Forms.Label RenameFromFilenameLabel;
		private System.Windows.Forms.CheckBox RenameAfterCheckBox;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button RenameButton;
		private System.Windows.Forms.Button RenameCancelButton;
		private System.Windows.Forms.TextBox RenameFromFilenameTextbox;
		private System.Windows.Forms.Label RenameToFilenameLabel;
		private System.Windows.Forms.Label label1;
	}
}