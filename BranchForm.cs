﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class BranchForm : Form
	{
		private string setButtonTooltipText = "Set the branch name of the current working folder.\nThis button will be disabled if the branch name text box is empty.";
		private string clearButtonTooltipText = "Reset the branch name back to that of the parent of the working directory";

		public bool Set { get; private set; }
		public bool Clean { get; private set; }
		public bool Force { get; private set; }
		public string BranchName { get; private set; }

		public BranchForm()
		{
			InitializeComponent();

			SetBranchRadioButton.Checked = true;
			BranchNameTextBox.Text = "";
			CleanBranchRadioButton.Checked = false;
			ForceBranchCheckBox.Checked = false;
			BranchOkButton.Text = "Set";
			BranchOkButton.Enabled = false;	// Since the BranchNameTextBox is initially empty, this is disabled.
			toolTip1.SetToolTip(BranchOkButton, setButtonTooltipText);

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 
		}


		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.BranchFormSize = this.Size;
		}

		private void OnFormLoad(object sender, EventArgs e)
		{
			this.Size = Properties.Settings.Default.BranchFormSize;
		}

		private void OnRadioButtonsChanged(object sender, EventArgs e)
		{
			if (SetBranchRadioButton.Checked)
			{
				ForceBranchCheckBox.Enabled = true;
				BranchNameTextBox.Enabled = true;
				BranchOkButton.Text = SetBranchRadioButton.Text;
				BranchOkButton.Enabled = BranchNameTextBox.Text != String.Empty;
				toolTip1.SetToolTip(BranchOkButton, setButtonTooltipText);
			}
			else
			{
				ForceBranchCheckBox.Enabled = false;
				BranchNameTextBox.Enabled = false;
				BranchOkButton.Text = CleanBranchRadioButton.Text;
				BranchOkButton.Enabled = true;
				toolTip1.SetToolTip(BranchOkButton, clearButtonTooltipText);
			}
		}

		private void OnClickedOkButton(object sender, EventArgs e)
		{
			Set = SetBranchRadioButton.Checked;
			Clean = CleanBranchRadioButton.Checked;

			BranchName = Set? BranchNameTextBox.Text : "";
			Force = Set ? ForceBranchCheckBox.Checked : false;

		}

		private void OnBranchNameTextChanged(object sender, EventArgs e)
		{
			BranchOkButton.Enabled = BranchNameTextBox.Text != String.Empty;
		}
	}
}
