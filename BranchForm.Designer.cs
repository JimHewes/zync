﻿namespace Zync
{
	partial class BranchForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BranchForm));
			this.SetBranchRadioButton = new System.Windows.Forms.RadioButton();
			this.BranchNameTextBox = new System.Windows.Forms.TextBox();
			this.CleanBranchRadioButton = new System.Windows.Forms.RadioButton();
			this.ForceBranchCheckBox = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.BranchCancelButton = new System.Windows.Forms.Button();
			this.BranchOkButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// SetBranchRadioButton
			// 
			this.SetBranchRadioButton.AutoSize = true;
			this.SetBranchRadioButton.Location = new System.Drawing.Point(26, 34);
			this.SetBranchRadioButton.Name = "SetBranchRadioButton";
			this.SetBranchRadioButton.Size = new System.Drawing.Size(41, 17);
			this.SetBranchRadioButton.TabIndex = 0;
			this.SetBranchRadioButton.TabStop = true;
			this.SetBranchRadioButton.Text = "Set";
			this.toolTip1.SetToolTip(this.SetBranchRadioButton, "Set the working directory branch name. The branch will not exist in the repositor" +
					"y until the next commit");
			this.SetBranchRadioButton.UseVisualStyleBackColor = true;
			this.SetBranchRadioButton.CheckedChanged += new System.EventHandler(this.OnRadioButtonsChanged);
			// 
			// BranchNameTextBox
			// 
			this.BranchNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.BranchNameTextBox.Location = new System.Drawing.Point(73, 34);
			this.BranchNameTextBox.Name = "BranchNameTextBox";
			this.BranchNameTextBox.Size = new System.Drawing.Size(259, 20);
			this.BranchNameTextBox.TabIndex = 1;
			this.toolTip1.SetToolTip(this.BranchNameTextBox, "The name of the branch to set.\r\nThe Set button will be disabled if this textbox i" +
					"s empty.");
			this.BranchNameTextBox.TextChanged += new System.EventHandler(this.OnBranchNameTextChanged);
			// 
			// CleanBranchRadioButton
			// 
			this.CleanBranchRadioButton.AutoSize = true;
			this.CleanBranchRadioButton.Location = new System.Drawing.Point(26, 106);
			this.CleanBranchRadioButton.Name = "CleanBranchRadioButton";
			this.CleanBranchRadioButton.Size = new System.Drawing.Size(49, 17);
			this.CleanBranchRadioButton.TabIndex = 3;
			this.CleanBranchRadioButton.TabStop = true;
			this.CleanBranchRadioButton.Text = "Clear";
			this.toolTip1.SetToolTip(this.CleanBranchRadioButton, "Reset the branch name back to that of the parent of the working directory");
			this.CleanBranchRadioButton.UseVisualStyleBackColor = true;
			// 
			// ForceBranchCheckBox
			// 
			this.ForceBranchCheckBox.AutoSize = true;
			this.ForceBranchCheckBox.Location = new System.Drawing.Point(50, 69);
			this.ForceBranchCheckBox.Name = "ForceBranchCheckBox";
			this.ForceBranchCheckBox.Size = new System.Drawing.Size(53, 17);
			this.ForceBranchCheckBox.TabIndex = 2;
			this.ForceBranchCheckBox.Text = "Force";
			this.toolTip1.SetToolTip(this.ForceBranchCheckBox, "Set the branch name even if that branch name already exists in the repository");
			this.ForceBranchCheckBox.UseVisualStyleBackColor = true;
			// 
			// BranchCancelButton
			// 
			this.BranchCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BranchCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BranchCancelButton.Location = new System.Drawing.Point(273, 139);
			this.BranchCancelButton.Name = "BranchCancelButton";
			this.BranchCancelButton.Size = new System.Drawing.Size(75, 23);
			this.BranchCancelButton.TabIndex = 5;
			this.BranchCancelButton.Text = "Cancel";
			this.toolTip1.SetToolTip(this.BranchCancelButton, "Cancel this dialog");
			this.BranchCancelButton.UseVisualStyleBackColor = true;
			// 
			// BranchOkButton
			// 
			this.BranchOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BranchOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.BranchOkButton.Location = new System.Drawing.Point(186, 138);
			this.BranchOkButton.Name = "BranchOkButton";
			this.BranchOkButton.Size = new System.Drawing.Size(75, 23);
			this.BranchOkButton.TabIndex = 4;
			this.BranchOkButton.Text = "OK";
			this.toolTip1.SetToolTip(this.BranchOkButton, "Set the branch name of the current working folder.\\nThis button will be disabled " +
					"if the branch name text box is empty.");
			this.BranchOkButton.UseVisualStyleBackColor = true;
			this.BranchOkButton.Click += new System.EventHandler(this.OnClickedOkButton);
			// 
			// BranchForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(360, 174);
			this.Controls.Add(this.BranchOkButton);
			this.Controls.Add(this.BranchCancelButton);
			this.Controls.Add(this.ForceBranchCheckBox);
			this.Controls.Add(this.CleanBranchRadioButton);
			this.Controls.Add(this.BranchNameTextBox);
			this.Controls.Add(this.SetBranchRadioButton);
			this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "BranchFormLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = global::Zync.Properties.Settings.Default.BranchFormLocation;
			this.MinimumSize = new System.Drawing.Size(368, 208);
			this.Name = "BranchForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Branch";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.Load += new System.EventHandler(this.OnFormLoad);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RadioButton SetBranchRadioButton;
		private System.Windows.Forms.TextBox BranchNameTextBox;
		private System.Windows.Forms.RadioButton CleanBranchRadioButton;
		private System.Windows.Forms.CheckBox ForceBranchCheckBox;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button BranchCancelButton;
		private System.Windows.Forms.Button BranchOkButton;
	}
}