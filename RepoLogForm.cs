﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Timers;

namespace Zync
{
	public partial class RepoLogForm : Form
	{
		public const int CiRevision = 0;
		public const int CiChangesetId = 1;
		public const int CiMessage = 2;
		public const int CiUser = 3;
		public const int CiDate = 4;
		public const int CiTags = 5;
		public const int CiBranch = 6;

		Model m_model;
		Controller m_controller;
        LogParams m_logParams;
        List<FileLogItem> m_list;
		Font m_defaultFont;

		public RepoLogForm(Model model, Controller controller, LogParams logParams)
		{
			m_model = model;
			m_controller = controller;
            m_logParams = logParams;

			InitializeComponent();
			m_defaultFont = m_TextBox.SelectionFont;		// Save default font for use with messages, since it will get will get changed for diffs
			
			// In the designer we set the ReadOnly property to true. But that also makes the 
			// background color automatically set to gray. We want it to stay as white, so reset 
			// it back to white here.
			m_TextBox.BackColor = Color.White;

			revisionColumnHeader.Width = Properties.Settings.Default.RepoLogRevisionColumnWidth;
			revisionColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogRevisionColumnDisplayIndex;
			changesetIdColumnHeader.Width = Properties.Settings.Default.RepoLogChangesetIdColumnWidth;
			changesetIdColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogChangesetIdColumnDisplayIndex;
			messageColumnHeader.Width = Properties.Settings.Default.RepoLogMessageColumnWidth;
			messageColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogMessageColumnDisplayIndex;
			userColumnHeader.Width = Properties.Settings.Default.RepoLogUserColumnWidth;
			userColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogUserColumnDisplayIndex;
			dateColumnHeader.Width = Properties.Settings.Default.RepoLogDateColumnWidth;
			dateColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogDateColumnDisplayIndex;
			tagsColumnHeader.Width = Properties.Settings.Default.RepoLogTagsColumnWidth;
			tagsColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogTagsColumnDisplayIndex;
			branchColumnHeader.Width = Properties.Settings.Default.RepoLogBranchColumnWidth;
			branchColumnHeader.DisplayIndex = Properties.Settings.Default.RepoLogBranchColumnDisplayIndex;

			this.Size = Properties.Settings.Default.RepoLogFormSize;
			this.Location = Properties.Settings.Default.RepoLogFormLocation;
			RepoLogSplitContainer.SplitterDistance = Properties.Settings.Default.RepoLogSplitterDistance;
			RepoLogBottomSplitContainer.SplitterDistance = Properties.Settings.Default.RepoLogBottomSplitterDistance;
			RepoLogBottomRightSplitContainer.SplitterDistance = Properties.Settings.Default.RepoLogBottomRightSplitterDistance;
			
            RefreshContents();

			// Select top item in the list. (Which is not done every time the contents are refreshed.)
			//if (m_repoLogListView.Items.Count > 0)
			//{
			//    m_repoLogListView.Items[0].Selected = true;
			//}

			// By setting the column header width to -1, we're telling it to automatically resize to 
			// the widest text in the column. Since no item is selected there is not yet any text in 
			// these lists. But setting the width now prevent the horizontal scroll bar from showing 
			// if the list view width happens to be greater than the container panel.
			m_repoLogTagListView.Columns[0].Width = -1;
			m_repoLogFileListView.Columns[0].Width = -1;


			Text = "Repository Log: " + m_logParams.repoPath;

		}


        private void RefreshContents()
        {
            LogParams lp = m_logParams;
			lp.verbose = true;
			lp.getFiles = true;
			
			m_repoLogListView.Items.Clear();

			m_list = m_model.GetLog(lp);
			int numColumns = m_repoLogListView.Columns.Count;
			string[] record = new string[numColumns];

			// Doing a parent command with no revision or filename gets the revision that the working files are based on.
			ParentsParams pp = new ParentsParams { filename = "", revision = "" };
			pp.repoPath = m_logParams.repoPath;
			List<FileLogItem> parents = m_model.GetParents(pp);
			int workingRevision = 0;
			if (parents.Count > 0)
			{
				workingRevision = parents[0].revision;
			}

 

			foreach (FileLogItem item in m_list)
			{
				record[CiRevision] = item.revision.ToString();
				record[CiChangesetId] = item.changesetId;
				record[CiMessage] = item.message.Contains("\n") ? item.message.Split('\n')[0] : item.message;
				record[CiUser] = item.user;
				record[CiDate] = item.date.ToShortDateString() + " " + item.date.ToShortTimeString();
				record[CiTags] = "";
				record[CiBranch] = (0 != item.branch.CompareTo("default")) ? item.branch : String.Empty;

				foreach (string tag in item.tags)
				{
					if (record[CiTags].Length == 0)
					{
						record[CiTags] = tag;
					}
					else
					{
						record[CiTags] += "; " + tag;
					}
				}


				// Create a new ListViewItem from the array of strings. Each string in the array is 
				// for a column, the text is shown in that column. This causes a number of SubItems 
				// to be created in the ListViewItem equal to the number of length of the array.
				ListViewItem lvi = new ListViewItem(record);
				lvi.Tag = item;		// store a reference to the LogItem in the Tag member so we can get back to the LogItem from the ListItem
				lvi.Name = record[CiRevision];

				// Show the current working revision in bold text.
				if (workingRevision == item.revision)
				{
					// This createsd a new font based on the existing for but modified to be bold
					lvi.Font = new System.Drawing.Font(lvi.Font, FontStyle.Bold);
				}

				m_repoLogListView.Items.Insert(0, lvi);
			}




			UpdateOutgoing();
        }

		/** @brief Sets the background color of all outgoing revisions to cyan.
		 * 
		 *	This sets the color of all lines inthe log, not just outgoing line, because if the 
		 *	outgoing status changes, we may need the revert lines back from cyan to white.
		 **/
		private void UpdateOutgoing()
		{
			List<FileLogItem> outgoingList = m_model.GetOutgoing(m_logParams.repoPath);
			bool found = false;

			foreach (ListViewItem logItem in m_repoLogListView.Items)
			{
				found = false;
				foreach (FileLogItem outItem in outgoingList)
				{
					// If the revision of the outgoing list matches the log revision, we found a match.
					if (logItem.SubItems[CiRevision].Text == outItem.revision.ToString())
					{
						found = true;
						break;
					}


				}
				logItem.BackColor = found ? Color.LightCyan : Color.White;
			}
		}


		private void OnColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
		{
			switch (e.ColumnIndex)
			{
				case CiRevision:
					Properties.Settings.Default.RepoLogRevisionColumnWidth = revisionColumnHeader.Width;
					break;
				case CiChangesetId:
					Properties.Settings.Default.RepoLogChangesetIdColumnWidth = changesetIdColumnHeader.Width;
					break;
				case CiMessage:
					Properties.Settings.Default.RepoLogMessageColumnWidth = messageColumnHeader.Width;
					break;
				case CiUser:
					Properties.Settings.Default.RepoLogUserColumnWidth = userColumnHeader.Width;
					break;
				case CiDate:
					Properties.Settings.Default.RepoLogDateColumnWidth = dateColumnHeader.Width;
					break;
				case CiTags:
					Properties.Settings.Default.RepoLogTagsColumnWidth = tagsColumnHeader.Width;
					break;
				case CiBranch:
					Properties.Settings.Default.RepoLogBranchColumnWidth = branchColumnHeader.Width;
					break;
			}
			Properties.Settings.Default.Save();
		}



		/** @brief This function gets called when the columns are reordered by the user. Column 
		 *			order is saved to application settings.
		 *
		 * When this function is entered, the column header display indexes have not get been 
		 * reordered. We only know the new index of the column that was changed. But we'd normally 
		 * have to figure out the new display indices of all the other columns.
		 * So rather than bother to calculate the new order ourselves, we just set a timer
		 * and wait for the new order to get set. Then by the time the timer handler 
		 * gets called, the new order has been established and we can easily save it 
		 * to the application settings.
		 * Alternatively, we could wait until the repository log form is closed to save settings, but I'd 
		 * rather save settings as soon as changes are made in case something catatrophic happens 
		 * in the program and the repository log form doesn't close properly. Then the changes are 
		 * still preserved. 
		 */
		private void OnColumnReordered(object sender, ColumnReorderedEventArgs e)
		{
			System.Timers.Timer timer = new System.Timers.Timer();
			timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			timer.Interval = 10;	// Set the Interval to 10 milliseconds. We want it to happen soon.
			timer.AutoReset = false;	// make it a one-shot timer
			timer.Enabled = true;
		}

		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			// Save the column order
			Properties.Settings.Default.RepoLogRevisionColumnDisplayIndex = revisionColumnHeader.DisplayIndex;
			Properties.Settings.Default.RepoLogChangesetIdColumnDisplayIndex = changesetIdColumnHeader.DisplayIndex;
			Properties.Settings.Default.RepoLogMessageColumnDisplayIndex = messageColumnHeader.DisplayIndex;
			Properties.Settings.Default.RepoLogUserColumnDisplayIndex = userColumnHeader.DisplayIndex;
			Properties.Settings.Default.RepoLogDateColumnDisplayIndex = dateColumnHeader.DisplayIndex;
			Properties.Settings.Default.RepoLogTagsColumnDisplayIndex = tagsColumnHeader.DisplayIndex;
			Properties.Settings.Default.RepoLogBranchColumnDisplayIndex = branchColumnHeader.DisplayIndex;

			Properties.Settings.Default.Save();
		}

		// Note that it's necessary to take control of loading and saving the window location to the settings 
		// and not let it be done automatically by the Designer. If you let the Designer do it, then it causes 
		// trouble in the case or multiple log forms open. When you move one form, it immediately saves the 
		// location to the settings, which causes an event to all other Log forms and they all reload the new location 
		// from the settings. So what you end up with is that all open log forms have thei upper-left corner stuck 
		// together so that you can't separate the windows.
		private void OnFormClosed(object sender, FormClosedEventArgs e)
		{
			if (this.WindowState == FormWindowState.Normal)
			{
				Properties.Settings.Default.RepoLogFormSize = this.Size;
				Properties.Settings.Default.RepoLogFormLocation = this.Location;
			}
			else
			{
				Properties.Settings.Default.RepoLogFormSize = this.RestoreBounds.Size;
				Properties.Settings.Default.RepoLogFormLocation = this.RestoreBounds.Location;
			}
			Properties.Settings.Default.RepoLogSplitterDistance = RepoLogSplitContainer.SplitterDistance;
			Properties.Settings.Default.RepoLogBottomSplitterDistance = RepoLogBottomSplitContainer.SplitterDistance;
			Properties.Settings.Default.RepoLogBottomRightSplitterDistance = RepoLogBottomRightSplitContainer.SplitterDistance;


			Properties.Settings.Default.Save();
		}

		private void OnSizeChanged(object sender, EventArgs e)
		{
			Properties.Settings.Default.RepoLogFormSize = this.Size;
		}

		private void OnLogItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
			ListViewItem lvItem = e.Item;

			// The items are displayed in reverse order---newest at the top---so the zero index of the display 
			// is really the last index into m_list[].
			//FileLogItem rlItem = m_list[m_list.Count - e.ItemIndex - 1];

			FileLogItem rlItem = (FileLogItem)e.Item.Tag;

			// When a list item is selected, this function gets called twice. 
            // Once for the previously selected item (IsSelected == false) and once for the newly selected item.
            // If no item was previously selected then this function only gets called once.
			if (!e.IsSelected)
			{
                // This is the previous selection.
				// We want to un-highlight the parents and children.
				foreach (RevIdPair parent in rlItem.parents)
				{
					ListViewItem[] lviParent = m_repoLogListView.Items.Find(parent.revision, false);	// The text of the listview item has the revision number

					if (lviParent.Length > 0)	// There should be no more than one item in the array since revision number are unique.
					{
						lviParent[0].ForeColor = Color.Black;	// restore backgroud color of parents to white
					}
				}
				foreach (RevIdPair child in rlItem.children)
				{
					ListViewItem[] lviChild = m_repoLogListView.Items.Find(child.revision, false);	// The text of the listview item has the revision number

					if (lviChild.Length > 0)	// There should be no more than one item in the array since revision number are unique.
					{
						lviChild[0].ForeColor = Color.Black;	// restore backgroud color of parents to white
					}
				}

				return;
			}

			// We want to un-highlight the parents.
			foreach (RevIdPair parent in rlItem.parents)
			{
				ListViewItem[] lviParent = m_repoLogListView.Items.Find(parent.revision, false);	// The text of the listview item has the revision number

				if (lviParent.Length > 0)	// There should be no more than one item in the array since revision number are unique.
				{
					lviParent[0].ForeColor = Color.Green;	// set backgroud color of parents to green
				}
			}
			foreach (RevIdPair child in rlItem.children)
			{
				ListViewItem[] lviChild = m_repoLogListView.Items.Find(child.revision, false);	// The text of the listview item has the revision number

				if (lviChild.Length > 0)	// There should be no more than one item in the array since revision number are unique.
				{
					lviChild[0].ForeColor = Color.Blue;	// restore backgroud color of parents to white
				}
			}


			// Get the changeset ID
			string changesetId = lvItem.SubItems[1].Text;

			// Get verbose information for this changeset, including the set of files that changed.
			//LogParams lp = new LogParams();
			//lp.logByRevision = true;
			//lp.startRevision = lp.endRevision = changesetId;
			//lp.verbose = true;
			//lp.getFiles = true;

			//List<FileLogItem> repoList = m_model.GetLog(lp);

			//if (repoList.Count != 1)
			//{
			//	throw new Exception("Expected to find changeset " + changesetId + " in the repository log, but did not.");
			//}
			//FileLogItem rli = repoList[0];

			// Add Files to the File list view on the bottom.
			m_repoLogFileListView.Items.Clear();
			string[] record = new string[1];
			foreach (string filename in rlItem.files)
			{
				record[0] = filename;
				m_repoLogFileListView.Items.Add(new ListViewItem(record));
			}

			// Add Tags to the Tag list view on the bottom.
			m_repoLogTagListView.Items.Clear();
			foreach (string tagname in rlItem.tags)
			{
				record[0] = tagname;
				m_repoLogTagListView.Items.Add(new ListViewItem(record));
			}



			// By setting the column header width to -1, we're telling it to automatically resize to 
			// the widest text in the column. Note we need to set this every time after we add new items.
			// (Also, it we wanted the column to autosize to the text in the column header, we could set 
			//  the width to -2.)
			m_repoLogTagListView.Columns[0].Width = -1;
			m_repoLogFileListView.Columns[0].Width = -1;

			/////////////////////////////////////////////////////

			m_TextBox.SelectionFont = m_defaultFont;
			m_TextBox.Clear();
			m_TextBox.WordWrap = true;
			m_TextBox.SelectedText = rlItem.message;
        }

		private void OnRepoLogFileListSelectionChanged(object sender, EventArgs e)
		{

			ListView.SelectedListViewItemCollection selectedItems = m_repoLogFileListView.SelectedItems;
			if ((selectedItems == null) || (selectedItems.Count < 1))
			{
				return;
			}

			// There should only be one selected item in the files list.
			ListViewItem fileItem = selectedItems[0];

			if (m_repoLogListView.SelectedItems.Count < 1)
			{
				// If no items are selected in the log view, we can't do anything because we don't know the revision.
				return;
			}

			m_TextBox.Clear();
		
			DiffParams diffParams;
			diffParams.filename = fileItem.Text;
			diffParams.revision2 = m_repoLogListView.SelectedItems[0].SubItems[0].Text;
			diffParams.repoPath = m_logParams.repoPath;
			diffParams.extCommand = "";	// don't use external diff here

			// We can't avoid making a specific call to get the file's parent. Although we could store the changeset's 
			// parent in the list, that's not the same thing as the file's parent. The file's parent is the last 
			// changeset in which the file was changed (in the same branch). That's not necessarily the previous 
			// changeset (in the same branch).
			ParentsParams pp = new ParentsParams { filename = diffParams.filename, revision = diffParams.revision2 };
			pp.repoPath = m_logParams.repoPath;
			List<FileLogItem> parents = m_model.GetParents(pp);

			diffParams.revision1 = (parents.Count > 0) ? parents[0].revision.ToString() : "";
			string theDiff = m_model.GetDiff(diffParams);
			if ((theDiff.Length == 0) && (parents.Count > 1))
			{
				diffParams.revision1 = parents[1].revision.ToString();
				theDiff = m_model.GetDiff(diffParams);
			}

			string[] lines = theDiff.Split('\n');
			m_TextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

			foreach (string line in lines)
			{
				m_TextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

				if (line.Length > 0)
				{
					if (line.Substring(0, 1) == "+")
					{
						m_TextBox.SelectionColor = Color.Green;
					}
					else if (line.Substring(0, 1) == "-")
					{
						m_TextBox.SelectionColor = Color.Red;
					}
					else if (line.Substring(0, 1) == "@")
					{
						m_TextBox.SelectionColor = Color.Purple;
					}
					else
					{
						m_TextBox.SelectionColor = Color.Black;
					}
				}
				m_TextBox.WordWrap = false;
				// SelectedText acts like an append text function. The line will be added in the font and color we previously selected.
				m_TextBox.SelectedText = line + "\n";

			}
		}

		private void OnRepoLogFileListMouseUp(object sender, MouseEventArgs e)
		{
			base.OnMouseUp(e);

			if (e.Button == MouseButtons.Right)
			{

					// ListViewItem clickedItem = m_fileLogListView.GetItemAt(e.X, e.Y);
					ContextMenuStrip contextMenu = new ContextMenuStrip();
					contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(FileContextMenuItemClicked);

					ToolStripMenuItem item;
					item = new ToolStripMenuItem("Diff with previous");
					contextMenu.Items.Add(item);
					item = new ToolStripMenuItem("Log...");
					contextMenu.Items.Add(item);
					item = new ToolStripMenuItem("Annotate...");
					contextMenu.Items.Add(item);
					if (contextMenu.Items.Count > 0)
					{
						contextMenu.Show(m_repoLogFileListView, e.Location);
					}
				
			}
		}

		private void FileContextMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			if (m_repoLogFileListView.SelectedItems.Count == 0)
			{
				return;
			}
			string menuText = e.ClickedItem.Text;

			string selectedFilename = m_repoLogFileListView.SelectedItems[0].Text;
			if (menuText.StartsWith("Diff"))
			{
				DiffParams diffParams;
				diffParams.filename = selectedFilename;
				diffParams.revision2 = m_repoLogListView.SelectedItems[0].SubItems[CiRevision].Text;
				diffParams.repoPath = m_logParams.repoPath;
				diffParams.extCommand = (Properties.Settings.Default.PrefUseExternalDiff) ?
						Properties.Settings.Default.PrefExternalDiffCommand : "";

				// We can't avoid making a specific call to get the file's parent. Although we could store the changeset's 
				// parent in the list, that's not the same thing as the file's parent. The file's parent is the last 
				// changeset in which the file was changed (in the same branch). That's not necessarily the previous 
				// changeset (in the same branch).
				ParentsParams pp = new ParentsParams { filename = diffParams.filename, revision = diffParams.revision2 };
				pp.repoPath = m_logParams.repoPath;
				List<FileLogItem> parents = m_model.GetParents(pp);

				diffParams.revision1 = "";
				if (parents.Count > 1)
				{
					diffParams.revision1 = parents[1].revision.ToString();
				}
				else if (parents.Count > 0)
				{
					diffParams.revision1 = parents[0].revision.ToString();
				}
				m_controller.DiffFile(diffParams);
			}
			if (menuText.StartsWith("Log"))
			{
				bool shiftDown = ((Control.ModifierKeys & Keys.Shift) == Keys.Shift);

				m_controller.LogFiles(this, shiftDown, m_logParams.repoPath, new List<string> { selectedFilename });
			}
			else if (menuText.StartsWith("Annotate"))
			{
				bool shiftDown = ((Control.ModifierKeys & Keys.Shift) == Keys.Shift);

				m_controller.AnnotateFile(this, shiftDown, m_logParams.repoPath, 
					selectedFilename, m_repoLogListView.SelectedItems[0].SubItems[CiRevision].Text);
			}
		}

		private void OnLogItemMouseUp(object sender, MouseEventArgs e)
		{
			base.OnMouseUp(e);

			if (e.Button == MouseButtons.Right)
			{
				ListViewItem clickedItem = ((ListView)sender).GetItemAt(e.X, e.Y);

				// index could be null if there was no item at the clickpoint
				if (clickedItem != null)
				{
					ContextMenuStrip contextMenu = new ContextMenuStrip();
					contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(LogContextMenuItemClicked);

					ToolStripMenuItem item;
					item = new ToolStripMenuItem("Tag...");
					item.Tag = clickedItem;	// Make the user data a reference to the clicked item so we have it in the menu handler.
					contextMenu.Items.Add(item);
					item = new ToolStripMenuItem("Update to this revision...");
					item.Tag = clickedItem;	// Make the user data a reference to the clicked item so we have it in the menu handler.
					contextMenu.Items.Add(item);

					if (contextMenu.Items.Count > 0)
					{
						contextMenu.Show(m_repoLogListView, e.Location);
					}
				}

			}
		}

		private void LogContextMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			string menuText = e.ClickedItem.Text;
			ListViewItem clickedLogItem = (ListViewItem)e.ClickedItem.Tag;
			if (menuText.StartsWith("Tag"))
			{

				bool cmdExecuted = m_controller.TagRevision(this, clickedLogItem.SubItems[CiRevision].Text, clickedLogItem.SubItems[CiChangesetId].Text);
				
				// repopulate to show changes
				if (cmdExecuted)
				{
					RefreshContents();
				}

			}
			else if (menuText.StartsWith("Update"))
			{
				m_controller.UpdateRepository(this, true, m_logParams.repoPath, clickedLogItem.SubItems[CiRevision].Text);

				RefreshContents();
			}

		}
	}
}
