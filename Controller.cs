﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Zync
{

	public enum CommandFlag
	{
		None = 0,
		LogRepo =		1 << 0,
		LogFile =		1 << 1,
		Push =			1 << 2,
		Pull =			1 << 3,
		Update =		1 << 4,
		Clone =			1 << 5,
		Commit =		1 << 6,
		Add =			1 << 7,
		Remove =		1 << 8,
		Diff =			1 << 9,
		Rename =		1 << 10,
		Revert =		1 << 11,
		Rollback =		1 << 12,
		Merge =			1 << 13,
		Create =		1 << 14,
		Annotate =		1 << 15,
		Tag =			1 << 16,
		Unmark =		1 << 17,
		Ignore =		1 << 18,
		StopIgnore =	1 << 19,
		View =			1 << 20,
		Branch =		1 << 21,
	};

	// The Flags attribute allows all possible combinations of the flags
	[Flags]
	public enum CommandFlags
	{
		None = CommandFlag.None,
		LogRepo = CommandFlag.LogRepo,
		LogFile = CommandFlag.LogFile,
		Push = CommandFlag.Push,
		Pull = CommandFlag.Pull,
		Update = CommandFlag.Update,
		Clone = CommandFlag.Clone,
		Commit = CommandFlag.Commit,
		Add = CommandFlag.Add,
		Remove = CommandFlag.Remove,
		Diff = CommandFlag.Diff,
		Rename = CommandFlag.Rename,
		Revert = CommandFlag.Revert,
		Rollback = CommandFlag.Rollback,
		Merge = CommandFlag.Merge,
		Branch = CommandFlag.Branch,
		Create = CommandFlag.Create,
		Annotate = CommandFlag.Annotate,
		Tag = CommandFlag.Tag,
		Unmark = CommandFlag.Unmark,
		Ignore = CommandFlag.Ignore,
		StopIgnore = CommandFlag.StopIgnore,
		View = CommandFlag.View,
	}


	public class Controller
	{
		Model m_model;
		MainForm m_mainForm;

        // MainForm is passed to the controller because MainForm is considered to be the View, or rather the main view.
		public Controller(Model model, MainForm mainForm)
		{
			m_model = model;
			m_mainForm = mainForm;
		}


		/** @brief Commit files in the currently selected folder.
		 * 
		 *  @throws ArgumentException	If the folderPath argument does not refer to a valid SCC repository.
		 * */
		public void Commit(IWin32Window owner)
		{
			string currentFolder = m_mainForm.GetCurrentFolder();
			if (!m_model.IsFolderScc(currentFolder))
			{
				throw new ArgumentException("The path specified is not a source code controlled directory.");
			}
			// Find out if there is any reason to commit. First, it must be that there are files that are either 
			// modified, added, or removed. Or, the current branch must have a name other than default. This is 
			// because you need to use the commit command to close a branch.
			FileStatusFlag flags = FileStatusFlag.Modified | FileStatusFlag.Added | FileStatusFlag.Removed;
			List<FileItem> fileList = m_model.GetFileList(currentFolder, flags);

			string currentBranch = m_model.GetCurrentBranch(currentFolder);

			if ((fileList.Count == 0) && (currentBranch.Equals("default")))
			{
				System.Windows.Forms.MessageBox.Show("No changes to commit.", "Zync");
				return;
			}

			CommitForm cf = new CommitForm(m_model, this, currentFolder);

			DialogResult result = cf.ShowDialog(owner);
			if (result == DialogResult.OK)
			{
				CommitParams commitParams = cf.GetCommitParams();
				if (commitParams.isValidCommit)
				{
					m_model.CommitFiles(commitParams);
				}
			}
		}

		//public bool CommitFiles(List<string> filesToCommit, string commitMessage)
		//{
		//    return m_model.CommitFiles(filesToCommit, commitMessage);
		//}

		/** @brief Show the logs of the selected files.
		**/
		public void LogFiles(IWin32Window owner, bool shiftDown, string folder, List<string> files)
		{

			LogParams lfp = new LogParams();

			lfp.logByDateTime = false;
			lfp.logByRevision = false;
			lfp.follow = false;

			if (!Properties.Settings.Default.LogFileFormDisplayOnlyOnShift || shiftDown)
			{

				LogFileParamForm lfpf = new LogFileParamForm();
				DialogResult res = lfpf.ShowDialog(owner);
				if (res == DialogResult.Cancel)
				{
					return;
				}

				lfp = lfpf.GetLogFileParams();
			}

			lfp.repoPath = folder;
			foreach (string s in files)
			{
				// I'm not sure how this works...each time through the loop, I can overwrite 
				// the previous reference to a log form, and the function can exit, and I   
				// no longer have no references to the form. Yet the form stays in existence 
				// until I manually close it. Does the framework keep a reference somewhere?
				lfp.filename = s;
				FileLogForm flf = new FileLogForm(m_model, this, lfp);
				flf.Show(owner);
			}
		}

		public void AddFiles(string folder, List<string> files)
		{
			// put up dialog if shift is hit

			AddParams addParams;
			addParams.files = files;
			addParams.repoPath = folder;

			m_model.AddFiles(addParams);
		}

		public void RemoveFiles(string folder, List<string> files)
		{

			List<FileItem> selectedFileItems = m_model.GetFileStatus(folder, files);

			RemoveForm rmf = new RemoveForm(selectedFileItems);

			DialogResult res = rmf.ShowDialog();
			if (res == DialogResult.OK)
			{

				RemoveParams removeParams = rmf.GetRemoveParams();
				removeParams.files = files;
				removeParams.repoPath = folder;

				m_model.RemoveFiles(removeParams);
			}
			return;

		}
		public void RevertFiles(string folder, List<string> files)
		{
			// put up dialog if shift is hit
			RevertParams revertParams;

			revertParams.repoPath = folder;
			revertParams.files = files;

			m_model.RevertFiles(revertParams);
		}

		public void IgnoreFiles(string folder, List<string> files)
		{
			// put up dialog if shift is hit

			IgnoreParams ignoreParams;
			ignoreParams.repoPath = folder;
			ignoreParams.files = files;


			m_model.IgnoreFiles(ignoreParams);
		}

		public void StopIgnoringFiles(string folder, List<string> files)
		{
			// put up dialog if shift is hit
			IgnoreParams ignoreParams;
			ignoreParams.repoPath = folder;
			ignoreParams.files = files;

			m_model.StopIgnoringSelectedFiles(ignoreParams);
		}


		/** @brief Annotates ones or more files in a given repository folder.
		 * 
		 * To annotate a single file, use a List with a single filename. This can be expressed easily 
		 * using the initialization:   new List<string>{myfilename}
		 * 
		 */
		public void AnnotateFile(IWin32Window owner, bool shiftDown, string repository, string file, string revision)
		{
			Debug.Assert(file != String.Empty);

			AnnotateParams annotateParams = new AnnotateParams();

			annotateParams.annotateRevision = false;
			annotateParams.showRevision = Properties.Settings.Default.AnnotateShowRevision;
			annotateParams.showChangeset = Properties.Settings.Default.AnnotateShowChangeset;
			annotateParams.showAuthor = Properties.Settings.Default.AnnotateShowUser;
			annotateParams.showDate = Properties.Settings.Default.AnnotateShowDate;
			annotateParams.showLineNumber = Properties.Settings.Default.AnnotateShowLineNumber;
			annotateParams.dontFollow = Properties.Settings.Default.AnnotateDontFollow;

			if (!Properties.Settings.Default.AnnotateFormDisplayOnlyOnShift || shiftDown)
			{
				AnnotateParamForm annotateParamsForm = new AnnotateParamForm(revision);
				DialogResult res = annotateParamsForm.ShowDialog(owner);
				if (res == DialogResult.Cancel)
				{
					return;
				}

				annotateParams = annotateParamsForm.GetAnnotateParams();
			}

			annotateParams.annotateRevision = !String.IsNullOrEmpty(annotateParams.revisionToAnnotate);
			annotateParams.repoPath = repository;
			annotateParams.showFilename = true;
			annotateParams.showRevision = true;
			annotateParams.showChangeset = true;
			annotateParams.showAuthor = true;
			annotateParams.showDate = true;
			annotateParams.showLineNumber = true;
			annotateParams.filename = file;

			List<AnnotateItem> annotateList = m_model.GetAnnotate(annotateParams);
			if (annotateList.Count == 0)
			{
				MessageBox.Show("No lines to display.");
			}
			else
			{
				AnnotateForm annotateForm = new AnnotateForm(m_model, annotateList, file);
				annotateForm.Show();
			}


		}


		/** @brief For one or more files, uses the external diff to show the difference between 
		 *			the working version and the latest commited version.
		 * 
		 *	@param folder	The full path to the repository where the files reside.
		 */
		public void DiffWorkingFiles(string folder, List<string> files)
		{
			List<string> selectedFiles = m_mainForm.GetSelectedFiles();
			DiffParams diffParams;
			diffParams.revision1 = diffParams.revision2 = string.Empty;
			diffParams.repoPath = folder;
			bool useExternalDiff = Properties.Settings.Default.PrefUseExternalDiff;
			diffParams.extCommand = (useExternalDiff) ?	Properties.Settings.Default.PrefExternalDiffCommand : "";

			foreach (string fi in files)
			{
				diffParams.filename = fi;

				if (useExternalDiff)
				{
					m_model.GetDiff(diffParams);
				}
				else
				{

					LogForm logForm = new LogForm();
					logForm.Text = "Diff " + fi;
					logForm.DiffHighlighting = true;
					logForm.LogText = SCC.Instance.GetDiff(diffParams);
					logForm.Show();
				}
			}

		}

		/** @brief Uses the external diff to show the difference between two revisions of a single file.
		 */ 
		public void DiffFile(DiffParams diffParams)
		{
			bool useExternalDiff = Properties.Settings.Default.PrefUseExternalDiff;
			diffParams.extCommand = (useExternalDiff) ? Properties.Settings.Default.PrefExternalDiffCommand : "";

			if (useExternalDiff)
			{
				SCC.Instance.GetDiff(diffParams);
			}
			else
			{

				LogForm logForm = new LogForm();
				logForm.Text = "Diff " + diffParams.filename;
				logForm.DiffHighlighting = true;
				logForm.LogText = m_model.GetDiff(diffParams);
				logForm.Show();
			}
		}

		public void RenameFile(IWin32Window owner, string folder, string filename, FileStatus fileStatus)
		{
			List<string> selectedFiles = m_mainForm.GetSelectedFiles();
			if (selectedFiles.Count != 1)
			{
				throw new Exception("Can only rename one file at a time but there are currently " + selectedFiles.Count + " files selected.");
			}

			RenameForm renameForm = new RenameForm(folder, filename, fileStatus);
			DialogResult result = renameForm.ShowDialog(owner);
			if (result == DialogResult.OK)
			{

				RenameParams renameParams = new RenameParams();
				renameParams.repoPath = folder;
				renameParams.oldFilename = renameForm.FromFilename;
				renameParams.newFilename = renameForm.ToFilename;
				renameParams.after = renameForm.After;

				m_model.RenameFile(renameParams);
			}


		}


		public void ViewSelectedRepository()
		{
			string currentFolder = m_mainForm.GetCurrentFolder();
			if (m_model.IsFolderScc(currentFolder))
			{
				SCC.Instance.ViewRepository(currentFolder);
			}
		}

		public void LogRepository(IWin32Window owner, string folder)
		{
#if false
			if (m_model.CurrentFolderIsScc)
			{
				LogForm logForm = new LogForm();
				logForm.Text = "Log Result";
				logForm.LogText = SCC.Instance.LogRepository(m_model.CurrentFolder);
				logForm.Show();
			}
#endif

			LogParams lp = new LogParams();

			lp.logByDateTime = false;
			lp.logByRevision = false;
			lp.follow = false;
			lp.filename = "";
			lp.repoPath = folder;


			RepoLogForm rlf = new RepoLogForm(m_model, this, lp);
			rlf.Show(owner);
		}

		public void RollbackRepository(IWin32Window owner, string folder)
		{

			if (m_model.IsFolderScc(folder))
			{
				//DialogResult result = MessageBox.Show(owner, "Are you sure you want to rollback the previous commit, push or pull?", "Rollback", MessageBoxButtons.YesNo);

				RollbackForm rbForm = new RollbackForm();

				DialogResult result = rbForm.ShowDialog(owner);

				if (result == DialogResult.Yes)
				{
					m_model.RollbackRepository(folder);
				}
			}
		}

		public void UnmarkResolvedFile(string filename)
		{
			m_model.UnmarkResolvedFile(m_mainForm.GetCurrentFolder() , filename);
		}

		public void BranchRepository(IWin32Window owner, string folder)
		{
			BranchParams branchParams;
			BranchForm branchForm = new BranchForm();
			DialogResult result = branchForm.ShowDialog(owner);

			if (result == DialogResult.OK)
			{
				branchParams.set = branchForm.Set;
				branchParams.clean = branchForm.Clean;
				branchParams.branchName = branchForm.BranchName;
				branchParams.force = branchForm.Force;
				branchParams.repoPath = folder;

				m_model.Branch(branchParams);
			}

		}

		public void Clone()
		{
			CloneForm cloneForm = new CloneForm(m_model, this, m_mainForm.GetCurrentFolder());
			cloneForm.ShowDialog();
		}

		public void CloneRepository(string sourcePath, string destinationPath, bool update, bool compressedTransfer)
		{
			m_model.CloneRepository(sourcePath, destinationPath, update, compressedTransfer);
		}

		/** @brief Create a repository in a specified folder

			The folder is not necessarily the current folder and if not, is not 
			made the selected folder after creating the repository.
		*/
		public void CreateRepository(string folder)
		{
			m_model.CreateRepositoryInFolder(folder);
		}

		/** @brief Updates a repository to the revision specified by the user in the GUI

			@param owner		Owner of the current window.
			@param shiftDown	If the application setting UpdateFormDisplayOnlyOnShift is true, then 
								the Update dialog will only be shown if shiftDown is true.
								If the application setting UpdateFormDisplayOnlyOnShift is false, then 
								this shiftDown parameter is ignored.
			@param folder		The folder of the repository to update.
			@param toRevision	If this string is not the empty string, it is the revision to populate the dialog 
								with. Or, if the dialog will not be shown (due to UpdateFormDisplayOnlyOnShift=true 
								and shiftDown=false) then immediately update to this revision. 
		**/
		public void UpdateRepository(IWin32Window owner, bool shiftDown, string folder, string toRevision)
		{
			UpdateParams updateParams = new UpdateParams();
			updateParams.byDateTime = false;
			updateParams.clean = false;
			if (toRevision != String.Empty)
			{
				updateParams.byRevision = true;
				updateParams.revision = toRevision;
			}
			else
			{
				updateParams.byRevision = false;
			}

			if (!Properties.Settings.Default.UpdateFormDisplayOnlyOnShift || shiftDown)
			{
				UpdateForm updateForm = new UpdateForm(toRevision);

				DialogResult result = updateForm.ShowDialog(owner);

				if (result == DialogResult.OK)
				{
					updateParams.repoPath = folder;
					updateParams.byDateTime = updateForm.ByDateTime;
					updateParams.byRevision = updateForm.ByRevision;
					updateParams.dateTime = updateForm.DateAndTime;
					updateParams.revision = updateForm.Revision;
					updateParams.clean = updateForm.GetCleanCopy;
				}
				else
				{
					return;
				}
			}

			m_model.UpdateRepository(updateParams);
		}

		/** @brief Executes a Pull command in the specified reposiroty folder.
		 * 
		 *	A dialog is displayed to choose the repository to pull from. If the dialog has been 
		 *	supressed, it will pull from the default repository.
		 */
		public void Pull(IWin32Window owner, bool shiftDown, string folder)
		{
			PullParams pullParams = new PullParams();
			pullParams.fromDefaultRepository = true;
			pullParams.byRevision = false;
			pullParams.update = false;
			pullParams.force = false;

			string defPush = m_model.GetDefaultPushRepository(folder);

			if (!Properties.Settings.Default.PullFormDisplayOnlyOnShift || shiftDown || (defPush == String.Empty))
			{
				PullForm pullForm = new PullForm(m_model, defPush);

				DialogResult result = pullForm.ShowDialog(owner);

				if (result == DialogResult.OK)
				{
					pullParams.repoPath = folder;
					pullParams.fromDefaultRepository = pullForm.FromDefault;
					pullParams.otherRepository = pullForm.OtherRepositoryPath;
					pullParams.byRevision = pullForm.ByRevision;
					pullParams.revision = pullForm.Revision;
					pullParams.update = pullForm.UpdateToNewTip;
					pullParams.force = pullForm.ForceEvenIfUnrelated;

				}
				else
				{
					return;
				}
			}

			m_model.PullFrom(pullParams);
		}

		/** @brief Executes a Push command in the specified reposiroty folder.
		 * 
		 *	A dialog is displayed to choose the repository to push to. If the dialog has been 
		 *	supressed, it will push to the default repository.
		 */
		public void Push(IWin32Window owner, string folder)
		{
			PushParams pushParams = new PushParams();
			pushParams.toDefaultRepository = true;
			pushParams.upToRevision = false;
			pushParams.allowNewBranch = false;
			pushParams.force = false;

			string defPush = m_model.GetDefaultPushRepository(folder);

			// Always show Push form---since this is a destructive operation, displaying 
			// this form serves as a warning.
			PushForm pushForm = new PushForm(m_model, defPush);

			DialogResult result = pushForm.ShowDialog(owner);

			if (result == DialogResult.OK)
			{
				pushParams.repoPath = folder;
				pushParams.toDefaultRepository = pushForm.ToDefault;
				pushParams.otherRepository = pushForm.OtherRepositoryPath;
				pushParams.upToRevision = pushForm.UpToRevision;
				pushParams.revision = pushForm.Revision;
				pushParams.force = pushForm.ForceEvenIfUnrelated;
				pushParams.allowNewBranch = pushForm.AllowNewBranch;
			}
			else
			{
				return;
			}


			m_model.PushTo(pushParams);
		}

		public void MergeRepository(IWin32Window owner, bool shiftDown, string folder)
		{
			MergeParams mergeParams = new MergeParams();
			mergeParams.withRevision = false;
			mergeParams.force = false;

			if (!Properties.Settings.Default.MergeFormDisplayOnlyOnShift || shiftDown || (m_model.GetNumberOfHeads(m_mainForm.GetCurrentFolder()) > 2))
			{

				MergeForm mergeForm = new MergeForm(m_model, folder);

				DialogResult result = mergeForm.ShowDialog(owner);

				if (result == DialogResult.OK)
				{
					mergeParams.repoPath = folder;
					mergeParams.withRevision= mergeForm.WithRevision;
					mergeParams.revision = mergeForm.Revision;
					mergeParams.force = mergeForm.Force;

				}
				else
				{
					return;
				}
			}

			m_model.MergeCurrentRepository(mergeParams);
		}

		/** @brief Shows the tag dialog to get tag parameters and then applies (or removes tags.
		 * 
		 *	@param owner		The parent window.
		 *	@param revision		The revision to tag, which may be a revision number or changeset ID. 
		 *						This may be left empty, in which case the tag dialog will display a 
		 *						textbox for the user to enter a revision number.
		 *	@retval				true if the tag command was issued.
		 *						false if the tag command was cancelled.
		 **/
		// The North Sea and Rameses III - Night Blossoms written in Sanskrit
		public bool TagRevision(IWin32Window owner, string revision, string changeset)
		{
			TagForm tagForm = new TagForm(revision, changeset);
			DialogResult result = tagForm.ShowDialog(owner);

			if (result == DialogResult.OK)
			{
				TagParams tagParams = tagForm.GetTagParams();
				tagParams.repoPath = m_mainForm.GetCurrentFolder();

				m_model.Tag(tagParams);
				return true;
			}
			return false;
		}


		public void SetCommandOutput(CommandOutput outputDelegate)
		{
			SCC.Instance.SetCommandOutput(outputDelegate);
		}

		public CommandFlags GetValidCommands(string repositoryPath, List<string> selectedFilenames)
		{
			CommandFlags cmdFlags = CommandFlags.None;

			bool currentFolderIsScc = m_model.IsFolderScc(repositoryPath);
			if (currentFolderIsScc)
			{
				cmdFlags |= CommandFlags.Push | CommandFlags.Pull | CommandFlags.Merge | CommandFlags.Update |
							CommandFlags.Commit | CommandFlags.LogRepo | CommandFlags.Rollback | CommandFlags.Branch;

				if(m_model.HasViewer)
				{
					cmdFlags |= CommandFlags.View;

				}
			}
			else
			{
				cmdFlags |= CommandFlags.Create;
			}

			// Build a list of the selected files with file status
			List<FileItem> selectedFileItems = m_model.GetFileStatus(repositoryPath, selectedFilenames);
			
			// Diff File
			if (selectedFileItems.Count > 0)
			{
				cmdFlags |= CommandFlags.Diff;
				foreach (FileItem fi in selectedFileItems)
				{
					if ((fi.status != FileStatus.Modified) && (fi.status != FileStatus.Clean))
					{
                        cmdFlags ^= CommandFlags.Diff;  // turn off flag
					}
				}
			}


			// Annotate File
			if (selectedFileItems.Count == 1 )
			{
                cmdFlags |= CommandFlags.Annotate;
				foreach (FileItem fi in selectedFileItems)
				{
					if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
						cmdFlags ^= CommandFlags.Annotate;  // turn off flag
					}
				}
			}

			// Log File
			if (selectedFileItems.Count > 0)
			{
                cmdFlags |= CommandFlags.LogFile;
                foreach (FileItem fi in selectedFileItems)
				{
					if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
                        cmdFlags ^= CommandFlags.LogFile;  // turn off flag
					}
				}
			}
	
			// Add File
			if (selectedFileItems.Count > 0)
			{
                cmdFlags |= CommandFlags.Add;
				foreach(FileItem fi in selectedFileItems)
				{
					if ((fi.status != FileStatus.NotTracked) && (fi.status != FileStatus.Removed))
					{
                        cmdFlags ^= CommandFlags.Add;  // turn off flag
                        break;
					}
				}
			}

			// Remove File
			if (selectedFileItems.Count > 0)
			{
                cmdFlags |= CommandFlags.Remove;
				foreach (FileItem fi in selectedFileItems)
				{
					if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
                        cmdFlags ^= CommandFlags.Remove;  // turn off flag
                        break;
					}
				}
			}

			// Revert File
			if (selectedFileItems.Count > 0)
			{
                cmdFlags |= CommandFlags.Revert;
                foreach (FileItem fi in selectedFileItems)
				{
					if ((fi.status != FileStatus.Modified) && (fi.status != FileStatus.Added) && (fi.status != FileStatus.Removed))
					{
                        cmdFlags ^= CommandFlags.Revert;  // turn off flag
                    }
				}
			}

			// Rename File
			if (selectedFileItems.Count == 1)
			{
				FileItem fi = selectedFileItems[0];
				// The NotTrack status has to be allowed for rename so that you can use the -After option to rename files that no longer have the name of the tracked file.
				if (fi.status == FileStatus.Clean || fi.status == FileStatus.Modified || fi.status == FileStatus.Deleted || fi.status == FileStatus.NotTracked)
				{
                    cmdFlags |= CommandFlags.Rename;  // turn ON flag
                }
			}

			// Ignore File
			if (selectedFileItems.Count > 0)
			{
                cmdFlags |= CommandFlags.Ignore;
                foreach (FileItem fi in selectedFileItems)
				{
					if (fi.status != FileStatus.NotTracked)
					{
                        cmdFlags ^= CommandFlags.Ignore;  // turn off flag
                    }
				}
			}

			// Stop Ignoring File
			if (selectedFileItems.Count > 0)
			{
                cmdFlags |= CommandFlags.StopIgnore;
                foreach (FileItem fi in selectedFileItems)
				{
					if (fi.status != FileStatus.Ignored)
					{
                        cmdFlags ^= CommandFlags.StopIgnore;  // turn off flag
                    }
				}
			}


			return cmdFlags;

		}
	}
}
