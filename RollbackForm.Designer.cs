﻿namespace Zync
{
	partial class RollbackForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RollbackForm));
			this.rollbackFormLabel1 = new System.Windows.Forms.Label();
			this.rollbackFormButtonYes = new System.Windows.Forms.Button();
			this.rollbackFormButtonCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// rollbackFormLabel1
			// 
			this.rollbackFormLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.rollbackFormLabel1.Location = new System.Drawing.Point(12, 20);
			this.rollbackFormLabel1.Name = "rollbackFormLabel1";
			this.rollbackFormLabel1.Size = new System.Drawing.Size(372, 108);
			this.rollbackFormLabel1.TabIndex = 0;
			this.rollbackFormLabel1.Text = resources.GetString("rollbackFormLabel1.Text");
			// 
			// rollbackFormButtonYes
			// 
			this.rollbackFormButtonYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.rollbackFormButtonYes.DialogResult = System.Windows.Forms.DialogResult.Yes;
			this.rollbackFormButtonYes.Location = new System.Drawing.Point(177, 135);
			this.rollbackFormButtonYes.Name = "rollbackFormButtonYes";
			this.rollbackFormButtonYes.Size = new System.Drawing.Size(75, 23);
			this.rollbackFormButtonYes.TabIndex = 1;
			this.rollbackFormButtonYes.Text = "Yes";
			this.rollbackFormButtonYes.UseVisualStyleBackColor = true;
			// 
			// rollbackFormButtonCancel
			// 
			this.rollbackFormButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.rollbackFormButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.rollbackFormButtonCancel.Location = new System.Drawing.Point(274, 135);
			this.rollbackFormButtonCancel.Name = "rollbackFormButtonCancel";
			this.rollbackFormButtonCancel.Size = new System.Drawing.Size(75, 23);
			this.rollbackFormButtonCancel.TabIndex = 2;
			this.rollbackFormButtonCancel.Text = "Cancel";
			this.rollbackFormButtonCancel.UseVisualStyleBackColor = true;
			// 
			// RollbackForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(396, 170);
			this.Controls.Add(this.rollbackFormButtonCancel);
			this.Controls.Add(this.rollbackFormButtonYes);
			this.Controls.Add(this.rollbackFormLabel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(404, 204);
			this.Name = "RollbackForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Zync Rollback";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label rollbackFormLabel1;
		private System.Windows.Forms.Button rollbackFormButtonYes;
		private System.Windows.Forms.Button rollbackFormButtonCancel;
	}
}