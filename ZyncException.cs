﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zync
{

	public class ZyncException : System.Exception
	{
		public ZyncException() { }
		public ZyncException(string message) : base(message) { }
		public ZyncException(string message, System.Exception inner) : base(message, inner) { }

		// Constructor needed for serialization 
		// when exception propagates from a remoting server to the client.
		protected ZyncException(System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) { }
	}
}
