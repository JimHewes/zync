﻿namespace Zync
{
	partial class MergeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MergeForm));
			this.headsWarningLabel = new System.Windows.Forms.Label();
			this.withRevisionCheckBox = new System.Windows.Forms.CheckBox();
			this.revisionTextBox = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.forceCheckBox = new System.Windows.Forms.CheckBox();
			this.mergeCancelButton = new System.Windows.Forms.Button();
			this.mergeOkButton = new System.Windows.Forms.Button();
			this.mergeShiftKeyCheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// headsWarningLabel
			// 
			this.headsWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.headsWarningLabel.Location = new System.Drawing.Point(12, 9);
			this.headsWarningLabel.Name = "headsWarningLabel";
			this.headsWarningLabel.Size = new System.Drawing.Size(379, 34);
			this.headsWarningLabel.TabIndex = 0;
			this.headsWarningLabel.Text = "Since the repository has more than two heads, you must select a revision to merge" +
				" with.";
			// 
			// withRevisionCheckBox
			// 
			this.withRevisionCheckBox.AutoSize = true;
			this.withRevisionCheckBox.Location = new System.Drawing.Point(12, 56);
			this.withRevisionCheckBox.Name = "withRevisionCheckBox";
			this.withRevisionCheckBox.Size = new System.Drawing.Size(90, 17);
			this.withRevisionCheckBox.TabIndex = 1;
			this.withRevisionCheckBox.Text = "With revision:";
			this.toolTip1.SetToolTip(this.withRevisionCheckBox, "Merge with the specified revision");
			this.withRevisionCheckBox.UseVisualStyleBackColor = true;
			this.withRevisionCheckBox.CheckedChanged += new System.EventHandler(this.OnWithRevisionCheckBoxChanged);
			// 
			// revisionTextBox
			// 
			this.revisionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.revisionTextBox.Location = new System.Drawing.Point(109, 53);
			this.revisionTextBox.Name = "revisionTextBox";
			this.revisionTextBox.Size = new System.Drawing.Size(282, 20);
			this.revisionTextBox.TabIndex = 2;
			this.toolTip1.SetToolTip(this.revisionTextBox, "Revision to merge with");
			this.revisionTextBox.TextChanged += new System.EventHandler(this.OnRevisionTextBoxTextChanged);
			// 
			// forceCheckBox
			// 
			this.forceCheckBox.AutoSize = true;
			this.forceCheckBox.Location = new System.Drawing.Point(12, 82);
			this.forceCheckBox.Name = "forceCheckBox";
			this.forceCheckBox.Size = new System.Drawing.Size(53, 17);
			this.forceCheckBox.TabIndex = 3;
			this.forceCheckBox.Text = "Force";
			this.toolTip1.SetToolTip(this.forceCheckBox, "Force a merge with outstanding changes");
			this.forceCheckBox.UseVisualStyleBackColor = true;
			// 
			// mergeCancelButton
			// 
			this.mergeCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.mergeCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.mergeCancelButton.Location = new System.Drawing.Point(319, 125);
			this.mergeCancelButton.Name = "mergeCancelButton";
			this.mergeCancelButton.Size = new System.Drawing.Size(75, 23);
			this.mergeCancelButton.TabIndex = 16;
			this.mergeCancelButton.Text = "Cancel";
			this.toolTip1.SetToolTip(this.mergeCancelButton, "Cancel the merge");
			this.mergeCancelButton.UseVisualStyleBackColor = true;
			// 
			// mergeOkButton
			// 
			this.mergeOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.mergeOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.mergeOkButton.Location = new System.Drawing.Point(230, 125);
			this.mergeOkButton.Name = "mergeOkButton";
			this.mergeOkButton.Size = new System.Drawing.Size(75, 23);
			this.mergeOkButton.TabIndex = 15;
			this.mergeOkButton.Text = "Merge";
			this.toolTip1.SetToolTip(this.mergeOkButton, "Merge");
			this.mergeOkButton.UseVisualStyleBackColor = true;
			this.mergeOkButton.Click += new System.EventHandler(this.OnClickedOkButton);
			// 
			// mergeShiftKeyCheckBox
			// 
			this.mergeShiftKeyCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.mergeShiftKeyCheckBox.AutoSize = true;
			this.mergeShiftKeyCheckBox.Location = new System.Drawing.Point(12, 129);
			this.mergeShiftKeyCheckBox.Name = "mergeShiftKeyCheckBox";
			this.mergeShiftKeyCheckBox.Size = new System.Drawing.Size(173, 17);
			this.mergeShiftKeyCheckBox.TabIndex = 14;
			this.mergeShiftKeyCheckBox.Text = "Display only if Shift key is down";
			this.toolTip1.SetToolTip(this.mergeShiftKeyCheckBox, "If checked, this dialog will not be shown when the merge command is invoked unles" +
					"s the Shift key is held down");
			this.mergeShiftKeyCheckBox.UseVisualStyleBackColor = true;
			// 
			// MergeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(403, 160);
			this.Controls.Add(this.mergeCancelButton);
			this.Controls.Add(this.mergeOkButton);
			this.Controls.Add(this.mergeShiftKeyCheckBox);
			this.Controls.Add(this.forceCheckBox);
			this.Controls.Add(this.revisionTextBox);
			this.Controls.Add(this.withRevisionCheckBox);
			this.Controls.Add(this.headsWarningLabel);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(411, 194);
			this.Name = "MergeForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Merge";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label headsWarningLabel;
		private System.Windows.Forms.CheckBox withRevisionCheckBox;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.TextBox revisionTextBox;
		private System.Windows.Forms.CheckBox forceCheckBox;
		private System.Windows.Forms.Button mergeCancelButton;
		private System.Windows.Forms.Button mergeOkButton;
		private System.Windows.Forms.CheckBox mergeShiftKeyCheckBox;
	}
}