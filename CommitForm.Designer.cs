﻿namespace Zync
{
	partial class CommitForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.Label commitMsgLabel;
			System.Windows.Forms.Label recentMsgLabel;
			System.Windows.Forms.Panel fileListPanel;
			System.Windows.Forms.Label fileListLabel;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommitForm));
			this.CloseBranchCheckBox = new System.Windows.Forms.CheckBox();
			this.checkAllButton = new System.Windows.Forms.Button();
			this.fileListBox = new System.Windows.Forms.CheckedListBox();
			this.commitCancelButton = new System.Windows.Forms.Button();
			this.upperSplitContainer = new System.Windows.Forms.SplitContainer();
			this.commitMsgPanel = new System.Windows.Forms.Panel();
			this.recentComboBox = new System.Windows.Forms.ComboBox();
			this.commitMsgTextBox = new System.Windows.Forms.TextBox();
			this.lowerSplitContainer = new System.Windows.Forms.SplitContainer();
			this.diffFileLabel = new System.Windows.Forms.Label();
			this.diffRichTextBox = new System.Windows.Forms.RichTextBox();
			this.commitButton = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			commitMsgLabel = new System.Windows.Forms.Label();
			recentMsgLabel = new System.Windows.Forms.Label();
			fileListPanel = new System.Windows.Forms.Panel();
			fileListLabel = new System.Windows.Forms.Label();
			fileListPanel.SuspendLayout();
			this.upperSplitContainer.Panel1.SuspendLayout();
			this.upperSplitContainer.Panel2.SuspendLayout();
			this.upperSplitContainer.SuspendLayout();
			this.commitMsgPanel.SuspendLayout();
			this.lowerSplitContainer.Panel1.SuspendLayout();
			this.lowerSplitContainer.Panel2.SuspendLayout();
			this.lowerSplitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// commitMsgLabel
			// 
			commitMsgLabel.AutoSize = true;
			commitMsgLabel.Location = new System.Drawing.Point(7, 7);
			commitMsgLabel.Name = "commitMsgLabel";
			commitMsgLabel.Size = new System.Drawing.Size(89, 13);
			commitMsgLabel.TabIndex = 2;
			commitMsgLabel.Text = "Commit message:";
			// 
			// recentMsgLabel
			// 
			recentMsgLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			recentMsgLabel.AutoSize = true;
			recentMsgLabel.Location = new System.Drawing.Point(12, 145);
			recentMsgLabel.Name = "recentMsgLabel";
			recentMsgLabel.Size = new System.Drawing.Size(45, 13);
			recentMsgLabel.TabIndex = 3;
			recentMsgLabel.Text = "Recent:";
			// 
			// fileListPanel
			// 
			fileListPanel.Controls.Add(this.CloseBranchCheckBox);
			fileListPanel.Controls.Add(this.checkAllButton);
			fileListPanel.Controls.Add(fileListLabel);
			fileListPanel.Controls.Add(this.fileListBox);
			fileListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			fileListPanel.Location = new System.Drawing.Point(0, 0);
			fileListPanel.Name = "fileListPanel";
			fileListPanel.Size = new System.Drawing.Size(612, 170);
			fileListPanel.TabIndex = 0;
			// 
			// CloseBranchCheckBox
			// 
			this.CloseBranchCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.CloseBranchCheckBox.AutoSize = true;
			this.CloseBranchCheckBox.Location = new System.Drawing.Point(10, 138);
			this.CloseBranchCheckBox.Name = "CloseBranchCheckBox";
			this.CloseBranchCheckBox.Size = new System.Drawing.Size(88, 17);
			this.CloseBranchCheckBox.TabIndex = 3;
			this.CloseBranchCheckBox.Text = "Close branch";
			this.toolTip1.SetToolTip(this.CloseBranchCheckBox, "Optionally close the branch if it is a named branch");
			this.CloseBranchCheckBox.UseVisualStyleBackColor = true;
			// 
			// checkAllButton
			// 
			this.checkAllButton.Location = new System.Drawing.Point(10, 27);
			this.checkAllButton.Name = "checkAllButton";
			this.checkAllButton.Size = new System.Drawing.Size(86, 23);
			this.checkAllButton.TabIndex = 3;
			this.checkAllButton.Text = "(Un)check All";
			this.checkAllButton.UseVisualStyleBackColor = true;
			this.checkAllButton.Click += new System.EventHandler(this.OnClickedCheckAllButton);
			// 
			// fileListLabel
			// 
			fileListLabel.AutoSize = true;
			fileListLabel.Location = new System.Drawing.Point(7, 6);
			fileListLabel.Name = "fileListLabel";
			fileListLabel.Size = new System.Drawing.Size(42, 13);
			fileListLabel.TabIndex = 2;
			fileListLabel.Text = "File List";
			// 
			// fileListBox
			// 
			this.fileListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.fileListBox.CheckOnClick = true;
			this.fileListBox.FormattingEnabled = true;
			this.fileListBox.HorizontalScrollbar = true;
			this.fileListBox.IntegralHeight = false;
			this.fileListBox.Items.AddRange(new object[] {
            "dummy1",
            "dummy2",
            "dummy3"});
			this.fileListBox.Location = new System.Drawing.Point(111, 6);
			this.fileListBox.Name = "fileListBox";
			this.fileListBox.Size = new System.Drawing.Size(497, 161);
			this.fileListBox.TabIndex = 1;
			this.fileListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.OnFileListboxItemChecked);
			this.fileListBox.SelectedIndexChanged += new System.EventHandler(this.OnSelectedListBoxIndexChanged);
			this.fileListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnFileListboxMouseUp);
			// 
			// commitCancelButton
			// 
			this.commitCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.commitCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.commitCancelButton.Location = new System.Drawing.Point(530, 536);
			this.commitCancelButton.Name = "commitCancelButton";
			this.commitCancelButton.Size = new System.Drawing.Size(75, 23);
			this.commitCancelButton.TabIndex = 0;
			this.commitCancelButton.Text = "Cancel";
			this.commitCancelButton.UseVisualStyleBackColor = true;
			// 
			// upperSplitContainer
			// 
			this.upperSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.upperSplitContainer.BackColor = System.Drawing.SystemColors.Control;
			this.upperSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.upperSplitContainer.ForeColor = System.Drawing.SystemColors.ControlText;
			this.upperSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.upperSplitContainer.Name = "upperSplitContainer";
			this.upperSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// upperSplitContainer.Panel1
			// 
			this.upperSplitContainer.Panel1.Controls.Add(this.commitMsgPanel);
			this.upperSplitContainer.Panel1MinSize = 88;
			// 
			// upperSplitContainer.Panel2
			// 
			this.upperSplitContainer.Panel2.Controls.Add(this.lowerSplitContainer);
			this.upperSplitContainer.Size = new System.Drawing.Size(616, 530);
			this.upperSplitContainer.SplitterDistance = 173;
			this.upperSplitContainer.TabIndex = 1;
			// 
			// commitMsgPanel
			// 
			this.commitMsgPanel.Controls.Add(recentMsgLabel);
			this.commitMsgPanel.Controls.Add(commitMsgLabel);
			this.commitMsgPanel.Controls.Add(this.recentComboBox);
			this.commitMsgPanel.Controls.Add(this.commitMsgTextBox);
			this.commitMsgPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.commitMsgPanel.Location = new System.Drawing.Point(0, 0);
			this.commitMsgPanel.Name = "commitMsgPanel";
			this.commitMsgPanel.Size = new System.Drawing.Size(612, 169);
			this.commitMsgPanel.TabIndex = 0;
			// 
			// recentComboBox
			// 
			this.recentComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.recentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.recentComboBox.FormattingEnabled = true;
			this.recentComboBox.Location = new System.Drawing.Point(73, 142);
			this.recentComboBox.Name = "recentComboBox";
			this.recentComboBox.Size = new System.Drawing.Size(530, 21);
			this.recentComboBox.TabIndex = 1;
			this.recentComboBox.SelectionChangeCommitted += new System.EventHandler(this.OnRecentItemSelected);
			// 
			// commitMsgTextBox
			// 
			this.commitMsgTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.commitMsgTextBox.Location = new System.Drawing.Point(6, 26);
			this.commitMsgTextBox.Multiline = true;
			this.commitMsgTextBox.Name = "commitMsgTextBox";
			this.commitMsgTextBox.Size = new System.Drawing.Size(602, 110);
			this.commitMsgTextBox.TabIndex = 0;
			this.commitMsgTextBox.TextChanged += new System.EventHandler(this.OnCommitMessageTextChanged);
			// 
			// lowerSplitContainer
			// 
			this.lowerSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lowerSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lowerSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.lowerSplitContainer.Name = "lowerSplitContainer";
			this.lowerSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// lowerSplitContainer.Panel1
			// 
			this.lowerSplitContainer.Panel1.Controls.Add(fileListPanel);
			this.lowerSplitContainer.Panel1MinSize = 90;
			// 
			// lowerSplitContainer.Panel2
			// 
			this.lowerSplitContainer.Panel2.Controls.Add(this.diffFileLabel);
			this.lowerSplitContainer.Panel2.Controls.Add(this.diffRichTextBox);
			this.lowerSplitContainer.Panel2MinSize = 10;
			this.lowerSplitContainer.Size = new System.Drawing.Size(616, 353);
			this.lowerSplitContainer.SplitterDistance = 174;
			this.lowerSplitContainer.TabIndex = 0;
			// 
			// diffFileLabel
			// 
			this.diffFileLabel.AutoSize = true;
			this.diffFileLabel.Location = new System.Drawing.Point(7, 8);
			this.diffFileLabel.Name = "diffFileLabel";
			this.diffFileLabel.Size = new System.Drawing.Size(49, 13);
			this.diffFileLabel.TabIndex = 1;
			this.diffFileLabel.Text = "Filename";
			// 
			// diffRichTextBox
			// 
			this.diffRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.diffRichTextBox.Location = new System.Drawing.Point(6, 24);
			this.diffRichTextBox.Name = "diffRichTextBox";
			this.diffRichTextBox.ReadOnly = true;
			this.diffRichTextBox.Size = new System.Drawing.Size(603, 140);
			this.diffRichTextBox.TabIndex = 0;
			this.diffRichTextBox.Text = "";
			this.diffRichTextBox.WordWrap = false;
			// 
			// commitButton
			// 
			this.commitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.commitButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.commitButton.Location = new System.Drawing.Point(449, 536);
			this.commitButton.Name = "commitButton";
			this.commitButton.Size = new System.Drawing.Size(75, 23);
			this.commitButton.TabIndex = 2;
			this.commitButton.Text = "Commit";
			this.commitButton.UseVisualStyleBackColor = true;
			this.commitButton.Click += new System.EventHandler(this.OnCommitButtonClicked);
			// 
			// CommitForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(617, 571);
			this.Controls.Add(this.commitButton);
			this.Controls.Add(this.upperSplitContainer);
			this.Controls.Add(this.commitCancelButton);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(215, 280);
			this.Name = "CommitForm";
			this.Text = "Commit";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.Load += new System.EventHandler(this.OnFormLoad);
			fileListPanel.ResumeLayout(false);
			fileListPanel.PerformLayout();
			this.upperSplitContainer.Panel1.ResumeLayout(false);
			this.upperSplitContainer.Panel2.ResumeLayout(false);
			this.upperSplitContainer.ResumeLayout(false);
			this.commitMsgPanel.ResumeLayout(false);
			this.commitMsgPanel.PerformLayout();
			this.lowerSplitContainer.Panel1.ResumeLayout(false);
			this.lowerSplitContainer.Panel2.ResumeLayout(false);
			this.lowerSplitContainer.Panel2.PerformLayout();
			this.lowerSplitContainer.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button commitCancelButton;
		private System.Windows.Forms.SplitContainer upperSplitContainer;
		private System.Windows.Forms.SplitContainer lowerSplitContainer;
		private System.Windows.Forms.Panel commitMsgPanel;
		private System.Windows.Forms.TextBox commitMsgTextBox;
		private System.Windows.Forms.ComboBox recentComboBox;
		private System.Windows.Forms.RichTextBox diffRichTextBox;
		private System.Windows.Forms.Button commitButton;
		private System.Windows.Forms.Label diffFileLabel;
		private System.Windows.Forms.CheckedListBox fileListBox;
		private System.Windows.Forms.Button checkAllButton;
		private System.Windows.Forms.CheckBox CloseBranchCheckBox;
		private System.Windows.Forms.ToolTip toolTip1;

	}
}