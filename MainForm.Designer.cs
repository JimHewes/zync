﻿namespace Zync
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.browseWorkingFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.m_mainToolStripContainer = new System.Windows.Forms.ToolStripContainer();
			this.m_statusStrip = new System.Windows.Forms.StatusStrip();
			this.m_statusDefaultPushLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.m_statusBranchLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.m_statusNumHeadsLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.m_mainOuterSplitContainer = new System.Windows.Forms.SplitContainer();
			this.m_mainInnerSplitContainer = new System.Windows.Forms.SplitContainer();
			this.m_folderView = new Zync.FolderView();
			this.m_fileView = new Zync.FileView();
			this.m_commandOutputTextBox = new System.Windows.Forms.RichTextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.appToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.repositoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pushToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.commitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mergeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.branchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.cloneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.logRepoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.rollbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.diffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.annotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.revertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ignoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.stopIgnoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.m_fileViewToolStrip = new System.Windows.Forms.ToolStrip();
			this.m_cleanCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_modifiedCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_addedCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_removedCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_deletedCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_ignoredCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_notTrackedCheckbox = new System.Windows.Forms.ToolStripButton();
			this.m_fileActionToolStrip = new System.Windows.Forms.ToolStrip();
			this.m_difButton = new System.Windows.Forms.ToolStripButton();
			this.m_logButton = new System.Windows.Forms.ToolStripButton();
			this.m_annotateButton = new System.Windows.Forms.ToolStripButton();
			this.m_addButton = new System.Windows.Forms.ToolStripButton();
			this.m_removeButton = new System.Windows.Forms.ToolStripButton();
			this.m_revertButton = new System.Windows.Forms.ToolStripButton();
			this.m_ignoreButton = new System.Windows.Forms.ToolStripButton();
			this.m_stopIgnoringButton = new System.Windows.Forms.ToolStripButton();
			this.m_renameButton = new System.Windows.Forms.ToolStripButton();
			this.m_workingFolderToolStrip = new System.Windows.Forms.ToolStrip();
			this.m_workingFolderComboBox = new System.Windows.Forms.ToolStripComboBox();
			this.m_workingFolderBrowseButton = new System.Windows.Forms.ToolStripButton();
			this.m_repositoryActionToolStrip = new System.Windows.Forms.ToolStrip();
			this.m_pullButton = new System.Windows.Forms.ToolStripButton();
			this.m_pushButton = new System.Windows.Forms.ToolStripButton();
			this.m_updateButton = new System.Windows.Forms.ToolStripButton();
			this.m_commitButton = new System.Windows.Forms.ToolStripButton();
			this.m_mergeButton = new System.Windows.Forms.ToolStripButton();
			this.m_branchButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.m_cloneButton = new System.Windows.Forms.ToolStripButton();
			this.m_createButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.m_viewButton = new System.Windows.Forms.ToolStripButton();
			this.m_repoLogButton = new System.Windows.Forms.ToolStripButton();
			this.m_rollbackButton = new System.Windows.Forms.ToolStripButton();
			this.m_mainToolStripContainer.BottomToolStripPanel.SuspendLayout();
			this.m_mainToolStripContainer.ContentPanel.SuspendLayout();
			this.m_mainToolStripContainer.TopToolStripPanel.SuspendLayout();
			this.m_mainToolStripContainer.SuspendLayout();
			this.m_statusStrip.SuspendLayout();
			this.m_mainOuterSplitContainer.Panel1.SuspendLayout();
			this.m_mainOuterSplitContainer.Panel2.SuspendLayout();
			this.m_mainOuterSplitContainer.SuspendLayout();
			this.m_mainInnerSplitContainer.Panel1.SuspendLayout();
			this.m_mainInnerSplitContainer.Panel2.SuspendLayout();
			this.m_mainInnerSplitContainer.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.m_fileViewToolStrip.SuspendLayout();
			this.m_fileActionToolStrip.SuspendLayout();
			this.m_workingFolderToolStrip.SuspendLayout();
			this.m_repositoryActionToolStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolTip1
			// 
			this.toolTip1.AutoPopDelay = 20000;
			this.toolTip1.InitialDelay = 500;
			this.toolTip1.ReshowDelay = 100;
			// 
			// m_mainToolStripContainer
			// 
			// 
			// m_mainToolStripContainer.BottomToolStripPanel
			// 
			this.m_mainToolStripContainer.BottomToolStripPanel.Controls.Add(this.m_statusStrip);
			// 
			// m_mainToolStripContainer.ContentPanel
			// 
			this.m_mainToolStripContainer.ContentPanel.Controls.Add(this.m_mainOuterSplitContainer);
			this.m_mainToolStripContainer.ContentPanel.Size = new System.Drawing.Size(952, 391);
			this.m_mainToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_mainToolStripContainer.Location = new System.Drawing.Point(0, 0);
			this.m_mainToolStripContainer.Name = "m_mainToolStripContainer";
			this.m_mainToolStripContainer.Size = new System.Drawing.Size(952, 521);
			this.m_mainToolStripContainer.TabIndex = 11;
			this.m_mainToolStripContainer.Text = "mainToolStripContainer";
			// 
			// m_mainToolStripContainer.TopToolStripPanel
			// 
			this.m_mainToolStripContainer.TopToolStripPanel.Controls.Add(this.menuStrip1);
			this.m_mainToolStripContainer.TopToolStripPanel.Controls.Add(this.m_fileViewToolStrip);
			this.m_mainToolStripContainer.TopToolStripPanel.Controls.Add(this.m_fileActionToolStrip);
			this.m_mainToolStripContainer.TopToolStripPanel.Controls.Add(this.m_workingFolderToolStrip);
			this.m_mainToolStripContainer.TopToolStripPanel.Controls.Add(this.m_repositoryActionToolStrip);
			// 
			// m_statusStrip
			// 
			this.m_statusStrip.AllowMerge = false;
			this.m_statusStrip.AutoSize = false;
			this.m_statusStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.m_statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_statusDefaultPushLabel,
            this.m_statusBranchLabel,
            this.m_statusNumHeadsLabel});
			this.m_statusStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
			this.m_statusStrip.Location = new System.Drawing.Point(0, 0);
			this.m_statusStrip.Name = "m_statusStrip";
			this.m_statusStrip.Size = new System.Drawing.Size(952, 56);
			this.m_statusStrip.TabIndex = 10;
			this.m_statusStrip.Text = "statusStrip1";
			// 
			// m_statusDefaultPushLabel
			// 
			this.m_statusDefaultPushLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
			this.m_statusDefaultPushLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_statusDefaultPushLabel.Name = "m_statusDefaultPushLabel";
			this.m_statusDefaultPushLabel.Size = new System.Drawing.Size(936, 13);
			this.m_statusDefaultPushLabel.Text = "m_statusDefaultPushLabel";
			this.m_statusDefaultPushLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_statusBranchLabel
			// 
			this.m_statusBranchLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
			this.m_statusBranchLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_statusBranchLabel.Name = "m_statusBranchLabel";
			this.m_statusBranchLabel.Size = new System.Drawing.Size(936, 13);
			this.m_statusBranchLabel.Text = "m_statusBranchLabel";
			this.m_statusBranchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_statusNumHeadsLabel
			// 
			this.m_statusNumHeadsLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
			this.m_statusNumHeadsLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_statusNumHeadsLabel.Name = "m_statusNumHeadsLabel";
			this.m_statusNumHeadsLabel.Size = new System.Drawing.Size(936, 13);
			this.m_statusNumHeadsLabel.Text = "m_statusNumHeadsLabel";
			this.m_statusNumHeadsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_mainOuterSplitContainer
			// 
			this.m_mainOuterSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_mainOuterSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.m_mainOuterSplitContainer.Name = "m_mainOuterSplitContainer";
			this.m_mainOuterSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// m_mainOuterSplitContainer.Panel1
			// 
			this.m_mainOuterSplitContainer.Panel1.Controls.Add(this.m_mainInnerSplitContainer);
			// 
			// m_mainOuterSplitContainer.Panel2
			// 
			this.m_mainOuterSplitContainer.Panel2.Controls.Add(this.m_commandOutputTextBox);
			this.m_mainOuterSplitContainer.Size = new System.Drawing.Size(952, 391);
			this.m_mainOuterSplitContainer.SplitterDistance = 296;
			this.m_mainOuterSplitContainer.TabIndex = 4;
			// 
			// m_mainInnerSplitContainer
			// 
			this.m_mainInnerSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_mainInnerSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.m_mainInnerSplitContainer.Name = "m_mainInnerSplitContainer";
			// 
			// m_mainInnerSplitContainer.Panel1
			// 
			this.m_mainInnerSplitContainer.Panel1.Controls.Add(this.m_folderView);
			// 
			// m_mainInnerSplitContainer.Panel2
			// 
			this.m_mainInnerSplitContainer.Panel2.Controls.Add(this.m_fileView);
			this.m_mainInnerSplitContainer.Size = new System.Drawing.Size(952, 296);
			this.m_mainInnerSplitContainer.SplitterDistance = 303;
			this.m_mainInnerSplitContainer.TabIndex = 3;
			// 
			// m_folderView
			// 
			this.m_folderView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_folderView.HideSelection = false;
			this.m_folderView.ImageIndex = 0;
			this.m_folderView.Location = new System.Drawing.Point(0, 0);
			this.m_folderView.Name = "m_folderView";
			this.m_folderView.SelectedImageIndex = 0;
			this.m_folderView.Size = new System.Drawing.Size(303, 296);
			this.m_folderView.TabIndex = 0;
			// 
			// m_fileView
			// 
			this.m_fileView.AllowColumnReorder = true;
			this.m_fileView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_fileView.FullRowSelect = true;
			this.m_fileView.HideSelection = false;
			this.m_fileView.Location = new System.Drawing.Point(0, 0);
			this.m_fileView.Name = "m_fileView";
			this.m_fileView.Size = new System.Drawing.Size(645, 296);
			this.m_fileView.TabIndex = 0;
			this.m_fileView.UseCompatibleStateImageBehavior = false;
			this.m_fileView.View = System.Windows.Forms.View.Details;
			// 
			// m_commandOutputTextBox
			// 
			this.m_commandOutputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_commandOutputTextBox.Location = new System.Drawing.Point(0, 0);
			this.m_commandOutputTextBox.Name = "m_commandOutputTextBox";
			this.m_commandOutputTextBox.Size = new System.Drawing.Size(952, 91);
			this.m_commandOutputTextBox.TabIndex = 0;
			this.m_commandOutputTextBox.Text = "";
			// 
			// menuStrip1
			// 
			this.menuStrip1.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "MainMenuLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.appToolStripMenuItem,
            this.repositoryToolStripMenuItem,
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = global::Zync.Properties.Settings.Default.MainMenuLocation;
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(952, 24);
			this.menuStrip1.TabIndex = 2;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// appToolStripMenuItem
			// 
			this.appToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preferencesToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem,
            this.quitToolStripMenuItem});
			this.appToolStripMenuItem.Name = "appToolStripMenuItem";
			this.appToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
			this.appToolStripMenuItem.Text = "&Application";
			// 
			// preferencesToolStripMenuItem
			// 
			this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
			this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.preferencesToolStripMenuItem.Text = "&Preferences...";
			this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.OnApplicationPreferences);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.aboutToolStripMenuItem.Text = "About...";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.OnApplicationAbout);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.quitToolStripMenuItem.Text = "&Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.OnApplicationQuit);
			// 
			// repositoryToolStripMenuItem
			// 
			this.repositoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pullToolStripMenuItem,
            this.pushToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.commitToolStripMenuItem,
            this.mergeToolStripMenuItem,
            this.branchToolStripMenuItem,
            this.toolStripSeparator2,
            this.cloneToolStripMenuItem,
            this.createToolStripMenuItem,
            this.toolStripSeparator3,
            this.viewToolStripMenuItem,
            this.logRepoToolStripMenuItem,
            this.rollbackToolStripMenuItem});
			this.repositoryToolStripMenuItem.Name = "repositoryToolStripMenuItem";
			this.repositoryToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
			this.repositoryToolStripMenuItem.Text = "&Repository";
			// 
			// pullToolStripMenuItem
			// 
			this.pullToolStripMenuItem.Name = "pullToolStripMenuItem";
			this.pullToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.pullToolStripMenuItem.Text = "&Pull...";
			this.pullToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// pushToolStripMenuItem
			// 
			this.pushToolStripMenuItem.Name = "pushToolStripMenuItem";
			this.pushToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.pushToolStripMenuItem.Text = "Pu&sh...";
			this.pushToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// updateToolStripMenuItem
			// 
			this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
			this.updateToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.updateToolStripMenuItem.Text = "&Update...";
			this.updateToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// commitToolStripMenuItem
			// 
			this.commitToolStripMenuItem.Name = "commitToolStripMenuItem";
			this.commitToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.commitToolStripMenuItem.Text = "&Commit...";
			this.commitToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// mergeToolStripMenuItem
			// 
			this.mergeToolStripMenuItem.Name = "mergeToolStripMenuItem";
			this.mergeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.mergeToolStripMenuItem.Text = "&Merge...";
			this.mergeToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// branchToolStripMenuItem
			// 
			this.branchToolStripMenuItem.Name = "branchToolStripMenuItem";
			this.branchToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.branchToolStripMenuItem.Text = "&Branch...";
			this.branchToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(133, 6);
			// 
			// cloneToolStripMenuItem
			// 
			this.cloneToolStripMenuItem.Name = "cloneToolStripMenuItem";
			this.cloneToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.cloneToolStripMenuItem.Text = "Clo&ne...";
			this.cloneToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// createToolStripMenuItem
			// 
			this.createToolStripMenuItem.Name = "createToolStripMenuItem";
			this.createToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.createToolStripMenuItem.Text = "Crea&te...";
			this.createToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(133, 6);
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.viewToolStripMenuItem.Text = "&View...";
			this.viewToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// logRepoToolStripMenuItem
			// 
			this.logRepoToolStripMenuItem.Name = "logRepoToolStripMenuItem";
			this.logRepoToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.logRepoToolStripMenuItem.Text = "&Log...";
			this.logRepoToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// rollbackToolStripMenuItem
			// 
			this.rollbackToolStripMenuItem.Name = "rollbackToolStripMenuItem";
			this.rollbackToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.rollbackToolStripMenuItem.Text = "&Rollback...";
			this.rollbackToolStripMenuItem.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.diffToolStripMenuItem,
            this.logToolStripMenuItem,
            this.annotateToolStripMenuItem,
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.revertToolStripMenuItem,
            this.ignoreToolStripMenuItem,
            this.stopIgnoringToolStripMenuItem,
            this.renameToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// diffToolStripMenuItem
			// 
			this.diffToolStripMenuItem.Name = "diffToolStripMenuItem";
			this.diffToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.diffToolStripMenuItem.Text = "&Diff...";
			this.diffToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// logToolStripMenuItem
			// 
			this.logToolStripMenuItem.Name = "logToolStripMenuItem";
			this.logToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.logToolStripMenuItem.Text = "&Log...";
			this.logToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// annotateToolStripMenuItem
			// 
			this.annotateToolStripMenuItem.Name = "annotateToolStripMenuItem";
			this.annotateToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.annotateToolStripMenuItem.Text = "A&nnotate...";
			this.annotateToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// addToolStripMenuItem
			// 
			this.addToolStripMenuItem.Name = "addToolStripMenuItem";
			this.addToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.addToolStripMenuItem.Text = "&Add...";
			this.addToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// removeToolStripMenuItem
			// 
			this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
			this.removeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.removeToolStripMenuItem.Text = "&Remove...";
			this.removeToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// revertToolStripMenuItem
			// 
			this.revertToolStripMenuItem.Name = "revertToolStripMenuItem";
			this.revertToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.revertToolStripMenuItem.Text = "Re&vert...";
			this.revertToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// ignoreToolStripMenuItem
			// 
			this.ignoreToolStripMenuItem.Name = "ignoreToolStripMenuItem";
			this.ignoreToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.ignoreToolStripMenuItem.Text = "&Ignore...";
			this.ignoreToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// stopIgnoringToolStripMenuItem
			// 
			this.stopIgnoringToolStripMenuItem.Name = "stopIgnoringToolStripMenuItem";
			this.stopIgnoringToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.stopIgnoringToolStripMenuItem.Text = "&Stop Ignoring...";
			this.stopIgnoringToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// renameToolStripMenuItem
			// 
			this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
			this.renameToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.renameToolStripMenuItem.Text = "Rena&me...";
			this.renameToolStripMenuItem.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_fileViewToolStrip
			// 
			this.m_fileViewToolStrip.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "FileViewToolstripLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.m_fileViewToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.m_fileViewToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_cleanCheckbox,
            this.m_modifiedCheckbox,
            this.m_addedCheckbox,
            this.m_removedCheckbox,
            this.m_deletedCheckbox,
            this.m_ignoredCheckbox,
            this.m_notTrackedCheckbox});
			this.m_fileViewToolStrip.Location = global::Zync.Properties.Settings.Default.FileViewToolstripLocation;
			this.m_fileViewToolStrip.Name = "m_fileViewToolStrip";
			this.m_fileViewToolStrip.Size = new System.Drawing.Size(175, 25);
			this.m_fileViewToolStrip.TabIndex = 1;
			// 
			// m_cleanCheckbox
			// 
			this.m_cleanCheckbox.Checked = global::Zync.Properties.Settings.Default.CleanCheckboxState;
			this.m_cleanCheckbox.CheckOnClick = true;
			this.m_cleanCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_cleanCheckbox.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.m_cleanCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_cleanCheckbox.Name = "m_cleanCheckbox";
			this.m_cleanCheckbox.Size = new System.Drawing.Size(23, 22);
			this.m_cleanCheckbox.Text = "C";
			this.m_cleanCheckbox.ToolTipText = "Show clean files";
			this.m_cleanCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_modifiedCheckbox
			// 
			this.m_modifiedCheckbox.Checked = global::Zync.Properties.Settings.Default.ModifiedCheckboxState;
			this.m_modifiedCheckbox.CheckOnClick = true;
			this.m_modifiedCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_modifiedCheckbox.Image = ((System.Drawing.Image)(resources.GetObject("m_modifiedCheckbox.Image")));
			this.m_modifiedCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_modifiedCheckbox.Name = "m_modifiedCheckbox";
			this.m_modifiedCheckbox.Size = new System.Drawing.Size(25, 22);
			this.m_modifiedCheckbox.Text = "Mo";
			this.m_modifiedCheckbox.ToolTipText = "Show modified files";
			this.m_modifiedCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_addedCheckbox
			// 
			this.m_addedCheckbox.Checked = global::Zync.Properties.Settings.Default.AddedCheckboxState;
			this.m_addedCheckbox.CheckOnClick = true;
			this.m_addedCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_addedCheckbox.Image = ((System.Drawing.Image)(resources.GetObject("m_addedCheckbox.Image")));
			this.m_addedCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_addedCheckbox.Name = "m_addedCheckbox";
			this.m_addedCheckbox.Size = new System.Drawing.Size(23, 22);
			this.m_addedCheckbox.Text = "A";
			this.m_addedCheckbox.ToolTipText = "Show added files";
			this.m_addedCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_removedCheckbox
			// 
			this.m_removedCheckbox.Checked = global::Zync.Properties.Settings.Default.RemovedCheckboxState;
			this.m_removedCheckbox.CheckOnClick = true;
			this.m_removedCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_removedCheckbox.Image = ((System.Drawing.Image)(resources.GetObject("m_removedCheckbox.Image")));
			this.m_removedCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_removedCheckbox.Name = "m_removedCheckbox";
			this.m_removedCheckbox.Size = new System.Drawing.Size(23, 22);
			this.m_removedCheckbox.Text = "R";
			this.m_removedCheckbox.ToolTipText = "Show removed files";
			this.m_removedCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_deletedCheckbox
			// 
			this.m_deletedCheckbox.Checked = global::Zync.Properties.Settings.Default.DeletedCheckboxState;
			this.m_deletedCheckbox.CheckOnClick = true;
			this.m_deletedCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_deletedCheckbox.Image = ((System.Drawing.Image)(resources.GetObject("m_deletedCheckbox.Image")));
			this.m_deletedCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_deletedCheckbox.Name = "m_deletedCheckbox";
			this.m_deletedCheckbox.Size = new System.Drawing.Size(23, 22);
			this.m_deletedCheckbox.Text = "Mi";
			this.m_deletedCheckbox.ToolTipText = "Show missing files";
			this.m_deletedCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_ignoredCheckbox
			// 
			this.m_ignoredCheckbox.Checked = global::Zync.Properties.Settings.Default.IgnoredCheckboxState;
			this.m_ignoredCheckbox.CheckOnClick = true;
			this.m_ignoredCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_ignoredCheckbox.Image = ((System.Drawing.Image)(resources.GetObject("m_ignoredCheckbox.Image")));
			this.m_ignoredCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_ignoredCheckbox.Name = "m_ignoredCheckbox";
			this.m_ignoredCheckbox.Size = new System.Drawing.Size(23, 22);
			this.m_ignoredCheckbox.Text = "I";
			this.m_ignoredCheckbox.ToolTipText = "Show ignored files";
			this.m_ignoredCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_notTrackedCheckbox
			// 
			this.m_notTrackedCheckbox.Checked = global::Zync.Properties.Settings.Default.NotTrackedCheckboxState;
			this.m_notTrackedCheckbox.CheckOnClick = true;
			this.m_notTrackedCheckbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_notTrackedCheckbox.Image = ((System.Drawing.Image)(resources.GetObject("m_notTrackedCheckbox.Image")));
			this.m_notTrackedCheckbox.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_notTrackedCheckbox.Name = "m_notTrackedCheckbox";
			this.m_notTrackedCheckbox.Size = new System.Drawing.Size(23, 22);
			this.m_notTrackedCheckbox.Text = "?";
			this.m_notTrackedCheckbox.ToolTipText = "Show untracked files";
			this.m_notTrackedCheckbox.CheckedChanged += new System.EventHandler(this.OnFileViewStatusButtonChanged);
			// 
			// m_fileActionToolStrip
			// 
			this.m_fileActionToolStrip.AllowItemReorder = true;
			this.m_fileActionToolStrip.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "FileActionToolstripLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.m_fileActionToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.m_fileActionToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_difButton,
            this.m_logButton,
            this.m_annotateButton,
            this.m_addButton,
            this.m_removeButton,
            this.m_revertButton,
            this.m_ignoreButton,
            this.m_stopIgnoringButton,
            this.m_renameButton});
			this.m_fileActionToolStrip.Location = global::Zync.Properties.Settings.Default.FileActionToolstripLocation;
			this.m_fileActionToolStrip.Name = "m_fileActionToolStrip";
			this.m_fileActionToolStrip.Size = new System.Drawing.Size(286, 25);
			this.m_fileActionToolStrip.TabIndex = 3;
			// 
			// m_difButton
			// 
			this.m_difButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_difButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_difButton.Name = "m_difButton";
			this.m_difButton.Size = new System.Drawing.Size(24, 22);
			this.m_difButton.Text = "Dif";
			this.m_difButton.ToolTipText = "Diff selected files";
			this.m_difButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_logButton
			// 
			this.m_logButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_logButton.Image = ((System.Drawing.Image)(resources.GetObject("m_logButton.Image")));
			this.m_logButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_logButton.Name = "m_logButton";
			this.m_logButton.Size = new System.Drawing.Size(28, 22);
			this.m_logButton.Text = "Log";
			this.m_logButton.ToolTipText = "Log selected files";
			this.m_logButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_annotateButton
			// 
			this.m_annotateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_annotateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_annotateButton.Name = "m_annotateButton";
			this.m_annotateButton.Size = new System.Drawing.Size(30, 22);
			this.m_annotateButton.Text = "Ann";
			this.m_annotateButton.ToolTipText = "Annotate selected files";
			this.m_annotateButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_addButton
			// 
			this.m_addButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_addButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_addButton.Name = "m_addButton";
			this.m_addButton.Size = new System.Drawing.Size(30, 22);
			this.m_addButton.Text = "Add";
			this.m_addButton.ToolTipText = "Add selected files";
			this.m_addButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_removeButton
			// 
			this.m_removeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_removeButton.Image = ((System.Drawing.Image)(resources.GetObject("m_removeButton.Image")));
			this.m_removeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_removeButton.Name = "m_removeButton";
			this.m_removeButton.Size = new System.Drawing.Size(32, 22);
			this.m_removeButton.Text = "Rem";
			this.m_removeButton.ToolTipText = "Remove selected files";
			this.m_removeButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_revertButton
			// 
			this.m_revertButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_revertButton.Image = ((System.Drawing.Image)(resources.GetObject("m_revertButton.Image")));
			this.m_revertButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_revertButton.Name = "m_revertButton";
			this.m_revertButton.Size = new System.Drawing.Size(30, 22);
			this.m_revertButton.Text = "Rev";
			this.m_revertButton.ToolTipText = "Revert selected files";
			this.m_revertButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_ignoreButton
			// 
			this.m_ignoreButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_ignoreButton.Image = ((System.Drawing.Image)(resources.GetObject("m_ignoreButton.Image")));
			this.m_ignoreButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_ignoreButton.Name = "m_ignoreButton";
			this.m_ignoreButton.Size = new System.Drawing.Size(27, 22);
			this.m_ignoreButton.Text = "Ign";
			this.m_ignoreButton.ToolTipText = "Ignore selected files";
			this.m_ignoreButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_stopIgnoringButton
			// 
			this.m_stopIgnoringButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_stopIgnoringButton.Image = ((System.Drawing.Image)(resources.GetObject("m_stopIgnoringButton.Image")));
			this.m_stopIgnoringButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_stopIgnoringButton.Name = "m_stopIgnoringButton";
			this.m_stopIgnoringButton.Size = new System.Drawing.Size(43, 22);
			this.m_stopIgnoringButton.Text = "StpIgn";
			this.m_stopIgnoringButton.ToolTipText = "Stop ignoring selected files";
			this.m_stopIgnoringButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_renameButton
			// 
			this.m_renameButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_renameButton.Image = ((System.Drawing.Image)(resources.GetObject("m_renameButton.Image")));
			this.m_renameButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_renameButton.Name = "m_renameButton";
			this.m_renameButton.Size = new System.Drawing.Size(30, 22);
			this.m_renameButton.Text = "Ren";
			this.m_renameButton.ToolTipText = "Rename selected file";
			this.m_renameButton.Click += new System.EventHandler(this.OnFileActionControlClicked);
			// 
			// m_workingFolderToolStrip
			// 
			this.m_workingFolderToolStrip.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "WorkingFolderToolstripLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.m_workingFolderToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.m_workingFolderToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_workingFolderComboBox,
            this.m_workingFolderBrowseButton});
			this.m_workingFolderToolStrip.Location = global::Zync.Properties.Settings.Default.WorkingFolderToolstripLocation;
			this.m_workingFolderToolStrip.Name = "m_workingFolderToolStrip";
			this.m_workingFolderToolStrip.Size = new System.Drawing.Size(387, 25);
			this.m_workingFolderToolStrip.TabIndex = 0;
			// 
			// m_workingFolderComboBox
			// 
			this.m_workingFolderComboBox.Name = "m_workingFolderComboBox";
			this.m_workingFolderComboBox.Size = new System.Drawing.Size(350, 25);
			this.m_workingFolderComboBox.SelectedIndexChanged += new System.EventHandler(this.OnWorkingFolderComboBoxIndexChanged);
			this.m_workingFolderComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
			// 
			// m_workingFolderBrowseButton
			// 
			this.m_workingFolderBrowseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_workingFolderBrowseButton.Image = ((System.Drawing.Image)(resources.GetObject("m_workingFolderBrowseButton.Image")));
			this.m_workingFolderBrowseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_workingFolderBrowseButton.Name = "m_workingFolderBrowseButton";
			this.m_workingFolderBrowseButton.Size = new System.Drawing.Size(23, 22);
			this.m_workingFolderBrowseButton.Text = "...";
			this.m_workingFolderBrowseButton.Click += new System.EventHandler(this.OnBrowseWorkingFolderButtonClicked);
			// 
			// m_repositoryActionToolStrip
			// 
			this.m_repositoryActionToolStrip.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "RepositoryActionToolstripLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.m_repositoryActionToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.m_repositoryActionToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_pullButton,
            this.m_pushButton,
            this.m_updateButton,
            this.m_commitButton,
            this.m_mergeButton,
            this.m_branchButton,
            this.toolStripSeparator4,
            this.m_cloneButton,
            this.m_createButton,
            this.toolStripSeparator5,
            this.m_viewButton,
            this.m_repoLogButton,
            this.m_rollbackButton});
			this.m_repositoryActionToolStrip.Location = global::Zync.Properties.Settings.Default.RepositoryActionToolstripLocation;
			this.m_repositoryActionToolStrip.Name = "m_repositoryActionToolStrip";
			this.m_repositoryActionToolStrip.Size = new System.Drawing.Size(323, 25);
			this.m_repositoryActionToolStrip.TabIndex = 4;
			// 
			// m_pullButton
			// 
			this.m_pullButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_pullButton.Image = ((System.Drawing.Image)(resources.GetObject("m_pullButton.Image")));
			this.m_pullButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_pullButton.Name = "m_pullButton";
			this.m_pullButton.Size = new System.Drawing.Size(25, 22);
			this.m_pullButton.Text = "Pul";
			this.m_pullButton.ToolTipText = "Pull from other repository";
			this.m_pullButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_pushButton
			// 
			this.m_pushButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_pushButton.Image = ((System.Drawing.Image)(resources.GetObject("m_pushButton.Image")));
			this.m_pushButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_pushButton.Name = "m_pushButton";
			this.m_pushButton.Size = new System.Drawing.Size(28, 22);
			this.m_pushButton.Text = "Pus";
			this.m_pushButton.ToolTipText = "Push to other repository";
			this.m_pushButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_updateButton
			// 
			this.m_updateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_updateButton.Image = ((System.Drawing.Image)(resources.GetObject("m_updateButton.Image")));
			this.m_updateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_updateButton.Name = "m_updateButton";
			this.m_updateButton.Size = new System.Drawing.Size(30, 22);
			this.m_updateButton.Text = "Upd";
			this.m_updateButton.ToolTipText = "Update working files from local repository";
			this.m_updateButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_commitButton
			// 
			this.m_commitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_commitButton.Image = ((System.Drawing.Image)(resources.GetObject("m_commitButton.Image")));
			this.m_commitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_commitButton.Name = "m_commitButton";
			this.m_commitButton.Size = new System.Drawing.Size(32, 22);
			this.m_commitButton.Text = "Com";
			this.m_commitButton.ToolTipText = "Commit files to local repository";
			this.m_commitButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_mergeButton
			// 
			this.m_mergeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_mergeButton.Image = ((System.Drawing.Image)(resources.GetObject("m_mergeButton.Image")));
			this.m_mergeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_mergeButton.Name = "m_mergeButton";
			this.m_mergeButton.Size = new System.Drawing.Size(29, 22);
			this.m_mergeButton.Text = "Mer";
			this.m_mergeButton.ToolTipText = "Merge";
			this.m_mergeButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_branchButton
			// 
			this.m_branchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_branchButton.Image = ((System.Drawing.Image)(resources.GetObject("m_branchButton.Image")));
			this.m_branchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_branchButton.Name = "m_branchButton";
			this.m_branchButton.Size = new System.Drawing.Size(27, 22);
			this.m_branchButton.Text = "Brh";
			this.m_branchButton.ToolTipText = "Create or reset a named branch";
			this.m_branchButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// m_cloneButton
			// 
			this.m_cloneButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_cloneButton.Image = ((System.Drawing.Image)(resources.GetObject("m_cloneButton.Image")));
			this.m_cloneButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_cloneButton.Name = "m_cloneButton";
			this.m_cloneButton.Size = new System.Drawing.Size(26, 22);
			this.m_cloneButton.Text = "Cln";
			this.m_cloneButton.ToolTipText = "Clone repository";
			this.m_cloneButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_createButton
			// 
			this.m_createButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_createButton.Image = ((System.Drawing.Image)(resources.GetObject("m_createButton.Image")));
			this.m_createButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_createButton.Name = "m_createButton";
			this.m_createButton.Size = new System.Drawing.Size(23, 22);
			this.m_createButton.Text = "Cr";
			this.m_createButton.ToolTipText = "Create repository at selected folder";
			this.m_createButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
			// 
			// m_viewButton
			// 
			this.m_viewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_viewButton.Image = ((System.Drawing.Image)(resources.GetObject("m_viewButton.Image")));
			this.m_viewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_viewButton.Name = "m_viewButton";
			this.m_viewButton.Size = new System.Drawing.Size(25, 22);
			this.m_viewButton.Text = "Vw";
			this.m_viewButton.ToolTipText = "View repository";
			this.m_viewButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_repoLogButton
			// 
			this.m_repoLogButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_repoLogButton.Image = ((System.Drawing.Image)(resources.GetObject("m_repoLogButton.Image")));
			this.m_repoLogButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_repoLogButton.Name = "m_repoLogButton";
			this.m_repoLogButton.Size = new System.Drawing.Size(28, 22);
			this.m_repoLogButton.Text = "Log";
			this.m_repoLogButton.ToolTipText = "Log repository";
			this.m_repoLogButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// m_rollbackButton
			// 
			this.m_rollbackButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.m_rollbackButton.Image = ((System.Drawing.Image)(resources.GetObject("m_rollbackButton.Image")));
			this.m_rollbackButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.m_rollbackButton.Name = "m_rollbackButton";
			this.m_rollbackButton.Size = new System.Drawing.Size(26, 22);
			this.m_rollbackButton.Text = "Rol";
			this.m_rollbackButton.ToolTipText = "Rollback last commit, push or pull";
			this.m_rollbackButton.Click += new System.EventHandler(this.OnRepoActionControlClicked);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(952, 521);
			this.Controls.Add(this.m_mainToolStripContainer);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "Zync";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.Load += new System.EventHandler(this.OnFormLoad);
			this.m_mainToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
			this.m_mainToolStripContainer.ContentPanel.ResumeLayout(false);
			this.m_mainToolStripContainer.TopToolStripPanel.ResumeLayout(false);
			this.m_mainToolStripContainer.TopToolStripPanel.PerformLayout();
			this.m_mainToolStripContainer.ResumeLayout(false);
			this.m_mainToolStripContainer.PerformLayout();
			this.m_statusStrip.ResumeLayout(false);
			this.m_statusStrip.PerformLayout();
			this.m_mainOuterSplitContainer.Panel1.ResumeLayout(false);
			this.m_mainOuterSplitContainer.Panel2.ResumeLayout(false);
			this.m_mainOuterSplitContainer.ResumeLayout(false);
			this.m_mainInnerSplitContainer.Panel1.ResumeLayout(false);
			this.m_mainInnerSplitContainer.Panel2.ResumeLayout(false);
			this.m_mainInnerSplitContainer.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.m_fileViewToolStrip.ResumeLayout(false);
			this.m_fileViewToolStrip.PerformLayout();
			this.m_fileActionToolStrip.ResumeLayout(false);
			this.m_fileActionToolStrip.PerformLayout();
			this.m_workingFolderToolStrip.ResumeLayout(false);
			this.m_workingFolderToolStrip.PerformLayout();
			this.m_repositoryActionToolStrip.ResumeLayout(false);
			this.m_repositoryActionToolStrip.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.FolderBrowserDialog browseWorkingFolderDialog;
		private System.Windows.Forms.SplitContainer m_mainInnerSplitContainer;
		private FolderView m_folderView;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.StatusStrip m_statusStrip;
		private System.Windows.Forms.ToolStripContainer m_mainToolStripContainer;
		private System.Windows.Forms.ToolStrip m_workingFolderToolStrip;
		private System.Windows.Forms.ToolStripComboBox m_workingFolderComboBox;
		private System.Windows.Forms.ToolStripButton m_workingFolderBrowseButton;
		private System.Windows.Forms.ToolStrip m_fileViewToolStrip;
		private System.Windows.Forms.ToolStripButton m_cleanCheckbox;
		private System.Windows.Forms.ToolStripButton m_modifiedCheckbox;
		private System.Windows.Forms.ToolStripButton m_addedCheckbox;
		private System.Windows.Forms.ToolStripButton m_removedCheckbox;
		private System.Windows.Forms.ToolStripButton m_deletedCheckbox;
		private System.Windows.Forms.ToolStripButton m_ignoredCheckbox;
		private System.Windows.Forms.ToolStripButton m_notTrackedCheckbox;
		private System.Windows.Forms.SplitContainer m_mainOuterSplitContainer;
		private System.Windows.Forms.RichTextBox m_commandOutputTextBox;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem appToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.ToolStrip m_fileActionToolStrip;
		private System.Windows.Forms.ToolStripButton m_difButton;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem diffToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton m_addButton;
		private System.Windows.Forms.ToolStripButton m_annotateButton;
		private System.Windows.Forms.ToolStripButton m_removeButton;
		private System.Windows.Forms.ToolStripButton m_logButton;
		private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem annotateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem repositoryToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton m_revertButton;
		private System.Windows.Forms.ToolStripMenuItem revertToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton m_ignoreButton;
		private System.Windows.Forms.ToolStripButton m_stopIgnoringButton;
		private System.Windows.Forms.ToolStripMenuItem ignoreToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem stopIgnoringToolStripMenuItem;
		private System.Windows.Forms.ToolStrip m_repositoryActionToolStrip;
		private System.Windows.Forms.ToolStripButton m_commitButton;
		private System.Windows.Forms.ToolStripButton m_updateButton;
		private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem commitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cloneToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem logRepoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem rollbackToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton m_viewButton;
		private System.Windows.Forms.ToolStripButton m_repoLogButton;
		private System.Windows.Forms.ToolStripButton m_cloneButton;
		private System.Windows.Forms.ToolStripButton m_createButton;
		private System.Windows.Forms.ToolStripButton m_rollbackButton;
		private System.Windows.Forms.ToolStripMenuItem pullToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem pushToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem mergeToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton m_pullButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton m_pushButton;
		private System.Windows.Forms.ToolStripButton m_mergeButton;
		private System.Windows.Forms.ToolStripButton m_branchButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripStatusLabel m_statusDefaultPushLabel;
		private System.Windows.Forms.ToolStripStatusLabel m_statusBranchLabel;
		private System.Windows.Forms.ToolStripStatusLabel m_statusNumHeadsLabel;
		private System.Windows.Forms.ToolStripButton m_renameButton;
		private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
		private FileView m_fileView;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem branchToolStripMenuItem;
	}

}

