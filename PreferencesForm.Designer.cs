﻿namespace Zync
{
	partial class PreferencesForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.Label label1;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreferencesForm));
			this.preferencesTabControl = new System.Windows.Forms.TabControl();
			this.generalTabPage = new System.Windows.Forms.TabPage();
			this.showIncomingCheckBox = new System.Windows.Forms.CheckBox();
			this.showOnlyRepositoryFoldersCheckBox = new System.Windows.Forms.CheckBox();
			this.defaultCloneSourceTextBox = new System.Windows.Forms.TextBox();
			this.diffTabPage = new System.Windows.Forms.TabPage();
			this.externalDiffTextBox = new System.Windows.Forms.TextBox();
			this.useExternalDiffCheckBox = new System.Windows.Forms.CheckBox();
			this.commandDialogsTabPage = new System.Windows.Forms.TabPage();
			this.skipDialogGroupBox = new System.Windows.Forms.GroupBox();
			this.skipPullDialogCheckBox = new System.Windows.Forms.CheckBox();
			this.skipAnnotateDialogCheckBox = new System.Windows.Forms.CheckBox();
			this.skipFileLogDialogCheckBox = new System.Windows.Forms.CheckBox();
			this.skipMergeDialogCheckBox = new System.Windows.Forms.CheckBox();
			this.skipUpdateDialogCheckBox = new System.Windows.Forms.CheckBox();
			this.preferencesOkButton = new System.Windows.Forms.Button();
			this.preferencesCancelButton = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			label1 = new System.Windows.Forms.Label();
			this.preferencesTabControl.SuspendLayout();
			this.generalTabPage.SuspendLayout();
			this.diffTabPage.SuspendLayout();
			this.commandDialogsTabPage.SuspendLayout();
			this.skipDialogGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(7, 80);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(137, 13);
			label1.TabIndex = 1;
			label1.Text = "Default clone source folder:";
			label1.Visible = false;
			// 
			// preferencesTabControl
			// 
			this.preferencesTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.preferencesTabControl.Controls.Add(this.generalTabPage);
			this.preferencesTabControl.Controls.Add(this.diffTabPage);
			this.preferencesTabControl.Controls.Add(this.commandDialogsTabPage);
			this.preferencesTabControl.Location = new System.Drawing.Point(3, 6);
			this.preferencesTabControl.Name = "preferencesTabControl";
			this.preferencesTabControl.SelectedIndex = 0;
			this.preferencesTabControl.Size = new System.Drawing.Size(363, 280);
			this.preferencesTabControl.TabIndex = 0;
			// 
			// generalTabPage
			// 
			this.generalTabPage.BackColor = System.Drawing.Color.Transparent;
			this.generalTabPage.Controls.Add(this.showIncomingCheckBox);
			this.generalTabPage.Controls.Add(this.showOnlyRepositoryFoldersCheckBox);
			this.generalTabPage.Controls.Add(label1);
			this.generalTabPage.Controls.Add(this.defaultCloneSourceTextBox);
			this.generalTabPage.Location = new System.Drawing.Point(4, 22);
			this.generalTabPage.Name = "generalTabPage";
			this.generalTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.generalTabPage.Size = new System.Drawing.Size(355, 254);
			this.generalTabPage.TabIndex = 0;
			this.generalTabPage.Text = "General";
			this.generalTabPage.UseVisualStyleBackColor = true;
			// 
			// showIncomingCheckBox
			// 
			this.showIncomingCheckBox.AutoSize = true;
			this.showIncomingCheckBox.Location = new System.Drawing.Point(10, 22);
			this.showIncomingCheckBox.Name = "showIncomingCheckBox";
			this.showIncomingCheckBox.Size = new System.Drawing.Size(99, 17);
			this.showIncomingCheckBox.TabIndex = 3;
			this.showIncomingCheckBox.Text = "Show Incoming";
			this.toolTip1.SetToolTip(this.showIncomingCheckBox, resources.GetString("showIncomingCheckBox.ToolTip"));
			this.showIncomingCheckBox.UseVisualStyleBackColor = true;
			// 
			// showOnlyRepositoryFoldersCheckBox
			// 
			this.showOnlyRepositoryFoldersCheckBox.AutoSize = true;
			this.showOnlyRepositoryFoldersCheckBox.Checked = global::Zync.Properties.Settings.Default.PrefShowOnlyRepoFolders;
			this.showOnlyRepositoryFoldersCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Zync.Properties.Settings.Default, "PrefShowOnlyRepoFolders", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.showOnlyRepositoryFoldersCheckBox.Location = new System.Drawing.Point(10, 45);
			this.showOnlyRepositoryFoldersCheckBox.Name = "showOnlyRepositoryFoldersCheckBox";
			this.showOnlyRepositoryFoldersCheckBox.Size = new System.Drawing.Size(157, 17);
			this.showOnlyRepositoryFoldersCheckBox.TabIndex = 2;
			this.showOnlyRepositoryFoldersCheckBox.Text = "Show only repository folders";
			this.toolTip1.SetToolTip(this.showOnlyRepositoryFoldersCheckBox, "If checked, only folders with repositories will be displayed in the folder view.\r" +
					"\nIf uncheck, all folders will be displayed. Folders with repositories have \r\nico" +
					"ns with a checkmark.");
			this.showOnlyRepositoryFoldersCheckBox.UseVisualStyleBackColor = true;
			// 
			// defaultCloneSourceTextBox
			// 
			this.defaultCloneSourceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.defaultCloneSourceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Zync.Properties.Settings.Default, "PrefDefaultCloneSource", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.defaultCloneSourceTextBox.Location = new System.Drawing.Point(10, 96);
			this.defaultCloneSourceTextBox.Name = "defaultCloneSourceTextBox";
			this.defaultCloneSourceTextBox.Size = new System.Drawing.Size(315, 20);
			this.defaultCloneSourceTextBox.TabIndex = 0;
			this.defaultCloneSourceTextBox.Text = global::Zync.Properties.Settings.Default.PrefDefaultCloneSource;
			this.defaultCloneSourceTextBox.Visible = false;
			// 
			// diffTabPage
			// 
			this.diffTabPage.BackColor = System.Drawing.Color.Transparent;
			this.diffTabPage.Controls.Add(this.externalDiffTextBox);
			this.diffTabPage.Controls.Add(this.useExternalDiffCheckBox);
			this.diffTabPage.Location = new System.Drawing.Point(4, 22);
			this.diffTabPage.Name = "diffTabPage";
			this.diffTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.diffTabPage.Size = new System.Drawing.Size(355, 254);
			this.diffTabPage.TabIndex = 1;
			this.diffTabPage.Text = "Diff";
			this.diffTabPage.UseVisualStyleBackColor = true;
			// 
			// externalDiffTextBox
			// 
			this.externalDiffTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Zync.Properties.Settings.Default, "PrefExternalDiffCommand", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.externalDiffTextBox.Location = new System.Drawing.Point(17, 44);
			this.externalDiffTextBox.Name = "externalDiffTextBox";
			this.externalDiffTextBox.Size = new System.Drawing.Size(254, 20);
			this.externalDiffTextBox.TabIndex = 1;
			this.externalDiffTextBox.Text = global::Zync.Properties.Settings.Default.PrefExternalDiffCommand;
			// 
			// useExternalDiffCheckBox
			// 
			this.useExternalDiffCheckBox.AutoSize = true;
			this.useExternalDiffCheckBox.Checked = global::Zync.Properties.Settings.Default.PrefUseExternalDiff;
			this.useExternalDiffCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.useExternalDiffCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Zync.Properties.Settings.Default, "PrefUseExternalDiff", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.useExternalDiffCheckBox.Location = new System.Drawing.Point(17, 21);
			this.useExternalDiffCheckBox.Name = "useExternalDiffCheckBox";
			this.useExternalDiffCheckBox.Size = new System.Drawing.Size(102, 17);
			this.useExternalDiffCheckBox.TabIndex = 0;
			this.useExternalDiffCheckBox.Text = "Use external diff";
			this.useExternalDiffCheckBox.UseVisualStyleBackColor = true;
			// 
			// commandDialogsTabPage
			// 
			this.commandDialogsTabPage.Controls.Add(this.skipDialogGroupBox);
			this.commandDialogsTabPage.Location = new System.Drawing.Point(4, 22);
			this.commandDialogsTabPage.Name = "commandDialogsTabPage";
			this.commandDialogsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.commandDialogsTabPage.Size = new System.Drawing.Size(355, 254);
			this.commandDialogsTabPage.TabIndex = 2;
			this.commandDialogsTabPage.Text = "Command Dialogs";
			this.commandDialogsTabPage.UseVisualStyleBackColor = true;
			// 
			// skipDialogGroupBox
			// 
			this.skipDialogGroupBox.Controls.Add(this.skipPullDialogCheckBox);
			this.skipDialogGroupBox.Controls.Add(this.skipAnnotateDialogCheckBox);
			this.skipDialogGroupBox.Controls.Add(this.skipFileLogDialogCheckBox);
			this.skipDialogGroupBox.Controls.Add(this.skipMergeDialogCheckBox);
			this.skipDialogGroupBox.Controls.Add(this.skipUpdateDialogCheckBox);
			this.skipDialogGroupBox.Location = new System.Drawing.Point(6, 6);
			this.skipDialogGroupBox.Name = "skipDialogGroupBox";
			this.skipDialogGroupBox.Size = new System.Drawing.Size(111, 206);
			this.skipDialogGroupBox.TabIndex = 0;
			this.skipDialogGroupBox.TabStop = false;
			this.skipDialogGroupBox.Text = "Skip dialogs";
			// 
			// skipPullDialogCheckBox
			// 
			this.skipPullDialogCheckBox.AutoSize = true;
			this.skipPullDialogCheckBox.Location = new System.Drawing.Point(7, 19);
			this.skipPullDialogCheckBox.Name = "skipPullDialogCheckBox";
			this.skipPullDialogCheckBox.Size = new System.Drawing.Size(43, 17);
			this.skipPullDialogCheckBox.TabIndex = 0;
			this.skipPullDialogCheckBox.Text = "Pull";
			this.skipPullDialogCheckBox.UseVisualStyleBackColor = true;
			// 
			// skipAnnotateDialogCheckBox
			// 
			this.skipAnnotateDialogCheckBox.AutoSize = true;
			this.skipAnnotateDialogCheckBox.Location = new System.Drawing.Point(6, 111);
			this.skipAnnotateDialogCheckBox.Name = "skipAnnotateDialogCheckBox";
			this.skipAnnotateDialogCheckBox.Size = new System.Drawing.Size(69, 17);
			this.skipAnnotateDialogCheckBox.TabIndex = 4;
			this.skipAnnotateDialogCheckBox.Text = "Annotate";
			this.skipAnnotateDialogCheckBox.UseVisualStyleBackColor = true;
			// 
			// skipFileLogDialogCheckBox
			// 
			this.skipFileLogDialogCheckBox.AutoSize = true;
			this.skipFileLogDialogCheckBox.Location = new System.Drawing.Point(6, 88);
			this.skipFileLogDialogCheckBox.Name = "skipFileLogDialogCheckBox";
			this.skipFileLogDialogCheckBox.Size = new System.Drawing.Size(63, 17);
			this.skipFileLogDialogCheckBox.TabIndex = 4;
			this.skipFileLogDialogCheckBox.Text = "File Log";
			this.skipFileLogDialogCheckBox.UseVisualStyleBackColor = true;
			// 
			// skipMergeDialogCheckBox
			// 
			this.skipMergeDialogCheckBox.AutoSize = true;
			this.skipMergeDialogCheckBox.Location = new System.Drawing.Point(7, 65);
			this.skipMergeDialogCheckBox.Name = "skipMergeDialogCheckBox";
			this.skipMergeDialogCheckBox.Size = new System.Drawing.Size(56, 17);
			this.skipMergeDialogCheckBox.TabIndex = 3;
			this.skipMergeDialogCheckBox.Text = "Merge";
			this.skipMergeDialogCheckBox.UseVisualStyleBackColor = true;
			// 
			// skipUpdateDialogCheckBox
			// 
			this.skipUpdateDialogCheckBox.AutoSize = true;
			this.skipUpdateDialogCheckBox.Location = new System.Drawing.Point(7, 42);
			this.skipUpdateDialogCheckBox.Name = "skipUpdateDialogCheckBox";
			this.skipUpdateDialogCheckBox.Size = new System.Drawing.Size(61, 17);
			this.skipUpdateDialogCheckBox.TabIndex = 2;
			this.skipUpdateDialogCheckBox.Text = "Update";
			this.skipUpdateDialogCheckBox.UseVisualStyleBackColor = true;
			// 
			// preferencesOkButton
			// 
			this.preferencesOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.preferencesOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.preferencesOkButton.Location = new System.Drawing.Point(200, 292);
			this.preferencesOkButton.Name = "preferencesOkButton";
			this.preferencesOkButton.Size = new System.Drawing.Size(75, 23);
			this.preferencesOkButton.TabIndex = 1;
			this.preferencesOkButton.Text = "Ok";
			this.preferencesOkButton.UseVisualStyleBackColor = true;
			this.preferencesOkButton.Click += new System.EventHandler(this.OnClickedPreferencesOk);
			// 
			// preferencesCancelButton
			// 
			this.preferencesCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.preferencesCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.preferencesCancelButton.Location = new System.Drawing.Point(291, 292);
			this.preferencesCancelButton.Name = "preferencesCancelButton";
			this.preferencesCancelButton.Size = new System.Drawing.Size(75, 23);
			this.preferencesCancelButton.TabIndex = 2;
			this.preferencesCancelButton.Text = "Cancel";
			this.preferencesCancelButton.UseVisualStyleBackColor = true;
			// 
			// PreferencesForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(369, 325);
			this.Controls.Add(this.preferencesCancelButton);
			this.Controls.Add(this.preferencesOkButton);
			this.Controls.Add(this.preferencesTabControl);
			this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "PreferencesFormLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = global::Zync.Properties.Settings.Default.PreferencesFormLocation;
			this.MinimumSize = new System.Drawing.Size(369, 325);
			this.Name = "PreferencesForm";
			this.Text = "Preferences";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.Load += new System.EventHandler(this.OnFormLoad);
			this.preferencesTabControl.ResumeLayout(false);
			this.generalTabPage.ResumeLayout(false);
			this.generalTabPage.PerformLayout();
			this.diffTabPage.ResumeLayout(false);
			this.diffTabPage.PerformLayout();
			this.commandDialogsTabPage.ResumeLayout(false);
			this.skipDialogGroupBox.ResumeLayout(false);
			this.skipDialogGroupBox.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl preferencesTabControl;
		private System.Windows.Forms.TabPage generalTabPage;
		private System.Windows.Forms.Button preferencesOkButton;
		private System.Windows.Forms.Button preferencesCancelButton;
		private System.Windows.Forms.TabPage diffTabPage;
		private System.Windows.Forms.TextBox defaultCloneSourceTextBox;
		private System.Windows.Forms.CheckBox showOnlyRepositoryFoldersCheckBox;
		private System.Windows.Forms.TextBox externalDiffTextBox;
		private System.Windows.Forms.CheckBox useExternalDiffCheckBox;
		private System.Windows.Forms.TabPage commandDialogsTabPage;
		private System.Windows.Forms.GroupBox skipDialogGroupBox;
		private System.Windows.Forms.CheckBox skipUpdateDialogCheckBox;
		private System.Windows.Forms.CheckBox skipPullDialogCheckBox;
		private System.Windows.Forms.CheckBox skipMergeDialogCheckBox;
		private System.Windows.Forms.CheckBox skipFileLogDialogCheckBox;
		private System.Windows.Forms.CheckBox skipAnnotateDialogCheckBox;
		private System.Windows.Forms.CheckBox showIncomingCheckBox;
		private System.Windows.Forms.ToolTip toolTip1;
	}
}