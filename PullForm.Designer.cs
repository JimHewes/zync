﻿namespace Zync
{
	partial class PullForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PullForm));
			this.pullByRevTextBox = new System.Windows.Forms.TextBox();
			this.pullShiftKeyCheckBox = new System.Windows.Forms.CheckBox();
			this.pullOkButton = new System.Windows.Forms.Button();
			this.pullCancelButton = new System.Windows.Forms.Button();
			this.pullUpdateCheckBox = new System.Windows.Forms.CheckBox();
			this.pullForceCheckBox = new System.Windows.Forms.CheckBox();
			this.pullByRevCheckBox = new System.Windows.Forms.CheckBox();
			this.pullFromDefaultRadioButton = new System.Windows.Forms.RadioButton();
			this.pullFromGroupBox = new System.Windows.Forms.GroupBox();
			this.pullFromOtherTextBoxDummy = new System.Windows.Forms.TextBox();
			this.pullFromOtherBrowseButton = new System.Windows.Forms.Button();
			this.pullFromOtherTextBox = new System.Windows.Forms.TextBox();
			this.pullFromDefaultLabel = new System.Windows.Forms.Label();
			this.pullFromOtherRadioButton = new System.Windows.Forms.RadioButton();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.pullFromGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// pullByRevTextBox
			// 
			this.pullByRevTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pullByRevTextBox.Location = new System.Drawing.Point(116, 116);
			this.pullByRevTextBox.Name = "pullByRevTextBox";
			this.pullByRevTextBox.Size = new System.Drawing.Size(229, 20);
			this.pullByRevTextBox.TabIndex = 1;
			// 
			// pullShiftKeyCheckBox
			// 
			this.pullShiftKeyCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pullShiftKeyCheckBox.AutoSize = true;
			this.pullShiftKeyCheckBox.Location = new System.Drawing.Point(12, 212);
			this.pullShiftKeyCheckBox.Name = "pullShiftKeyCheckBox";
			this.pullShiftKeyCheckBox.Size = new System.Drawing.Size(173, 17);
			this.pullShiftKeyCheckBox.TabIndex = 2;
			this.pullShiftKeyCheckBox.Text = "Display only if Shift key is down";
			this.toolTip1.SetToolTip(this.pullShiftKeyCheckBox, resources.GetString("pullShiftKeyCheckBox.ToolTip"));
			this.pullShiftKeyCheckBox.UseVisualStyleBackColor = true;
			// 
			// pullOkButton
			// 
			this.pullOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.pullOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.pullOkButton.Location = new System.Drawing.Point(227, 206);
			this.pullOkButton.Name = "pullOkButton";
			this.pullOkButton.Size = new System.Drawing.Size(75, 23);
			this.pullOkButton.TabIndex = 3;
			this.pullOkButton.Text = "Pull";
			this.toolTip1.SetToolTip(this.pullOkButton, "Pull from the repository");
			this.pullOkButton.UseVisualStyleBackColor = true;
			this.pullOkButton.Click += new System.EventHandler(this.OnClickedOkButton);
			// 
			// pullCancelButton
			// 
			this.pullCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.pullCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.pullCancelButton.Location = new System.Drawing.Point(316, 206);
			this.pullCancelButton.Name = "pullCancelButton";
			this.pullCancelButton.Size = new System.Drawing.Size(75, 23);
			this.pullCancelButton.TabIndex = 4;
			this.pullCancelButton.Text = "Cancel";
			this.toolTip1.SetToolTip(this.pullCancelButton, "Cancel the pull");
			this.pullCancelButton.UseVisualStyleBackColor = true;
			// 
			// pullUpdateCheckBox
			// 
			this.pullUpdateCheckBox.AutoSize = true;
			this.pullUpdateCheckBox.Location = new System.Drawing.Point(12, 144);
			this.pullUpdateCheckBox.Name = "pullUpdateCheckBox";
			this.pullUpdateCheckBox.Size = new System.Drawing.Size(122, 17);
			this.pullUpdateCheckBox.TabIndex = 5;
			this.pullUpdateCheckBox.Text = "Update working files";
			this.toolTip1.SetToolTip(this.pullUpdateCheckBox, "After pulling, update working files to the new tip");
			this.pullUpdateCheckBox.UseVisualStyleBackColor = true;
			// 
			// pullForceCheckBox
			// 
			this.pullForceCheckBox.AutoSize = true;
			this.pullForceCheckBox.Location = new System.Drawing.Point(12, 170);
			this.pullForceCheckBox.Name = "pullForceCheckBox";
			this.pullForceCheckBox.Size = new System.Drawing.Size(135, 17);
			this.pullForceCheckBox.TabIndex = 6;
			this.pullForceCheckBox.Text = "Force even if unrelated";
			this.toolTip1.SetToolTip(this.pullForceCheckBox, "Pull even if the remote repository is unrelated to the local one");
			this.pullForceCheckBox.UseVisualStyleBackColor = true;
			// 
			// pullByRevCheckBox
			// 
			this.pullByRevCheckBox.AutoSize = true;
			this.pullByRevCheckBox.Location = new System.Drawing.Point(12, 118);
			this.pullByRevCheckBox.Name = "pullByRevCheckBox";
			this.pullByRevCheckBox.Size = new System.Drawing.Size(80, 17);
			this.pullByRevCheckBox.TabIndex = 0;
			this.pullByRevCheckBox.Text = "By revision:";
			this.toolTip1.SetToolTip(this.pullByRevCheckBox, "Pull from a specific revision.");
			this.pullByRevCheckBox.UseVisualStyleBackColor = true;
			this.pullByRevCheckBox.CheckedChanged += new System.EventHandler(this.OnByRevisionCheckBoxChanged);
			// 
			// pullFromDefaultRadioButton
			// 
			this.pullFromDefaultRadioButton.AutoSize = true;
			this.pullFromDefaultRadioButton.Location = new System.Drawing.Point(19, 19);
			this.pullFromDefaultRadioButton.Name = "pullFromDefaultRadioButton";
			this.pullFromDefaultRadioButton.Size = new System.Drawing.Size(62, 17);
			this.pullFromDefaultRadioButton.TabIndex = 7;
			this.pullFromDefaultRadioButton.TabStop = true;
			this.pullFromDefaultRadioButton.Text = "Default:";
			this.toolTip1.SetToolTip(this.pullFromDefaultRadioButton, "Pull from the default repository");
			this.pullFromDefaultRadioButton.UseVisualStyleBackColor = true;
			// 
			// pullFromGroupBox
			// 
			this.pullFromGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pullFromGroupBox.Controls.Add(this.pullFromOtherTextBoxDummy);
			this.pullFromGroupBox.Controls.Add(this.pullFromOtherBrowseButton);
			this.pullFromGroupBox.Controls.Add(this.pullFromOtherTextBox);
			this.pullFromGroupBox.Controls.Add(this.pullFromDefaultLabel);
			this.pullFromGroupBox.Controls.Add(this.pullFromOtherRadioButton);
			this.pullFromGroupBox.Controls.Add(this.pullFromDefaultRadioButton);
			this.pullFromGroupBox.Location = new System.Drawing.Point(12, 12);
			this.pullFromGroupBox.Name = "pullFromGroupBox";
			this.pullFromGroupBox.Size = new System.Drawing.Size(379, 84);
			this.pullFromGroupBox.TabIndex = 8;
			this.pullFromGroupBox.TabStop = false;
			this.pullFromGroupBox.Text = "From";
			// 
			// pullFromOtherTextBoxDummy
			// 
			this.pullFromOtherTextBoxDummy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pullFromOtherTextBoxDummy.Location = new System.Drawing.Point(79, 58);
			this.pullFromOtherTextBoxDummy.Name = "pullFromOtherTextBoxDummy";
			this.pullFromOtherTextBoxDummy.Size = new System.Drawing.Size(229, 20);
			this.pullFromOtherTextBoxDummy.TabIndex = 12;
			this.pullFromOtherTextBoxDummy.TabStop = false;
			this.toolTip1.SetToolTip(this.pullFromOtherTextBoxDummy, "The other repository to choose from. If the Other radio button is selected, then " +
					"you must enter text here to enable the Pull button.");
			this.pullFromOtherTextBoxDummy.Visible = false;
			// 
			// pullFromOtherBrowseButton
			// 
			this.pullFromOtherBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pullFromOtherBrowseButton.Location = new System.Drawing.Point(340, 49);
			this.pullFromOtherBrowseButton.Name = "pullFromOtherBrowseButton";
			this.pullFromOtherBrowseButton.Size = new System.Drawing.Size(32, 23);
			this.pullFromOtherBrowseButton.TabIndex = 11;
			this.pullFromOtherBrowseButton.Text = "...";
			this.toolTip1.SetToolTip(this.pullFromOtherBrowseButton, "Browse for another repository.");
			this.pullFromOtherBrowseButton.UseVisualStyleBackColor = true;
			this.pullFromOtherBrowseButton.Click += new System.EventHandler(this.OnBrowseOtherRepositoryButtonClicked);
			// 
			// pullFromOtherTextBox
			// 
			this.pullFromOtherTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pullFromOtherTextBox.Location = new System.Drawing.Point(104, 50);
			this.pullFromOtherTextBox.Name = "pullFromOtherTextBox";
			this.pullFromOtherTextBox.Size = new System.Drawing.Size(229, 20);
			this.pullFromOtherTextBox.TabIndex = 10;
			this.toolTip1.SetToolTip(this.pullFromOtherTextBox, "The other repository to choose from. If the Other radio button is selected, then " +
					"you must enter text here to enable the Pull button.");
			this.pullFromOtherTextBox.Visible = false;
			this.pullFromOtherTextBox.TextChanged += new System.EventHandler(this.OnPullFromOtherTextBoxTextChanged);
			// 
			// pullFromDefaultLabel
			// 
			this.pullFromDefaultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pullFromDefaultLabel.Location = new System.Drawing.Point(101, 21);
			this.pullFromDefaultLabel.Name = "pullFromDefaultLabel";
			this.pullFromDefaultLabel.Size = new System.Drawing.Size(272, 25);
			this.pullFromDefaultLabel.TabIndex = 9;
			this.pullFromDefaultLabel.Text = "display default repository here";
			// 
			// pullFromOtherRadioButton
			// 
			this.pullFromOtherRadioButton.AutoSize = true;
			this.pullFromOtherRadioButton.Location = new System.Drawing.Point(19, 50);
			this.pullFromOtherRadioButton.Name = "pullFromOtherRadioButton";
			this.pullFromOtherRadioButton.Size = new System.Drawing.Size(54, 17);
			this.pullFromOtherRadioButton.TabIndex = 8;
			this.pullFromOtherRadioButton.TabStop = true;
			this.pullFromOtherRadioButton.Text = "Other:";
			this.toolTip1.SetToolTip(this.pullFromOtherRadioButton, "Pull from a repository other than the default. If there is no default, then this " +
					"will be your only choice");
			this.pullFromOtherRadioButton.UseVisualStyleBackColor = true;
			this.pullFromOtherRadioButton.CheckedChanged += new System.EventHandler(this.OnPullFromOtherRadioButtonChanged);
			// 
			// toolTip1
			// 
			this.toolTip1.AutoPopDelay = 30000;
			this.toolTip1.InitialDelay = 500;
			this.toolTip1.ReshowDelay = 100;
			// 
			// PullForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(403, 244);
			this.Controls.Add(this.pullFromGroupBox);
			this.Controls.Add(this.pullForceCheckBox);
			this.Controls.Add(this.pullUpdateCheckBox);
			this.Controls.Add(this.pullCancelButton);
			this.Controls.Add(this.pullOkButton);
			this.Controls.Add(this.pullShiftKeyCheckBox);
			this.Controls.Add(this.pullByRevTextBox);
			this.Controls.Add(this.pullByRevCheckBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(411, 278);
			this.Name = "PullForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Pull";
			this.pullFromGroupBox.ResumeLayout(false);
			this.pullFromGroupBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox pullByRevTextBox;
		private System.Windows.Forms.CheckBox pullShiftKeyCheckBox;
		private System.Windows.Forms.Button pullOkButton;
		private System.Windows.Forms.Button pullCancelButton;
		private System.Windows.Forms.CheckBox pullUpdateCheckBox;
		private System.Windows.Forms.CheckBox pullForceCheckBox;
		private System.Windows.Forms.CheckBox pullByRevCheckBox;
		private System.Windows.Forms.RadioButton pullFromDefaultRadioButton;
		private System.Windows.Forms.GroupBox pullFromGroupBox;
		private System.Windows.Forms.Button pullFromOtherBrowseButton;
		private System.Windows.Forms.TextBox pullFromOtherTextBox;
		private System.Windows.Forms.Label pullFromDefaultLabel;
		private System.Windows.Forms.RadioButton pullFromOtherRadioButton;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.TextBox pullFromOtherTextBoxDummy;
	}
}