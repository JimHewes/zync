﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class CommitForm : Form
	{
		private Point m_fileListBoxMouseLocation = new Point(0, 0);
		bool m_allowItemCheck = false;
		private Timer aTimer = null;
		Model m_model = null;
		Controller m_controller = null;
		string m_repositoryPath;
		bool m_isMergeCommit = false;
		CommitParams m_commitParams;
		
		public CommitForm(Model model, Controller controller, string repositoryPath)
		{
			InitializeComponent();

			m_model = model;
			m_controller = controller;
			m_repositoryPath = repositoryPath;

			m_commitParams.fileList = new List<string>();
			m_commitParams.isValidCommit = false;
			m_commitParams.repoPath = repositoryPath;

			// Disable commit button until there is text in the commit message box
			commitButton.Enabled = false;
			aTimer = new Timer();
			aTimer.Tick += new EventHandler(OnSelectedCommitFileChanged);
			aTimer.Interval = 10;


			PopulateRecent();
			PopulateFileList(true);

			// In the designer we set the ReadOnly property to true. But that also makes the 
			// background color automatically set to gray. We want it to stay as white, so reset 
			// it back to white here.
			diffRichTextBox.BackColor = Color.White;

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// There are ways to loop over all controls and set the MouseEnter handler for all controls.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			CloseBranchCheckBox.MouseEnter += (s, ea) => {toolTip1.Active = false; toolTip1.Active = true; };
			toolTip1.AutoPopDelay = 30000; 
			
			string branchName = m_model.GetCurrentBranch(repositoryPath);
			CloseBranchCheckBox.Enabled = !branchName.Equals("default");
			CloseBranchCheckBox.Checked = false;


		}

		public class FileListBoxItem
		{
			public string filename;
			public FileStatus status;
			public override string ToString()
			{
				return "[" + status.ToString().Substring(0, 1) + "] " + filename;
			}
		}

		private void PopulateRecent()
		{
			recentComboBox.Items.Clear();

			CommitMessages cm = new CommitMessages();

			List<string> messageList = cm.GetList();

			foreach (string s in messageList)
			{
				recentComboBox.Items.Add(s);
			}
			if (recentComboBox.Items.Count > 0)
			{
				recentComboBox.SelectedIndex = 0;
			}

		}

		private void PopulateFileList(bool itemsChecked)
		{
			FileStatusFlag flags = FileStatusFlag.Modified | FileStatusFlag.Added | FileStatusFlag.Removed;

			List<FileItem> fileList = m_model.GetFileList(m_repositoryPath, flags);

			// Look for an item in the list that doesn't have ResolveStatus.None. If we find one, disable the file listbox items.
			m_isMergeCommit = fileList.Find(fi => fi.resolveStatus != ResolveStatus.None).filename != null;

			fileListBox.Items.Clear();

			string modStr = FileStatusFlag.Modified.ToString();
			string cleanStr = FileStatusFlag.Clean.ToString();

			m_allowItemCheck = true;	// disable the guard
			foreach (FileItem fi in fileList)
			{
				FileListBoxItem fli = new FileListBoxItem();
				fli.filename = fi.filename;
				fli.status = fi.status;
				fileListBox.Items.Add(fli);
				fileListBox.SetItemChecked(fileListBox.Items.Count - 1, itemsChecked);
				fileListBox.Enabled = !m_isMergeCommit;
			}
			m_allowItemCheck = false;	// re-enable the guard

			if (fileListBox.Items.Count > 0)
			{
				fileListBox.SelectedIndex = 0;
			}
		}

		private void OnCommitMessageTextChanged(object sender, EventArgs e)
		{
			commitButton.Enabled = (commitMsgTextBox.Text.Length > 0) ? true : false;
		}

		private void OnRecentItemSelected(object sender, EventArgs e)
		{
			commitMsgTextBox.SelectedText = recentComboBox.Text;
		}

		private void OnClickedCheckAllButton(object sender, EventArgs e)
		{
			// Find out whether there are more checked items or more unchecked items
			int checkedItems = fileListBox.CheckedItems.Count;
			int uncheckedItems = fileListBox.Items.Count - checkedItems;

			bool newCheckState = (checkedItems < uncheckedItems) ? true : false;

			m_allowItemCheck = true;	// disable the guard
			for (int i = 0; i < fileListBox.Items.Count; ++i)
			{
				fileListBox.SetItemChecked(i, newCheckState);
			}
			m_allowItemCheck = false;	// re-enable the guard

		}

		private void OnFileListboxMouseUp(object sender, MouseEventArgs e)
		{
			base.OnMouseUp(e);
			CheckedListBox lb = (CheckedListBox)sender;

			if (e.Button == MouseButtons.Right)
			{
				Point clickPoint = new Point(e.X, e.Y);
				int index = lb.IndexFromPoint(clickPoint);

				// index could be -1 if there was no item at the clickpoint
				if (index >= 0)
				{
					string displayText = lb.Items[index].ToString();
					
					ContextMenuStrip m_contextMenu = new ContextMenuStrip();
					m_contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContextMenuItemClicked);


					ToolStripMenuItem item;
					// Show context menu 
					m_contextMenu.Items.Clear();

					// String off the leading file status text
					string filename = displayText.Substring(4);
					FileListBoxItem listboxItem = (FileListBoxItem)lb.Items[index];

					if (listboxItem.status == FileStatus.Modified)
					{
						item = new ToolStripMenuItem("Diff " + listboxItem.filename);
						item.Tag = listboxItem.filename;
						m_contextMenu.Items.Add(item);
					}

					if (m_contextMenu.Items.Count > 0)
					{
						m_contextMenu.Show(lb, clickPoint);
					}

					lb.SelectedIndex = index;
				}

			}
		}

		void ContextMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

			string filename = (string)e.ClickedItem.Tag;

			if (e.ClickedItem.Text.Substring(0, 4) == "Diff")
			{
				DiffParams diffParams;
				diffParams.revision1 = diffParams.revision2 = string.Empty;
				diffParams.filename = filename;
				diffParams.repoPath = m_commitParams.repoPath;
				diffParams.extCommand = (Properties.Settings.Default.PrefUseExternalDiff) ?
						Properties.Settings.Default.PrefExternalDiffCommand : "";

				m_controller.DiffFile(diffParams);
			}
		}


		private void OnFileListboxItemChecked(object sender, ItemCheckEventArgs e)
		{
			// The item-checked event does provide us with the mouse position when the item 
			// was checked. But we can get the position using Cursor.Position.
			// Unfortunately, that is in screen coordinates. But we can translate that to 
			// coordinates relative to the fileListBox
			Point curPos = Cursor.Position;
			Point clickPoint = fileListBox.PointToClient(curPos);

			// Since there are times when we want to programmatically change the check state, 
			// and the mouse could be anywhere at those times, we use a guard. Then we 
			// disable the guard when we want to change the check state programmatically.

			if (!m_allowItemCheck)
			{
				if (clickPoint.X > 12)
				{
					e.NewValue = e.CurrentValue;
				}
			}
		}

		private void OnFormLoad(object sender, EventArgs e)
		{
			this.Location = Properties.Settings.Default.CommitWindowLocation;
			this.Size = Properties.Settings.Default.CommitWindowSize;
			upperSplitContainer.SplitterDistance = Properties.Settings.Default.CommitUpperSplitterDistance;
			lowerSplitContainer.SplitterDistance = Properties.Settings.Default.CommitLowerSplitterDistance;
		}

		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.CommitWindowLocation = this.Location;
			Properties.Settings.Default.CommitWindowSize = this.Size;
			Properties.Settings.Default.CommitUpperSplitterDistance = upperSplitContainer.SplitterDistance;
			Properties.Settings.Default.CommitLowerSplitterDistance = lowerSplitContainer.SplitterDistance;

			Properties.Settings.Default.Save();
		}

		/** @brief This is the handler that gets called when the selected item in the commit file listbox changes.
		 * 
		 * All this function does is start a timer so that the job gets done later in the 
		 * OnSelectedCommitFileChanged() function. We want to show the diff text in the 
		 * lower RichTextBox of this dialog. But if we do that now, it will bog down the the UI.
		 * The right-click context menu gets delayed and doesn't feel very snappy.
		 * 
		 */
		private void OnSelectedListBoxIndexChanged(object sender, EventArgs e)
		{
			aTimer.Enabled = true;
		}

		/** @brief This function handles the timer event which is sent 
		 *			when the user changed the selected commit file.
		 *			
		 * This should be executed during idle time because it can take a second or two to finish.
		 */
		private void OnSelectedCommitFileChanged(object source, EventArgs e)
		{
			aTimer.Enabled = false;

			// For some reason, if I remove the parentheses from around fileListBox.SelectedItem 
			// I get a null for selectedItem, as if the cast has higher precedence than 
			// the member access (dot) operator. But that shouldn't be.

			FileListBoxItem selectedItem = (FileListBoxItem)(fileListBox.SelectedItem);
			if(selectedItem == null)
			{
				return;
			}
			diffFileLabel.Text = selectedItem.filename + " file status";

			diffRichTextBox.Clear();

			string statusLine = selectedItem.filename;

			switch (selectedItem.status)
			{
				case FileStatus.Modified:
					statusLine += " has been modified in you local folder.\n";
					break;
				case FileStatus.Added:
					statusLine += " will be added to the repository.\n";
					break;
				case FileStatus.Removed:
					statusLine += " will be removed from the the repository.\n";
					break;
			}
			Font oldFont = diffRichTextBox.SelectionFont;
			diffRichTextBox.SelectionFont = new Font("Courier", 8, FontStyle.Bold);
			diffRichTextBox.SelectionColor = Color.Black;

			// SelectedText acts like an append text function. The line will be added in the font and color we previously selected.
			diffRichTextBox.SelectedText = statusLine;
			
			DiffParams diffParams;
			diffParams.filename = selectedItem.filename;
			diffParams.revision1 = diffParams.revision2 = string.Empty;
			diffParams.repoPath = m_commitParams.repoPath;
			diffParams.extCommand = "";	// don't use external diff here

			string[] lines = m_model.GetDiff(diffParams).Split('\n');
			diffRichTextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

			foreach (string line in lines)
			{
				diffRichTextBox.SelectionFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);

				if (line.Length > 0)
				{
					if (line.Substring(0, 1) == "+")
					{
						diffRichTextBox.SelectionColor = Color.Green;
					}
					else if (line.Substring(0, 1) == "-")
					{
						diffRichTextBox.SelectionColor = Color.Red;
					}
					else if (line.Substring(0, 1) == "@")
					{
						diffRichTextBox.SelectionColor = Color.Purple;
					}
					else
					{
						diffRichTextBox.SelectionColor = Color.Black;
					}
				}

				// SelectedText acts like an append text function. The line will be added in the font and color we previously selected.
				diffRichTextBox.SelectedText = line + "\n";

			}

		}

		private void OnCommitButtonClicked(object sender, EventArgs e)
		{
			// Get the list of files that have checkmarks.

			// Since CloseBranchCheckBox is disabled of this is the default branch, 
			// it won't be checked unless this is a named branch.
			m_commitParams.closeBranch = CloseBranchCheckBox.Checked;
			
			// If this is a commit of a merge, we're not allowed to specify any files. Just do a commit.
            // Also, if all possible files are checked, don't specify any files. Some operations don't work the 
            // same if you specify all the files. For example, when commiting an .hgsub file, Mercurial won't 
            // recognize that there is a subrepository and won't create a .hgsubstate file if any files are specified/
			m_commitParams.isValidCommit = false;
            if (m_isMergeCommit || m_commitParams.closeBranch)
            {
				m_commitParams.isValidCommit = true;
            }
            else
            {
                // If all items are checked, do a commmit with no filenames.
                // If only some items are checked, do a commmit with filenames.
                // If there are no items checked, don't do a commmit at all.
                if(fileListBox.CheckedItems.Count > 0)
                {   // if only some items are checked, commit with filenames.
                    if (fileListBox.Items.Count != fileListBox.CheckedItems.Count)
                    {
                        foreach (FileListBoxItem fli in fileListBox.CheckedItems)
                        {
							m_commitParams.fileList.Add(fli.filename);
                        }
                    }
					m_commitParams.isValidCommit = true;
                }
			}

			// Save commit message
			CommitMessages cm = new CommitMessages();
			cm.Add(commitMsgTextBox.Text);
			cm.Save();

			m_commitParams.commitMessage = commitMsgTextBox.Text;
#if false
			if (m_commitParams.isValidCommit)
			{
				bool succeeded = m_controller.CommitFiles(m_commitParams.fileList, commitMsgTextBox.Text);

				if (succeeded)
				{

					// In earlier versions, the commit form didn't close after a commit but repopulated 
					// the list of files that still were not commited. This code is left here in cast that 
					// behavior is brought back. (Optionally selected by a preference maybe?)
					commitMsgTextBox.Clear();

					PopulateFileList(false);
					diffFileLabel.Text = string.Empty;
					diffRichTextBox.Clear();


					// Trigger a re-read of the folder.
					// Note: this might not be necessary after we get the filesystem monitoring working.
					//EventManager.Instance.PostEvent(new SelectedDirectoryChanged(this, m_folderPath));
				}
			}
			PopulateRecent();
#endif
		}

		public CommitParams GetCommitParams()
		{
			return m_commitParams;
		}

	}




}
