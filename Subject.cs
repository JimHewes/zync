﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zync
{
	public class Subject
	{
		private List<Observer> m_observers = new List<Observer>();

		public virtual void Attach(Observer observer)
		{
			// The Equals function returns true if two references point to the same object.
			bool exists = m_observers.Exists(s => s.Equals(observer));

			if (!exists)
			{
				m_observers.Add(observer);
			}

		}

		public virtual void Detach(Observer observer)
		{
			m_observers.RemoveAll(s => s.Equals(observer));
		}

		public void Notify(Aspect aspect)
		{
			foreach (Observer observer in m_observers)
			{
				observer.Update(this, aspect);
			}
		}

	}
}
