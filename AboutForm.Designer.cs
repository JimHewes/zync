﻿namespace Zync
{
	partial class AboutForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
			this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.logoPictureBox = new System.Windows.Forms.PictureBox();
			this.m_productNameLabel = new System.Windows.Forms.Label();
			this.m_versionLabel = new System.Windows.Forms.Label();
			this.m_copyrightLabel = new System.Windows.Forms.Label();
			this.m_companyNameLabel = new System.Windows.Forms.Label();
			this.m_descriptionTextBox = new System.Windows.Forms.TextBox();
			this.okButton = new System.Windows.Forms.Button();
			this.tableLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanel
			// 
			this.tableLayoutPanel.ColumnCount = 2;
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
			this.tableLayoutPanel.Controls.Add(this.logoPictureBox, 0, 0);
			this.tableLayoutPanel.Controls.Add(this.m_productNameLabel, 1, 0);
			this.tableLayoutPanel.Controls.Add(this.m_versionLabel, 1, 1);
			this.tableLayoutPanel.Controls.Add(this.m_copyrightLabel, 1, 2);
			this.tableLayoutPanel.Controls.Add(this.m_companyNameLabel, 1, 3);
			this.tableLayoutPanel.Controls.Add(this.m_descriptionTextBox, 1, 4);
			this.tableLayoutPanel.Controls.Add(this.okButton, 1, 5);
			this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
			this.tableLayoutPanel.Name = "tableLayoutPanel";
			this.tableLayoutPanel.RowCount = 6;
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.Size = new System.Drawing.Size(417, 265);
			this.tableLayoutPanel.TabIndex = 0;
			// 
			// logoPictureBox
			// 
			this.logoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
			this.logoPictureBox.Location = new System.Drawing.Point(3, 3);
			this.logoPictureBox.Name = "logoPictureBox";
			this.tableLayoutPanel.SetRowSpan(this.logoPictureBox, 6);
			this.logoPictureBox.Size = new System.Drawing.Size(131, 259);
			this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.logoPictureBox.TabIndex = 12;
			this.logoPictureBox.TabStop = false;
			// 
			// m_productNameLabel
			// 
			this.m_productNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_productNameLabel.Location = new System.Drawing.Point(143, 0);
			this.m_productNameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
			this.m_productNameLabel.MaximumSize = new System.Drawing.Size(0, 17);
			this.m_productNameLabel.Name = "m_productNameLabel";
			this.m_productNameLabel.Size = new System.Drawing.Size(271, 17);
			this.m_productNameLabel.TabIndex = 19;
			this.m_productNameLabel.Text = "Zync";
			this.m_productNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_versionLabel
			// 
			this.m_versionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_versionLabel.Location = new System.Drawing.Point(143, 26);
			this.m_versionLabel.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
			this.m_versionLabel.MaximumSize = new System.Drawing.Size(0, 17);
			this.m_versionLabel.Name = "m_versionLabel";
			this.m_versionLabel.Size = new System.Drawing.Size(271, 17);
			this.m_versionLabel.TabIndex = 0;
			this.m_versionLabel.Text = "Version";
			this.m_versionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_copyrightLabel
			// 
			this.m_copyrightLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_copyrightLabel.Location = new System.Drawing.Point(143, 52);
			this.m_copyrightLabel.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
			this.m_copyrightLabel.MaximumSize = new System.Drawing.Size(0, 17);
			this.m_copyrightLabel.Name = "m_copyrightLabel";
			this.m_copyrightLabel.Size = new System.Drawing.Size(271, 17);
			this.m_copyrightLabel.TabIndex = 21;
			this.m_copyrightLabel.Text = "Copyright © 2010";
			this.m_copyrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.m_copyrightLabel.Click += new System.EventHandler(this.m_copyrightLabel_Click);
			// 
			// m_companyNameLabel
			// 
			this.m_companyNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_companyNameLabel.Location = new System.Drawing.Point(143, 78);
			this.m_companyNameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
			this.m_companyNameLabel.MaximumSize = new System.Drawing.Size(0, 17);
			this.m_companyNameLabel.Name = "m_companyNameLabel";
			this.m_companyNameLabel.Size = new System.Drawing.Size(271, 17);
			this.m_companyNameLabel.TabIndex = 22;
			this.m_companyNameLabel.Text = "Author: Jim Hewes";
			this.m_companyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_descriptionTextBox
			// 
			this.m_descriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_descriptionTextBox.Location = new System.Drawing.Point(143, 107);
			this.m_descriptionTextBox.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
			this.m_descriptionTextBox.Multiline = true;
			this.m_descriptionTextBox.Name = "m_descriptionTextBox";
			this.m_descriptionTextBox.ReadOnly = true;
			this.m_descriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.m_descriptionTextBox.Size = new System.Drawing.Size(271, 126);
			this.m_descriptionTextBox.TabIndex = 23;
			this.m_descriptionTextBox.TabStop = false;
			this.m_descriptionTextBox.Text = "Zync is a GUI interface for the Mercurial version control system.";
			// 
			// okButton
			// 
			this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.okButton.Location = new System.Drawing.Point(339, 239);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(75, 23);
			this.okButton.TabIndex = 24;
			this.okButton.Text = "&OK";
			// 
			// AboutForm
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(435, 283);
			this.Controls.Add(this.tableLayoutPanel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutForm";
			this.Padding = new System.Windows.Forms.Padding(9);
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "About Zync";
			this.tableLayoutPanel.ResumeLayout(false);
			this.tableLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
		private System.Windows.Forms.PictureBox logoPictureBox;
		private System.Windows.Forms.Label m_productNameLabel;
		private System.Windows.Forms.Label m_versionLabel;
		private System.Windows.Forms.Label m_copyrightLabel;
		private System.Windows.Forms.Label m_companyNameLabel;
		private System.Windows.Forms.TextBox m_descriptionTextBox;
		private System.Windows.Forms.Button okButton;
	}
}
