﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class LogFileParamForm : Form
	{

		public bool LogByDate			{ get; private set; }
		public bool LogByRevision		{ get; private set; }
		public DateTime FromDateTime	{ get; private set; }
		public DateTime ToDateTime		{ get; private set; }
		public string FromRevision		{ get; private set; }
		public string ToRevision		{ get; private set; }
		public bool Follow				{ get; private set; }

		private LogParams m_params;


		public LogFileParamForm()
		{
			InitializeComponent();
			UpdateDateAndRevisionControls();

			logFileParamShiftKeyCheckBox.Checked = Properties.Settings.Default.LogFileFormDisplayOnlyOnShift;

		}

		private void OnClickedByDateCheckbox(object sender, EventArgs e)
		{
			if (ByDateCheckBox.Checked)
			{
				ByRevisionCheckBox.Checked = false;
			}
			UpdateDateAndRevisionControls();
		}

		public void UpdateDateAndRevisionControls()
		{
			FromDatePicker.Enabled = ByDateCheckBox.Checked;
			ToDatePicker.Enabled = ByDateCheckBox.Checked;
			FromRevisionTextBox.Enabled = ByRevisionCheckBox.Checked;
			ToRevisionTextBox.Enabled = ByRevisionCheckBox.Checked;
		}

		private void OnClickedByRevisionCheckbox(object sender, EventArgs e)
		{
			if (ByRevisionCheckBox.Checked)
			{
				ByDateCheckBox.Checked = false;
			}
			UpdateDateAndRevisionControls();
		}

		private void ValidateDate()
		{
			bool dateRangeIsValid = (FromDatePicker.Value < ToDatePicker.Value);
			LogFileParamOkButton.Enabled = dateRangeIsValid;
		}

		private void OnDateChanged(object sender, EventArgs e)
		{
			ValidateDate();
		}

		private void OnClickedOkButton(object sender, EventArgs e)
		{
			LogByDate = ByDateCheckBox.Checked;
			LogByRevision = ByRevisionCheckBox.Checked;
			FromDateTime = FromDatePicker.Value;
			ToDateTime = ToDatePicker.Value;
			FromRevision = FromRevisionTextBox.Text;
			ToRevision = ToRevisionTextBox.Text;
			Follow = FollowCheckbox.Checked;

			m_params.logByDateTime = ByDateCheckBox.Checked;
			m_params.logByRevision = ByRevisionCheckBox.Checked;
			m_params.startDateTime = FromDatePicker.Value;
			m_params.endDateTime = ToDatePicker.Value;
			m_params.startRevision = FromRevisionTextBox.Text;
			m_params.endRevision = ToRevisionTextBox.Text;
			m_params.follow = FollowCheckbox.Checked;

		}

		public LogParams GetLogFileParams()
		{
			return m_params;
		}

		private void OnShiftCheckboxChanged(object sender, EventArgs e)
		{
			Properties.Settings.Default.LogFileFormDisplayOnlyOnShift = logFileParamShiftKeyCheckBox.Checked;
		}

		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.Save();
		}
		
	}
}
