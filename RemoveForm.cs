﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class RemoveForm : Form
	{
		private RemoveParams m_params;

		public RemoveForm(List<FileItem> fileList)
		{
			InitializeComponent();

			foreach (FileItem fi in fileList)
			{
				FileListBoxItem fli = new FileListBoxItem();
				fli.filename = fi.filename;
				fli.status = fi.status;
				removeFilesListBox.Items.Add(fli);
			}
			removeAfterCheckBox.Checked = false;
			removeForceCheckBox.Checked = false;
			UpdateEffectsText();

		}

		public class FileListBoxItem
		{
			public string filename;
			public FileStatus status;
			public override string ToString()
			{
				return "[" + status.ToString().Substring(0, 1) + "] " + filename;
			}
		}

		private void UpdateEffectsText()
		{

			bool afterIsChecked = removeAfterCheckBox.Checked;
			bool forceIsChecked = removeForceCheckBox.Checked;

			if (afterIsChecked && !forceIsChecked)
			{
				removeEffectsText1.Text = "- Only missing [!] files will be scheduled for removal.";
				removeEffectsText2.Text = "- No other files will be affected.";
				removeEffectsText3.Text = String.Empty;
			}
			else if (!afterIsChecked && forceIsChecked)
			{
				removeEffectsText1.Text = "- All will be scheduled for removal.";
				removeEffectsText2.Text = "- Clean [C] and Modified [M] files will be deleted from disk.";
				removeEffectsText3.Text = "- Changes to Modified files will be lost.";
			}
			else if (afterIsChecked && forceIsChecked)
			{
				removeEffectsText1.Text = "- All will be scheduled for removal.";
				removeEffectsText2.Text = "- No files will be deleted from disk.";
				removeEffectsText3.Text = String.Empty;
			}
			else
			{
				removeEffectsText1.Text = "- Clean [C] files will be scheduled for removal and deleted from disk.";
				removeEffectsText2.Text = "- Missing [!] files will be scheduled for removal.";
				removeEffectsText3.Text = "- No other files will be affected.";
			}
		}

		private void OnAfterOrForceCheckboxChanged(object sender, EventArgs e)
		{
			UpdateEffectsText();
		}

		public RemoveParams GetRemoveParams()
		{
			return m_params;
		}

		private void OnClickedRemoveButton(object sender, EventArgs e)
		{
			m_params.after = removeAfterCheckBox.Checked;
			m_params.force = removeForceCheckBox.Checked;
		}
	}
}
