﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class MergeForm : Form
	{
		Model m_model;
		public bool Force { get; private set; }
		public bool WithRevision { get; private set; }
		public string Revision { get; private set; }

		public MergeForm(Model model, string currentFolder)
		{
			InitializeComponent();

			m_model = model;

			revisionTextBox.Text = String.Empty;
			int numberOfHeads = model.GetNumberOfHeads(currentFolder);
			//int numberOfHeads = 3;

			if (numberOfHeads > 2)
			{
				withRevisionCheckBox.Checked = true;
				withRevisionCheckBox.Enabled = false;
				headsWarningLabel.Visible = true;
				revisionTextBox.Enabled = true;

			}
			else
			{
				headsWarningLabel.Visible = false;
				headsWarningLabel.Enabled = false;
				withRevisionCheckBox.Checked = false;
				headsWarningLabel.Visible = false;
				revisionTextBox.Enabled = false;

				Size offset = new Size(0, 30);
				withRevisionCheckBox.Location -= offset;
				revisionTextBox.Location -= offset;
				forceCheckBox.Location -= offset;

				// mergeShiftKeyCheckBox, mergeOkButton, and mergeCancelButton do not need to be re-located.
				// Since they are anchored to the bottom of the form, they will automatically move when the form size is changed.

				this.MinimumSize -= offset;
				this.Size -= offset;


			}

			mergeShiftKeyCheckBox.Checked = Properties.Settings.Default.MergeFormDisplayOnlyOnShift;

			mergeOkButton.Enabled = !(withRevisionCheckBox.Checked && (revisionTextBox.Text == String.Empty));

			// Workaround for a Windows bug in which a tooltip won't reappear if if has already 
			// timed out once. See http://stackoverflow.com/questions/559707/winforms-tooltip-will-not-re-appear-after-first-use .
			// You can loop over all controls and set the MouseEnter handler for all controls as below.
			// But I never bothered to figure out how to do that when you have controls within controls, such 
			// as checkboxes within a SplitContainer which is in turn within the form.
			foreach (Control control in this.Controls)
			{
				control.MouseEnter += (s, ea) => { toolTip1.Active = false; toolTip1.Active = true; };
			}
			toolTip1.AutoPopDelay = 30000; 

		}

		private void OnWithRevisionCheckBoxChanged(object sender, EventArgs e)
		{
			revisionTextBox.Enabled = withRevisionCheckBox.Checked;
		}

		private void OnRevisionTextBoxTextChanged(object sender, EventArgs e)
		{
			mergeOkButton.Enabled = revisionTextBox.Text != String.Empty;
		}

		private void OnClickedOkButton(object sender, EventArgs e)
		{

			WithRevision = withRevisionCheckBox.Checked;
			if (WithRevision)
			{
				Revision = revisionTextBox.Text;
			}

			Force = forceCheckBox.Checked;

			Properties.Settings.Default.MergeFormDisplayOnlyOnShift = mergeShiftKeyCheckBox.Checked;
			Properties.Settings.Default.Save();
		}
	}
}
