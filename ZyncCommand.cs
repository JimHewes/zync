﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Zync
{
	public class ZyncCommand
	{
		public ZyncCommand(){ WaitForExit = true; }

		public string CurrentFolder { get; set; }
		public string ExePath { get; set; }
		public string Arguments { get; set; }
		public string Result { get; private set; }
		public string ErrorMsg { get; private set; }

		/** @brief Exit code of the command. Zero means success. **/
		public int ExitCode { get; private set; }
		public bool Succeeded { get { return ExitCode == 0;} }

		public bool WaitForExit { get; set; }
		public void Clear()
		{
			CurrentFolder = String.Empty;
			ExePath = String.Empty;
			Arguments = String.Empty;
			Result = String.Empty;
			ErrorMsg = String.Empty;
			ExitCode = 0;
		}

		/** @brief Executes the command that we set up using the ppublic properties.
		 * 
		 * It is not a considered failure (i.e. an exception) if the command that was run 
		 * finishes with an unsuccessful exit code. The Execute function still successfully 
		 * did it's job to execute the command.
		 * 
		 */
		public void Execute()
		{
			string oldCurrentFolder = Directory.GetCurrentDirectory();
			if (CurrentFolder.Length > 0)
			{
				Directory.SetCurrentDirectory(CurrentFolder);
			}

			System.Diagnostics.Process proc = new System.Diagnostics.Process();
			proc.EnableRaisingEvents = false;
			proc.StartInfo.FileName = ExePath;

			proc.StartInfo.Arguments = Arguments;
			proc.StartInfo.CreateNoWindow = true;

			// If we specify to redirect any of the standard I/O channels at all, 
			// then we need to redirect ALL three channels. That's because py2exe will 
			// throw an exception if any of the handles for the standard channels are NULL.
			// py2exe is the python compiler that was used to create hg.exe so that you don't 
			// need to install the python interpreter.
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.RedirectStandardError = true;
			proc.StartInfo.RedirectStandardInput = true;

			proc.StartInfo.UseShellExecute = false;

			proc.Start();


			if (WaitForExit)
			{
				Result = proc.StandardOutput.ReadToEnd();
				ErrorMsg = proc.StandardError.ReadToEnd();

				proc.WaitForExit();
				Directory.SetCurrentDirectory(oldCurrentFolder);
				ExitCode = proc.ExitCode;

			}
			else
			{
				Directory.SetCurrentDirectory(oldCurrentFolder);
			}
		}
	}
}
