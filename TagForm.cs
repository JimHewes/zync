﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	public partial class TagForm : Form
	{
		private TagParams m_tagParams;

		private string m_changeset;

		/**
		 * the --remove option is compatible with -force. The tage is just removed successfully.
		 * the --remove option can be sued with -local, but the operation will fail if the existing tag is not local.
		 * the --remove option cannot be used with the --rev or the operation will fail.
		 * 
		 * */
		public TagForm(string revision, string changeset)
		{
			InitializeComponent();

			// store the revision in a common place so we can get it from here later.
			revisionTextBox.Text = revision;

			m_changeset = changeset;

			// If no revision is given, then we enable the textBox to let the user enter one.
			if (revision.Length > 0)
			{
				revisionLabel.Text += "      " + revision;
				changesetLabel.Text += "   " + changeset;

				revisionTextBox.Enabled = false;
				revisionTextBox.Visible = false;
			}
			else
			{
				revisionTextBox.Enabled = true;
			}



			ValidateTagButton();
		}

		public TagParams GetTagParams()
		{
			return m_tagParams;
		}

		private void OnClickedTagButton(object sender, EventArgs e)
		{
			m_tagParams.revision = revisionTextBox.Text;
			m_tagParams.commitMessage = tagCommitMessageTextBox.Text;
			m_tagParams.replaceExisting = forceCheckBox.Checked;
			m_tagParams.makeLocal = makeLocalCheckBox.Checked;
			m_tagParams.remove = removeCheckBox.Checked;

			List<string> lineList = new List<string>(tagsTextBox.Lines);
			m_tagParams.tagList = new List<string>();

			// remove empty lines
			foreach (string line in lineList)
			{
				if (line.Length > 0)
				{
					// remove leading an trailing whitespace characters from the tag.
					line.Trim();
					m_tagParams.tagList.Add(line);
				}
			}
		}

		private void OnMakeLocalCheckBoxChanged(object sender, EventArgs e)
		{
			// Checked has the new state of the checkbox (as opposed to the previous state).
			if (makeLocalCheckBox.Checked)
			{
				removeCheckBox.Checked = false;
				removeCheckBox.Enabled = false;
			}
			else
			{
				removeCheckBox.Enabled = true;
			}
		}

		private void OnRemoveCheckBoxChanged(object sender, EventArgs e)
		{
			// Checked has the new state of the checkbox (as opposed to the previous state).
			if (removeCheckBox.Checked)
			{
				makeLocalCheckBox.Checked = false;
				makeLocalCheckBox.Enabled = false;
			}
			else
			{
				makeLocalCheckBox.Enabled = true;
			}

			RefreshAutoCommitMessage();
			ValidateTagButton();
		}

		private void OnTagsTextBoxTextChanged(object sender, EventArgs e)
		{
			RefreshAutoCommitMessage();
			ValidateTagButton();
		}

		private void RefreshAutoCommitMessage()
		{
			string[] lines = tagCommitMessageTextBox.Lines;

			if (lines.Length > 0)
			{
				string firstLine = lines[0].Trim();
				if (firstLine.Length != 0)
				{
					if(	!(firstLine.StartsWith("Added tag ") && firstLine.Contains(" for changeset "))
						&& 
						!firstLine.StartsWith("Removed tag "))
					// don't affect first line
					return;
				}
			}

			// Build a new default commit message
			List<string> nonBlankLines = new List<string>();
			foreach (string line in tagsTextBox.Lines)
			{
				string trimmed = line.Trim();
				if (trimmed.Length > 0)
				{
					nonBlankLines.Add(trimmed);
				}
			}

			string newline = "";
			if (nonBlankLines.Count > 0)
			{
				newline = removeCheckBox.Checked ? "Removed tag " : "Added tag ";
				bool addComma = false;
				foreach (string line in nonBlankLines)
				{
					if (line.Length > 0)
					{
						// remove leading an trailing whitespace characters from the tag.
						newline += (addComma ? ", " : "") + line.Trim();
						addComma = true;
					}
				}
				if (!removeCheckBox.Checked)
				{
					newline += " for changeset " + m_changeset;
				}
			}

			int newLinesSize = (tagCommitMessageTextBox.Lines.Length > 1) ? tagCommitMessageTextBox.Lines.Length : 1;
			string[] newLines = new string[newLinesSize];
			newLines[0] = newline;

			for (int i = 1; i < tagCommitMessageTextBox.Lines.Length; ++i)
			{
				newLines[i] = tagCommitMessageTextBox.Lines[i];
			}
			tagCommitMessageTextBox.Lines = newLines;

		}

		private void ValidateTagButton()
		{
			bool isValid = false;
			if (tagsTextBox.Lines.Length > 0)	// no tags mean invalid
			{
				// Get non-blank lines
				List<string> nonBlankLines = new List<string>();
				foreach (string line in tagsTextBox.Lines)
				{
					string trimmed = line.Trim();
					if (trimmed.Length > 0)
					{
						nonBlankLines.Add(trimmed);
					}
				}

				if (nonBlankLines.Count > 0)
				{
					if (removeCheckBox.Checked)
					{
						// If we're removing a tag, that tag must exist.
						// For now, we don't check this.
						isValid = true;

					}
					else
					{
						isValid = true;
					}
				}
			}

			tagButton.Enabled = isValid;
		}

	}
}
