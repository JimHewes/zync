﻿namespace Zync
{
	partial class AnnotateParamForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnnotateParamForm));
			this.DontFollowCheckbox = new System.Windows.Forms.CheckBox();
			this.annotateShiftKeyCheckBox = new System.Windows.Forms.CheckBox();
			this.annotateCancelButton = new System.Windows.Forms.Button();
			this.annotateOkButton = new System.Windows.Forms.Button();
			this.revisionCheckBox = new System.Windows.Forms.CheckBox();
			this.revisionTextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// DontFollowCheckbox
			// 
			this.DontFollowCheckbox.AutoSize = true;
			this.DontFollowCheckbox.Location = new System.Drawing.Point(12, 59);
			this.DontFollowCheckbox.Name = "DontFollowCheckbox";
			this.DontFollowCheckbox.Size = new System.Drawing.Size(213, 17);
			this.DontFollowCheckbox.TabIndex = 1;
			this.DontFollowCheckbox.Text = "Don\'t follow across copies and renames";
			this.DontFollowCheckbox.UseVisualStyleBackColor = true;
			// 
			// annotateShiftKeyCheckBox
			// 
			this.annotateShiftKeyCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.annotateShiftKeyCheckBox.AutoSize = true;
			this.annotateShiftKeyCheckBox.Location = new System.Drawing.Point(12, 114);
			this.annotateShiftKeyCheckBox.Name = "annotateShiftKeyCheckBox";
			this.annotateShiftKeyCheckBox.Size = new System.Drawing.Size(173, 17);
			this.annotateShiftKeyCheckBox.TabIndex = 6;
			this.annotateShiftKeyCheckBox.Text = "Display only if Shift key is down";
			this.annotateShiftKeyCheckBox.UseVisualStyleBackColor = true;
			// 
			// annotateCancelButton
			// 
			this.annotateCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.annotateCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.annotateCancelButton.Location = new System.Drawing.Point(289, 110);
			this.annotateCancelButton.Name = "annotateCancelButton";
			this.annotateCancelButton.Size = new System.Drawing.Size(75, 23);
			this.annotateCancelButton.TabIndex = 9;
			this.annotateCancelButton.Text = "Cancel";
			this.annotateCancelButton.UseVisualStyleBackColor = true;
			// 
			// annotateOkButton
			// 
			this.annotateOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.annotateOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.annotateOkButton.Location = new System.Drawing.Point(208, 110);
			this.annotateOkButton.Name = "annotateOkButton";
			this.annotateOkButton.Size = new System.Drawing.Size(75, 23);
			this.annotateOkButton.TabIndex = 8;
			this.annotateOkButton.Text = "Annotate";
			this.annotateOkButton.UseVisualStyleBackColor = true;
			this.annotateOkButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnClickedOkButton);
			// 
			// revisionCheckBox
			// 
			this.revisionCheckBox.AutoSize = true;
			this.revisionCheckBox.Location = new System.Drawing.Point(12, 25);
			this.revisionCheckBox.Name = "revisionCheckBox";
			this.revisionCheckBox.Size = new System.Drawing.Size(70, 17);
			this.revisionCheckBox.TabIndex = 10;
			this.revisionCheckBox.Text = "Revision:";
			this.revisionCheckBox.UseVisualStyleBackColor = true;
			this.revisionCheckBox.CheckedChanged += new System.EventHandler(this.OnAnnotateRevisionCheckboxChanged);
			// 
			// revisionTextBox
			// 
			this.revisionTextBox.Location = new System.Drawing.Point(88, 23);
			this.revisionTextBox.Name = "revisionTextBox";
			this.revisionTextBox.Size = new System.Drawing.Size(71, 20);
			this.revisionTextBox.TabIndex = 11;
			// 
			// AnnotateParamForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(376, 145);
			this.Controls.Add(this.revisionTextBox);
			this.Controls.Add(this.revisionCheckBox);
			this.Controls.Add(this.annotateCancelButton);
			this.Controls.Add(this.annotateOkButton);
			this.Controls.Add(this.annotateShiftKeyCheckBox);
			this.Controls.Add(this.DontFollowCheckbox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(384, 179);
			this.Name = "AnnotateParamForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Annotate";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox DontFollowCheckbox;
		private System.Windows.Forms.CheckBox annotateShiftKeyCheckBox;
		private System.Windows.Forms.Button annotateCancelButton;
		private System.Windows.Forms.Button annotateOkButton;
		private System.Windows.Forms.CheckBox revisionCheckBox;
		private System.Windows.Forms.TextBox revisionTextBox;
	}
}