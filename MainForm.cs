﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Windows.Forms;
using System.Text.RegularExpressions;



namespace Zync
{
    /** @brief main form for the Zync Application
     * 
     *  Note on the main window controls:
     *  In order to get the controls to resize correctly and to save and load the
     *  window size, I had to do it the following way. I first put a tableLayout control 
     *  that fills the whole window (docked). This table has two rows and one column, thus two cells.
     *  In the top cell I put a normal panel to hold controls. This is necessary because only 
     *  one control can go into a table cell, so if you want to put multiple controls in there 
     *  you need to put them in a panel and put the panel in the cell. The top cell is set 
     *  to a fixed height. Then in the "control" panel I put the various edit controls and buttons.
     *  In the botton cell of the table layout I put the split container. This is set as anchored 
     *  on all four sides (although docking to center seems to give the same behavior).
     *  None of the panel or container sizes are saved to settings. Only the main window 
     *  size, main window location, and the splitter distance are saved.
     * 
     */
	public partial class MainForm : Form, Observer
	{
		//private string m_workingFolder;
		ImageList m_fileStatusIcons;
		ImageList m_fileActionIcons;
		ImageList m_repositoryActionIcons;
		RecentWorkingFolders m_workingFolderHistory;
		private string m_rootFolder;

		private Controller m_controller = null;
		private Model m_model = null;
		private bool m_initialized = false;

		private FileStatusFlag m_statusVisibility = FileStatusFlag.None;

        /** @brief Sent if the current root folder changes.
            The root folder is the highest parent folder in the folder view.
        */
        public class RootFolderAspect : Aspect { }
        private static RootFolderAspect rootFolderAspect = new RootFolderAspect();

        Subject m_subject = new Subject();

		public MainForm()
		{
			Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
			Console.WriteLine("Local user config path: {0}", config.FilePath);

			// Note that under Mono, the platform can be detected using System.Environment.OSVersion.Platform.
			// I haven't made use of this yet, but should be helpful for things like a default path.
			// See:   http://www.mono-project.com/FAQ:_Technical 


#if false
			Regex reChangeset = new Regex(@"changeset:\s+(?<revision>\d+):(?<changeset>\w+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			Regex reDate = new Regex(@"date:\s+(?<weekday>\w+)\s+(?<month>\w+)\s+(?<date>\d+)\s+(?<hour>\d+):(?<minute>\d+):(?<second>\d+)\s+(?<year>\d+)\s+(?<zone>[-+]?\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			Regex reSummary = new Regex(@"summary:\s+(?<summary>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

			string sample = @"^summary:     Not sure what changes are made, but the files on my flash drive are later. So I'm checking in the changes on my hard drive here before overwriting with the newer files from my flash drive.";
			MatchCollection matches = reSummary.Matches(sample);

			GroupCollection groups = matches[0].Groups;
			//int numMergedFiles = Int32.Parse(groups["date"].Value);
			string numUpdatedFiles = groups["summary"].Value;
			//int year = Int32.Parse(groups["year"].Value);
			//int zone = Int32.Parse(groups["zone"].Value);
#endif

			// Here we set the Source Code Control type to Mercurial (as opposed to some other SCC).
			// This must be called before InitializeComponent because components might 
			// need to use it in their constructors.
			SCC.SetInstance(new Mercurial());

			// Handle the case when the version control isn't installed
			if (!SCC.Instance.IsInstalled())
			{
				MessageBox.Show(this, SCC.Instance.GetName() + " does not appear to be installed. Exiting.", "Zync");

				// Exit the app. Not that calling Application.Exit() doesn't totally close the app.
				System.Diagnostics.Process.GetCurrentProcess().Kill();
				return;
			}


			m_model = new Model();
			m_controller = new Controller(m_model, this);

			InitializeComponent();


			m_workingFolderHistory = new RecentWorkingFolders();

			foreach (string wf in m_workingFolderHistory.GetList())
			{
				m_workingFolderComboBox.Items.Add(wf);
			}

			m_rootFolder = Properties.Settings.Default.WorkingFolder;
			if ((m_rootFolder == String.Empty) || !Directory.Exists(m_rootFolder))
			{
				m_rootFolder = System.IO.Directory.GetCurrentDirectory();
				Properties.Settings.Default.SelectedFolder = m_rootFolder;
			}

			m_workingFolderComboBox.Text = m_rootFolder;

			m_controller.SetCommandOutput(new CommandOutput(ShowCommandOutput));
			ShowCommandOutput("Local user config path: " + config.FilePath);
			ShowCommandOutput("\nWorking folder history file: " + m_workingFolderHistory.Filepath);
			CommitMessages cm = new CommitMessages();
			ShowCommandOutput("Commit message history file: " + cm.Filepath);
			CloneSourceHistory csh = new CloneSourceHistory();
			ShowCommandOutput("Clone Source history file: " + csh.Filepath);

			// I'd prefer to pass arguments into the contructor rather than have an Initialize funciton.
			// But this is a necessary consequence of using the visual designer.
			m_folderView.Initialize(m_model, m_controller, this);
			m_fileView.Initialize(m_model, m_controller, this, m_folderView);


			m_statusDefaultPushLabel.Text = string.Empty;
			m_statusNumHeadsLabel.Text = string.Empty;

			// Make sure you have the ButtonIcons.bmp file included in the Solution Explorer.
			// Click on ButtonIcons.bmp	in the Solution Explorer and check the properties window---
			// make sure the Build Action is set to Embedded Resource.
			Stream s = this.GetType().Assembly.GetManifestResourceStream("Zync.Resources.FileStatusIcons.bmp");
			m_fileStatusIcons = new ImageList();
			m_fileStatusIcons.Images.AddStrip(Image.FromStream(s));
			s.Close();

			// Make sure you have the filelist.bmp file included in the Solution Explorer.
			// Click on filelist.bmp in the Solution Explorer and check the properties window---
			// make sure the Build Action is set to Embedded Resource.
			Stream s2 = this.GetType().Assembly.GetManifestResourceStream("Zync.Resources.FileActionIcons.bmp");
			m_fileActionIcons = new ImageList();
			m_fileActionIcons.Images.AddStrip(Image.FromStream(s2));
			s2.Close();

			Stream s3 = this.GetType().Assembly.GetManifestResourceStream("Zync.Resources.RepoActionIcons.bmp");
			m_repositoryActionIcons = new ImageList();
			m_repositoryActionIcons.Images.AddStrip(Image.FromStream(s3));
			s3.Close();


			m_cleanCheckbox.DisplayStyle = 
			m_modifiedCheckbox.DisplayStyle = 
			m_addedCheckbox.DisplayStyle =
			m_removedCheckbox.DisplayStyle = 
			m_deletedCheckbox.DisplayStyle = 
			m_ignoredCheckbox.DisplayStyle = 
			m_notTrackedCheckbox.DisplayStyle = ToolStripItemDisplayStyle.Image;

			m_cleanCheckbox.Image = m_fileStatusIcons.Images[0];
			m_modifiedCheckbox.Image = m_fileStatusIcons.Images[9];
			m_addedCheckbox.Image = m_fileStatusIcons.Images[3];
			m_removedCheckbox.Image = m_fileStatusIcons.Images[13];
			m_deletedCheckbox.Image = m_fileStatusIcons.Images[5];
			m_ignoredCheckbox.Image = m_fileStatusIcons.Images[11];
			m_notTrackedCheckbox.Image = m_fileStatusIcons.Images[2];


			m_difButton.DisplayStyle =
			m_logButton.DisplayStyle =
			m_addButton.DisplayStyle =
			m_removeButton.DisplayStyle =
			m_revertButton.DisplayStyle =
			m_ignoreButton.DisplayStyle =
			m_stopIgnoringButton.DisplayStyle = 
			m_annotateButton.DisplayStyle =
			m_renameButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
			m_addButton.Image = m_fileActionIcons.Images[0];
			m_removeButton.Image = m_fileActionIcons.Images[3];
			m_revertButton.Image = m_fileActionIcons.Images[15];
			m_ignoreButton.Image = m_fileActionIcons.Images[16];
			m_stopIgnoringButton.Image = m_fileActionIcons.Images[17];
			m_difButton.Image = m_fileActionIcons.Images[9];
			m_logButton.Image = m_fileActionIcons.Images[10];
			m_annotateButton.Image = m_fileActionIcons.Images[12];
			m_renameButton.Image = m_fileActionIcons.Images[19];

			m_pullButton.DisplayStyle =
			m_pushButton.DisplayStyle =
			m_updateButton.DisplayStyle =
			m_commitButton.DisplayStyle =
			m_mergeButton.DisplayStyle =
			m_branchButton.DisplayStyle =
			m_cloneButton.DisplayStyle =
			m_createButton.DisplayStyle = 
			m_viewButton.DisplayStyle =
			m_repoLogButton.DisplayStyle =
			m_rollbackButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
			m_pullButton.Image = m_repositoryActionIcons.Images[7];
			m_pushButton.Image = m_repositoryActionIcons.Images[8];
			m_updateButton.Image = m_repositoryActionIcons.Images[0];
			m_commitButton.Image = m_repositoryActionIcons.Images[1];
			m_mergeButton.Image = m_repositoryActionIcons.Images[9];
			m_branchButton.Image = m_repositoryActionIcons.Images[10];
			m_cloneButton.Image = m_repositoryActionIcons.Images[4];
			m_createButton.Image = m_repositoryActionIcons.Images[5];
			m_viewButton.Image = m_repositoryActionIcons.Images[2];
			m_repoLogButton.Image = m_repositoryActionIcons.Images[3];
			m_rollbackButton.Image = m_repositoryActionIcons.Images[6];



			UpdateFileActionControls();
			UpdateRepositoryActionControls();

			m_statusVisibility = (FileStatusFlag)Properties.Settings.Default.FileStatusVisibility;


			// We want to set the state of the buttons based on the model state, but changing the buttons will 
			// trigger calls to our button handler which will try to change the model. 
			//
			m_cleanCheckbox.Checked = (m_statusVisibility & FileStatusFlag.Clean) == FileStatusFlag.Clean;
			m_modifiedCheckbox.Checked = (m_statusVisibility & FileStatusFlag.Modified) == FileStatusFlag.Modified;
			m_addedCheckbox.Checked = (m_statusVisibility & FileStatusFlag.Added) == FileStatusFlag.Added;
			m_removedCheckbox.Checked = (m_statusVisibility & FileStatusFlag.Removed) == FileStatusFlag.Removed;
			m_deletedCheckbox.Checked = (m_statusVisibility & FileStatusFlag.Deleted) == FileStatusFlag.Deleted;
			m_notTrackedCheckbox.Checked = (m_statusVisibility & FileStatusFlag.NotTracked) == FileStatusFlag.NotTracked;
			m_ignoredCheckbox.Checked = (m_statusVisibility & FileStatusFlag.Ignored) == FileStatusFlag.Ignored;

			m_fileView.SetStatusVisibility(m_statusVisibility);

			// This doesn't seem to have an effect on the toolstrip buttons. I think the toolbars 
			// probably have their own tooltip system.
			toolTip1.AutoPopDelay = 30000; 
	
			// Attach to the model as an observer
			m_model.Attach(this);
			m_fileView.Attach(this);
			m_folderView.Attach(this);

			m_initialized = true;

		}

		public void ShowCommandOutput(string text)
		{
			m_commandOutputTextBox.AppendText(text + "\n");

			// Scroll the text box to the bottom so that the new txt can be seen.
			m_commandOutputTextBox.SelectionStart = m_commandOutputTextBox.Text.Length; 
			m_commandOutputTextBox.SelectionLength = 0;
			m_commandOutputTextBox.ScrollToCaret();
		}


        public void Notify(Aspect aspect)
        {
            m_subject.Notify(aspect);
        }
        public virtual void Attach(Observer observer)
        {
            m_subject.Attach(observer);
        }
        public virtual void Detach(Observer observer)
        {
            m_subject.Detach(observer);
        }


		public void Update(Subject subject, Aspect interest)
		{
			string currentFolder = m_folderView.GetCurrentFolder();
			if (interest is FileView.SelectedFilesAspect)
			{
				UpdateFileActionControls();
			}
			else if ((interest is FolderView.CurrentFolderAspect) || (interest is Model.FolderStatusChangeAspect))
			{
				UpdateRepositoryActionControls();
				
				if (m_model.IsFolderScc(currentFolder))
				{
					string defPush = m_model.GetDefaultPushRepository(currentFolder);
					if (defPush == String.Empty)
					{
						defPush = "none";
					}
					m_statusDefaultPushLabel.Text = "Default push:  " + defPush;
				}
				else
				{
					m_statusDefaultPushLabel.Text = String.Empty;
				}

				int numHeads = m_model.GetNumberOfHeads(currentFolder);
				m_statusNumHeadsLabel.Text = "Heads: " + numHeads;

				string branch = m_model.GetCurrentBranch(currentFolder);
				m_statusBranchLabel.Text = "Branch: " + branch;

			}
			else if (interest is Model.StatusChangeAspect)
			{
				// Status as a SSC folder did not change (Model.FolderStatusChangeAspect)---the 
				// folder stayed as an SCC folder. But the status of the repository changed in some way.
				// Such as, an update was done so that the parent of the working copy is now a different branch 
				// or a commit was done so that number of heads was changed.
				int numHeads = m_model.GetNumberOfHeads(currentFolder);
				m_statusNumHeadsLabel.Text = "Heads: " + numHeads;

				string branch = m_model.GetCurrentBranch(currentFolder);
				m_statusBranchLabel.Text = "Branch: " + branch;
			}
		}

		private void UpdateRepositoryActionControls()
		{

			List<string> selectedFiles = m_fileView.GetSelectedFiles();
			string currentRepoPath = m_folderView.GetCurrentFolder();

			CommandFlags flags = m_controller.GetValidCommands(currentRepoPath, selectedFiles);

			bool valid = ((flags & CommandFlags.Pull) != 0);
			pullToolStripMenuItem.Enabled = valid;
			m_pullButton.Enabled = valid;

			valid = ((flags & CommandFlags.Push) != 0);
			pushToolStripMenuItem.Enabled = valid;
			m_pushButton.Enabled = valid;

			valid = ((flags & CommandFlags.Merge) != 0);
			mergeToolStripMenuItem.Enabled = valid;
			m_mergeButton.Enabled = valid;

			valid = ((flags & CommandFlags.Update) != 0);
			updateToolStripMenuItem.Enabled = valid;
			m_updateButton.Enabled = valid;

			valid = ((flags & CommandFlags.Commit) != 0);
			commitToolStripMenuItem.Enabled = valid;
			m_commitButton.Enabled = valid;	// false for now

			valid = ((flags & CommandFlags.Create) != 0);
			createToolStripMenuItem.Enabled = valid;
			m_createButton.Enabled = valid;

			valid = ((flags & CommandFlags.Rollback) != 0);
			rollbackToolStripMenuItem.Enabled = valid;
			m_rollbackButton.Enabled = valid;

			valid = ((flags & CommandFlags.LogRepo) != 0);
			logRepoToolStripMenuItem.Enabled = valid;
			m_repoLogButton.Enabled = valid;

			valid = ((flags & CommandFlags.View) != 0);
			viewToolStripMenuItem.Enabled = valid;
			m_viewButton.Enabled = valid;

			valid = ((flags & CommandFlags.Branch) != 0);
			branchToolStripMenuItem.Enabled = valid;
			m_branchButton.Enabled = valid;

#if false
			bool currentFolderIsScc = m_model.CurrentFolderIsScc;


			pullToolStripMenuItem.Enabled = currentFolderIsScc;
			m_pullButton.Enabled = currentFolderIsScc;

			pushToolStripMenuItem.Enabled = currentFolderIsScc;
			m_pushButton.Enabled = currentFolderIsScc;

			mergeToolStripMenuItem.Enabled = currentFolderIsScc;
			m_mergeButton.Enabled = currentFolderIsScc;

			updateToolStripMenuItem.Enabled = currentFolderIsScc;
			m_updateButton.Enabled = currentFolderIsScc;
		
			commitToolStripMenuItem.Enabled = currentFolderIsScc;
			m_commitButton.Enabled = currentFolderIsScc;	// false for now
		
			createToolStripMenuItem.Enabled = !currentFolderIsScc;
			m_createButton.Enabled = !currentFolderIsScc;
	
			rollbackToolStripMenuItem.Enabled = currentFolderIsScc;
			m_rollbackButton.Enabled = currentFolderIsScc;
			
			logRepoToolStripMenuItem.Enabled = currentFolderIsScc;
			m_repoLogButton.Enabled = currentFolderIsScc;

			bool enableView = currentFolderIsScc && m_model.HasViewer;
			viewToolStripMenuItem.Enabled = enableView;
			m_viewButton.Enabled = enableView;		
#endif
			
		}

		private void UpdateFileActionControls()
		{

			List<string> selectedFiles = m_fileView.GetSelectedFiles();
			string currentRepoPath = m_folderView.GetCurrentFolder();

			CommandFlags flags = m_controller.GetValidCommands(currentRepoPath, selectedFiles);

			bool valid = ((flags & CommandFlags.Diff) != 0);
			m_difButton.Enabled = valid;
			diffToolStripMenuItem.Enabled = valid;

			valid = ((flags & CommandFlags.Annotate) != 0);
			m_annotateButton.Enabled = valid;
			annotateToolStripMenuItem.Enabled = valid;

			valid = ((flags & CommandFlags.LogFile) != 0);
			m_logButton.Enabled = valid;
			logToolStripMenuItem.Enabled = valid;

			valid = ((flags & CommandFlags.Add) != 0);
			m_addButton.Enabled = valid;
			addToolStripMenuItem.Enabled = valid;


			valid = ((flags & CommandFlags.Remove) != 0);
			m_removeButton.Enabled = valid;
			removeToolStripMenuItem.Enabled = valid;


			valid = ((flags & CommandFlags.Revert) != 0);
			m_revertButton.Enabled = valid;
			revertToolStripMenuItem.Enabled = valid;

			valid = ((flags & CommandFlags.Rename) != 0);
			m_renameButton.Enabled = valid;
			renameToolStripMenuItem.Enabled = valid;


			valid = ((flags & CommandFlags.Ignore) != 0);
			m_ignoreButton.Enabled = valid;
			ignoreToolStripMenuItem.Enabled = valid;

			valid = ((flags & CommandFlags.StopIgnore) != 0);
			m_stopIgnoringButton.Enabled = valid;
			stopIgnoringToolStripMenuItem.Enabled = valid;


#if false

			List<FileItem> selectedFiles = m_model.GetSelectedFiles();

			// Diff File button
			bool enableDiff = false;
			if (selectedFiles.Count > 0)
			{
				enableDiff = true;
				foreach (FileItem fi in selectedFiles)
				{
					if ((fi.status != FileStatus.Modified) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
						enableDiff = false;
					}
				}
			}
			m_difButton.Enabled = enableDiff;
			diffToolStripMenuItem.Enabled = enableDiff;

			// Annotate File button
			bool enableAnnotate = false;
			if (selectedFiles.Count > 0 )
			{
				enableAnnotate = true;
				foreach (FileItem fi in selectedFiles)
				{
					if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
						enableAnnotate = false;
					}
				}
			}
			m_annotateButton.Enabled = enableAnnotate;
			annotateToolStripMenuItem.Enabled = enableAnnotate;

			// Log File button
			bool enableLog = false;
			if (selectedFiles.Count > 0)
			{
				enableLog = true;
				foreach (FileItem fi in selectedFiles)
				{
					if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
						enableLog = false;
						break;
					}
				}
			}
			m_logButton.Enabled = enableLog;
			logToolStripMenuItem.Enabled = enableLog;
	
			// Add File button
			bool enableAdd = false;
			if (selectedFiles.Count > 0)
			{
				enableAdd = true;
				foreach(FileItem fi in selectedFiles)
				{
					if ((fi.status != FileStatus.NotTracked) && (fi.status != FileStatus.Removed))
					{
						enableAdd = false;
						break;
					}
				}
			}
			m_addButton.Enabled = enableAdd;
			addToolStripMenuItem.Enabled = enableAdd;

			// Remove File button
			bool enableRemove = false;
			if (selectedFiles.Count > 0)
			{
				enableRemove = true;
				foreach (FileItem fi in selectedFiles)
				{
					if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified))
					{
						enableRemove = false;
						break;
					}
				}
			}
			m_removeButton.Enabled = enableRemove;
			removeToolStripMenuItem.Enabled = enableRemove;

			// Revert File button
			bool enableRevert = false;
			if (selectedFiles.Count > 0)
			{
				enableRevert = true;
				foreach (FileItem fi in selectedFiles)
				{
					if ((fi.status != FileStatus.Modified) && (fi.status != FileStatus.Added) && (fi.status != FileStatus.Removed))
					{
						enableRevert = false;
					}
				}
			}
			m_revertButton.Enabled = enableRevert;
			revertToolStripMenuItem.Enabled = enableRevert;

			// Rename File button
			bool enableRename = false;
			if (selectedFiles.Count == 1)
			{
				FileItem fi = selectedFiles[0];
				if (fi.status == FileStatus.Clean || fi.status == FileStatus.Modified || fi.status == FileStatus.Deleted)
				{
						enableRename = true;
				}
			}
			m_renameButton.Enabled = enableRename;
			renameToolStripMenuItem.Enabled = enableRename;


			// Ignore File button
			bool enableIgnore = false;
			if (selectedFiles.Count > 0)
			{
				enableIgnore = true;
				foreach (FileItem fi in selectedFiles)
				{
					if (fi.status != FileStatus.NotTracked)
					{
						enableIgnore = false;
					}
				}
			}
			m_ignoreButton.Enabled = enableIgnore;
			ignoreToolStripMenuItem.Enabled = enableIgnore;

			// Stop Ignoring File button
			bool enableStopIgnoring = false;
			if (selectedFiles.Count > 0)
			{
				enableStopIgnoring = true;
				foreach (FileItem fi in selectedFiles)
				{
					if (fi.status != FileStatus.Ignored)
					{
						enableStopIgnoring = false;
					}
				}
			}
			m_stopIgnoringButton.Enabled = enableStopIgnoring;
			stopIgnoringToolStripMenuItem.Enabled = enableStopIgnoring;
#if false
			// Rename File button
			bool enableRename = false;
			if (selectedFiles.Count == 1)
			{
				enableRevert = true;
				foreach (FileItem fi in selectedFiles)
				{
					if (fi.status != FileStatus.Modified)
					{
						enableRevert = false;
					}
				}
			}
			m_revertButton.Enabled = enableRevert;
			revertToolStripMenuItem.Enabled = enableRevert;
#endif

#endif
		}
		
		/** @brief	A function that executes when the form loads. Sets controls to their previous saved 
		 *			application settings.
		 * 
		 * Do NOT let the designer save these settings automatically. You need to do it manually here for each control.
		 * The reason has to do with "how two way data binding works in the context of property 
		 * sets that mutually affect each other". That is noted here (but not described in detail):
		 * 
		 * http://blogs.msdn.com/rprabhu/archive/2005/11/28/497792.aspx
		 * 
		 * If you let the designer save the location of the main window, then you will get weird behavior. If you 
		 * adjust the splitter distance, then when you try to move the form the splitter distance will snap back 
		 * to the previous value. Likewise, if you let the designer save the client size of the main window then 
		 * when you try to resize the form the splitter distance will snap back to the previous value. Also, if 
		 * you just open and close the application, the splitter distance keeps increasing every time you do.
		 * 
		 * */
		private void OnFormLoad(object sender, EventArgs e)
		{
			this.Size = Properties.Settings.Default.MainWindowSize;
			this.Location = Properties.Settings.Default.MainWindowLocation;
			this.WindowState = Properties.Settings.Default.MainWindowState;
			m_mainOuterSplitContainer.SplitterDistance = Properties.Settings.Default.CommandOutputSplitterDistance;
			m_mainInnerSplitContainer.SplitterDistance = Properties.Settings.Default.MainSplitterDistance;
			
			// Saving and loading the Location property of the toolstrips saves their location 
			// on a toolstrip, but it doesn't remember which side the strip was docked to. To 
			// maintain that information we need to save and load the ToolStripManager setting explicitly.
			//
			// WARNING!! If you add another ToolStrip, MenuStrip, ot StatusStrip in the Designer, you must comment 
			// out this line once. Otherwise it will load the old settings that didn't have the new strip and 
			// that new strip won't be visible. So comment this line out and let the settings be saved with the new strip.
			// Then you can uncomment this line again.
			// (However, it may be necessary to get rid of this altogether as it would cause havoc. Then the toolstrips' 
			// location on other toolbars won't be remembered.)

			ToolStripManager.LoadSettings(this);

#if false
			////////////////////////////////////////////////////////////////////////
			// Ths following lines help to workaround a bug in the the location of tools strips.
			// See the following link for more info: 
			//   http://social.msdn.microsoft.com/forums/en-US/winforms/thread/656f5332-610d-42c3-ae2d-0ffb84a74b34/
			if (m_fileViewToolStrip.Parent == m_mainToolStripContainer.TopToolStripPanel)
				m_fileViewToolStrip.Parent = m_mainToolStripContainer.BottomToolStripPanel;
			if (m_fileActionToolStrip.Parent == m_mainToolStripContainer.TopToolStripPanel)
				m_fileActionToolStrip.Parent = m_mainToolStripContainer.BottomToolStripPanel;
			if (m_workingFolderToolStrip.Parent == m_mainToolStripContainer.TopToolStripPanel)
				m_workingFolderToolStrip.Parent = m_mainToolStripContainer.BottomToolStripPanel;
			if (m_repositoryActionToolStrip.Parent == m_mainToolStripContainer.TopToolStripPanel)
				m_repositoryActionToolStrip.Parent = m_mainToolStripContainer.BottomToolStripPanel;
			ToolStripManager.LoadSettings(this);
			///////////////////////////////////////////////////////////////////////////
#endif
		}

		/** @brief A function that executes when the form closes. Saves application settings.
		 * 
		 * Do NOT let the designer save the window size, location, and splitter settings automatically. 
		 * You need to do it manually here for each control.
		 * The reason has to do with "how two way data binding works in the context of property 
		 * sets that mutually affect each other". That is noted here (but not described in detail):
		 * 
		 * http://blogs.msdn.com/rprabhu/archive/2005/11/28/497792.aspx
		 * 
		 * If you let the designer save the location of the main window, then you will get weird behavior. If you 
		 * adjust the splitter distance, then when you try to move the form the splitter distance will snap back 
		 * to the previous value. Likewise, if you let the designer save the client size of the main window then 
		 * when you try to resize the form the splitter distance will snap back to the previous value. Also, if 
		 * you just open and close the application, the splitter distance keeps increasing every time you do.
		 * 
		 * */
		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			// Saving and loading the Location property of the toolstrips saves their location 
			// on a toolstrip, but it doesn't remember which side the strip was docked to. To 
			// maintain that information we need to save and load the ToolStripManager setting explicitly.
			ToolStripManager.SaveSettings(this);

			Properties.Settings.Default.MainWindowState = this.WindowState;

			if (this.WindowState == FormWindowState.Normal)
			{
				Properties.Settings.Default.MainWindowSize = this.Size;
				Properties.Settings.Default.MainWindowLocation = this.Location;
			}
			else
			{
				Properties.Settings.Default.MainWindowSize = this.RestoreBounds.Size;
				Properties.Settings.Default.MainWindowLocation = this.RestoreBounds.Location;
			}
			Properties.Settings.Default.MainSplitterDistance = m_mainInnerSplitContainer.SplitterDistance;
			Properties.Settings.Default.CommandOutputSplitterDistance = m_mainOuterSplitContainer.SplitterDistance;

			Properties.Settings.Default.Save();

			m_workingFolderHistory.Save();

		}

		private void OnBrowseWorkingFolderButtonClicked(object sender, EventArgs e)
		{
			if(DialogResult.OK == browseWorkingFolderDialog.ShowDialog())
			{
				// m_workingFolderTextbox.Text = browseWorkingFolderDialog.SelectedPath;
				m_workingFolderComboBox.Text = browseWorkingFolderDialog.SelectedPath;

				m_workingFolderHistory.Add(m_workingFolderComboBox.Text);
				RefreshWorkingFolderComboboxHistory();
				m_rootFolder = m_workingFolderComboBox.Text;
				Notify(rootFolderAspect);
			}
		}

		private void OnFileViewStatusButtonChanged(object sender, EventArgs e)
		{
			if (m_initialized)
			{
				// Preserve state of folder visibility
				FileStatusFlag flags = m_statusVisibility & FileStatusFlag.Folder;

				flags |= m_cleanCheckbox.Checked ? FileStatusFlag.Clean : 0;
				flags |= m_modifiedCheckbox.Checked ? FileStatusFlag.Modified : 0;
				flags |= m_notTrackedCheckbox.Checked ? FileStatusFlag.NotTracked : 0;
				flags |= m_addedCheckbox.Checked ? FileStatusFlag.Added : 0;
				flags |= m_removedCheckbox.Checked ? FileStatusFlag.Removed : 0;
				flags |= m_deletedCheckbox.Checked ? FileStatusFlag.Deleted : 0;
				flags |= m_ignoredCheckbox.Checked ? FileStatusFlag.Ignored : 0;

				m_fileView.SetStatusVisibility(flags);
				Properties.Settings.Default.FileStatusVisibility = (uint)flags;

			}
		}

		private void OnFileActionControlClicked(object sender, EventArgs e)
		{
			bool shiftDown = ((Control.ModifierKeys & Keys.Shift) == Keys.Shift);
			string currentFolder = GetCurrentFolder();
			List<string> selectedFiles = m_fileView.GetSelectedFiles();

			if (sender.Equals(m_difButton) || sender.Equals(diffToolStripMenuItem))
			{
				m_controller.DiffWorkingFiles(currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_logButton) || sender.Equals(logToolStripMenuItem))
			{
				m_controller.LogFiles(this, shiftDown, currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_annotateButton) || sender.Equals(annotateToolStripMenuItem))
			{
				if (selectedFiles.Count == 1)
				{
					m_controller.AnnotateFile(this, shiftDown, currentFolder, selectedFiles[0], "");
				}
				else
				{
					throw new Exception("Can only annotate one file at a time but there are currently " + selectedFiles.Count + " files selected.");
				}
			}
			else if (sender.Equals(m_addButton) || sender.Equals(addToolStripMenuItem))
			{
				m_controller.AddFiles(currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_removeButton) || sender.Equals(removeToolStripMenuItem))
			{
				m_controller.RemoveFiles(currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_revertButton) || sender.Equals(revertToolStripMenuItem))
			{
				m_controller.RevertFiles(currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_ignoreButton) || sender.Equals(ignoreToolStripMenuItem))
			{
				m_controller.IgnoreFiles(currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_stopIgnoringButton) || sender.Equals(stopIgnoringToolStripMenuItem))
			{
				m_controller.StopIgnoringFiles(currentFolder, selectedFiles);
			}
			else if (sender.Equals(m_renameButton) || sender.Equals(renameToolStripMenuItem))
			{
				if (selectedFiles.Count == 1)
				{
					m_controller.RenameFile(this, currentFolder, selectedFiles[0], m_fileView.GetFileStatus(selectedFiles[0]));
				}
				else
				{
					throw new Exception("Can only rename one file at a time but there are currently " + selectedFiles.Count + " files selected.");
				}
			}
		}

		private void OnRepoActionControlClicked(object sender, EventArgs e)
		{
			bool shiftDown = ((Control.ModifierKeys & Keys.Shift) == Keys.Shift);
			string currentFolder = GetCurrentFolder();

			if (sender.Equals(m_commitButton) || sender.Equals(commitToolStripMenuItem))
			{
				m_controller.Commit(this);
			}
			else if (sender.Equals(m_pushButton) || sender.Equals(pushToolStripMenuItem))
			{
				m_controller.Push(this, currentFolder);
			}
			else if (sender.Equals(m_pullButton) || sender.Equals(pullToolStripMenuItem))
			{
				m_controller.Pull(this, shiftDown, currentFolder);
			}
			else if (sender.Equals(m_updateButton) || sender.Equals(updateToolStripMenuItem))
			{
				m_controller.UpdateRepository(this, shiftDown, currentFolder, "");
			}
			else if (sender.Equals(m_mergeButton) || sender.Equals(mergeToolStripMenuItem))
			{
				m_controller.MergeRepository(this, shiftDown, currentFolder);
			}
			else if (sender.Equals(m_branchButton) || sender.Equals(branchToolStripMenuItem))
			{
				m_controller.BranchRepository(this, currentFolder);
			}
			else if (sender.Equals(m_repoLogButton) || sender.Equals(logRepoToolStripMenuItem))
			{
				m_controller.LogRepository(this, currentFolder);
			}
			else if (sender.Equals(m_cloneButton) || sender.Equals(cloneToolStripMenuItem))
			{
				m_controller.Clone();
			}
			else if (sender.Equals(m_createButton) || sender.Equals(createToolStripMenuItem))
			{
				m_controller.CreateRepository(m_folderView.GetCurrentFolder());
			}
			else if (sender.Equals(m_rollbackButton) || sender.Equals(rollbackToolStripMenuItem))
			{
				m_controller.RollbackRepository(this, m_folderView.GetCurrentFolder());
			}
			else if (sender.Equals(m_viewButton) || sender.Equals(viewToolStripMenuItem))
			{
				m_controller.ViewSelectedRepository();
			}


		}

		private void OnKeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)		// return
			{
				e.Handled = true;

				if (!Directory.Exists(m_workingFolderComboBox.Text))
				{
					MessageBox.Show(this, "Directory does not exist.");
					return;
				}

				// The path passed in must be rooted. That means it must start with 
				// either a drive specifier or a "//".
				if (!Path.IsPathRooted(m_workingFolderComboBox.Text))
				{
					MessageBox.Show(this, "The path has no root.");
					return;
				}

				m_workingFolderHistory.Add(m_workingFolderComboBox.Text);
				RefreshWorkingFolderComboboxHistory();
				Properties.Settings.Default.WorkingFolder = m_workingFolderComboBox.Text;
				m_rootFolder = m_workingFolderComboBox.Text;
				Notify(rootFolderAspect);

			}
		}

		// This function will get called when the user makes a selection from the drop down list 
		// of the working folder combobox. By the time this function is called, the Text property 
		// of the combobox will already have been set to the new selection.
		private void OnWorkingFolderComboBoxIndexChanged(object sender, EventArgs e)
		{
			if (!Directory.Exists(m_workingFolderComboBox.Text))
			{
			    MessageBox.Show(this, "Directory does not exist.");
                return;
			}

			// The path passed in must be rooted. That means it must start with 
			// either a drive specifier or a "//".
			if (!Path.IsPathRooted(m_workingFolderComboBox.Text))
			{
			    MessageBox.Show(this, "The path has no root.");
				return;
			}

			m_workingFolderHistory.Add(m_workingFolderComboBox.Text);

			// Calling RefreshWorkingFolderComboboxHistory() will trigger another IndexChanged event 
			// and send us back in here revursively, which we don't want. So temporarily 
			// disable the even while we update the history list.
			m_workingFolderComboBox.SelectedIndexChanged -= OnWorkingFolderComboBoxIndexChanged;
			RefreshWorkingFolderComboboxHistory();
			m_workingFolderComboBox.SelectedIndexChanged += OnWorkingFolderComboBoxIndexChanged;

			Properties.Settings.Default.WorkingFolder = m_workingFolderComboBox.Text;
			m_rootFolder = m_workingFolderComboBox.Text;
			Notify(rootFolderAspect);

		}

         
        public string GetRootFolder()
        {
			return m_rootFolder;
        }

		private void RefreshWorkingFolderComboboxHistory()
		{
			string savetext = m_workingFolderComboBox.Text;	// save the text because the call to Clear() will clear it.
			m_workingFolderComboBox.Items.Clear();
			foreach (string wf in m_workingFolderHistory.GetList())
			{
				m_workingFolderComboBox.Items.Add(wf);
			}
			m_workingFolderComboBox.Text = savetext;
		}

		//int dis = global::Zync.Properties.Settings.Default.SplitterDistance;
		private void OnFormShown(object sender, EventArgs e)
		{
			m_mainInnerSplitContainer.SplitterDistance = global::Zync.Properties.Settings.Default.MainSplitterDistance;
			m_mainOuterSplitContainer.SplitterDistance = global::Zync.Properties.Settings.Default.CommandOutputSplitterDistance;
		}

		private void OnApplicationPreferences(object sender, EventArgs e)
		{
			PreferencesForm prefForm = new PreferencesForm();

			bool oldSORF = Properties.Settings.Default.ShowOnlyRepositoryFolders;
			bool oldSI = Properties.Settings.Default.ShowIncoming;
			prefForm.ShowDialog();

			if((oldSORF != Properties.Settings.Default.ShowOnlyRepositoryFolders) || 
				(oldSI != Properties.Settings.Default.ShowIncoming))
			{
				m_folderView.RefreshView();
			}
		}
		
		private void OnApplicationAbout(object sender, EventArgs e)
		{
			AboutForm aboutForm = new AboutForm();
			aboutForm.ShowDialog(this);
		}

		private void OnApplicationQuit(object sender, EventArgs e)
		{
			Application.Exit();
		}

		public string GetCurrentFolder()
		{
			return m_folderView.GetCurrentFolder();
		}
		public List<string> GetSelectedFiles()
		{
			return m_fileView.GetSelectedFiles();
		}
		public void SetCurrentFolder(string folder)
		{
			m_folderView.SelectDirectory(folder);
		}


	}

	public class RecentWorkingFolders : RecentText
	{
		public RecentWorkingFolders()
			: base("Zync", "workingfolders.xml", "WorkingFolders","Path", 8)
		{}
	}

}
