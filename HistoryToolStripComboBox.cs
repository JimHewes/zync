﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Zync
{
	class HistoryToolStripComboBox : ToolStripComboBox
	{
		string m_filename;

		RecentText m_historyTextFile;

		public HistoryToolStripComboBox(string appName, string filename)
		{
			m_filename = filename;
			m_historyTextFile = new RecentText(appName, filename, "HistoryText", "Text", 10);
		}

		protected override void OnSelectionChangeCommitted(EventArgs e)
		{
			base.OnSelectionChangeCommitted(e);

			m_historyTextFile.Add(Text);
			RefreshComboBoxHistory();

		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);

			if (e.KeyChar == 13)		// return
			{
				if (Directory.Exists(Text))
				{
					m_historyTextFile.Add(Text);
					RefreshComboBoxHistory();
				}
				else
				{
					MessageBox.Show("Directory does not exist.");
				}
				e.Handled = true;
			}
		}

		private void RefreshComboBoxHistory()
		{
			string savetext = Text;	// save the text because the call to Clear() will clear it.
			Items.Clear();
			foreach (string htf in m_historyTextFile.GetList())
			{
				Items.Add(htf);
			}
			Text = savetext;
		}
	}

}
