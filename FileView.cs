﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Timers;


namespace Zync
{
	public partial class FileView : ListView, Observer
	{

		public class SelectedFilesAspect : Aspect { }
		private static SelectedFilesAspect selectedFilesAspect = new SelectedFilesAspect();

		Subject m_subject = new Subject();

		private Model m_model = null;
		private Controller m_controller = null;
		MainForm m_mainForm = null;
		FolderView m_folderView;

		// The reason we define the column headers in code here rather than use 
		// the Visual Studio designer is because the designer will put these headers 
		// in the MainForm which isn't where we want them. They are better encapsulated 
		// in this custom FileView class.
		private System.Windows.Forms.ColumnHeader filenameHeader = new System.Windows.Forms.ColumnHeader();
		private System.Windows.Forms.ColumnHeader pathHeader = new System.Windows.Forms.ColumnHeader();
		private System.Windows.Forms.ColumnHeader statusHeader = new System.Windows.Forms.ColumnHeader();
		private System.Windows.Forms.ColumnHeader sizeHeader = new System.Windows.Forms.ColumnHeader();
		private System.Windows.Forms.ColumnHeader dateHeader = new System.Windows.Forms.ColumnHeader();
		private bool m_sortAscending = true;

		public const int CiFilename = 0;
		public const int CiPath = 1;
		public const int CiStatus = 2;
		public const int CiFileSize = 3;
		public const int CiFileDateTime = 4;
		public const int numColumns = 5;

		private int m_columnSortIndex = CiStatus;

		private FileStatusFlag m_statusVisibility;


      /** This is a list of all the files under source code control for the current working 
         *  directory or for whatever parent is the working directory. The reason it's a member 
         *  variable is because we don't want to re-read the list just to display a different subset 
         *  of the files. So this list contains all files of all status, and you can filter it for display.
         */  
		enum ImgIndex:int
		{
			CleanText, CleanBinary, NotTracked, Added, MergeConflicts, DeletedFile,
			SccFolder, Folder, UpDir, MissingFolder, ModText, ModBinary, IgnoredFile,
			IgnoredFolder, RemovedFile, AddedBinary, CleanUnicode, ModUnicode, AddedUnicode,
			CleanE, ModE, AddedE, 
			NumImages		// NumImage is not an index. It is used only to get the number of indexes in this enumeration.
		};

		public FileView()
		{
			InitializeComponent();

			Columns.AddRange(new ColumnHeader[] {
            filenameHeader,
			pathHeader,
            statusHeader,
            sizeHeader,
			dateHeader});

			filenameHeader.Text = "Filename";
			filenameHeader.Width = Properties.Settings.Default.FilenameColumnWidth;
			filenameHeader.DisplayIndex = Properties.Settings.Default.FilenameColumnDisplayIndex;

			pathHeader.Text = "Path";
			pathHeader.Width = Properties.Settings.Default.PathColumnWidth;
			pathHeader.DisplayIndex = Properties.Settings.Default.PathColumnDisplayIndex;

			statusHeader.Text = "Status";
			statusHeader.Width = Properties.Settings.Default.StatusColumnWidth;
			statusHeader.TextAlign = HorizontalAlignment.Center;
			statusHeader.DisplayIndex = Properties.Settings.Default.StatusColumnDisplayIndex;

			sizeHeader.Text = "Size"; 
			sizeHeader.Width = Properties.Settings.Default.SizeColumnWidth;
			sizeHeader.TextAlign = HorizontalAlignment.Right;
			sizeHeader.DisplayIndex = Properties.Settings.Default.SizeColumnDisplayIndex;

			dateHeader.Text = "Date";
			dateHeader.Width = Properties.Settings.Default.DateColumnWidth;
			dateHeader.TextAlign = HorizontalAlignment.Left;
			dateHeader.DisplayIndex = Properties.Settings.Default.DateColumnDisplayIndex;

			// Load the FileView images from the resource.
			// For instructions on how to add an image as a resource and use it, 
			// see http://support.microsoft.com/kb/324567 . 
			// Make sure you have the FileViewIcons.bmp file included in the Solution Explorer.
			// Click on FileViewIcons.bmp in the Solution Explorer and check the properties window---
			// make sure the Build Action is set to Embedded Resource.
			// ListView seems to throw an exception when I try to set TransparentColor. I read that it 
			// doesn't like doing transparency using BMP files, so if I ever want transparency here 
			// I'll need to chage the file to PNG or something else.
			Stream s = this.GetType().Assembly.GetManifestResourceStream("Zync.Resources.FileViewIcons.bmp");
			ImageList imageList = new ImageList();
			imageList.Images.AddStrip(Image.FromStream(s));
			s.Close();
			imageList.ImageSize = new Size(32, 16);

			// Assign the ImageList to the FileView.
			LargeImageList = imageList;
			SmallImageList = imageList;

			ListViewItemSorter = new ListViewItemComparer(m_columnSortIndex);

			// Set the FileView control's default image index.
			//ImageIndex = 0;



		}

		// We set the model and controller at the same time because we might use both
		public void Initialize(Model model, Controller controller, MainForm mainForm, FolderView folderView)
		{
			m_model = model;
			m_controller = controller;
			m_mainForm = mainForm;
			m_folderView = folderView;

			m_model.Attach(this);
			m_folderView.Attach(this);
 
			RefreshSelected();
		}

		public void Update(Subject subject, Aspect interest)
		{
			if ((interest is FolderView.CurrentFolderAspect) || (interest is Model.StatusChangeAspect))
			{
				RefreshSelected();
			}
			else if (interest is Model.FolderStatusChangeAspect)
			{
				if(((Model.FolderStatusChangeAspect)interest).FolderPath.Equals(m_mainForm.GetCurrentFolder()))
				{
					RefreshSelected();
				}
			}

		}

		public void SetStatusVisibility(FileStatusFlag flags)
		{
			m_statusVisibility = flags;
			RefreshSelected();
		}
		
		public void Notify(Aspect aspect)
		{
			m_subject.Notify(aspect);
		}
		public virtual void Attach(Observer observer)
		{
			m_subject.Attach(observer);
		}
		public virtual void Detach(Observer observer)
		{
			m_subject.Detach(observer);
		}


		protected override void OnPaint(PaintEventArgs pe)
		{
			base.OnPaint(pe);
		}

		protected override void OnColumnWidthChanged(ColumnWidthChangedEventArgs e)
		{

			switch (e.ColumnIndex)
			{
				case CiFilename:
					Properties.Settings.Default.FilenameColumnWidth = filenameHeader.Width;
					break;
				case CiStatus:
					Properties.Settings.Default.StatusColumnWidth = statusHeader.Width;
					break;
				case CiFileSize:
					Properties.Settings.Default.SizeColumnWidth = sizeHeader.Width;
					break;
				case CiFileDateTime:
					Properties.Settings.Default.DateColumnWidth = dateHeader.Width;
					break;
			}
			base.OnColumnWidthChanged(e);
			Properties.Settings.Default.Save();
		}


		/** @brief This function gets called when the columns are reorder by the user. Column 
		 *			order is saved to application settings.
		 *
		 * When this function is entered, the column header display indexes have not get been 
		 * reordered. You can only get the old order. There is no handler we can override to 
		 * get the new order. So rather than bother to calculate the new order ourselves, we 
		 * just set a timer and wait for the new order to get set. Then by the time the timer 
		 * handler gets called, the new order has been established and we can easily save it 
		 * to the application settings.
		 * Alternatively, we could wait until the file view is closed to save settings, but I prefer 
		 * to save settings as soon as changes are made in case something catatrophic happens 
		 * in the program and the file view doesn't close properly. Then the changes are 
		 * still preserved. 
		 */
		protected override void OnColumnReordered(ColumnReorderedEventArgs e)
		{
			System.Timers.Timer timer = new System.Timers.Timer();
			timer.Elapsed+=new ElapsedEventHandler(OnTimedEvent);
			timer.Interval=10;	// Set the Interval to 10 milliseconds. We want it to happen soon.
			timer.AutoReset = false;	// make it a one-shot timer
			timer.Enabled=true;
	
			base.OnColumnReordered(e);
		}

		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			// Save the column order
			Properties.Settings.Default.FilenameColumnDisplayIndex = filenameHeader.DisplayIndex;
			Properties.Settings.Default.StatusColumnDisplayIndex = statusHeader.DisplayIndex;
			Properties.Settings.Default.SizeColumnDisplayIndex = sizeHeader.DisplayIndex;
			Properties.Settings.Default.DateColumnDisplayIndex = dateHeader.DisplayIndex;
			Properties.Settings.Default.Save();

		}

		public void RefreshSelected()
		{
			ListViewItemComparer comparer = (ListViewItemComparer)ListViewItemSorter;
			ListViewItemSorter = null;

			// Turn off sorting while adding items
			BeginUpdate();

			// Is this a source code control directory?
			//if (m_model.IsSccFolder(m_currentDirectory))
			{
				// record is an array of the strings that are to be shown in each column of a row.
				//		record[0] is the Filename column
				//		record[1] is the Status column
				//		record[2] is the Size column
				//		record[3] is the Date column
				// This order is the same regardless of the order in which the columns happen to be displayed.
				string[] record = new string[numColumns];

				Items.Clear();
				ImgIndex imgIndex = ImgIndex.CleanText;
				ListViewItem lvi;
				record[CiStatus] = record[CiPath] = record[CiFileSize] = record[CiFileDateTime] = "";

				string currentFolder = m_folderView.GetCurrentFolder();

				// Current folder will be empty if there is no selected folder.
				if (currentFolder.Length == 0)
				{
					return;
				}
				if (Directory.GetParent(currentFolder) != null)
				{
					record[CiFilename] = "..";
					lvi = new ListViewItem(record);

					// Specify the image (in the filename column) based on the file status and whether the file is read-only.
					lvi.ImageIndex = (int)ImgIndex.UpDir;

					// Save the status to the tag. It is later used for the context menu.
					lvi.Tag = new FileItem() { filename = "..", status = FileStatus.Folder };

					Items.Add(lvi);
				}

				// Show subdirectories
				string[] subdirs = Directory.GetDirectories(currentFolder);

				foreach (string s in subdirs)
				{
					// to be created in the ListViewItem equal to the number of length of the array.
					string ss = Path.GetFileName(s);
					record[CiFilename] = ss;
					lvi = new ListViewItem(record);
					
					bool isSccDir = m_model.IsFolderScc(s);
					if (!Properties.Settings.Default.ShowOnlyRepositoryFolders || isSccDir)
					{
						// Specify the image (in the filename column) based on the file status and whether the file is read-only.
						lvi.ImageIndex = isSccDir ? (int)ImgIndex.SccFolder : (int)ImgIndex.Folder;

						// Save the status to the tag. It is later used for the context menu.
						//lvi.Tag = FileStatus.Folder;
						lvi.Tag = new FileItem() { filename = ss, status = FileStatus.Folder };

						Items.Add(lvi);
					}

				}

				List<FileItem> fileList;
				if (m_model.IsFolderScc(currentFolder))
				{
					FileStatusFlag flags = FileStatusFlag.Modified | FileStatusFlag.Clean | FileStatusFlag.NotTracked |
											FileStatusFlag.Added | FileStatusFlag.Deleted | FileStatusFlag.Removed | FileStatusFlag.Ignored;
					fileList = m_model.GetFileList(currentFolder, flags);
				}
				else
				{
					string[] rawFileList = Directory.GetFiles(currentFolder);
					fileList = new List<FileItem>();
					foreach (string s in rawFileList)
					{
						// The filenames in the file list are full paths, so we need to get just the filename
						fileList.Add(new FileItem { filename = Path.GetFileName(s), status = FileStatus.NotTracked });
					}

				}


				foreach (FileItem fileItem in fileList)
				{
					switch(fileItem.status)
					{
						case FileStatus.Clean:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.Clean) == FileStatusFlag.Clean ? "Clean" : String.Empty;
							imgIndex = ImgIndex.CleanText;
							break;
						case FileStatus.Modified:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.Modified) == FileStatusFlag.Modified ? "Modified" : String.Empty;
							imgIndex = ImgIndex.ModText;
							break;
						case FileStatus.Added:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.Added) == FileStatusFlag.Added ? "Added" : String.Empty;
							imgIndex = ImgIndex.Added;
							break;
						case FileStatus.Removed:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.Removed) == FileStatusFlag.Removed ? "Removed" : String.Empty;
							imgIndex = ImgIndex.RemovedFile;
							break;
						case FileStatus.Deleted:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.Deleted) == FileStatusFlag.Deleted ? "Deleted" : String.Empty;
							imgIndex = ImgIndex.DeletedFile;
							break;
						case FileStatus.NotTracked:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.NotTracked) == FileStatusFlag.NotTracked ? "?" : String.Empty;
							record[CiStatus] = m_model.IsFolderScc(currentFolder) ? record[CiStatus] : "?";	// override if this is not a SCC directory. Always show files.
							imgIndex = ImgIndex.NotTracked;
							break;
						case FileStatus.Ignored:
							record[CiStatus] = (m_statusVisibility & FileStatusFlag.Ignored) == FileStatusFlag.Ignored ? "Ignored" : String.Empty;
							imgIndex = ImgIndex.IgnoredFile;
							break;
					}


                    // If Length > 0 it means the button for that type of file is down 
                    // and so we want to show this type of file.
					if (record[CiStatus].Length > 0)
					{
						record[CiFilename] = Path.GetFileName(fileItem.filename);
						string path = Path.GetDirectoryName(fileItem.filename);
						record[CiPath] = (path == String.Empty) ? String.Empty : Path.Combine(".", Path.GetDirectoryName(fileItem.filename));

						FileInfo fileInfo = new FileInfo(Path.Combine(currentFolder, fileItem.filename));
						int fileSizeInKB = 0;
						long fileSizeInBytes = 0;
						long fileDateTimeTicks = 0;
						if (fileInfo.Exists)
						{
							if (fileInfo.Length > 0)
							{
								fileSizeInKB = (int)((fileInfo.Length / 1024.0) + 0.5);
								fileSizeInKB = (fileSizeInKB > 0) ? fileSizeInKB : 1;
								fileSizeInBytes = fileInfo.Length;
							}
							record[CiFileSize] = fileSizeInKB.ToString() + " KB";

							record[CiFileDateTime] = fileInfo.LastWriteTime.ToShortDateString() + " " + fileInfo.LastWriteTime.ToShortTimeString();
							fileDateTimeTicks = fileInfo.LastWriteTime.Ticks;
						}
						else
						{
							record[CiPath] = record[CiFileSize] = record[CiFileDateTime] = string.Empty;
						}


						// Create a new ListViewItem from the array of strings. Each string in the array is 
						// for a column, the text is shown in that column. This causes a number of SubItems 
						// to be created in the ListViewItem equal to the number of length of the array.
						lvi = new ListViewItem(record);

						// Specify the image (in the filename column) based on the file status and whether the file is read-only.
						//lvi.ImageIndex = fileInfo.IsReadOnly ? (int)ImgIndex.NumImages + (int)imgIndex : (int)imgIndex;
						lvi.ImageIndex = fileItem.resolveStatus == ResolveStatus.Unresolved ? ((int)ImgIndex.NumImages * 2) + (int)imgIndex :
									fileItem.resolveStatus == ResolveStatus.Resolved ? (int)ImgIndex.NumImages + (int)imgIndex : (int)imgIndex;
						lvi.BackColor = fileItem.resolveStatus == ResolveStatus.Unresolved ? Color.FromArgb(0xFF, 0xD0, 0xD0) :
									fileItem.resolveStatus == ResolveStatus.Resolved ? Color.FromArgb(0xD0, 0xFF, 0xD0) : Color.White;

						// Save the full filepath and status to the tag. It is later used for the context menu.
						lvi.Tag = fileItem;

						// Save the file size to that column's Tag. This is later used for sorting according to file size.
						// Same with the date-time. 
						lvi.SubItems[CiFileSize].Tag = fileSizeInBytes;
						lvi.SubItems[CiFileDateTime].Tag = fileDateTimeTicks;

						// Set the Name of the ListViewItem to be the filename. We can then later use the filename to search the 
						// file list and get this list item.
						lvi.Name = record[CiFilename];

						Items.Add(lvi);
					}
				}

				EndUpdate();
				ListViewItemSorter = comparer;
				Sort();

#if false
				string[] files = Directory.GetFiles(directory);

				string[] record = new string[2];
				foreach (string f in files)
				{
					record[0] = Path.GetFileName(f);

					switch (SCC.Instance.GetFileStatus(f))
					{
						case FileStatus.Clean:		record[1] = "Clean";		break;
						case FileStatus.Modified:	record[1] = "Modified";		break;
						case FileStatus.Added:		record[1] = "Added";		break;
						case FileStatus.Removed:	record[1] = "Removed";		break;
						case FileStatus.Deleted:	record[1] = "Deleted";		break;
						case FileStatus.NotTracked:	record[1] = "Not tracked";	break;
						case FileStatus.Ignored:	record[1] = "Ignored";		break;
					}
					Items.Add(new ListViewItem(record));

				}
#endif
			}
			//else
			//{
				// Not a source code control directory. So clear the view.
			//	Items.Clear();
			//}

			//EventManager.Instance.PostEvent(new SelectedFilesChanged(this));

			// There are no longer any selected files since the directory changed
			Notify(selectedFilesAspect);
#if false
				ZyncCommand zc = new ZyncCommand();
				zc.CurrentFolder = zdi.fullPath;
				zc.ExePath = "hg";
				zc.Arguments = "status -c";
				if (Properties.Settings.Default.ModifiedCheckboxState)
				{
					zc.Arguments += " -m";
				}

				zc.Execute();

				string result = zc.Result;

				//List<string> lines = SeparateToLines(result);
				string[] lines = result.Split('\n');

				string[] record = new string[2];

				foreach (string s in lines)
				{
					//m_fileView
					if (s.Length > 0)
					{
						record[0] = s.Substring(2);
						record[1] = s.Substring(0, 1);

						m_fileView.Items.Add(new ListViewItem(record));

					}
				}

			}
#endif
		}

		struct ContextMenuCommand
		{
			public string text;
			public int numSelectedAllowed;	// if more than this number of file is selected, the command is not shown. -1 allow any number of files to be selected.
			public FileStatusFlag flags;
			public string toolTipText;
		}

		ContextMenuCommand[] contextMenuCommands = new ContextMenuCommand[]
		{
			new ContextMenuCommand() { text = "Diff...", numSelectedAllowed = -1, flags = FileStatusFlag.Modified | FileStatusFlag.Clean },
			new ContextMenuCommand() { text = "Log...", numSelectedAllowed = -1, flags = FileStatusFlag.Clean | FileStatusFlag.Deleted | FileStatusFlag.Modified | FileStatusFlag.Removed},
			new ContextMenuCommand() { text = "Annotate...", numSelectedAllowed = 1, flags = FileStatusFlag.Clean | FileStatusFlag.Deleted | FileStatusFlag.Modified | FileStatusFlag.Removed},
			new ContextMenuCommand() { text = "Add...", numSelectedAllowed = -1, flags = FileStatusFlag.NotTracked},
			new ContextMenuCommand() { text = "Remove...", numSelectedAllowed = -1, flags = FileStatusFlag.Added | FileStatusFlag.Clean | FileStatusFlag.Modified | FileStatusFlag.Deleted, 
										toolTipText = "Any selected files with Added status will immediately revert to being not tracked.\nAll others will be scheduled to be removed on the next commit" },
			new ContextMenuCommand() { text = "Revert", numSelectedAllowed = -1, flags = FileStatusFlag.Added | FileStatusFlag.Removed | FileStatusFlag.Modified | FileStatusFlag.Deleted},
			new ContextMenuCommand() { text = "Ignore", numSelectedAllowed = -1, flags = FileStatusFlag.NotTracked},
			new ContextMenuCommand() { text = "Stop Ignoring", numSelectedAllowed = -1, flags = FileStatusFlag.Ignored},
			new ContextMenuCommand() { text = "Rename...", numSelectedAllowed = 1, flags = FileStatusFlag.Clean | FileStatusFlag.Modified | FileStatusFlag.Deleted | FileStatusFlag.NotTracked},

		};

		protected override void OnMouseDoubleClick(System.Windows.Forms.MouseEventArgs e)
		{
			base.OnMouseDoubleClick(e);

			ListViewItem clickedItem = GetItemAt(e.X, e.Y);
			
			FileStatusFlag clickedItemStatus = (FileStatusFlag)(( FileItem)(clickedItem.Tag)).status;
			if (clickedItemStatus == FileStatusFlag.Folder)
			{
				string newDir;
				if (clickedItem.Text == "..")	// if this is the "up" directory folder item
				{
					DirectoryInfo di = Directory.GetParent(m_mainForm.GetCurrentFolder());
					newDir = di.FullName;
				}
				else
				{
					newDir = Path.Combine(m_mainForm.GetCurrentFolder(), clickedItem.Text);
				}
				m_mainForm.SetCurrentFolder(newDir);
			}

		}


		ContextMenuStrip m_contextMenu;

		protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
		{
			base.OnMouseUp(e);
			if (!m_model.IsFolderScc(m_mainForm.GetCurrentFolder()))
			{
				return;
			}

			if (e.Button == MouseButtons.Right)
			{
				FileStatusFlag flags = FileStatusFlag.None;
				bool multipleSelectedItems = (SelectedItems.Count > 1);

				foreach (ListViewItem lvi in SelectedItems)
				{
					flags |= (FileStatusFlag)((FileItem)(lvi.Tag)).status;
				}

				ListViewItem clickedItem = GetItemAt(e.X, e.Y);

				if (clickedItem != null)
				{

					FileStatusFlag clickedItemStatus = (FileStatusFlag)((FileItem)(clickedItem.Tag)).status;

					m_contextMenu = new ContextMenuStrip();
					m_contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(OnContextMenuItemClicked);
					ToolStripMenuItem item;

					// For each command that may potentially show up in the context menu,
					// if that command does not support the status of all selected files,
					// then that command does not appear in the menu.
					// Specifically, if the flags variable has a bit on that the command's flags does not, 
					// then that command does not appear in the menu. If flags has any bits turned on that cmc.flags 
					// doesn't then the result of ORing them will not equal cmc.flags
					foreach (ContextMenuCommand cmc in contextMenuCommands)
					{
						if((flags | cmc.flags) == cmc.flags)	
						{
							item = new ToolStripMenuItem(cmc.text);
							item.Tag = clickedItem;
							item.ToolTipText = cmc.toolTipText;
							if ((cmc.numSelectedAllowed < 0) || (SelectedItems.Count <= cmc.numSelectedAllowed))
							{
								m_contextMenu.Items.Add(item);
							}
						}
					}

					if (m_contextMenu.Items.Count > 0)
					{
						Point clickPoint = new Point(e.X, e.Y);
						m_contextMenu.Show(this, clickPoint);
					}

				}
			}
		}

		/** @brief Get the status of a file in the file view.

			This function is mostly for the benefit of other classes. While they could just call the 
			model to get this information, the FileView already has it in memory and will be faster.

			(Consider changing the Model/View/Controller design so that the model caches this information, 
			more like the Qt Model/View.)

			@param filepath		the path+filename of the file relative to the repository folder. This is the  
								the path displayed in the FileView plus the filename in the FileView.
			@returns			the FileStatus of the file.

		    @throws	exception if the file is not in the file view. Presumably the filename has been recently 
					obtained with a call to something like GetSelectedFiles().
		**/
		public FileStatus GetFileStatus(string filepath)
		{
			// The filepath includes not just the filename but the path, so we need to look into the 
			// FileItem (in the Tag) which stores the path in its filename member.
			foreach (ListViewItem item in Items)
			{
				FileItem fi = (FileItem)item.Tag;
				if (fi.filename == filepath)
				{
					return fi.status;
				}
			}
			throw new Exception("File not found in FileView.GetFileStatus().");
#if false
			ListViewItem[] lvi = Items.Find(filepath, false);
			if(lvi.Length == 1)
			{
				return ((FileItem)(lvi[0].Tag)).status;
			}
			else
			{
				throw new Exception("File not found in FileView.GetFileStatus().");
			}
#endif
		}

		void OnContextMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			m_contextMenu.Close();

			bool shiftDown = ((Control.ModifierKeys & Keys.Shift) == Keys.Shift);

			string currentFolder = m_mainForm.GetCurrentFolder();

			//ListViewItem listViewItem = (ListViewItem)e.ClickedItem.Tag;
			//string filename = listViewItem.SubItems[0].Text;
			//FileStatus fileStatus = (FileStatus)listViewItem.Tag;
			List<string> selectedFiles = GetSelectedFiles();

			switch (e.ClickedItem.Text)
			{
				case "Diff...":
					m_controller.DiffWorkingFiles(currentFolder, selectedFiles); 
					break;
				case "Annotate...":
					if (selectedFiles.Count == 1)
					{
						m_controller.AnnotateFile(this, shiftDown, currentFolder, selectedFiles[0], ""); 
					}
					else
					{
						throw new Exception("Can only annotate one file at a time but there are currently " + selectedFiles.Count + " files selected.");
					}
					break;
				case "Log...":
					m_controller.LogFiles(this, shiftDown, currentFolder, selectedFiles); 
					break;
				case "Add...":
					m_controller.AddFiles(currentFolder, selectedFiles); 
					break;
				case "Remove...":
					m_controller.RemoveFiles(currentFolder, selectedFiles); 
					break;
				case "Revert":
					m_controller.RevertFiles(currentFolder, selectedFiles); 
					break;
				case "Ignore":
					m_controller.IgnoreFiles(currentFolder, selectedFiles); 
					break;
				case "Stop Ignoring":
					m_controller.StopIgnoringFiles(currentFolder, selectedFiles); 
					break;
				case "Rename...":
					if (selectedFiles.Count == 1)
					{
						FileStatus fileStatus = GetFileStatus(selectedFiles[0]);
						m_controller.RenameFile(this, currentFolder, selectedFiles[0], fileStatus);
					}
					else
					{
						throw new Exception("Can only rename one file at a time but there are currently " + selectedFiles.Count + " files selected.");
					}
					break;
			};

		}

		protected override void OnItemSelectionChanged(System.Windows.Forms.ListViewItemSelectionChangedEventArgs e)
		{
			List<FileItem> fileList = new List<FileItem>();

			foreach (ListViewItem lvi in SelectedItems)
			{
				fileList.Add((FileItem)(lvi.Tag));
			}

			Notify(selectedFilesAspect);
		}

		public List<string> GetSelectedFiles()
		{

            List<string> fileList = new List<string>();

            foreach (ListViewItem lvi in SelectedItems)
            {
				//fileList.Add(lvi.Text);
				fileList.Add(((FileItem)(lvi.Tag)).filename);
            }

            return fileList;
		}

		protected override void OnColumnClick(ColumnClickEventArgs e)
		{
			if (m_columnSortIndex == e.Column)
			{
				m_sortAscending = !m_sortAscending;
			}			
			m_columnSortIndex = e.Column;
			
			((ListViewItemComparer)ListViewItemSorter).SetColumn(e.Column, m_sortAscending);
			Sort();
		}

	}


    // Implements the manual sorting of items by columns.
	class ListViewItemComparer : System.Collections.IComparer
    {
        private int col;
		bool m_ascending;
        public ListViewItemComparer()
        {
            col = 0;
			m_ascending = true;
        }
        public ListViewItemComparer(int column)
        {
            col = column;
			m_ascending = true;
		}
		public void SetColumn(int column, bool ascending)
		{
			col = column;
			m_ascending = ascending;
		}

		int GetScore(FileStatus fs)
		{
			return 
				(fs == FileStatus.Folder) ? 10 :
				(fs == FileStatus.Modified) ? 7 :
				(fs == FileStatus.Added) ? 6 :
				(fs == FileStatus.Deleted) ? 5 :
				(fs == FileStatus.Removed) ? 4 :
				(fs == FileStatus.Clean) ? 3 :
				(fs == FileStatus.Ignored) ? 2 :
				(fs == FileStatus.NotTracked) ? 1 : 0;
		}

		public int Compare(object x, object y)
        {
			ListViewItem a = (ListViewItem)x;
			ListViewItem b = (ListViewItem)y;
			FileStatus aFileStatus = ((FileItem)(a.Tag)).status;
			FileStatus bFileStatus = ((FileItem)(b.Tag)).status;

			// Take care of the case when one of the items is the "Up" folder, which is always at the top.
			if ((aFileStatus == FileStatus.Folder) && (a.SubItems[FileView.CiFilename].Text == ".."))
			{
				return -1;
			}
			if ((bFileStatus == FileStatus.Folder) && (b.SubItems[FileView.CiFilename].Text == ".."))
			{
				return 1;
			}

			// Take care of the case when one or both of the items is a folder.
			if ((aFileStatus == FileStatus.Folder) && (bFileStatus != FileStatus.Folder))
			{
				return -1;
			}
			if ((aFileStatus != FileStatus.Folder) && (bFileStatus == FileStatus.Folder))
			{
				return 1;
			}
			if ((aFileStatus == FileStatus.Folder) && (bFileStatus == FileStatus.Folder))
			{
				// Always sort according to column 0 no matter what column was clicked.
				if (m_ascending)
				{
					return String.Compare(a.SubItems[0].Text, b.SubItems[FileView.CiFilename].Text);
				}
				else
				{
					return String.Compare(b.SubItems[0].Text, a.SubItems[FileView.CiFilename].Text);
				}
			}

			int aScore;
			int bScore;
			long aFileSize;
			long bFileSize;
			long aFileTime;
			long bFileTime;

			switch (col)
			{
				case FileView.CiStatus:	// file status
					aScore = GetScore(aFileStatus);
					bScore = GetScore(bFileStatus);
					if (aScore != bScore)
					{
						return m_ascending ? bScore - aScore : aScore - bScore;
					}
					break;
				case FileView.CiFileSize:	// file size
					aFileSize = (long)(a.SubItems[FileView.CiFileSize].Tag);
					bFileSize = (long)(b.SubItems[FileView.CiFileSize].Tag);

					if (aFileSize != bFileSize)
					{
						return (int)(m_ascending ? bFileSize - aFileSize : aFileSize - bFileSize);
					}
					break;
				case FileView.CiFileDateTime:	// file date time
					aFileTime = (long)(a.SubItems[FileView.CiFileDateTime].Tag);
					bFileTime = (long)(b.SubItems[FileView.CiFileDateTime].Tag);

					if (aFileTime != bFileTime)
					{
						long diff = m_ascending ? (bFileTime - aFileTime) : (aFileTime - bFileTime);
						return (diff < 0 ) ? -1 : 1;
					}
					break;
				case FileView.CiPath:	// file path
					if (m_ascending)
					{
						return String.Compare(a.SubItems[FileView.CiPath].Text, b.SubItems[FileView.CiPath].Text);
					}
					else
					{
						return String.Compare(b.SubItems[FileView.CiPath].Text, a.SubItems[FileView.CiPath].Text);
					}

			};

			if (m_ascending)
			{
				return String.Compare(a.SubItems[FileView.CiFilename].Text, b.SubItems[FileView.CiFilename].Text);
			}
			else
			{
				return String.Compare(b.SubItems[FileView.CiFilename].Text, a.SubItems[FileView.CiFilename].Text);
			}
		}
    }

}
