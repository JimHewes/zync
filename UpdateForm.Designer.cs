﻿namespace Zync
{
	partial class UpdateForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateForm));
			this.updateOkButton = new System.Windows.Forms.Button();
			this.updateCleanCopyCheckBox = new System.Windows.Forms.CheckBox();
			this.updateByRevTextBox = new System.Windows.Forms.TextBox();
			this.updateDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.updateTimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.updateCancelButton = new System.Windows.Forms.Button();
			this.updateByRevCheckBox = new System.Windows.Forms.CheckBox();
			this.updateShiftKeyCheckBox = new System.Windows.Forms.CheckBox();
			this.updateByDateCheckBox = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// updateOkButton
			// 
			this.updateOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.updateOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.updateOkButton.Location = new System.Drawing.Point(206, 130);
			this.updateOkButton.Name = "updateOkButton";
			this.updateOkButton.Size = new System.Drawing.Size(75, 23);
			this.updateOkButton.TabIndex = 5;
			this.updateOkButton.Text = "Update";
			this.updateOkButton.UseVisualStyleBackColor = true;
			this.updateOkButton.Click += new System.EventHandler(this.OnClickedOkButton);
			// 
			// updateCleanCopyCheckBox
			// 
			this.updateCleanCopyCheckBox.AutoSize = true;
			this.updateCleanCopyCheckBox.Location = new System.Drawing.Point(12, 82);
			this.updateCleanCopyCheckBox.Name = "updateCleanCopyCheckBox";
			this.updateCleanCopyCheckBox.Size = new System.Drawing.Size(116, 17);
			this.updateCleanCopyCheckBox.TabIndex = 1;
			this.updateCleanCopyCheckBox.Text = "Get the clean copy";
			this.updateCleanCopyCheckBox.UseVisualStyleBackColor = true;
			// 
			// updateByRevTextBox
			// 
			this.updateByRevTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.updateByRevTextBox.Cursor = System.Windows.Forms.Cursors.Default;
			this.updateByRevTextBox.Location = new System.Drawing.Point(125, 24);
			this.updateByRevTextBox.Name = "updateByRevTextBox";
			this.updateByRevTextBox.Size = new System.Drawing.Size(245, 20);
			this.updateByRevTextBox.TabIndex = 4;
			// 
			// updateDateDateTimePicker
			// 
			this.updateDateDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.updateDateDateTimePicker.Cursor = System.Windows.Forms.Cursors.Default;
			this.updateDateDateTimePicker.CustomFormat = "dd-MMM-yyyy";
			this.updateDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.updateDateDateTimePicker.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.updateDateDateTimePicker.Location = new System.Drawing.Point(125, 51);
			this.updateDateDateTimePicker.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
			this.updateDateDateTimePicker.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
			this.updateDateDateTimePicker.Name = "updateDateDateTimePicker";
			this.updateDateDateTimePicker.Size = new System.Drawing.Size(156, 20);
			this.updateDateDateTimePicker.TabIndex = 6;
			// 
			// updateTimeDateTimePicker
			// 
			this.updateTimeDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.updateTimeDateTimePicker.CustomFormat = "hh:mm tt";
			this.updateTimeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.updateTimeDateTimePicker.Location = new System.Drawing.Point(291, 51);
			this.updateTimeDateTimePicker.Name = "updateTimeDateTimePicker";
			this.updateTimeDateTimePicker.ShowUpDown = true;
			this.updateTimeDateTimePicker.Size = new System.Drawing.Size(79, 20);
			this.updateTimeDateTimePicker.TabIndex = 8;
			this.updateTimeDateTimePicker.Value = new System.DateTime(2009, 6, 7, 0, 0, 0, 0);
			// 
			// updateCancelButton
			// 
			this.updateCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.updateCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.updateCancelButton.Location = new System.Drawing.Point(295, 130);
			this.updateCancelButton.Name = "updateCancelButton";
			this.updateCancelButton.Size = new System.Drawing.Size(75, 23);
			this.updateCancelButton.TabIndex = 9;
			this.updateCancelButton.Text = "Cancel";
			this.updateCancelButton.UseVisualStyleBackColor = true;
			// 
			// updateByRevCheckBox
			// 
			this.updateByRevCheckBox.AutoSize = true;
			this.updateByRevCheckBox.Location = new System.Drawing.Point(12, 26);
			this.updateByRevCheckBox.Name = "updateByRevCheckBox";
			this.updateByRevCheckBox.Size = new System.Drawing.Size(80, 17);
			this.updateByRevCheckBox.TabIndex = 7;
			this.updateByRevCheckBox.Text = "By revision:";
			this.updateByRevCheckBox.UseVisualStyleBackColor = true;
			this.updateByRevCheckBox.CheckedChanged += new System.EventHandler(this.OnByRevisionCheckedChanged);
			// 
			// updateShiftKeyCheckBox
			// 
			this.updateShiftKeyCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.updateShiftKeyCheckBox.AutoSize = true;
			this.updateShiftKeyCheckBox.Location = new System.Drawing.Point(12, 136);
			this.updateShiftKeyCheckBox.Name = "updateShiftKeyCheckBox";
			this.updateShiftKeyCheckBox.Size = new System.Drawing.Size(173, 17);
			this.updateShiftKeyCheckBox.TabIndex = 3;
			this.updateShiftKeyCheckBox.Text = "Display only if Shift key is down";
			this.updateShiftKeyCheckBox.UseVisualStyleBackColor = true;
			// 
			// updateByDateCheckBox
			// 
			this.updateByDateCheckBox.AutoSize = true;
			this.updateByDateCheckBox.Location = new System.Drawing.Point(12, 54);
			this.updateByDateCheckBox.Name = "updateByDateCheckBox";
			this.updateByDateCheckBox.Size = new System.Drawing.Size(65, 17);
			this.updateByDateCheckBox.TabIndex = 2;
			this.updateByDateCheckBox.Text = "By date:";
			this.updateByDateCheckBox.UseVisualStyleBackColor = true;
			this.updateByDateCheckBox.CheckedChanged += new System.EventHandler(this.OnByDateCheckedChanged);
			// 
			// toolTip1
			// 
			this.toolTip1.AutoPopDelay = 30000;
			this.toolTip1.InitialDelay = 500;
			this.toolTip1.ReshowDelay = 100;
			// 
			// UpdateForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(382, 168);
			this.Controls.Add(this.updateCancelButton);
			this.Controls.Add(this.updateTimeDateTimePicker);
			this.Controls.Add(this.updateByRevCheckBox);
			this.Controls.Add(this.updateDateDateTimePicker);
			this.Controls.Add(this.updateOkButton);
			this.Controls.Add(this.updateByRevTextBox);
			this.Controls.Add(this.updateShiftKeyCheckBox);
			this.Controls.Add(this.updateByDateCheckBox);
			this.Controls.Add(this.updateCleanCopyCheckBox);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(390, 202);
			this.Name = "UpdateForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Update";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button updateOkButton;
		private System.Windows.Forms.CheckBox updateCleanCopyCheckBox;
		private System.Windows.Forms.CheckBox updateByDateCheckBox;
		private System.Windows.Forms.CheckBox updateShiftKeyCheckBox;
		private System.Windows.Forms.TextBox updateByRevTextBox;
		private System.Windows.Forms.DateTimePicker updateDateDateTimePicker;
		private System.Windows.Forms.CheckBox updateByRevCheckBox;
		private System.Windows.Forms.DateTimePicker updateTimeDateTimePicker;
		private System.Windows.Forms.Button updateCancelButton;
		private System.Windows.Forms.ToolTip toolTip1;
	}
}