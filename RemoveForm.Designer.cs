﻿namespace Zync
{
	partial class RemoveForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.Label label2;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveForm));
			this.removeEffectsText1 = new System.Windows.Forms.Label();
			this.removeEffectsText3 = new System.Windows.Forms.Label();
			this.removeEffectsText2 = new System.Windows.Forms.Label();
			this.removeFilesListBox = new System.Windows.Forms.ListBox();
			this.removeFilesOkButton = new System.Windows.Forms.Button();
			this.removeFilesCancelButton = new System.Windows.Forms.Button();
			this.removeAfterCheckBox = new System.Windows.Forms.CheckBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.removeForceCheckBox = new System.Windows.Forms.CheckBox();
			label2 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(16, 13);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(251, 13);
			label2.TabIndex = 5;
			label2.Text = "Are you sure you want to remove the following files?";
			// 
			// removeEffectsText1
			// 
			this.removeEffectsText1.AutoSize = true;
			this.removeEffectsText1.Location = new System.Drawing.Point(6, 18);
			this.removeEffectsText1.Name = "removeEffectsText1";
			this.removeEffectsText1.Size = new System.Drawing.Size(265, 13);
			this.removeEffectsText1.TabIndex = 4;
			this.removeEffectsText1.Text = "- Added files will be immediately removed from tracking.";
			// 
			// removeEffectsText3
			// 
			this.removeEffectsText3.AutoSize = true;
			this.removeEffectsText3.Location = new System.Drawing.Point(6, 50);
			this.removeEffectsText3.Name = "removeEffectsText3";
			this.removeEffectsText3.Size = new System.Drawing.Size(166, 13);
			this.removeEffectsText3.TabIndex = 6;
			this.removeEffectsText3.Text = "- No files will be deleted from disk.";
			// 
			// removeEffectsText2
			// 
			this.removeEffectsText2.AutoSize = true;
			this.removeEffectsText2.Location = new System.Drawing.Point(6, 34);
			this.removeEffectsText2.Name = "removeEffectsText2";
			this.removeEffectsText2.Size = new System.Drawing.Size(306, 13);
			this.removeEffectsText2.TabIndex = 4;
			this.removeEffectsText2.Text = "- All other files will be scheduled for removal on the next commit.";
			// 
			// removeFilesListBox
			// 
			this.removeFilesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.removeFilesListBox.FormattingEnabled = true;
			this.removeFilesListBox.Location = new System.Drawing.Point(19, 46);
			this.removeFilesListBox.Name = "removeFilesListBox";
			this.removeFilesListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.removeFilesListBox.Size = new System.Drawing.Size(366, 56);
			this.removeFilesListBox.TabIndex = 0;
			// 
			// removeFilesOkButton
			// 
			this.removeFilesOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.removeFilesOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.removeFilesOkButton.Location = new System.Drawing.Point(251, 268);
			this.removeFilesOkButton.Name = "removeFilesOkButton";
			this.removeFilesOkButton.Size = new System.Drawing.Size(75, 23);
			this.removeFilesOkButton.TabIndex = 2;
			this.removeFilesOkButton.Text = "Remove";
			this.removeFilesOkButton.UseVisualStyleBackColor = true;
			this.removeFilesOkButton.Click += new System.EventHandler(this.OnClickedRemoveButton);
			// 
			// removeFilesCancelButton
			// 
			this.removeFilesCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.removeFilesCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.removeFilesCancelButton.Location = new System.Drawing.Point(333, 268);
			this.removeFilesCancelButton.Name = "removeFilesCancelButton";
			this.removeFilesCancelButton.Size = new System.Drawing.Size(75, 23);
			this.removeFilesCancelButton.TabIndex = 3;
			this.removeFilesCancelButton.Text = "Cancel";
			this.removeFilesCancelButton.UseVisualStyleBackColor = true;
			// 
			// removeAfterCheckBox
			// 
			this.removeAfterCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.removeAfterCheckBox.AutoSize = true;
			this.removeAfterCheckBox.Location = new System.Drawing.Point(19, 117);
			this.removeAfterCheckBox.Name = "removeAfterCheckBox";
			this.removeAfterCheckBox.Size = new System.Drawing.Size(161, 17);
			this.removeAfterCheckBox.TabIndex = 7;
			this.removeAfterCheckBox.Text = "Remove only missing [!] files.";
			this.removeAfterCheckBox.UseVisualStyleBackColor = true;
			this.removeAfterCheckBox.CheckedChanged += new System.EventHandler(this.OnAfterOrForceCheckboxChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.groupBox1.Controls.Add(this.removeEffectsText1);
			this.groupBox1.Controls.Add(this.removeEffectsText2);
			this.groupBox1.Controls.Add(this.removeEffectsText3);
			this.groupBox1.Location = new System.Drawing.Point(18, 172);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(363, 80);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Effects On Listed Files";
			// 
			// removeForceCheckBox
			// 
			this.removeForceCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.removeForceCheckBox.AutoSize = true;
			this.removeForceCheckBox.Location = new System.Drawing.Point(19, 140);
			this.removeForceCheckBox.Name = "removeForceCheckBox";
			this.removeForceCheckBox.Size = new System.Drawing.Size(91, 17);
			this.removeForceCheckBox.TabIndex = 7;
			this.removeForceCheckBox.Text = "Force remove";
			this.removeForceCheckBox.UseVisualStyleBackColor = true;
			this.removeForceCheckBox.CheckedChanged += new System.EventHandler(this.OnAfterOrForceCheckboxChanged);
			// 
			// RemoveForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(419, 303);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.removeForceCheckBox);
			this.Controls.Add(this.removeAfterCheckBox);
			this.Controls.Add(label2);
			this.Controls.Add(this.removeFilesCancelButton);
			this.Controls.Add(this.removeFilesOkButton);
			this.Controls.Add(this.removeFilesListBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(427, 337);
			this.Name = "RemoveForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Remove Files";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox removeFilesListBox;
		private System.Windows.Forms.Button removeFilesOkButton;
		private System.Windows.Forms.Button removeFilesCancelButton;
		private System.Windows.Forms.Label removeEffectsText1;
		private System.Windows.Forms.Label removeEffectsText3;
		private System.Windows.Forms.Label removeEffectsText2;
		private System.Windows.Forms.CheckBox removeAfterCheckBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox removeForceCheckBox;
	}
}