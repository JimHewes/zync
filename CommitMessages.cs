﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;

namespace Zync
{
	/** @brief	The CommitMessages class represents the list of recent commit messages used.
	 * 
	 * Recent commit messages are saved to an XML file. The CommitMessages class is an 
	 * in-memory representation of that file allowing easy access to recent commit messages.
	 * The file is stored in the user's local application data folder.
	 */

#if true
	public class CommitMessages : RecentText
	{
		public CommitMessages()
			: base("Zync", "commitmsgs.xml", "CommitMessages","Message", 15)
		{}
	}

#else
	public class CommitMessages
	{
		private string m_localAppData;
		private const string m_appName = "Zync";
		private const string m_commitFilename = "commitmsgs.xml";
		private string m_filePath;

		private const string m_rootNodeName = "CommitMessages";
		private const string m_maxMsgsNodeName = "MaxMessages";
		private const string m_messageNodeName = "Message";
		private const string m_indexAttrName = "Index";

		private const int m_defaultMaxNumberOfMessages = 15;
		private const int m_maxCapacity = 1000;	///< never let the max number of messages exceed this value. 
		private int m_maxNumberOfMessages = m_defaultMaxNumberOfMessages;

		List<string> m_messageList;

		public CommitMessages()
		{

			// Get the file path of the XML file
			m_localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

			m_filePath = Path.Combine(Path.Combine(m_localAppData, m_appName), m_commitFilename);

			//MakeSureFileExists();

			Load();


		}


		/** @brief Removes all messages.
		 * 
		 * Calling this function will cause all message to be deleted. But they will still exist 
		 * in the file until Save() is called.
		 * 
		 **/
		public void Clear()
		{
			m_messageList.Clear();
		}


		/** @brief Adds the given message as the most recent message in the list.
		 * 
		 * Note that calling this function will cause the oldest message to be deleted if the 
		 * list is already full. But the old messages still exist in the file until Save() is called.
		 * 
		 *	@param message	The message to add.
		 **/
		public void Add(string message)
		{
			// If there are any messages in the list that exactly match the one we're about 
			// to insert, then remove them first.
			m_messageList.RemoveAll(s => s == message);

			// Insert the most recent message at the beginning.
			m_messageList.Insert(0, message);

			// Trim old entries, which are at the end.
			int messagesToTrim = m_messageList.Count - m_maxNumberOfMessages;
			if (messagesToTrim > 0)
			{
				m_messageList.RemoveRange(m_messageList.Count - messagesToTrim, messagesToTrim);
			}
		}

		public List<string> GetList()
		{
			return m_messageList;
		}



		private void Load()
		{

			XmlDocument doc = new XmlDocument();	///< The XML document loaded from the disk file.
			
			// Although not directly documented, the Load function will throw an 
			// UnauthorizedAccessException if the file cannot be opened for reading.
			// It will throw a FileNotFoundException if the file doesn't exist.
			// But we catch exceptions because we don't require the file to exist.
			// If it doesn't, then we just go with defaults.
			try
			{
				doc.Load(m_filePath);
			}
			catch
			{
				m_messageList = new List<string>(m_defaultMaxNumberOfMessages);
				return;
			}

			// We should only get back one node here.
			var maxNumberOfMessagesNodeList = doc.DocumentElement.SelectNodes(m_maxMsgsNodeName);
			Debug.Assert(maxNumberOfMessagesNodeList.Count == 1 || maxNumberOfMessagesNodeList.Count == 0,
				"There are more than one [" + m_maxMsgsNodeName + "] elements in the file.");

			int maxNumberOfMessages = m_defaultMaxNumberOfMessages;
			if(maxNumberOfMessagesNodeList.Count == 1)
			{
				maxNumberOfMessages = Int32.Parse(maxNumberOfMessagesNodeList[0].InnerText);
			}
			else if (maxNumberOfMessagesNodeList.Count == 0)
			{
				// If there is no MaxMessages node, create one with the default value
				XmlElement mmNode = doc.CreateElement(m_maxMsgsNodeName);
				mmNode.InnerText = m_defaultMaxNumberOfMessages.ToString();
				doc.DocumentElement.AppendChild(mmNode);

			}
			else if (maxNumberOfMessagesNodeList.Count > 1)
			{
				// If there are more than one, find the one with the highest value.
				int maxValueFound = 0;
				foreach (XmlNode node in maxNumberOfMessagesNodeList)
				{
					int valueFound = Int32.Parse(node.InnerText);
					maxValueFound = Math.Min(m_maxCapacity, Math.Max(valueFound, maxValueFound));
				}
				// We don't need to clean up by removing extraneous nodes.
				// We're going to rewrite the entire file anyway if anything gets changed.
				maxNumberOfMessages = maxValueFound;
			}

			// Create a list with the capacity the size of the max messages we want to allow.
			m_messageList = new List<string>(maxNumberOfMessages);

			var messageNodeList = doc.DocumentElement.SelectNodes(m_messageNodeName);
			foreach (XmlNode node in messageNodeList)
			{
				// Get this node's index
				int index = Int32.Parse(node.Attributes.GetNamedItem(m_indexAttrName).Value);

				// if the index is outside the max number of messages, ignore it.
				if (index < maxNumberOfMessages)
				{
					// Lists cannot automatically grow when you use the index operator. They can only 
					// grow using the Add function (or AddRange). So before we try to assign the message to 
					// an slots in the list, check if the list has enough slots.
					// (There are more clever ways to do this with later versions of .NET Framework, 
					// but we're trying to stick with 2.0.)
					if (index > m_messageList.Count - 1)
					{
						int itemsToAdd = index - m_messageList.Count;
						while (itemsToAdd-- > 0)
						{	// Most of the time this loop won't even execute, i.e. if the 
							// file has all messages in order with no gaps.
							m_messageList.Add(default(string));
						}
						m_messageList.Add(node.InnerText);
					}
					else
					{
						// The reason we use the index operator and not Insert is because we don't want to move 
						// all following items up. We just want to overwrite the item in this slot.
						m_messageList[index] = node.InnerText;
					}
				}

				// Now let's remove any empty entries because we assume the user doesn't care about those.
				m_messageList.RemoveAll(s => s == string.Empty);
			}
		}

		/** @brief Saves the commit message to the file on disk.
		 * 
		 * The user of this class must call this Save() function to have the changes saved.
		 * The data is NOT saved automatically when the class is destroyed.
		 * 
		 * 
		 **/
		public void Save()
		{

			// Generate an XML document.
			XmlDocument newDoc = new XmlDocument();

			XmlDeclaration xmlDeclaration = newDoc.CreateXmlDeclaration("1.0", "utf-8", null);

			// Create the root element
			XmlElement rootNode = newDoc.CreateElement(m_rootNodeName);
			newDoc.InsertBefore(xmlDeclaration, newDoc.DocumentElement);
			newDoc.AppendChild(rootNode);

			XmlElement mmNode = newDoc.CreateElement(m_maxMsgsNodeName);
			mmNode.InnerText = m_defaultMaxNumberOfMessages.ToString();
			newDoc.DocumentElement.AppendChild(mmNode);

			for (int i = 0; i < m_messageList.Count; ++i)
			{
				XmlElement newNode = newDoc.CreateElement(m_messageNodeName);
				newNode.SetAttribute(m_indexAttrName, i.ToString());
				newNode.InnerText = m_messageList[i];
				newDoc.DocumentElement.AppendChild(newNode);
			}

			// Make sure the folder exists.
			string appFolder = Path.Combine(m_localAppData, m_appName);
			if (!Directory.Exists(appFolder))
			{
				Directory.CreateDirectory(appFolder);
			}	

			newDoc.Save(m_filePath);
		}

	}
#endif
}
