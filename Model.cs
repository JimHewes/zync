﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Zync
{
	/* @brief The model class encapsulates the state of data in the application.

		The Model class is primarily concerned with state of things such as:\n
		The "root" folder (used by the folder view but may be set from elsewhere.) \n
		The current directory \n
		The list of files in that directory and their status \n
		A list of currently selected files, a subset.
		For each type of file state (clean, modified), what is the visibility of that state \n
		Which files are selected \n
		\n
		We don't care about sortng the file list; that's handled by the view.

		Why are selected files kept track of here an not just in the view? Because multiple views 
		may trigger an action that involves the selected files.
	*/
	public class Model : Subject
	{

		public class StatusChangeAspect : Aspect { }

		/** @brief Sent if a folder changes its status as a source code controlled folder.
					This is not necessarily the selected folder.
		*/		
		public class FolderStatusChangeAspect : Aspect 
		{
			public string FolderPath { get; private set;}
			public FolderStatusChangeAspect(string folderPath)
			{
				FolderPath = folderPath;
			}
		}

		private static StatusChangeAspect statusChangeAspect = new StatusChangeAspect();


		//private static FolderStatusChangeAspect folderStatusChangeAspect = null;

		public Model()
		{
			m_hasViewer = SCC.Instance.HasViewer();
		}

		/** This is a list of all the files under source code control for the current working 
			directory or for whatever parent is the working directory. The reason it's a member 
			variable is because we don't want to re-read the list just to display a different subset 
			of the files. So this list contains all files of all status, and you can filter it for display.
			*/
		private List<FileItem> m_fileList = new List<FileItem>();

		// The m_currentFolder represents the folder of the file list.
		// This is only temporary because for now we only store one list.
		// This is NOT the global currently selected folder and is not necessarily 
		// the one in the folderview, but usually is..
		private string m_currentFolder = "";


		/** brief Cached value of whether or not the source code control has a view option.
		 * 
		 *	This is pretty specific to Mercurial.
		 **/
		private bool m_hasViewer;

		public bool HasViewer
		{
			get { return m_hasViewer; }
		}

		public List<FileItem> GetFileList(string folder, FileStatusFlag flags)
		{
			/////////////////////////
			// !!!!!! This call is temporary. Once the Model class can update its data based on changes 
			// to the file system we won't have to force this refresh.
			//////////////////////////
			EnsureFileListIsCurrent(folder);


			List<FileItem> fileList = new List<FileItem>();

			foreach (FileItem fi in m_fileList)
			{
				FileStatusFlag status = (FileStatusFlag)fi.status;
				if ((status & flags) == status)
				{
					fileList.Add(fi);	// FileItem is a struct, so we're adding a copy of the item, not another reference.
				}
			}

			return fileList;
		}

		public bool IsFolderScc(string folderPath)
		{
			return SCC.Instance.IsSCCDirectory(folderPath);
		}

		private void RefreshSccFileList(string folder)
		{
			// Do this even if the new directory is the same as the old one. It might be 
			// the same directory, but the status of the files might have changed.
			if (SCC.Instance.IsSCCDirectory(folder))
			{
				FileStatusFlag flags = FileStatusFlag.Modified | FileStatusFlag.Clean | FileStatusFlag.NotTracked |
										FileStatusFlag.Added | FileStatusFlag.Deleted | FileStatusFlag.Removed | FileStatusFlag.Ignored;
				m_fileList = SCC.Instance.GetFileList(folder, flags);
			}
			else
			{
				// This is not a repository directory, so get all files that are in the directory.
				string[] fileList = System.IO.Directory.GetFiles(folder);
				m_fileList.Clear();
				foreach (string s in fileList)
				{
					// The filenames in the file list are full paths, so we need to get just the filename
					m_fileList.Add(new FileItem { filename = System.IO.Path.GetFileName(s), status = FileStatus.NotTracked });
				}
			}
			m_currentFolder = folder;
		}
		

		/** @brief Given a list of filenames, returns a corresponding list of FileItems
		 * 
		 *	FileItems hold both the filename and the status of files. Call this function if you have a list 
		 *	of files and you need to get the status of them.
		 *	If the folder is not a source code control folder, then all the file will have a status of NotTracked.
		 *	*/
		public List<FileItem> GetFileStatus(string folderPath, List<string> files)
		{
			List<FileItem> fileItemList = new List<FileItem>();

			if (folderPath != m_currentFolder)
			{
				RefreshSccFileList(folderPath);
			}
			FileItem foundItem;
			foreach (string s in files)
			{
				foundItem = m_fileList.Find(item => item.filename == s);
				fileItemList.Add(foundItem);
			}

			return fileItemList;

		}



		public void AddFiles(AddParams addParams)
		{
			if (addParams.files.Count == 0)
			{
				return;
			}

			EnsureFileListIsCurrent(addParams.repoPath);


			// Make sure we only try to Add untracked or removed files.
			// The view will probably ensure it, but this is a double check.
			foreach (string s in addParams.files)
			{
				FileItem fi = m_fileList.Find(item => item.filename == s);
				if (fi.filename == null)
				{
					throw new ArgumentException("The file " + s + " is not tracked by source code control.");
				}

				if ((fi.status != FileStatus.NotTracked) && (fi.status != FileStatus.Removed))	// make sure we only try to add untracked files.
				{
					throw new ArgumentException("The file " + s + " does not have the correct status.");
				}
			}

			SCC.Instance.AddFiles(addParams.repoPath, addParams.files);

			RefreshSccFileList(addParams.repoPath);
			Notify(statusChangeAspect);
		}
		
		public void RenameFile(RenameParams renameParams)
		{
			bool thereWereChanges = false;
			thereWereChanges = SCC.Instance.RenameFile(renameParams.repoPath , renameParams);
			if (thereWereChanges)
			{
				RefreshSccFileList(renameParams.repoPath);
				Notify(statusChangeAspect);
			}
		}


		public void RemoveFiles(RemoveParams removeParams)
		{
			if (removeParams.files.Count == 0)
			{
				return;
			}

			EnsureFileListIsCurrent(removeParams.repoPath);	// ensure m_fileList has the list of files for repository removeParams.repoPath

			// Make sure all the files to be reverted have the correct status 
			foreach (string s in removeParams.files)
			{
				FileItem fi = m_fileList.Find(item => item.filename == s);
				if (fi.filename == null)
				{
					throw new ArgumentException("The file " + s + " is not tracked by source code control.");
				}
				if ((fi.status != FileStatus.Clean) && (fi.status != FileStatus.Modified) && 
					(fi.status != FileStatus.Added) && (fi.status != FileStatus.Deleted))
				{
					throw new ArgumentException("The file " + s + " does not have the correct status to be reverted.");
				}
			}

			SCC.Instance.RemoveFiles(removeParams.repoPath, removeParams);

			RefreshSccFileList(removeParams.repoPath);
			Notify(statusChangeAspect);

		}

		private void EnsureFileListIsCurrent(string folder)
		{
			if (m_currentFolder != folder)
			{
				if (IsFolderScc(folder))
				{
					RefreshSccFileList(folder);
				}
				else
				{
					throw new ArgumentException("The folder is not a SCC folder: " + folder);
				}
			}
		}

		public void RevertFiles(RevertParams revertParams)
		{
			if (revertParams.files.Count == 0)
			{
				return;
			}

			EnsureFileListIsCurrent(revertParams.repoPath);	// ensure m_fileList has the list of files for repository revertParams.repoPath

			// Make sure all the files to be reverted have the correct status 
			foreach (string s in revertParams.files)
			{
				FileItem fi = m_fileList.Find(item => item.filename == s);
				if( fi.filename == null)
				{
					throw new ArgumentException("The file " + s + " is not tracked by source code control.");
				}
				if ((fi.status != FileStatus.Added) && (fi.status != FileStatus.Removed) &&
					(fi.status != FileStatus.Modified) && (fi.status != FileStatus.Deleted))
				{
					throw new ArgumentException("The file " + s + " does not have the correct status to be reverted.");
				}
			}


			SCC.Instance.RevertFiles(revertParams.repoPath, revertParams.files);

			RefreshSccFileList(revertParams.repoPath);
			Notify(statusChangeAspect);
		}

		public void IgnoreFiles(IgnoreParams ignoreParams)
		{
			if(ignoreParams.files.Count == 0)
			{
				return;
			}

			EnsureFileListIsCurrent(ignoreParams.repoPath);	// ensure m_fileList has the list of files for repository ignoreParams.repoPath

			// Make sure we only try to ignore untracked files.
			// The view will probably ensure it, but this is a double check.
			foreach (string s in ignoreParams.files)
			{
				FileItem fi = m_fileList.Find(item => item.filename == s);
				if (fi.filename == null)
				{
					throw new ArgumentException("The file " + s + " was not found.");
				}
				if (fi.status != FileStatus.NotTracked)
				{
					throw new ArgumentException("The file " + s + " does not have the correct status.");
				}
			}

			SCC.Instance.IgnoreFiles(ignoreParams.repoPath, ignoreParams.files);
			RefreshSccFileList(ignoreParams.repoPath);
			Notify(statusChangeAspect);
		}


		public void StopIgnoringSelectedFiles(IgnoreParams ignoreParams)
		{
			if (ignoreParams.files.Count == 0)
			{
				return;
			}

			EnsureFileListIsCurrent(ignoreParams.repoPath);	// ensure m_fileList has the list of files for repository ignoreParams.repoPath

			// Make sure we only try to stop ignoring files that are ignored.
			// The view will probably ensure it, but this is a double check.
			foreach (string s in ignoreParams.files)
			{
				FileItem fi = m_fileList.Find(item => item.filename == s);
				if (fi.filename == null)
				{
					throw new ArgumentException("The file " + s + " was not found.");
				}
				if (fi.status != FileStatus.Ignored)
				{
					throw new ArgumentException("The file " + s + " does not have the correct status.");
				}
			}

			SCC.Instance.StopIgnoringFiles(ignoreParams.repoPath, ignoreParams.files);
			RefreshSccFileList(ignoreParams.repoPath);
			Notify(statusChangeAspect);
		}

		public void RollbackRepository(string folder)
		{
			if (IsFolderScc(folder))
			{
				SCC.Instance.Rollback(folder);
				RefreshSccFileList(folder);
				Notify(statusChangeAspect);
			}
		}
		
		public void Tag(TagParams tagParams)
		{
			SCC.Instance.Tag(tagParams.repoPath, tagParams);
		}

		public void UnmarkResolvedFile(string folder, string filename)
		{
			if (IsFolderScc(folder))
			{
				SCC.Instance.UnmarkResolvedFile(folder, filename);
				RefreshSccFileList(folder);
				Notify(statusChangeAspect);
			}

		}



		/** @brief Create a repository in a specified folder

			The folder is not necessarily the current folder and if not, is not 
			made the selected folder after creating the repository.
			If the folder IS the currently selected folder, then it is refreshed.
		*/
		public void CreateRepositoryInFolder(string folder)
		{
			if (!SCC.Instance.IsSCCDirectory(folder))
			{
				SCC.Instance.CreateRepository(folder);	// will throw if repository creation failed

				// We don't know if this is the current folder selected by the view.
				// But send a notification that the status changed and let the view decide what to display.
				//
				// Later, possibly include a parameter to specify if this new repository will be an
				// "active" one. That is, one whose data is cached and is monitored for changes.
				
				// RefreshSccFileList();
				Notify(new FolderStatusChangeAspect(folder));
			}

		}


		public void UpdateRepository(UpdateParams updateParams)
		{
			bool thereWereChanges = SCC.Instance.Update(updateParams.repoPath, updateParams);
			if (thereWereChanges)
			{
				RefreshSccFileList(updateParams.repoPath);
				Notify(statusChangeAspect);
			}
		}

		public void PushTo(PushParams pushParams)
		{
			bool thereWereChanges = false;
			SCC.Instance.Push(pushParams.repoPath, pushParams);
			if (thereWereChanges)
			{
				RefreshSccFileList(pushParams.repoPath);
				Notify(statusChangeAspect);
			}
		}

		public void MergeCurrentRepository(MergeParams mergeParams)
		{
			if (SCC.Instance.Merge(mergeParams.repoPath, mergeParams))
			{
				RefreshSccFileList(mergeParams.repoPath);
				Notify(statusChangeAspect);
			}
		}


		/** @brief Pulls from the current folder repository or another specified repository.

			@param repositoryToPullFrom		The repository to pull from. If you want to pull from 
											the "default push" repository of the current folder,
											this parameter should be the empty string.
		*/
		public void PullFrom(PullParams pullParams)
		{
			bool thereWereChanges = SCC.Instance.Pull(pullParams.repoPath, pullParams);
			if (thereWereChanges)
			{
				RefreshSccFileList(pullParams.repoPath);
				Notify(statusChangeAspect);
			}
		}


		/** @brief Commit a set of files to the current repository

			Of the list of files passed in, only those files which exist in the current repository and 
			which are commitable are commited.

			@param filesToCommit	A list of filnames of files to commit. This files must exist 
									in the repository of the CurrentFolder or they are ignored.
			@param commitMessage	A commit message to be included with the commit.
		*/
		public bool CommitFiles(CommitParams commitParams) // List<string> commitFiles, string commitMessage)
		{
			List<string> filesToCommit = new List<string>();
#if true
			// This does a check to make sure any file really have modified, added, or removed status.
			foreach (string filename in commitParams.fileList)
			{
				// If the file item is found, it is returned. Otherwise a FileItem with the 
				// default value is returned, in which case the fileItem.filename should be empty.
				FileItem fileItem = m_fileList.Find(fi => fi.filename == filename);
				
				{
					if((fileItem.filename != String.Empty) && 
						((fileItem.status == FileStatus.Modified) || 
						(fileItem.status == FileStatus.Added)  || 
						(fileItem.status == FileStatus.Removed)))
					{
						filesToCommit.Add(filename);
					}
				}
			}
			commitParams.fileList = filesToCommit;
#endif
			// Note that there need not be any files in order to do a commit.
			// This purpose of the commit might be to merge or to close a branch.

			bool result = SCC.Instance.CommitFiles(commitParams);
			if (result)
			{
				RefreshSccFileList(commitParams.repoPath);
				Notify(statusChangeAspect);
			}
			return result;
		}

		public string GetDiff(DiffParams diffParams)
		{
			return SCC.Instance.GetDiff(diffParams);
		}

		public void CloneRepository(string sourcePath, string destinationPath, bool update, bool compressedTransfer)
		{
			SCC.Instance.CloneRepository(sourcePath, destinationPath, update, compressedTransfer);
		}

		public string GetDefaultPushRepository(string folder)
		{
			return SCC.Instance.GetDefaultPushRepository(folder);
		}
		public int GetNumberOfHeads(string folder)
		{
			return SCC.Instance.GetNumberOfHeads(folder);
		}
		public List<AnnotateItem> GetAnnotate(AnnotateParams annParams)
		{
			return SCC.Instance.Annotate(annParams);
		}
		public string GetCurrentBranch(string folder)
		{
			return SCC.Instance.GetCurrentBranch(folder);
		}
		public bool HasIncoming(string folderPath)
		{
			return SCC.Instance.HasIncoming(folderPath);
		}

		public List<FileLogItem> GetLog(LogParams logParams)
		{
			return SCC.Instance.GetLog(logParams);
		}

		public List<FileLogItem> GetOutgoing(string folder)
		{
			return SCC.Instance.GetOutgoing(folder);
		}
		public List<FileLogItem> GetParents(ParentsParams parentsParams)
		{
			return SCC.Instance.GetParents(parentsParams.repoPath, parentsParams);
		}
		public void Branch(BranchParams branchParams)
		{
			bool thereWereChanges = SCC.Instance.Branch(branchParams);
			if (thereWereChanges)
			{
				Notify(new FolderStatusChangeAspect(branchParams.repoPath));
			}
		}
	}

	public struct UpdateParams
	{
		public string repoPath;
		public bool byRevision;
		public bool byDateTime;
		public bool clean;

		public DateTime dateTime;
		public string revision;
	};

	public struct PullParams
	{
		public string repoPath;
		public bool fromDefaultRepository;
		public bool byRevision;
		public bool update;
		public bool force;

		public string otherRepository;
		public string revision;
	};

	public struct PushParams
	{
		public string repoPath;
		public bool toDefaultRepository;
		public bool upToRevision;
		public bool allowNewBranch;
		public bool force;

		public string otherRepository;
		public string revision;
	};

	public struct MergeParams
	{
		public string repoPath;
		public bool withRevision;
		public bool force;

		public string revision;
	}
	public struct BranchParams
	{
		public string repoPath;
		// Although set and clean are separate members here, only one should be set to true.
		// This is simpler than creating an enumeration.
		public bool set;
		public bool clean;
		public bool force;
		public string branchName;
	}

	/** @brief Parameters for the Log command

		How to specify the revision.\n
		You may specify any combination of startRevision and endRevision. For Mercurial,
		you can specify by revision number or by hash ID.\n
		\n
		Examples:\n
		\n
		To get all revisions, leave both startRevision and endRevision strings empty.\n
		To get a range of revisions such as 4 through 8, set startRevision to 4 and leave endRevision to 8.\n
		To get a single revision, set both startRevision and endRevision to the same value.\n
		To get all revisions 20 and later, set startRevision to 20 and leave endRevision empty.\n
		To get all revisions up to and including 20, set endRevision to 20 and leave startRevision empty.\n
		To reverse the order of revisions, swap the values of startRevision and endRevision.\n

	*/
	public struct LogParams
	{
		public string repoPath;
		public string filename;
		public bool logByRevision;
		public bool logByDateTime;
		public string startRevision;
		public string endRevision;
		public DateTime startDateTime;
		public DateTime endDateTime;
		public bool verbose;
		public bool getFiles;
		public bool follow;
	}

	//public struct LogRepoParams
	//{
	//    public bool logByRevision;
	//    public bool logByDateTime;
	//    public string startRevision;
	//    public string endRevision;
	//    public DateTime startDateTime;
	//    public DateTime endDateTime;
	//    public bool verbose;
	//}

	/** @brief Parameters to the Model::GetDiff() and SCC:GetDiff() functions.
	 * 
	 * If both revision1 and revision2 are specified, the difference between those two revisions 
	 * are shown.
	 * If only revision1 or revision2 is specified, then the difference between that revision and 
	 * the working directory is shown.
	 * If no revisions are specified, then the difference between the working directory file and its
	 * parent are shown.
	 * 
	 * Note that there doesn't seem to be any benefit to using Mercurial's --change option other than saving 
	 * you a few keystrokes on the command line. using --change <REV> has the same effect as specifying 
	 * a revision and the next earlier revision. This works for a file even if that file wasn't modified 
	 * in the next previous revision---it just compares to the state that file was in at that revision.
	 * Although, without using --change you do have to use revision numbers rather than changeset IDs 
	 * because you don't know what the previous changeset ID was.
	 **/
	public struct DiffParams
	{
		public string repoPath;
		public string filename;
		public string extCommand;	// external command
		public string revision1;
		public string revision2;
	}

	public struct RenameParams
	{
		public string repoPath;
		public string oldFilename;
		public string newFilename;
		public bool after;
	}

	public struct AddParams
	{
		public string repoPath;
		public List<string> files;
	}

	public struct AnnotateParams
	{
        public string repoPath;
        public string filename;
		public bool annotateRevision;
		public string revisionToAnnotate;
		public bool showRevision;
		public bool showChangeset;
		public bool showAuthor;
		public bool showDate;
		public bool showFilename;
		public bool showLineNumber;
		public bool dontFollow;
	}

	public struct RemoveParams
	{
        public string repoPath;
		public List<string> files;
		public bool after;
		public bool force;
	}

	public struct RevertParams
	{
		public string repoPath;
		public List<string> files;
	}

	public struct ParentsParams
	{
		public string repoPath;
		public string filename;
		public string revision;
	}

	public struct TagParams
	{
		public string repoPath;
		public string revision;
		public List<string> tagList;
		public string commitMessage;
		public bool replaceExisting;
		public bool makeLocal;
		public bool remove;
	}

	public struct CommitParams
	{
		public string repoPath;
		public List<string> fileList;
		public string commitMessage;
		public bool closeBranch;

		// That there are no filenames in the list doesn't tell us whether it's OK to commit
		// because this may be a merge commit or we may want to commit all modified files.
		// But it may be invalid if the user didn't check any files in the commit dialog.
		public bool isValidCommit;
	}

	public struct IgnoreParams
	{
		public string repoPath;
		public List<string> files;
	}
}
