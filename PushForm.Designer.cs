﻿namespace Zync
{
	partial class PushForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PushForm));
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.pushToOtherBrowseButton = new System.Windows.Forms.Button();
			this.pushToOtherTextBox = new System.Windows.Forms.TextBox();
			this.pushToOtherRadioButton = new System.Windows.Forms.RadioButton();
			this.pushToDefaultRadioButton = new System.Windows.Forms.RadioButton();
			this.pushForceCheckBox = new System.Windows.Forms.CheckBox();
			this.pushCancelButton = new System.Windows.Forms.Button();
			this.pushOkButton = new System.Windows.Forms.Button();
			this.pushUpToRevCheckBox = new System.Windows.Forms.CheckBox();
			this.pushToOtherTextBoxDummy = new System.Windows.Forms.TextBox();
			this.pushToGroupBox = new System.Windows.Forms.GroupBox();
			this.pushToDefaultLabel = new System.Windows.Forms.Label();
			this.pushUpToRevTextBox = new System.Windows.Forms.TextBox();
			this.pushAllowNewBranchCheckBox = new System.Windows.Forms.CheckBox();
			this.pushToGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// pushToOtherBrowseButton
			// 
			this.pushToOtherBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pushToOtherBrowseButton.Location = new System.Drawing.Point(340, 49);
			this.pushToOtherBrowseButton.Name = "pushToOtherBrowseButton";
			this.pushToOtherBrowseButton.Size = new System.Drawing.Size(32, 23);
			this.pushToOtherBrowseButton.TabIndex = 11;
			this.pushToOtherBrowseButton.Text = "...";
			this.toolTip1.SetToolTip(this.pushToOtherBrowseButton, "Browse for another repository.");
			this.pushToOtherBrowseButton.UseVisualStyleBackColor = true;
			this.pushToOtherBrowseButton.Click += new System.EventHandler(this.OnBrowseOtherRepositoryButtonClicked);
			// 
			// pushToOtherTextBox
			// 
			this.pushToOtherTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pushToOtherTextBox.Location = new System.Drawing.Point(104, 50);
			this.pushToOtherTextBox.Name = "pushToOtherTextBox";
			this.pushToOtherTextBox.Size = new System.Drawing.Size(229, 20);
			this.pushToOtherTextBox.TabIndex = 10;
			this.toolTip1.SetToolTip(this.pushToOtherTextBox, "The other repository to choose from. If the Other radio button is selected, then " +
					"you must enter text here to enable the Push button.");
			this.pushToOtherTextBox.Visible = false;
			this.pushToOtherTextBox.TextChanged += new System.EventHandler(this.OnPushToOtherTextBoxTextChanged);
			// 
			// pushToOtherRadioButton
			// 
			this.pushToOtherRadioButton.AutoSize = true;
			this.pushToOtherRadioButton.Location = new System.Drawing.Point(19, 50);
			this.pushToOtherRadioButton.Name = "pushToOtherRadioButton";
			this.pushToOtherRadioButton.Size = new System.Drawing.Size(54, 17);
			this.pushToOtherRadioButton.TabIndex = 8;
			this.pushToOtherRadioButton.TabStop = true;
			this.pushToOtherRadioButton.Text = "Other:";
			this.toolTip1.SetToolTip(this.pushToOtherRadioButton, "Push to a repository other than the default. If there is no default, then this wi" +
					"ll be your only choice");
			this.pushToOtherRadioButton.UseVisualStyleBackColor = true;
			this.pushToOtherRadioButton.CheckedChanged += new System.EventHandler(this.OnPushToOtherRadioButtonChanged);
			// 
			// pushToDefaultRadioButton
			// 
			this.pushToDefaultRadioButton.AutoSize = true;
			this.pushToDefaultRadioButton.Location = new System.Drawing.Point(19, 19);
			this.pushToDefaultRadioButton.Name = "pushToDefaultRadioButton";
			this.pushToDefaultRadioButton.Size = new System.Drawing.Size(62, 17);
			this.pushToDefaultRadioButton.TabIndex = 7;
			this.pushToDefaultRadioButton.TabStop = true;
			this.pushToDefaultRadioButton.Text = "Default:";
			this.toolTip1.SetToolTip(this.pushToDefaultRadioButton, "Push to the default repository");
			this.pushToDefaultRadioButton.UseVisualStyleBackColor = true;
			// 
			// pushForceCheckBox
			// 
			this.pushForceCheckBox.AutoSize = true;
			this.pushForceCheckBox.Location = new System.Drawing.Point(12, 170);
			this.pushForceCheckBox.Name = "pushForceCheckBox";
			this.pushForceCheckBox.Size = new System.Drawing.Size(135, 17);
			this.pushForceCheckBox.TabIndex = 15;
			this.pushForceCheckBox.Text = "Force even if unrelated";
			this.toolTip1.SetToolTip(this.pushForceCheckBox, "Push even if the remote repository is unrelated to the local one");
			this.pushForceCheckBox.UseVisualStyleBackColor = true;
			// 
			// pushCancelButton
			// 
			this.pushCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.pushCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.pushCancelButton.Location = new System.Drawing.Point(316, 203);
			this.pushCancelButton.Name = "pushCancelButton";
			this.pushCancelButton.Size = new System.Drawing.Size(75, 23);
			this.pushCancelButton.TabIndex = 13;
			this.pushCancelButton.Text = "Cancel";
			this.toolTip1.SetToolTip(this.pushCancelButton, "Cancel the push");
			this.pushCancelButton.UseVisualStyleBackColor = true;
			// 
			// pushOkButton
			// 
			this.pushOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.pushOkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.pushOkButton.Location = new System.Drawing.Point(227, 203);
			this.pushOkButton.Name = "pushOkButton";
			this.pushOkButton.Size = new System.Drawing.Size(75, 23);
			this.pushOkButton.TabIndex = 12;
			this.pushOkButton.Text = "Push";
			this.toolTip1.SetToolTip(this.pushOkButton, "Push to the repository");
			this.pushOkButton.UseVisualStyleBackColor = true;
			this.pushOkButton.Click += new System.EventHandler(this.OnClickedOkButton);
			// 
			// pushUpToRevCheckBox
			// 
			this.pushUpToRevCheckBox.AutoSize = true;
			this.pushUpToRevCheckBox.Location = new System.Drawing.Point(12, 118);
			this.pushUpToRevCheckBox.Name = "pushUpToRevCheckBox";
			this.pushUpToRevCheckBox.Size = new System.Drawing.Size(94, 17);
			this.pushUpToRevCheckBox.TabIndex = 9;
			this.pushUpToRevCheckBox.Text = "Up to revision:";
			this.toolTip1.SetToolTip(this.pushUpToRevCheckBox, "Push changes only up to this revision");
			this.pushUpToRevCheckBox.UseVisualStyleBackColor = true;
			this.pushUpToRevCheckBox.CheckedChanged += new System.EventHandler(this.OnUpToRevisionCheckBoxChanged);
			// 
			// pushToOtherTextBoxDummy
			// 
			this.pushToOtherTextBoxDummy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pushToOtherTextBoxDummy.Location = new System.Drawing.Point(90, 58);
			this.pushToOtherTextBoxDummy.Name = "pushToOtherTextBoxDummy";
			this.pushToOtherTextBoxDummy.Size = new System.Drawing.Size(229, 20);
			this.pushToOtherTextBoxDummy.TabIndex = 12;
			this.pushToOtherTextBoxDummy.TabStop = false;
			this.toolTip1.SetToolTip(this.pushToOtherTextBoxDummy, "The other repository to choose from. If the Other radio button is selected, then " +
					"you must enter text here to enable the Push button.");
			this.pushToOtherTextBoxDummy.Visible = false;
			// 
			// pushToGroupBox
			// 
			this.pushToGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pushToGroupBox.Controls.Add(this.pushToOtherTextBoxDummy);
			this.pushToGroupBox.Controls.Add(this.pushToOtherBrowseButton);
			this.pushToGroupBox.Controls.Add(this.pushToOtherTextBox);
			this.pushToGroupBox.Controls.Add(this.pushToDefaultLabel);
			this.pushToGroupBox.Controls.Add(this.pushToOtherRadioButton);
			this.pushToGroupBox.Controls.Add(this.pushToDefaultRadioButton);
			this.pushToGroupBox.Location = new System.Drawing.Point(12, 12);
			this.pushToGroupBox.Name = "pushToGroupBox";
			this.pushToGroupBox.Size = new System.Drawing.Size(379, 84);
			this.pushToGroupBox.TabIndex = 16;
			this.pushToGroupBox.TabStop = false;
			this.pushToGroupBox.Text = "To";
			// 
			// pushToDefaultLabel
			// 
			this.pushToDefaultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pushToDefaultLabel.Location = new System.Drawing.Point(101, 21);
			this.pushToDefaultLabel.Name = "pushToDefaultLabel";
			this.pushToDefaultLabel.Size = new System.Drawing.Size(272, 25);
			this.pushToDefaultLabel.TabIndex = 9;
			this.pushToDefaultLabel.Text = "display default repository here";
			// 
			// pushUpToRevTextBox
			// 
			this.pushUpToRevTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pushUpToRevTextBox.Location = new System.Drawing.Point(116, 116);
			this.pushUpToRevTextBox.Name = "pushUpToRevTextBox";
			this.pushUpToRevTextBox.Size = new System.Drawing.Size(229, 20);
			this.pushUpToRevTextBox.TabIndex = 10;
			// 
			// pushAllowNewBranchCheckBox
			// 
			this.pushAllowNewBranchCheckBox.AutoSize = true;
			this.pushAllowNewBranchCheckBox.Location = new System.Drawing.Point(12, 144);
			this.pushAllowNewBranchCheckBox.Name = "allowNewBranchCheckBox";
			this.pushAllowNewBranchCheckBox.Size = new System.Drawing.Size(159, 17);
			this.pushAllowNewBranchCheckBox.TabIndex = 15;
			this.pushAllowNewBranchCheckBox.Text = "Allow pushing a new branch";
			this.pushAllowNewBranchCheckBox.UseVisualStyleBackColor = true;
			// 
			// PushForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(403, 241);
			this.Controls.Add(this.pushToGroupBox);
			this.Controls.Add(this.pushAllowNewBranchCheckBox);
			this.Controls.Add(this.pushForceCheckBox);
			this.Controls.Add(this.pushCancelButton);
			this.Controls.Add(this.pushOkButton);
			this.Controls.Add(this.pushUpToRevTextBox);
			this.Controls.Add(this.pushUpToRevCheckBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(411, 257);
			this.Name = "PushForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Push";
			this.pushToGroupBox.ResumeLayout(false);
			this.pushToGroupBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.GroupBox pushToGroupBox;
		private System.Windows.Forms.Button pushToOtherBrowseButton;
		private System.Windows.Forms.TextBox pushToOtherTextBox;
		private System.Windows.Forms.Label pushToDefaultLabel;
		private System.Windows.Forms.RadioButton pushToOtherRadioButton;
		private System.Windows.Forms.RadioButton pushToDefaultRadioButton;
		private System.Windows.Forms.CheckBox pushForceCheckBox;
		private System.Windows.Forms.Button pushCancelButton;
		private System.Windows.Forms.Button pushOkButton;
		private System.Windows.Forms.TextBox pushUpToRevTextBox;
		private System.Windows.Forms.CheckBox pushUpToRevCheckBox;
		private System.Windows.Forms.TextBox pushToOtherTextBoxDummy;
		private System.Windows.Forms.CheckBox pushAllowNewBranchCheckBox;
	}
}