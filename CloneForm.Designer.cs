﻿namespace Zync
{
	partial class CloneForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.Label cloneSourceLabel;
			System.Windows.Forms.Label cloneDestinationLabel;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CloneForm));
			this.cloneButton = new System.Windows.Forms.Button();
			this.cloneCancelButton = new System.Windows.Forms.Button();
			this.cloneSourceBrowseButton = new System.Windows.Forms.Button();
			this.cloneDestinationBrowseButton = new System.Windows.Forms.Button();
			this.cloneDestinationTextBox = new System.Windows.Forms.TextBox();
			this.cloneNoupdateCheckBox = new System.Windows.Forms.CheckBox();
			this.cloneCompressedCheckBox = new System.Windows.Forms.CheckBox();
			this.cloneSourceFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.cloneDestinationFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.cloneSourceComboBox = new System.Windows.Forms.ComboBox();
			cloneSourceLabel = new System.Windows.Forms.Label();
			cloneDestinationLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cloneSourceLabel
			// 
			cloneSourceLabel.AutoSize = true;
			cloneSourceLabel.Location = new System.Drawing.Point(13, 32);
			cloneSourceLabel.Name = "cloneSourceLabel";
			cloneSourceLabel.Size = new System.Drawing.Size(44, 13);
			cloneSourceLabel.TabIndex = 3;
			cloneSourceLabel.Text = "Source:";
			// 
			// cloneDestinationLabel
			// 
			cloneDestinationLabel.AutoSize = true;
			cloneDestinationLabel.Location = new System.Drawing.Point(12, 61);
			cloneDestinationLabel.Name = "cloneDestinationLabel";
			cloneDestinationLabel.Size = new System.Drawing.Size(63, 13);
			cloneDestinationLabel.TabIndex = 6;
			cloneDestinationLabel.Text = "Destination:";
			// 
			// cloneButton
			// 
			this.cloneButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cloneButton.Location = new System.Drawing.Point(236, 169);
			this.cloneButton.Name = "cloneButton";
			this.cloneButton.Size = new System.Drawing.Size(75, 23);
			this.cloneButton.TabIndex = 0;
			this.cloneButton.Text = "Clone";
			this.toolTip1.SetToolTip(this.cloneButton, "Clone the repository specified in the Source textbox.");
			this.cloneButton.UseVisualStyleBackColor = true;
			this.cloneButton.Click += new System.EventHandler(this.OnClickedCloneButton);
			// 
			// cloneCancelButton
			// 
			this.cloneCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cloneCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cloneCancelButton.Location = new System.Drawing.Point(326, 169);
			this.cloneCancelButton.Name = "cloneCancelButton";
			this.cloneCancelButton.Size = new System.Drawing.Size(75, 23);
			this.cloneCancelButton.TabIndex = 1;
			this.cloneCancelButton.Text = "Cancel";
			this.toolTip1.SetToolTip(this.cloneCancelButton, "Cancel the clone dialog.");
			this.cloneCancelButton.UseVisualStyleBackColor = true;
			// 
			// cloneSourceBrowseButton
			// 
			this.cloneSourceBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cloneSourceBrowseButton.Location = new System.Drawing.Point(361, 32);
			this.cloneSourceBrowseButton.Name = "cloneSourceBrowseButton";
			this.cloneSourceBrowseButton.Size = new System.Drawing.Size(40, 23);
			this.cloneSourceBrowseButton.TabIndex = 4;
			this.cloneSourceBrowseButton.Text = "...";
			this.toolTip1.SetToolTip(this.cloneSourceBrowseButton, "Browse for the source repository directory.");
			this.cloneSourceBrowseButton.UseVisualStyleBackColor = true;
			this.cloneSourceBrowseButton.Click += new System.EventHandler(this.OnClickedSourceBrowseButton);
			// 
			// cloneDestinationBrowseButton
			// 
			this.cloneDestinationBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cloneDestinationBrowseButton.Location = new System.Drawing.Point(361, 58);
			this.cloneDestinationBrowseButton.Name = "cloneDestinationBrowseButton";
			this.cloneDestinationBrowseButton.Size = new System.Drawing.Size(40, 23);
			this.cloneDestinationBrowseButton.TabIndex = 7;
			this.cloneDestinationBrowseButton.Text = "...";
			this.toolTip1.SetToolTip(this.cloneDestinationBrowseButton, "Browse for the destination directory.");
			this.cloneDestinationBrowseButton.UseVisualStyleBackColor = true;
			this.cloneDestinationBrowseButton.Click += new System.EventHandler(this.OnClickedDestinationBrowseButton);
			// 
			// cloneDestinationTextBox
			// 
			this.cloneDestinationTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cloneDestinationTextBox.Location = new System.Drawing.Point(82, 58);
			this.cloneDestinationTextBox.Name = "cloneDestinationTextBox";
			this.cloneDestinationTextBox.Size = new System.Drawing.Size(273, 20);
			this.cloneDestinationTextBox.TabIndex = 5;
			this.toolTip1.SetToolTip(this.cloneDestinationTextBox, "Full path of the working folder of the new clone.\r\nThis folder must not already e" +
					"xist---it will be created. \r\nBut its parent folder must already exist.\r\nIt must " +
					"not be the same as the Source folder.");
			this.cloneDestinationTextBox.TextChanged += new System.EventHandler(this.OnDestinationTextChanged);
			// 
			// cloneNoupdateCheckBox
			// 
			this.cloneNoupdateCheckBox.AutoSize = true;
			this.cloneNoupdateCheckBox.Location = new System.Drawing.Point(16, 105);
			this.cloneNoupdateCheckBox.Name = "cloneNoupdateCheckBox";
			this.cloneNoupdateCheckBox.Size = new System.Drawing.Size(186, 17);
			this.cloneNoupdateCheckBox.TabIndex = 8;
			this.cloneNoupdateCheckBox.Text = "Do not update new working folder";
			this.toolTip1.SetToolTip(this.cloneNoupdateCheckBox, "When checked, the repository is cloned but the working files are not updated.");
			this.cloneNoupdateCheckBox.UseVisualStyleBackColor = true;
			// 
			// cloneCompressedCheckBox
			// 
			this.cloneCompressedCheckBox.AutoSize = true;
			this.cloneCompressedCheckBox.Location = new System.Drawing.Point(16, 129);
			this.cloneCompressedCheckBox.Name = "cloneCompressedCheckBox";
			this.cloneCompressedCheckBox.Size = new System.Drawing.Size(122, 17);
			this.cloneCompressedCheckBox.TabIndex = 9;
			this.cloneCompressedCheckBox.Text = "Compressed transfer";
			this.toolTip1.SetToolTip(this.cloneCompressedCheckBox, "When checked, the data is transferred in compressed form, which may be faster.");
			this.cloneCompressedCheckBox.UseVisualStyleBackColor = true;
			// 
			// cloneSourceComboBox
			// 
			this.cloneSourceComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cloneSourceComboBox.FormattingEnabled = true;
			this.cloneSourceComboBox.Location = new System.Drawing.Point(82, 32);
			this.cloneSourceComboBox.Name = "cloneSourceComboBox";
			this.cloneSourceComboBox.Size = new System.Drawing.Size(273, 21);
			this.cloneSourceComboBox.TabIndex = 10;
			this.toolTip1.SetToolTip(this.cloneSourceComboBox, "Full path to the repository to clone.");
			this.cloneSourceComboBox.SelectedIndexChanged += new System.EventHandler(this.OnSourceSelectedIndexChanged);
			this.cloneSourceComboBox.TextChanged += new System.EventHandler(this.OnSourceTextChanged);
			// 
			// CloneForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(413, 204);
			this.Controls.Add(this.cloneSourceComboBox);
			this.Controls.Add(this.cloneCompressedCheckBox);
			this.Controls.Add(this.cloneNoupdateCheckBox);
			this.Controls.Add(this.cloneDestinationBrowseButton);
			this.Controls.Add(cloneDestinationLabel);
			this.Controls.Add(this.cloneDestinationTextBox);
			this.Controls.Add(this.cloneSourceBrowseButton);
			this.Controls.Add(cloneSourceLabel);
			this.Controls.Add(this.cloneCancelButton);
			this.Controls.Add(this.cloneButton);
			this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Zync.Properties.Settings.Default, "CloneFormLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = global::Zync.Properties.Settings.Default.CloneFormLocation;
			this.MinimumSize = new System.Drawing.Size(290, 237);
			this.Name = "CloneForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Clone a Repository";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.Load += new System.EventHandler(this.OnFormLoad);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button cloneButton;
		private System.Windows.Forms.Button cloneCancelButton;
		private System.Windows.Forms.Button cloneSourceBrowseButton;
		private System.Windows.Forms.Button cloneDestinationBrowseButton;
		private System.Windows.Forms.TextBox cloneDestinationTextBox;
		private System.Windows.Forms.CheckBox cloneNoupdateCheckBox;
		private System.Windows.Forms.CheckBox cloneCompressedCheckBox;
		private System.Windows.Forms.FolderBrowserDialog cloneSourceFolderBrowserDialog;
		private System.Windows.Forms.FolderBrowserDialog cloneDestinationFolderBrowserDialog;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ComboBox cloneSourceComboBox;
	}
}